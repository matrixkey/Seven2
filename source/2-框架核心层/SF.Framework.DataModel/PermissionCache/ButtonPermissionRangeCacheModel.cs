﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataModel.PermissionCache
{
    /// <summary>
    /// 按钮权限范围缓存模型
    /// </summary>
    public class ButtonPermissionRangeCacheModel
    {
        /// <summary>
        /// 所属菜单ID
        /// </summary>
        public string MenuID { get; set; }

        /// <summary>
        /// 按钮名称
        /// </summary>
        public string ButtonName { get; set; }

        /// <summary>
        /// 按钮标记码，前台控制按钮代码中可能会需要用到，因此同菜单下请不要重复。值可以诸如：Add、Edit、EanbleOrDisable、Remove等。
        /// </summary>
        public string ButtonSymbol { get; set; }

        /// <summary>
        /// 图标样式
        /// </summary>
        public string ButtonIconClass { get; set; }

        /// <summary>
        /// 按钮点击事件
        /// </summary>
        public string ButtonHandler { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int SortNumber { get; set; }

        /// <summary>
        /// 分组标记
        /// </summary>
        public string GroupMark { get; set; }

        /// <summary>
        /// 是否禁用
        /// </summary>
        public bool IsDisabled { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;

namespace SF.Framework.DataModel.PermissionCache
{
    /// <summary>
    /// 用户账号模型，登录校验时专用
    /// </summary>
    public class UserAccountModel
    {
        /// <summary>
        /// 用户实体
        /// </summary>
        public SysUser UserEntity { get; set; }
    }
}

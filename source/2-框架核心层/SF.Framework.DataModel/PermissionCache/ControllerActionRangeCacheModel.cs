﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataModel.PermissionCache
{
    /// <summary>
    /// 控制器行为范围缓存模型
    /// </summary>
    public class ControllerActionRangeCacheModel
    {
        /// <summary>
        /// 菜单ID
        /// </summary>
        public string MenuID { get; set; }

        /// <summary>
        /// 控制器区域名称
        /// </summary>
        public string ControllerAreaName { get; set; }

        /// <summary>
        /// 控制器名称
        /// </summary>
        public string ControllerName { get; set; }

        /// <summary>
        /// 行为ID
        /// </summary>
        public string ActionID { get; set; }

        /// <summary>
        /// 行为名称
        /// </summary>
        public string ActionName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataModel.Import
{
    /// <summary>
    /// 省市区数据模型
    /// </summary>
    public class ProvinceCityAreaModel
    {
        public string Code { get; set; }

        public string Name { get; set; }

        public string ParentCode { get; set; }
    }
}

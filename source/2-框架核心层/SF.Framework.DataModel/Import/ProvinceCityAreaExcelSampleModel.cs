﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace SF.Framework.DataModel.Import
{
    /// <summary>
    /// 省市区数据从excel文件中转型用的简易模型
    /// </summary>
    public class ProvinceCityAreaExcelSampleModel
    {
        [Description("编号和名称")]
        public string CodeAndName { get; set; }
    }
}

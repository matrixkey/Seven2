﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataModel.Base
{
    /// <summary>
    /// 表示所有视图模型的基类
    /// </summary>
    public abstract class BaseViewModel : BaseModel
    {
    }
}

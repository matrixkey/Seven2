﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataModel.Repositories
{
    public class PositionSelectorListModel
    {
        /// <summary>
        /// 岗位主键
        /// </summary>
        public string PositionID { get; set; }

        /// <summary>
        /// 岗位名称
        /// </summary>
        public string PositionName { get; set; }

        /// <summary>
        /// 所属机构ID
        /// </summary>
        public string FK_OrganID { get; set; }

        /// <summary>
        /// 所属机构名称
        /// </summary>
        public string OrganName { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int SortNumber { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataModel.Repositories
{
    public class DataDictionaryListModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 业务编号
        /// </summary>
        public string BusinessCode { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int SortNumber { get; set; }

        /// <summary>
        /// 类别标志，同一类的数据字典，该属性值应相同，以方便按标志获取数据，并且“不同类的数据字典”的类别标志不可重复。
        /// </summary>
        public string TypeSymbol { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        public bool IsShow { get; set; }

        /// <summary>
        /// 是否系统数据
        /// </summary>
        public bool IsSystem { get; set; }
    }
}

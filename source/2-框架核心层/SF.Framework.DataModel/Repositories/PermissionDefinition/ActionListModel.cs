﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataModel.Repositories
{
    public class ActionListModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 所属控制器ID
        /// </summary>
        public string FK_ControllerID { get; set; }

        /// <summary>
        /// 所属控制器名称
        /// </summary>
        public string ControllerName { get; set; }

        /// <summary>
        /// 所属控制器区域名称
        /// </summary>
        public string ControllerAreaName { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public ActionType Type { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int SortNumber { get; set; }

        /// <summary>
        /// 描述信息
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 限制标记
        /// </summary>
        public ActionLimitedAttribute LimitedAttribute { get; set; }
    }
}

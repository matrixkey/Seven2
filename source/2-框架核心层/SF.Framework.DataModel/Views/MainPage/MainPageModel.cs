﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataModel.Views
{
    public class MainPageModel : SF.Framework.DataModel.Base.BaseViewModel
    {
        /// <summary>
        /// 系统名称
        /// </summary>
        public string SystemName { get; set; }

        /// <summary>
        /// 登录使用的帐号
        /// </summary>
        public string Account { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 上次使用的主菜单
        /// </summary>
        public string DefaultRootMenuID { get; set; }

        /// <summary>
        /// 上次使用的主题
        /// </summary>
        public string DefaultThemeName { get; set; }

        /// <summary>
        /// 首页北部panel是否折叠
        /// </summary>
        public bool North { get; set; }

        /// <summary>
        /// 首页西部panel是否折叠
        /// </summary>
        public bool West { get; set; }

        /// <summary>
        /// 首页南部panel是否折叠
        /// </summary>
        public bool South { get; set; }

        /// <summary>
        /// 版权信息
        /// </summary>
        public string Copyright { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataModel.Views
{
    public class RolePermissionFormModel
    {
        /// <summary>
        /// 菜单ID
        /// </summary>
        public string MenuID { get; set; }

        /// <summary>
        /// 菜单权限ID
        /// </summary>
        public string PermissionID { get; set; }

        /// <summary>
        /// 打勾状态
        /// </summary>
        public bool Check { get; set; }
    }
}

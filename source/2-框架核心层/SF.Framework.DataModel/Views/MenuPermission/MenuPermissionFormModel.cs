﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataModel.Views
{
    public class MenuPermissionFormModel : SF.Framework.DataModel.Base.BaseViewModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 所属菜单ID
        /// </summary>
        public string FK_MenuID { get; set; }

        /// <summary>
        /// 所属菜单名称
        /// </summary>
        public string MenuName { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public MenuPermissionType Type { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int SortNumber { get; set; }

        /// <summary>
        /// 关联的行为ID串
        /// </summary>
        public string ActionIDs { get; set; }

        /// <summary>
        /// 关联的行为信息
        /// </summary>
        public IEnumerable<string> ActionInfos { get; set; }

        /// <summary>
        /// 分组标记
        /// </summary>
        public string GroupMark { get; set; }

        /// <summary>
        /// 是否总是可用的。true 则表示该权限始终开放，并且不显示在“可分配权限”范围之内。
        /// </summary>
        public bool AlwaysUseable { get; set; }

        #region 按钮字段

        /// <summary>
        /// 按钮名称
        /// </summary>
        public string ButtonName { get; set; }

        /// <summary>
        /// 按钮标记码，前台控制按钮代码中可能会需要用到，因此同菜单下请不要重复。值可以诸如：Add、Edit、EanbleOrDisable、Remove等。
        /// </summary>
        public string ButtonSymbol { get; set; }

        /// <summary>
        /// 图标样式
        /// </summary>
        public string ButtonIconClass { get; set; }

        /// <summary>
        /// 按钮点击事件
        /// </summary>
        public string ButtonHandler { get; set; }

        #endregion
    }
}

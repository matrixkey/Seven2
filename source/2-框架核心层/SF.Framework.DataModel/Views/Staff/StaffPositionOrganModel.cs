﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataModel.Views
{
    public class StaffPositionOrganModel
    {
        /// <summary>
        /// 关系主键
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 机构ID，该字段属于冗余字段，从 岗位ID 计算可得。
        /// </summary>
        public string FK_OrganID { get; set; }

        /// <summary>
        /// 机构名称
        /// </summary>
        public string OrganName { get; set; }

        /// <summary>
        /// 岗位ID
        /// </summary>
        public string FK_PositionID { get; set; }

        /// <summary>
        /// 岗位名称
        /// </summary>
        public string PositionName { get; set; }

        /// <summary>
        /// 是否为主要关系
        /// </summary>
        public bool IsMaster { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataModel.Views
{
    public class StaffFormModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 岗位关系集合
        /// </summary>
        public IEnumerable<StaffPositionOrganModel> PositionRelations { get; set; }

        /// <summary>
        /// 岗位关系集合的字符串形式，格式为 岗位ID,是否主从;岗位ID,是否主从;岗位ID,是否主从。用于保存时候接收表单数据
        /// </summary>
        public string StringPositionRelations { get; set; }
    }
}

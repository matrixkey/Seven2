﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataModel.Views
{
    public class MenuFormModel : SF.Framework.DataModel.Base.BaseViewModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 编号
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 排序号
        /// </summary>
        public int SortNumber { get; set; }

        /// <summary>
        /// 图标样式
        /// </summary>
        public string IconClass { get; set; }

        /// <summary>
        /// 父级菜单ID
        /// </summary>
        public string ParentID { get; set; }

        /// <summary>
        /// 父级菜单名称
        /// </summary>
        public string ParentName { get; set; }

        /// <summary>
        /// 行为ID
        /// </summary>
        public string FK_ActionID { get; set; }

        /// <summary>
        /// 控制器名称
        /// </summary>
        public string ControllerName { get; set; }

        /// <summary>
        /// 行为名称
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// 是否显示
        /// </summary>
        public bool IsShow { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.ComponentModel.DataAnnotations.Schema;

namespace SF.Framework.DataCore.EntityFramework
{
    /// <summary>
    /// 实体配置基类
    /// </summary>
    /// <typeparam name="TEntity">实体类型</typeparam>
    public abstract class EntityConfiguration<TEntity> : EntityTypeConfiguration<TEntity>, IEntityConfiguration where TEntity : EntityModelBaseForReflection
    {
        public EntityConfiguration()
        {
            Type entityType = typeof(TEntity);
            TableAttribute tableAttr = entityType.GetCustomAttributes<TableAttribute>().FirstOrDefault();
            if (tableAttr != null && !string.IsNullOrWhiteSpace(tableAttr.Name))
            { ToTable(tableAttr.Name); }
        }

        public void RegistTo(ConfigurationRegistrar configurations)
        {
            configurations.Add(this);
        }
    }
}

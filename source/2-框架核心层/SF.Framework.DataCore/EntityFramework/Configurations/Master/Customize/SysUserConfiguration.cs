﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataCore.EntityFramework.Configurations.Master
{
    partial class SysUserConfiguration
    {
        partial void SysUserConfigurationAppend()
        {
            this.Property(p => p.ID).IsRequired().HasMaxLength(32);

            this.Property(p => p.UserName).IsRequired().HasMaxLength(100);

            this.Property(p => p.Password).IsRequired().HasMaxLength(100);
        }
    }
}

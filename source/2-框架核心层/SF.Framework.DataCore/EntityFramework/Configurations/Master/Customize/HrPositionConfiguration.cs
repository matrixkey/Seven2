﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataCore.EntityFramework.Configurations.Master
{
    partial class HrPositionConfiguration
    {
        partial void HrPositionConfigurationAppend()
        {
            this.Property(p => p.ID).IsRequired().HasMaxLength(32);

            this.Property(p => p.Name).IsRequired().HasMaxLength(200);

            this.Property(p => p.FK_OrganID).IsRequired().HasMaxLength(32);
        }
    }
}

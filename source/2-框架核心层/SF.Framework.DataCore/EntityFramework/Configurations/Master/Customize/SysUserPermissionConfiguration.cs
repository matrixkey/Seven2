﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataCore.EntityFramework.Configurations.Master
{
    partial class SysUserPermissionConfiguration
    {
        partial void SysUserPermissionConfigurationAppend()
        {
            this.Property(p => p.ID).IsRequired().HasMaxLength(32);

            this.Property(p => p.FK_UserID).IsRequired().HasMaxLength(32);

            this.Property(p => p.FK_PermissionID).IsRequired().HasMaxLength(32);
        }
    }
}

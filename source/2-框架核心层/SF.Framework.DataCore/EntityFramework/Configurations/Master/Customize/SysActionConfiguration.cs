﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataCore.EntityFramework.Configurations.Master
{
    partial class SysActionConfiguration
    {
        partial void SysActionConfigurationAppend()
        {
            this.Property(p => p.ID).IsRequired().HasMaxLength(32);

            this.Property(p => p.Name).IsRequired().HasMaxLength(100);

            this.Property(p => p.FK_ControllerID).IsRequired().HasMaxLength(32);
        }
    }
}

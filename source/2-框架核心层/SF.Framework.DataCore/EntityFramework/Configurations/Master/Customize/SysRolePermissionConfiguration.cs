﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataCore.EntityFramework.Configurations.Master
{
    partial class SysRolePermissionConfiguration
    {
        partial void SysRolePermissionConfigurationAppend()
        {
            this.Property(p => p.ID).IsRequired().HasMaxLength(32);

            this.Property(p => p.FK_RoleID).IsRequired().HasMaxLength(32);

            this.Property(p => p.FK_PermissionID).IsRequired().HasMaxLength(32);
        }
    }
}

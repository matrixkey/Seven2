﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataCore.EntityFramework.Configurations.Master
{
    partial class BasicDataDictionaryConfiguration
    {
        partial void BasicDataDictionaryConfigurationAppend()
        {
            this.Property(p => p.ID).IsRequired().HasMaxLength(32);

            this.Property(p => p.Name).IsRequired().HasMaxLength(100);

            this.Property(p => p.BusinessCode).HasMaxLength(50);

            this.Property(p => p.ParentID).IsRequired().HasMaxLength(32);

            this.Property(p => p.TypeSymbol).IsRequired().HasMaxLength(100);
        }
    }
}

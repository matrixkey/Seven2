﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataCore.EntityFramework.Configurations.Master
{
    partial class SysMenuPermissionConfiguration
    {
        partial void SysMenuPermissionConfigurationAppend()
        {
            this.Property(p => p.ID).IsRequired().HasMaxLength(32);

            this.Property(p => p.Name).IsRequired().HasMaxLength(100);

            this.Property(p => p.FK_MenuID).IsRequired().HasMaxLength(32);

            this.Property(p => p.ActionIDs).IsRequired();

            this.Property(p => p.GroupMark).HasMaxLength(50);

            this.Property(p => p.ButtonName).HasMaxLength(50);

            this.Property(p => p.ButtonSymbol).HasMaxLength(50);

            this.Property(p => p.ButtonIconClass).HasMaxLength(50);
        }
    }
}

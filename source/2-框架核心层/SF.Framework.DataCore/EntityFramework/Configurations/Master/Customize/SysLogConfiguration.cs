﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataCore.EntityFramework.Configurations.Master
{
    partial class SysLogConfiguration
    {
        partial void SysLogConfigurationAppend()
        {
            this.Property(p => p.ID).IsRequired().HasMaxLength(32);

            this.Property(p => p.Content).IsRequired();

            this.Property(p => p.OperationUserName).IsRequired();

            this.Property(p => p.OperationUserName).IsRequired();

            this.Property(p => p.OperationIpAddress).IsRequired();
        }
    }
}

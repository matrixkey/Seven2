﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataCore.EntityFramework.Configurations.Master
{
    partial class SysMenuConfiguration
    {
        partial void SysMenuConfigurationAppend()
        {
            this.Property(p => p.ID).IsRequired().HasMaxLength(32);

            this.Property(p => p.Name).IsRequired().HasMaxLength(100);

            this.Property(p => p.Code).IsRequired().HasMaxLength(100);

            this.Property(p => p.IconClass).IsRequired().HasMaxLength(50);

            this.Property(p => p.ParentID).IsRequired().HasMaxLength(32);

            this.Property(p => p.FK_ActionID).HasMaxLength(32);
        }
    }
}

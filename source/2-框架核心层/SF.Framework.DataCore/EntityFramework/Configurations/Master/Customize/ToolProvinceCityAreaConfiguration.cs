﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataCore.EntityFramework.Configurations.Master
{
    partial class ToolProvinceCityAreaConfiguration
    {
        partial void ToolProvinceCityAreaConfigurationAppend()
        {
            this.Property(p => p.ID).IsRequired().HasMaxLength(32);

            this.Property(p => p.Code).IsRequired().HasMaxLength(6);

            this.Property(p => p.Name).IsRequired().HasMaxLength(50);

            this.Property(p => p.ParentCode).HasMaxLength(6);
        }
    }
}

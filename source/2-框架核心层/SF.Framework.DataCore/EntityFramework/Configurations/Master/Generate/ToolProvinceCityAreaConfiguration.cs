﻿// <autogenerated>
//   This file was generated by T4 code generator MultiTemplate.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>


using System;

using SF.Framework.Entity.Master;

namespace SF.Framework.DataCore.EntityFramework.Configurations.Master
{
	/// <summary>
    /// 数据表映射
    /// </summary>
    partial class ToolProvinceCityAreaConfiguration : EntityConfiguration<ToolProvinceCityArea>
    {
        /// <summary>
        /// 数据表映射构造函数
        /// </summary>
        public ToolProvinceCityAreaConfiguration()
        {
            ToolProvinceCityAreaConfigurationNormal();
            ToolProvinceCityAreaConfigurationAppend();
        }

        /// <summary>
        /// 默认的数据映射
        /// </summary>
        public void ToolProvinceCityAreaConfigurationNormal()
        {
        }

        /// <summary>
        /// 额外的数据映射
        /// </summary>
        partial void ToolProvinceCityAreaConfigurationAppend();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.Entity.Master
{
    /// <summary>
    /// 日志
    /// </summary>
    public class SysLog : EntityModelBaseForReflection
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 日志类型
        /// </summary>
        public SystemLogType Type { get; set; }

        /// <summary>
        /// 日志信息
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// 操作人用户名
        /// </summary>
        public string OperationUserName { get; set; }

        /// <summary>
        /// 操作人姓名
        /// </summary>
        public string OperationRealName { get; set; }

        /// <summary>
        /// 操作时间
        /// </summary>
        public DateTime OperationDateTime { get; set; }

        /// <summary>
        /// 操作人IP地址
        /// </summary>
        public string OperationIpAddress { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.Entity.Master
{
    /// <summary>
    /// 角色权限
    /// </summary>
    public class SysRolePermission : EntityBaseModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 角色ID
        /// </summary>
        public string FK_RoleID { get; set; }

        /// <summary>
        /// 权限ID
        /// </summary>
        public string FK_PermissionID { get; set; }

        /// <summary>
        /// 选中状态
        /// </summary>
        public bool Check { get; set; }
    }
}

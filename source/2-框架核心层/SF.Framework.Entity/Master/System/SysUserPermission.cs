﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.Entity.Master
{
    /// <summary>
    /// 用户权限
    /// </summary>
    public class SysUserPermission : EntityBaseModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 用户ID
        /// </summary>
        public string FK_UserID { get; set; }

        /// <summary>
        /// 菜单权限ID
        /// </summary>
        public string FK_PermissionID { get; set; }

        /// <summary>
        /// 选中状态
        /// Check == true and Indeterminate == false，忽略角色权限，就是checked
        /// Check == true and Indeterminate == true，从角色权限继承，就是三态
        /// Check == false，忽略角色权限，就是unchecked
        /// </summary>
        public bool Check { get; set; }

        /// <summary>
        /// 打勾状态
        /// Check == true and Indeterminate == false，忽略角色权限，就是checked
        /// Check == true and Indeterminate == true，从角色权限继承，就是三态
        /// Check == false，忽略角色权限，就是unchecked
        /// </summary>
        public bool Indeterminate { get; set; }
    }
}

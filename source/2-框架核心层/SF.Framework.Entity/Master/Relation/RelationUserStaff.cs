﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.Entity.Master
{
    /// <summary>
    /// 用户员工关系
    /// </summary>
    public class RelationUserStaff : EntityBaseModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 用户ID
        /// </summary>
        public string FK_UserID { get; set; }

        /// <summary>
        /// 员工ID
        /// </summary>
        public string FK_StaffID { get; set; }
    }
}

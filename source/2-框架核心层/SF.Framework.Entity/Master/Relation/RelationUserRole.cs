﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.Entity.Master
{
    /// <summary>
    /// 用户角色关系
    /// </summary>
    public class RelationUserRole : EntityBaseModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 用户ID
        /// </summary>
        public string FK_UserID { get; set; }

        /// <summary>
        /// 角色ID
        /// </summary>
        public string FK_RoleID { get; set; }

        /// <summary>
        /// 是否为主要关系
        /// </summary>
        public bool IsMaster { get; set; }
    }
}

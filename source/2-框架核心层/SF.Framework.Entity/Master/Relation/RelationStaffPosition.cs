﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.Entity.Master
{
    /// <summary>
    /// 员工岗位机构关系表
    /// </summary>
    public class RelationStaffPosition : EntityBaseModel
    {
        /// <summary>
        /// 主键
        /// </summary>
        public string ID { get; set; }

        /// <summary>
        /// 员工ID
        /// </summary>
        public string FK_StaffID { get; set; }

        /// <summary>
        /// 岗位ID
        /// </summary>
        public string FK_PositionID { get; set; }

        /// <summary>
        /// 是否为主要关系
        /// </summary>
        public bool IsMaster { get; set; }
    }
}

﻿数据库实体项目库，以一级文件夹的方式来区分不同数据库下的实体。

项目命名空间：SF.Framework.Entity

根目录文件夹A表示数据库A（文件夹名称与数据库名称无关），那么 文件夹A 下的 entity 的命名空间：SF.Framework.Entity.A
根目录文件夹B表示数据库B（文件夹名称与数据库名称无关），那么 文件夹B 下的 entity 的命名空间：SF.Framework.Entity.B
...
也可以在 文件夹A 和 entity 之间再加小文件夹，只要保证 entity 的命名空间是 SF.Framework.Entity.一级文件夹名称 即可


反射逻辑如下：

找到 SF.Framework.Entity.dll 程序中，继承于 EntityBaseModel 的Type集合。

将该Type集合按 “各Type的命名空间去掉 SF.Framework.Entity 后的第一个 . 符号后的名称 作为 Type 的前缀”进行分类，将Type集合
分为多组，如A组、B组。

遍历A组，利用组内Type元素的 Name 和 组名A，
去生成 指定项目类库下的指定路径中的Generate文件夹下的A文件夹（若不存在会自动创建）下的 Type 类。

遍历B组...
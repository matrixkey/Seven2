﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel.DataAnnotations;

using SF.Utilities;

namespace SF.Framework.DataAchieve.Utilities
{
    internal static class EntityMapper
    {
        /// <summary>
        /// 实体的 主键属性 的缓存
        /// </summary>
        private static Dictionary<Type, List<PropertyInfo>> PrimaryKeysPropertiesCache = new Dictionary<Type, List<PropertyInfo>>();

        /// <summary>
        /// 实体的 获取属性值 的委托缓存
        /// </summary>
        private static Dictionary<Type, Dictionary<string, Delegate>> PropertyValueGetterDelegatesCache = new Dictionary<Type, Dictionary<string, Delegate>>();




        /// <summary>
        /// 获取 实体数据模型 中所有 主键属性 和 主键属性的值 组成的一个键值集合。如果该 实体数据模型类型 没有定义主键，则返回一个空集合。
        /// </summary>
        /// <typeparam name="TEntity">实体数据模型类型</typeparam>
        /// <param name="entity">实体对象</param>
        /// <returns>一个键值集合，每个元素都是单个主键属性 和 主键属性的值。</returns>
        public static IEnumerable<KeyValuePair<PropertyInfo, object>> GetPrimaryKeyPropertyTypeValue<TEntity>(TEntity entity) where TEntity : SF.Framework.EntityModelBaseForReflection
        {
            List<KeyValuePair<PropertyInfo, object>> result = new List<KeyValuePair<PropertyInfo, object>>();
            Type entityType = typeof(TEntity);

            PropertyInfo[] primaryKeysProperties = GetPrimaryKeys<TEntity>();

            Dictionary<string, Delegate> dels;
            if (PropertyValueGetterDelegatesCache.TryGetValue(entityType, out dels))
            {
                foreach (var item in primaryKeysProperties)
                {
                    var del = dels.FirstOrDefault(f => f.Key == item.Name);
                    if (del.IsNull())
                    {
                        Delegate temp = CreatePropertyValueGetterDelegate<TEntity>(item);
                        if (!temp.IsNull())
                        {
                            dels.Add(item.Name, temp);
                            result.Add(new KeyValuePair<PropertyInfo, object>(item, ((Func<TEntity, object>)temp)(entity)));
                        }
                    }
                    else
                    {
                        result.Add(new KeyValuePair<PropertyInfo, object>(item, ((Func<TEntity, object>)del.Value)(entity)));
                    }
                }
            }
            else
            {
                dels = new Dictionary<string, Delegate>();
                foreach (var item in primaryKeysProperties)
                {
                    Delegate temp = CreatePropertyValueGetterDelegate<TEntity>(item);
                    if (!temp.IsNull())
                    {
                        dels.Add(item.Name, temp);
                        result.Add(new KeyValuePair<PropertyInfo, object>(item, ((Func<TEntity, object>)temp)(entity)));
                    }
                }
                PropertyValueGetterDelegatesCache.Add(entityType, dels);
            }

            return result.AsEnumerable();
        }

        /// <summary>
        /// 设置 <see cref="property"/> 属性在 实体数据模型类型 中的值。
        /// </summary>
        /// <typeparam name="TEntity">实体数据模型类型</typeparam>
        /// <param name="entity">实体数据模型</param>
        /// <param name="property">要设置值的属性</param>
        /// <param name="value">要设置的值</param>
        public static void SetPropertyValue<TEntity>(TEntity entity, PropertyInfo property, object value) where TEntity : SF.Framework.EntityModelBaseForReflection
        {
            property.SetValue(entity, value);
        }



        /// <summary>
        /// 获取 实体数据模型类型 中的所有主键组成的一个数组。如果该 实体数据模型类型 没有定义主键，则返回一个空数组。
        /// </summary>
        /// <typeparam name="TEntity">实体数据模型类型</typeparam>
        /// <returns>一个数组，其中每个元素都是主键属性。</returns>
        public static PropertyInfo[] GetPrimaryKeys<TEntity>() where TEntity : SF.Framework.EntityModelBaseForReflection
        {
            List<PropertyInfo> result;
            Type t = typeof(TEntity);
            string name = t.Name;
            if (!PrimaryKeysPropertiesCache.TryGetValue(t, out result))
            {
                result = new List<PropertyInfo>();
                foreach (var prop in t.GetProperties())
                {
                    if (prop.HasAttribute(typeof(KeyAttribute)) || string.Equals(prop.Name, "id", StringComparison.InvariantCultureIgnoreCase) || string.Equals(prop.Name, name + "id", StringComparison.InvariantCultureIgnoreCase))
                    {
                        result.Add(prop);
                    }
                }
                try
                {
                    PrimaryKeysPropertiesCache.Add(t, result);
                }
                catch
                {
                    //此处不抛出异常，因为可能是由于并发导致的，可忽略。
                }
            }

            return result.ToArray();
        }

        /// <summary>
        /// 创建 获取实体数据模型类型中指定属性的属性值 的委托。如果该 实体数据模型类型 没有定义该指定属性，则返回null。
        /// </summary>
        /// <typeparam name="TEntity">实体数据模型类型</typeparam>
        /// <returns></returns>
        private static Func<TEntity, object> CreatePropertyValueGetterDelegate<TEntity>(PropertyInfo property) where TEntity : SF.Framework.EntityModelBaseForReflection
        {
            Type entityType = typeof(TEntity);
            if (!entityType.GetProperties().Any(a => a.Name == property.Name)) { return null; }

            ParameterExpression paramExpr = Expression.Parameter(entityType, "e");
            MemberExpression bodyExpr = Expression.Property(paramExpr, property);

            return Expression.Lambda<Func<TEntity, object>>(bodyExpr, paramExpr).Compile();
        }
    }
}

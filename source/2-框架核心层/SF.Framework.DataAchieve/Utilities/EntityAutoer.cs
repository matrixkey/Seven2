﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Utilities;

namespace SF.Framework.DataAchieve.Utilities
{
    internal static class EntityAutoer
    {
        private static readonly object sync = new object();

        private static ISessionFactory sf;

        internal static ISessionFactory Sf
        {
            get
            {
                if (sf == null)
                {
                    lock (sync)
                    {
                        var types = typeof(ISessionFactory).GetSubClass().Where(w => !w.IsAbstract);
                        if (types.IsNullOrEmpty()) { return null; }
                        sf = Activator.CreateInstance(types.FirstOrDefault()) as ISessionFactory;
                    }
                }
                return sf;
            }
        }

        /// <summary>
        /// 实体中 记录最后更新信息 的属性的关键字
        /// </summary>
        private static string UpdatePropertiesKeyWord
        {
            get { return "lastmodify"; }
        }

        /// <summary>
        /// 实体中 自动设置主键值 的主键类型符合条件
        /// </summary>
        private static Type TypeOfAutoSetPrimaryKeyValue
        {
            get { return typeof(string); }
        }

        /// <summary>
        /// 获取当前登录用户名
        /// </summary>
        /// <param name="defaultUserName">指定的默认用户名</param>
        /// <returns>当前登录用户名</returns>
        internal static string GetCurrentUserName(string defaultUserName = "guest")
        {
            string result = defaultUserName;
            if (Sf != null && !string.IsNullOrWhiteSpace(Sf.UserName)) { result = Sf.UserName; }

            return result;
        }

        /// <summary>
        /// 新增或更新单个实体时对实体的基本属性自动赋值
        /// </summary>
        /// <typeparam name="TEntity">实体类型</typeparam>
        /// <param name="entity">目标实体对象</param>
        /// <param name="isUpdate">是否更新动作，默认为false，即默认为新增动作</param>
        internal static void SetBaseProperty<TEntity>(TEntity entity, bool isUpdate = false) where TEntity : SF.Framework.EntityModelBaseForReflection
        {
            if (!CheckHasBaseProperty<TEntity>()) { return; }
            var baseEntity = entity as SF.Framework.EntityBaseModel;
            string currentUserName = GetCurrentUserName();

            if (!isUpdate)
            {
                #region 新增时

                var primaryKeyTypeValues = EntityMapper.GetPrimaryKeyPropertyTypeValue(entity);
                foreach (var item in primaryKeyTypeValues)
                {
                    if (item.Key.PropertyType == typeof(string) && string.IsNullOrWhiteSpace(item.Value as string))
                    {
                        EntityMapper.SetPropertyValue(entity, item.Key, SF.Utilities.GuidHelper.GetNewFormatGuid());
                    }
                }
                if (string.IsNullOrWhiteSpace(baseEntity.CreateUser))
                {
                    baseEntity.CreateUser = currentUserName;
                    baseEntity.CreateDate = DateTime.Now;
                }

                #endregion
            }
            else
            {
                #region 更新时
                baseEntity.LastModifyUser = currentUserName;
                baseEntity.LastModifyDate = DateTime.Now;
                #endregion
            }
        }

        /// <summary>
        /// 新增或更新实体集合时对每个实体的基本属性自动赋值
        /// </summary>
        /// <typeparam name="TEntity">实体类型</typeparam>
        /// <param name="entities">目标实体对象集合</param>
        /// <param name="isUpdate">是否更新动作，默认为false，即默认为新增动作</param>
        internal static void SetBaseProperty<TEntity>(IEnumerable<TEntity> entities, bool isUpdate = false) where TEntity : SF.Framework.EntityModelBaseForReflection
        {
            if (!CheckHasBaseProperty<TEntity>()) { return; }

            string currentUserName = GetCurrentUserName();

            if (!isUpdate)
            {
                #region 新增时

                foreach (var entity in entities)
                {
                    var baseEntity = entity as SF.Framework.EntityBaseModel;
                    var primaryKeyTypeValues = EntityMapper.GetPrimaryKeyPropertyTypeValue(entity);
                    foreach (var item in primaryKeyTypeValues)
                    {
                        if (item.Key.PropertyType == typeof(string) && string.IsNullOrWhiteSpace(item.Value as string))
                        {
                            EntityMapper.SetPropertyValue(entity, item.Key, SF.Utilities.GuidHelper.GetNewFormatGuid());
                        }
                    }
                    if (string.IsNullOrWhiteSpace(baseEntity.CreateUser))
                    {
                        baseEntity.CreateUser = currentUserName;
                        baseEntity.CreateDate = DateTime.Now;
                    }
                }

                #endregion
            }
            else
            {
                #region 更新时

                foreach (var entity in entities)
                {
                    var baseEntity = entity as SF.Framework.EntityBaseModel;

                    baseEntity.LastModifyUser = currentUserName;
                    baseEntity.LastModifyDate = DateTime.Now;
                }

                #endregion
            }
        }

        /// <summary>
        /// 获取 实体基类 <see cref="SF.Framework.EntityBaseModel"/> 中的更新相关字段名称集合
        /// </summary>
        /// <typeparam name="TEntity">实体类型</typeparam>
        /// <returns></returns>
        internal static IEnumerable<string> GetBaseUpdateProperties<TEntity>() where TEntity : SF.Framework.EntityModelBaseForReflection
        {
            if (!CheckHasBaseProperty<TEntity>()) { return new string[] { }; }
            return typeof(SF.Framework.EntityBaseModel).GetProperties().Where(w => w.GetMethod != null && w.SetMethod != null && w.Name.ToLower().Contains(UpdatePropertiesKeyWord)).Select(s => s.Name);
        }

        /// <summary>
        /// 检查 TEntity 是否继承与 <see cref="SF.Framework.EntityBaseModel"/>
        /// </summary>
        /// <typeparam name="TEntity">实体类型</typeparam>
        /// <returns>若继承与<see cref="SF.Framework.EntityBaseModel"/>则返回 true ，否则返回 false 。</returns>
        internal static bool CheckHasBaseProperty<TEntity>()
        {
            return typeof(TEntity).IsInhertOf(typeof(SF.Framework.EntityBaseModel));
        }
    }
}

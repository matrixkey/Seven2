﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

using SF.Data.Entity;
using SF.Data.Entity.Practices;
using SF.Framework.DataInterface;
using SF.Web.IntelligentQuery.Extensions;
using SF.Utilities;

namespace SF.Framework.DataAchieve.EntityFramework
{
    /// <summary>
    /// 仓储基本接口的实现
    /// </summary>
    /// <typeparam name="TEntity">实体类型</typeparam>
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : EntityModelBaseForReflection, new()
    {
        #region 构造方法

        public BaseRepository()
        { }

        /// <summary>
        /// 该构造方法仅在仓储中被使用，因此设置访问修饰符为protected
        /// </summary>
        /// <param name="unit">工作单元</param>
        protected BaseRepository(UnitOfWork unit)
            : this()
        { this.UnitOfWork = unit; }

        #endregion

        #region 属性

        #region protected 仅派生类可用

        /// <summary>
        /// 当前仓储所在的工作单元对象
        /// </summary>
        protected UnitOfWork UnitOfWork { get; set; }

        #endregion

        #region private 私有，仅当前类可用

        /// <summary>
        /// 数据上下文对象
        /// </summary>
        private System.Data.Entity.DbContext Db
        {
            get;
            set;
        }

        private DbSet<TEntity> DbSet { get { return this.Db.Set<TEntity>(); } }

        #endregion

        #endregion

        #region 对接口的实现方法。公开，允许开放给其他项目调用

        /// <summary>
        /// 设置工作单元对象
        /// <param name="unit">工作单元对象</param>
        /// </summary>
        public void SetUnitOfWork(object unit)
        {
            if (unit is UnitOfWork) { this.UnitOfWork = unit as UnitOfWork; }
            else
            {
                throw ExceptionHelper.ThrowDataAccessException("给仓储设置工作单元时发生异常，参数 unit 不是一个工作单元对象。");
            }
        }

        /// <summary>
        /// 设置当前仓储的 System.Data.Entity.DbContext 对象。本方法专供 各数据库仓储基类在初始化时调用。
        /// </summary>
        /// <param name="db">数据库上下文对象</param>
        public void SetDataContext(object db)
        {
            if (db is System.Data.Entity.DbContext) { this.Db = db as System.Data.Entity.DbContext; }
            else
            {
                throw ExceptionHelper.ThrowDataAccessException("给仓储设置数据库上下文时发生异常，参数 db 不是一个数据库上下文对象。");
            }
        }

        #region 数据查询

        /// <summary>
        /// 主键查询
        /// </summary>
        /// <param name="keyValues">键值</param>
        /// <returns>实体</returns>
        public virtual TEntity Find(IEnumerable<object> keyValues)
        {
            this.CheckUnitOfWork();
            Check.NotEmpty(keyValues);
            keyValues = keyValues.Where(keyValue => keyValue != null);

            return this.DbSet.Find(keyValues).Duplicate();
        }

        /// <summary>
        /// 主键查询
        /// </summary>
        /// <param name="keyValues">键值</param>
        /// <returns>实体</returns>
        public virtual TEntity Find(params object[] keyValues)
        {
            this.CheckUnitOfWork();
            Check.NotEmpty(keyValues);

            return this.DbSet.Find(keyValues).Duplicate();
        }

        #endregion

        #region 数据操作

        #region 添加

        /// <summary>
        /// 添加实体
        /// </summary>
        /// <param name="entity">实体</param>
        public void Add(TEntity entity)
        {
            if (entity == null)
            { return; }
            this.CheckUnitOfWork();

            Utilities.EntityAutoer.SetBaseProperty(entity, false);
            this.DbSet.Add(entity);
        }

        /// <summary>
        /// 批量添加实体
        /// </summary>
        /// <param name="entities">实体集合</param>
        public void AddRange(IEnumerable<TEntity> entities)
        {
            if (entities.IsNullOrEmpty())
            { return; }
            this.CheckUnitOfWork();

            Utilities.EntityAutoer.SetBaseProperty(entities, false);

            DbSet<TEntity> set = this.DbSet;
            bool autoDetectChangesEnabled = this.Db.Configuration.AutoDetectChangesEnabled;
            this.Db.Configuration.AutoDetectChangesEnabled = false;

            foreach (TEntity entity in entities)
            {
                if (entity == null)
                { continue; }

                set.Add(entity);
            }

            this.Db.Configuration.AutoDetectChangesEnabled = autoDetectChangesEnabled;
        }

        /// <summary>
        /// 批量添加实体
        /// </summary>
        /// <param name="entities">实体集合</param>
        public void AddRange(params TEntity[] entities)
        {
            this.AddRange(entities.AsEnumerable());
        }

        #endregion

        #region 更改

        /// <summary>
        /// 更改实体
        /// </summary>
        /// <param name="entity">实体对象</param>
        public void Update(TEntity entity)
        {
            if (entity == null)
            { return; }
            this.CheckUnitOfWork();

            Utilities.EntityAutoer.SetBaseProperty(entity, true);

            entity = entity.DuplicateWithNonNavigations(this.Db);
            DbEntityEntry<TEntity> entry = this.Db.FindEntry(entity, true);
            if (entry.State == EntityState.Detached)
            {
                this.DbSet.Attach(entity);
            }
            entry.State = EntityState.Modified;
        }

        /// <summary>
        /// 更改实体，可指定要更新的属性
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <param name="properties">要更新的属性名称集合，若属性为空则表示更新所有属性。</param>
        public void Update(TEntity entity, IEnumerable<string> properties)
        {
            if (entity == null)
            { return; }
            this.CheckUnitOfWork();

            Utilities.EntityAutoer.SetBaseProperty(entity, true);

            DbSet<TEntity> set = this.DbSet;
            entity = entity.DuplicateWithNonNavigations(this.Db);
            DbEntityEntry<TEntity> entry = this.Db.FindEntry(entity, false);
            if (entry.State == EntityState.Detached)
            {
                set.Attach(entity);
            }
            entry.State = EntityState.Unchanged;

            var values = entity.ToDictionary();
            IEnumerable<string> updateProperties = properties.Union(Utilities.EntityAutoer.GetBaseUpdateProperties<TEntity>()).Distinct();
            foreach (string name in updateProperties)
            {
                DbPropertyEntry property = entry.Property(name);
                if (property == null) { throw new Exception("要更新的字段 " + name + " 并不存在于 表 " + this.Db.GetTableName<TEntity>() + " 中。"); }
                property.IsModified = true;
                property.CurrentValue = values[name];
            }
        }

        /// <summary>
        /// 更改实体，可指定要更新的属性
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <param name="properties">要更新的属性名称集合，若属性为空则表示更新所有属性。</param>
        public void Update(TEntity entity, params string[] properties)
        {
            this.Update(entity, properties.AsEnumerable());
        }

        /// <summary>
        /// 批量更改实体
        /// </summary>
        /// <param name="entities">实体集合</param>
        public void UpdateRange(IEnumerable<TEntity> entities)
        {
            if (entities.IsNullOrEmpty())
            { return; }
            this.CheckUnitOfWork();

            Utilities.EntityAutoer.SetBaseProperty(entities, true);

            var set = this.DbSet;
            bool autoDetectChangesEnabled = this.Db.Configuration.AutoDetectChangesEnabled;
            this.Db.Configuration.AutoDetectChangesEnabled = false;

            foreach (TEntity entity in entities)
            {
                if (entity == null)
                { continue; }

                TEntity temp = entity.DuplicateWithNonNavigations(this.Db);
                DbEntityEntry<TEntity> entry = this.Db.FindEntry(temp, true);
                if (entry.State == EntityState.Detached)
                {
                    set.Attach(temp);
                }
                entry.State = EntityState.Modified;
            }

            this.Db.Configuration.AutoDetectChangesEnabled = autoDetectChangesEnabled;
        }

        /// <summary>
        /// 批量更改实体
        /// </summary>
        /// <param name="entities">实体集合</param>
        public void UpdateRange(params TEntity[] entities)
        {
            this.UpdateRange(entities.AsEnumerable());
        }

        /// <summary>
        /// 批量更改实体，可指定要更新的属性
        /// </summary>
        /// <param name="entities">实体集合</param>
        /// <param name="properties">要更新的属性名称集合，若属性为空则表示更新所有属性。</param>
        public void UpdateRange(IEnumerable<TEntity> entities, IEnumerable<string> properties)
        {
            if (entities.IsNullOrEmpty())
            { return; }
            this.CheckUnitOfWork();

            IEnumerable<string> updateProperties = properties.Union(Utilities.EntityAutoer.GetBaseUpdateProperties<TEntity>()).Distinct();

            var set = this.DbSet;
            bool autoDetectChangesEnabled = this.Db.Configuration.AutoDetectChangesEnabled;
            this.Db.Configuration.AutoDetectChangesEnabled = false;

            foreach (TEntity entity in entities)
            {
                if (entity == null)
                { continue; }

                Utilities.EntityAutoer.SetBaseProperty(entity, true);
                TEntity temp = entity.DuplicateWithNonNavigations(this.Db);
                DbEntityEntry<TEntity> entry = this.Db.FindEntry(temp, false);
                if (entry.State == EntityState.Detached)
                {
                    set.Attach(temp);
                }
                entry.State = EntityState.Unchanged;

                var values = temp.ToDictionary();
                foreach (string name in updateProperties)
                {
                    DbPropertyEntry property = entry.Property(name);
                    if (property == null) { throw new Exception("要更新的字段 " + name + " 并不存在于 表 " + this.Db.GetTableName<TEntity>() + " 中。"); }
                    property.IsModified = true;
                    property.CurrentValue = values[name];
                }
            }

            this.Db.Configuration.AutoDetectChangesEnabled = autoDetectChangesEnabled;
        }

        /// <summary>
        /// 批量更改实体，可指定要更新的属性
        /// </summary>
        /// <param name="entities">实体集合</param>
        /// <param name="properties">要更新的属性名称集合</param>
        public void UpdateRange(IEnumerable<TEntity> entities, params string[] properties)
        {
            this.UpdateRange(entities, properties.AsEnumerable());
        }

        #endregion

        #region 删除

        /// <summary>
        /// 主键删除实体
        /// </summary>
        /// <param name="key">键值</param>
        public void Delete(object key)
        {
            if (key == null || string.IsNullOrWhiteSpace(key.ToString()))
            { return; }
            //this.CheckUnitOfWork(); //Find里会检查

            TEntity entity = this.Find(key);

            this.Delete(entity);
        }

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="entity">实体</param>
        public void Delete(TEntity entity)
        {
            if (entity == null)
            { return; }
            this.CheckUnitOfWork();

            entity = entity.DuplicateWithNonNavigations(this.Db);

            DbSet<TEntity> set = this.DbSet;
            DbEntityEntry<TEntity> entry = this.Db.FindEntry(entity, false);
            if (entry.State == EntityState.Detached)
            {
                set.Attach(entity); set.Remove(entity);
            }
            else
            { entry.State = EntityState.Deleted; }
        }

        /// <summary>
        /// 批量删除实体
        /// </summary>
        /// <param name="entities">实体集合</param>
        public void DeleteRange(IEnumerable<TEntity> entities)
        {
            if (entities.IsNullOrEmpty())
            { return; }
            this.CheckUnitOfWork();
            var set = this.DbSet;

            bool autoDetectChangesEnabled = this.Db.Configuration.AutoDetectChangesEnabled;
            this.Db.Configuration.AutoDetectChangesEnabled = false;

            foreach (TEntity entity in entities)
            {
                if (entity == null)
                { continue; }

                TEntity temp = entity.DuplicateWithNonNavigations(this.Db);
                DbEntityEntry<TEntity> entry = this.Db.FindEntry(temp, false);
                if (entry.State == EntityState.Detached)
                {
                    set.Attach(temp);
                }
                else
                {
                    entry.State = EntityState.Deleted;
                }
            }

            this.Db.Configuration.AutoDetectChangesEnabled = autoDetectChangesEnabled;
        }

        /// <summary>
        /// 批量删除实体
        /// </summary>
        /// <param name="entities">实体集合</param>
        public void DeleteRange(params TEntity[] entities)
        {
            this.DeleteRange(entities.AsEnumerable());
        }

        /// <summary>
        /// 根据条件删除实体
        /// </summary>
        /// <param name="predicate">查询条件</param>
        public void DeleteRange(Expression<Func<TEntity, bool>> predicate)
        {
            this.DeleteRange(this.DbSet.Where(predicate).ToArray());
        }

        /// <summary>
        /// 根据条件删除实体
        /// </summary>
        /// <param name="predicate">查询条件</param>
        public void DeleteRange(Expression<Func<TEntity, int, bool>> predicate)
        {
            this.DeleteRange(this.DbSet.Where(predicate).ToArray());
        }

        #endregion

        #endregion

        #endregion

        #region 自身实现的方法

        #region private 私有，仅当前类可用

        /// <summary>
        /// 检验当前仓储对象的UnitOfWork是否已初始化，若未初始化则抛出异常。
        /// </summary>
        private void CheckUnitOfWork()
        {
            if (this.UnitOfWork == null)
            {
                throw new Exception("当前使用的仓储对象未初始化其所属工作单元对象。");
            }
        }

        #endregion

        #region protected 仅自己和派生类可用

        #region 智能查询的条件组装

        /// <summary>
        /// 将string格式的约定查询条件转化成查询条件模型对象
        /// </summary>
        /// <param name="model">string格式的约定查询条件</param>
        /// <returns>查询条件模型对象</returns>
        protected SF.Web.IntelligentQuery.Model.QueryModel ConvertQueryModel(string model)
        {
            return SF.Web.IntelligentQuery.Converter.QueryConverter.ToQueryModel(model);
        }

        /// <summary>
        /// 将string格式的约定查询条件转化成查询条件模型对象，同时将其中常规的参数组装成Dictionary以输出参数返回
        /// </summary>
        /// <param name="model">string格式的约定查询条件</param>
        /// <param name="dic">常规格式的查询条件组装成Dictionary以输出参数返回，Key是参数名，Value是参数值</param>
        /// <returns>查询条件模型对象</returns>
        protected SF.Web.IntelligentQuery.Model.QueryModel ConvertQueryModel(string model, ref Dictionary<string, string> dic)
        {
            return SF.Web.IntelligentQuery.Converter.QueryConverter.ToQueryModel(model, ref dic);
        }

        #endregion

        #endregion

        #region internal 仅当前项目中可用

        /// <summary>
        /// 获取 <see cref="TEntity"/> 的数据查询器
        /// </summary>
        /// <returns>数据查询器</returns>
        internal IQueryable<TEntity> Query()
        {
            return this.DbSet.AsQueryable();
        }

        /// <summary>
        /// 获取 <see cref="TEntity"/> 的数据查询器
        /// </summary>
        /// <param name="predicate">查询条件</param>
        /// <returns>数据查询器</returns>
        internal IQueryable<TEntity> Query(Expression<Func<TEntity, bool>> predicate)
        {
            return this.DbSet.Where(predicate);
        }

        /// <summary>
        /// 根据序列化的查询条件获取 <see cref="TEntity"/> 的数据查询器
        /// </summary>
        /// <param name="model">序列化的查询条件</param>
        /// <returns>数据查询器</returns>
        internal IQueryable<TEntity> Query(string model)
        {
            var query = this.Query();
            Dictionary<string, string> dicModel = new Dictionary<string, string>();
            var queryModel = this.ConvertQueryModel(model, ref dicModel);

            return query.Where(queryModel);
        }

        #region 查询是否存在记录

        /// <summary>
        /// 根据指定的条件表达式判断符合条件的记录是否存在
        /// </summary>
        /// <param name="predicate">条件表达式</param>
        /// <returns>存在记录，返回 true 。不存在记录，则返回 false 。</returns>
        internal virtual bool Any(Expression<Func<TEntity, bool>> predicate)
        {
            this.CheckUnitOfWork();
            return this.DbSet.Any(predicate);
        }

        #endregion

        #region 查询最大值

        /// <summary>
        /// 根据指定的条件表达式和指定的Max对象表达式返回最大结果值
        /// </summary>
        /// <typeparam name="TResult">返回类型</typeparam>
        /// <param name="predicate">条件表达式</param>
        /// <param name="selector">要应用于每个元素的投影函数。</param>
        /// <returns>序列中的最大值</returns>
        internal virtual TResult Max<TResult>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, TResult>> selector)
        {
            this.CheckUnitOfWork();
            return this.DbSet.Where(predicate).Max(selector);
        }

        #endregion

        #region 查询数量

        /// <summary>
        /// 根据指定的条件表达式返回符合条件的记录的数量
        /// </summary>
        /// <param name="predicate">条件表达式</param>
        /// <returns>符合条件的记录的数量</returns>
        internal virtual int Count(Expression<Func<TEntity, bool>> predicate)
        {
            this.CheckUnitOfWork();
            return this.DbSet.Count(predicate);
        }

        #endregion

        #endregion

        #endregion


    }
}

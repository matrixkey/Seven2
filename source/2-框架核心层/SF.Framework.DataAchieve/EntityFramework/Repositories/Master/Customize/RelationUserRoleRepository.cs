﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataAchieve.EntityFramework.Repositories.Master
{
    partial class RelationUserRoleRepository
    {
        /// <summary>
        /// 检查指定ID的角色是否存在与用户的关系
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <returns>存在关系则返回 true ，否则返回 false 。</returns>
        public bool CheckUserExistInRole(string roleId)
        {
            return this.Any(a => a.FK_RoleID == roleId);
        }
    }
}

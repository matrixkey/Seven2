﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Utilities;

namespace SF.Framework.DataAchieve.EntityFramework.Repositories.Master
{
    partial class ToolProvinceCityAreaRepository
    {
        /// <summary>
        /// 检查指定的省市区编号是否存在
        /// </summary>
        /// <param name="code">省市区编号</param>
        /// <returns>存在返回 true ，否则返回 false 。</returns>
        public bool CheckCodeExist(string code)
        {
            return this.Any(a => a.Code == code);
        }

        /// <summary>
        /// 获取所有省市区实体集合
        /// </summary>
        /// <returns>所有省市区实体集合</returns>
        public IEnumerable<ToolProvinceCityArea> GetAll()
        {
            return this.Query().ToArray();
        }
    }
}

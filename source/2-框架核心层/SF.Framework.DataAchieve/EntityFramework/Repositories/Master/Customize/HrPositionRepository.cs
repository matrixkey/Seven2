﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.DataAchieve.EntityFramework.Repositories.Master
{
    partial class HrPositionRepository
    {
        /// <summary>
        /// 获取指定ID的岗位编辑数据模型
        /// </summary>
        /// <param name="id">岗位ID</param>
        /// <returns>岗位编辑数据模型</returns>
        public PositionFormModel GetPositionFormModel(string id)
        {
            var positionQuery = this.Query();
            var organQuery = new HrOrganizationRepository(this.UnitOfWork).Query();

            var query = from a in positionQuery
                        join b in organQuery on a.FK_OrganID equals b.ID into ab
                        from DeptInfo in ab.DefaultIfEmpty()
                        where a.ID == id
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.FK_OrganID,
                            OrganName = DeptInfo == null ? "" : DeptInfo.Name,
                            a.SortNumber
                        };

            return query.FirstOrDefault().CastTo<PositionFormModel>();
        }

        /// <summary>
        /// 根据机构ID获取该机构下的岗位数据模型集合
        /// </summary>
        /// <param name="organId">机构ID</param>
        /// <returns>该机构下的岗位数据模型集合</returns>
        public IEnumerable<PositionListModel> GetPositionModels(string organId)
        {
            var positionQuery = this.Query();
            var organQuery = new HrOrganizationRepository(this.UnitOfWork).Query();

            var query = from a in positionQuery
                        join b in organQuery on a.FK_OrganID equals b.ID into ab
                        from DeptInfo in ab.DefaultIfEmpty()
                        where a.FK_OrganID == organId
                        orderby a.SortNumber
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.FK_OrganID,
                            OrganName = DeptInfo == null ? "" : DeptInfo.Name,
                            a.SortNumber
                        };

            return query.ToArray().Select(s => s.CastTo<PositionListModel>());
        }

        /// <summary>
        /// 获取岗位列表数据，供岗位选择器使用，带岗位的完整所属信息
        /// </summary>
        /// <param name="positionIds">岗位ID集合</param>
        /// <param name="positionName">岗位名称，模糊查询</param>
        /// <param name="organId">机构ID</param>
        /// <returns></returns>
        public IEnumerable<PositionSelectorListModel> GetPositionSelectorModels(IEnumerable<string> positionIds, string positionName, string organId)
        {
            string sql = @"select a.ID as PositionID,a.Name as PositionName,
                            b.ID as FK_OrganID,b.Name as OrganName, b.Code as OrganCode
                            from HrPosition a
                            left join HrOrganization b on a.FK_OrganID = b.id
                            where 1 = 1 ";
            List<System.Data.SqlClient.SqlParameter> p = new List<System.Data.SqlClient.SqlParameter>();

            if (!string.IsNullOrWhiteSpace(organId))
            {
                sql += " and a.FK_OrganID = @OrganID";
                p.Add(new System.Data.SqlClient.SqlParameter { ParameterName = "@OrganID", Value = organId });
            }
            if (!string.IsNullOrWhiteSpace(positionName))
            {
                sql += " and a.Name like @PositionName";
                p.Add(new System.Data.SqlClient.SqlParameter { ParameterName = "@PositionName", Value = "%" + positionName + "%" });
            }
            if (!positionIds.IsNullOrEmpty())
            {
                //sql += " and a.ID in (@PositionIDs)";
                //p.Add(new System.Data.SqlClient.SqlParameter { ParameterName = "@PositionIDs", Value = SqlParamsHelper.BuildInParam(positionIds, true) });

                sql += " and a.ID in (" + SqlParamsHelper.BuildInParam(positionIds, false) + ")";
            }

            return p.IsNullOrEmpty() ? this.Database.SqlQuery<PositionSelectorListModel>(sql).ToArray() : this.Database.SqlQuery<PositionSelectorListModel>(sql, p.ToArray()).ToArray();

        }

        /// <summary>
        /// 检查指定机构ID下是否存在岗位
        /// </summary>
        /// <param name="organId">机构ID</param>
        /// <returns>若存在岗位，返回 true ，否则返回 false 。</returns>
        public bool CheckPositionExistInOrgan(string organId)
        {
            return this.Any(a => a.FK_OrganID == organId);
        }
    }
}

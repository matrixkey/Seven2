﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.DataAchieve.EntityFramework.Repositories.Master
{
    partial class SysMenuPermissionRepository
    {
        /// <summary>
        /// 获取所有菜单权限实体集合
        /// </summary>
        /// <returns>菜单权限实体集合</returns>
        public IEnumerable<SysMenuPermission> GetAllMenuPermissions()
        {
            return this.Query().OrderBy(o => o.FK_MenuID).ToArray(); ;
        }

        /// <summary>
        /// 根据菜单权限ID集合获取菜单权限集合
        /// </summary>
        /// <param name="ids">菜单权限ID集合</param>
        /// <returns>菜单权限集合</returns>
        public IEnumerable<SysMenuPermission> GetMenuPermissions(IEnumerable<string> ids)
        {
            return this.Query(w => ids.Contains(w.ID)).OrderBy(o => o.FK_MenuID).ThenBy(o => o.SortNumber).ToArray();
        }

        /// <summary>
        /// 获取除指定菜单权限ID集合之外的菜单权限集合
        /// </summary>
        /// <param name="ids">要排除的菜单权限ID集合</param>
        /// <param name="filterAlwaysUseable">是否过滤掉总是可用的菜单权限数据</param>
        /// <returns>菜单权限集合</returns>
        public IEnumerable<SysMenuPermission> GetMenuPermissionsExcept(IEnumerable<string> ids, bool filterAlwaysUseable)
        {
            var query = this.Query(w => !ids.Contains(w.ID));
            if (filterAlwaysUseable)
            {
                query = query.Where(w => !w.AlwaysUseable);
            }

            return query.OrderBy(o => o.FK_MenuID).ThenBy(o => o.SortNumber).ToArray();
        }

        /// <summary>
        /// 获取总是可用的菜单权限集合
        /// </summary>
        /// <returns>菜单权限集合</returns>
        public IEnumerable<SysMenuPermission> GetAlwaysUseablePermissions()
        {
            return this.Query(w => w.AlwaysUseable).ToArray();
        }

        /// <summary>
        /// 获取除指定id之外的类型为按钮的菜单权限集合
        /// </summary>
        /// <param name="ids">要排除的菜单权限ID集合</param>
        /// <returns>按钮菜单权限集合</returns>
        public IEnumerable<SysMenuPermission> GetButtonMenuPermissionsExcept(IEnumerable<string> ids)
        {
            return this.Query(w => w.Type == MenuPermissionType.Button && !ids.Contains(w.ID)).OrderBy(o => o.FK_MenuID).ThenBy(o => o.SortNumber).ToArray();
        }

        /// <summary>
        /// 获取指定ID的菜单权限数据编辑模型
        /// </summary>
        /// <param name="id">菜单权限ID</param>
        /// <returns>菜单权限数据编辑模型</returns>
        public MenuPermissionFormModel GetMenuPermissionEditModel(string id)
        {
            var menuQuery = new SysMenuRepository(this.UnitOfWork).Query();
            var menuPermissionQuery = this.Query();

            var query = from a in menuPermissionQuery
                        join b in menuQuery on a.FK_MenuID equals b.ID into ab
                        from MenuInfo in ab.DefaultIfEmpty()
                        where a.ID == id
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.FK_MenuID,
                            MenuName = MenuInfo == null ? "" : MenuInfo.Name,
                            a.Type,
                            a.SortNumber,
                            a.ActionIDs,
                            a.GroupMark,
                            a.ButtonName,
                            a.ButtonIconClass,
                            a.ButtonSymbol,
                            a.ButtonHandler
                        };

            var model = query.FirstOrDefault().CastTo<MenuPermissionFormModel>();

            //获取 ActionIDs 对应的 行为信息，最终数据格式：控制器名/行为名[行为类型]
            var actionInfos = new SysActionRepository(this.UnitOfWork).GetActionModels(model.ActionIDs.ToArray(",", true));
            model.ActionInfos = actionInfos.Select(s => s.ControllerName + "/" + s.Name + "[" + s.Type.GetEnumMember() + "] " + s.Description);

            return model;
        }

        /// <summary>
        /// 获取指定菜单的菜单权限列表数据模型集合
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <returns>菜单权限列表数据模型集合</returns>
        public IEnumerable<MenuPermissionListModel> GetMenuPermissionModels(string menuId)
        {
            var menuQuery = new SysMenuRepository(this.UnitOfWork).Query();
            var menuPermissionQuery = this.Query();

            var query = from a in menuPermissionQuery
                        join b in menuQuery on a.FK_MenuID equals b.ID into ab
                        from MenuInfo in ab.DefaultIfEmpty()
                        where a.FK_MenuID == menuId
                        orderby a.SortNumber
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.Type,
                            a.SortNumber,
                            a.GroupMark,
                            a.ButtonName,
                            a.ButtonIconClass,
                            a.ButtonSymbol,
                            a.ButtonHandler
                        };

            return query.ToArray().Select(s => s.CastTo<MenuPermissionListModel>());
        }

        /// <summary>
        /// 根据菜单权限ID删除菜单权限及被关联的数据
        /// </summary>
        /// <param name="id">菜单权限ID</param>
        /// <returns>返回影响的行数</returns>
        public int DeleteMenuPermissionWithRelation(string id)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append(" delete from SysMenuPermission where ID = @ID; ");
            sql.Append(" delete from SysUserPermission where FK_PermissionID = @ID; ");
            sql.Append(" delete from SysRolePermission where FK_PermissionID = @ID; ");

            try
            {
                return this.Database.ExecuteSqlCommand(System.Data.Entity.TransactionalBehavior.EnsureTransaction, sql.ToString(), new System.Data.SqlClient.SqlParameter("@ID", id));
            }
            catch (Exception e)
            {
                Log4Helper.Error(e, "数据访问层异常");
                return -1;
            }
        }
    }
}

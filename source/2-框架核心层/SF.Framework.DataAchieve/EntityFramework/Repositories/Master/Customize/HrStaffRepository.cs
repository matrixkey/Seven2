﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.DataAchieve.EntityFramework.Repositories.Master
{
    partial class HrStaffRepository
    {
        /// <summary>
        /// 获取指定id的员工的编辑数据模型
        /// </summary>
        /// <param name="id">员工ID</param>
        /// <returns>员工的编辑数据模型</returns>
        public StaffFormModel GetStaffFormModel(string id)
        {
            var staffQuery = this.Query();

            //查询员工基本信息
            var query0 = from a in staffQuery
                         where a.ID == id
                         select new
                         {
                             a.ID,
                             a.RealName
                         };

            var resultModel = query0.FirstOrDefault().CastTo<StaffFormModel>();

            if (resultModel != null)
            {
                //查询员工岗位机构关系信息
                var relationQuery1 = new RelationStaffPositionRepository(this.UnitOfWork).Query();
                var positionQuery = new HrPositionRepository(this.UnitOfWork).Query();
                var organQuery = new HrOrganizationRepository(this.UnitOfWork).Query();

                var query1 = from a in relationQuery1
                             join b in positionQuery on a.FK_PositionID equals b.ID into ab
                             from PositionInfo in ab.DefaultIfEmpty()
                             join c in organQuery on PositionInfo.FK_OrganID equals c.ID into ac
                             from OrganInfo in ac.DefaultIfEmpty()
                             where a.FK_StaffID == id
                             orderby a.IsMaster descending
                             select new
                             {
                                 a.ID,
                                 FK_OrganID = PositionInfo == null ? "" : PositionInfo.FK_OrganID,
                                 OrganName = OrganInfo == null ? "" : OrganInfo.Name,
                                 a.FK_PositionID,
                                 PositionName = PositionInfo == null ? "" : PositionInfo.Name,
                                 a.IsMaster
                             };

                var positionOrganRelations = query1.ToArray().Select(s => s.CastTo<StaffPositionOrganModel>());
                resultModel.PositionRelations = positionOrganRelations;
            }

            return resultModel;
        }

        /// <summary>
        /// 获取员工列表分页数据
        /// </summary>
        /// <param name="staffIds">员工ID集合</param>
        /// <param name="realName">员工名称，模糊查询</param>
        /// <param name="organId">机构ID</param>
        /// <param name="queryModel">序列化的查询条件</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">每页条数</param>
        /// <returns>员工列表分页数据</returns>
        public SF.ComponentModel.Paging.PagingData GetStaffListPagerData(IEnumerable<string> staffIds, string realName, string organId, string queryModel, int pageIndex, int pageSize)
        {
            var staffQuery = this.Query(queryModel);
            var relationQuery = new RelationStaffPositionRepository(this.UnitOfWork).Query();
            var positionQuery = new HrPositionRepository(this.UnitOfWork).Query();
            var organQuery = new HrOrganizationRepository(this.UnitOfWork).Query();

            var query = from a in relationQuery
                        join b in staffQuery on a.FK_StaffID equals b.ID into ab
                        from StaffInfo in ab.DefaultIfEmpty()
                        join c in positionQuery on a.FK_PositionID equals c.ID into ac
                        from PositionInfo in ac.DefaultIfEmpty()
                        join d in organQuery on PositionInfo.FK_OrganID equals d.ID into ad
                        from OrganInfo in ad.DefaultIfEmpty()
                        where a.IsMaster && StaffInfo != null
                        select new
                        {
                            StaffInfo.ID,
                            StaffInfo.RealName,
                            a.FK_PositionID,
                            PositionName = PositionInfo == null ? "" : PositionInfo.Name,
                            FK_OrganID = PositionInfo == null ? "" : PositionInfo.FK_OrganID,
                            OrganName = OrganInfo == null ? "" : OrganInfo.Name
                        };

            if (!staffIds.IsNullOrEmpty())
            {
                query = query.Where(w => staffIds.Contains(w.ID));
            }
            if (!string.IsNullOrWhiteSpace(realName))
            {
                query = query.Where(w => w.RealName.Contains(realName));
            }
            if (!string.IsNullOrWhiteSpace(organId) && organId != organQuery.Where(w => w.ParentID == EntityContract.ParentIDDefaultValueWhenIDIsString).Select(s => s.ID).FirstOrDefault())
            {
                query = query.Where(w => w.FK_OrganID == organId);
            }

            return query.Select(s => new { s.ID, s.RealName, s.FK_PositionID, s.PositionName, s.FK_OrganID, s.OrganName }).Distinct().OrderBy(o => o.FK_PositionID).ToPagingDataObject(pageIndex - 1, pageSize);
        }
    }
}

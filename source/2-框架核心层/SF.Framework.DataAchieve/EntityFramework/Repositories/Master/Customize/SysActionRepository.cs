﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.DataAchieve.EntityFramework.Repositories.Master
{
    partial class SysActionRepository
    {
        /// <summary>
        /// 获取指定主键的行为的编辑数据模型
        /// </summary>
        /// <param name="id">行为主键</param>
        /// <returns>行为的编辑数据模型</returns>
        public ActionFormModel GetActionFormModel(string id)
        {
            var controllerQuery = new SysControllerRepository(this.UnitOfWork).Query();
            var actionQuery = this.Query();

            var query = from a in actionQuery
                        join b in controllerQuery on a.FK_ControllerID equals b.ID into ab
                        from ControllerInfo in ab.DefaultIfEmpty()
                        where a.ID == id
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.FK_ControllerID,
                            ControllerName = ControllerInfo.Name,
                            ControllerAreaName = ControllerInfo.AreaName,
                            a.Type,
                            a.SortNumber,
                            a.Description,
                            a.LimitedAttribute
                        };

            return query.FirstOrDefault().CastTo<ActionFormModel>();
        }

        /// <summary>
        /// 获取指定控制器 <see cref="controllerId"/> 的所有行为数据模型集合
        /// </summary>
        /// <param name="controllerId">控制器ID</param>
        /// <param name="actionName">行为名称，用于模糊查询，空则不进行匹配查询</param>
        /// <returns>行为数据模型集合</returns>
        public IEnumerable<ActionListModel> GetActionModels(string controllerId, string actionName)
        {
            var controllerQuery = new SysControllerRepository(this.UnitOfWork).Query();
            var actionQuery = this.Query();

            if (!string.IsNullOrWhiteSpace(actionName)) { actionQuery = actionQuery.Where(w => w.Name.Contains(actionName)); }

            var query = from a in actionQuery
                        join b in controllerQuery on a.FK_ControllerID equals b.ID into ab
                        from ControllerInfo in ab.DefaultIfEmpty()
                        orderby a.SortNumber
                        where a.FK_ControllerID == controllerId
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.FK_ControllerID,
                            ControllerName = ControllerInfo.Name,
                            ControllerAreaName = ControllerInfo.AreaName,
                            a.Type,
                            a.SortNumber,
                            a.Description,
                            a.LimitedAttribute
                        };

            return query.ToArray().Select(s => s.CastTo<ActionListModel>());
        }

        /// <summary>
        /// 获取指定ID集合的行为数据模型集合
        /// </summary>
        /// <param name="ids">行为ID集合</param>
        /// <returns>行为数据模型集合</returns>
        public IEnumerable<ActionListModel> GetActionModels(IEnumerable<string> ids)
        {
            if (ids.IsNullOrEmpty()) { return new List<ActionListModel>().AsEnumerable(); }

            var controllerQuery = new SysControllerRepository(this.UnitOfWork).Query();
            var actionQuery = this.Query();

            var query = from a in actionQuery
                        join b in controllerQuery on a.FK_ControllerID equals b.ID into ab
                        from ControllerInfo in ab.DefaultIfEmpty()
                        orderby a.SortNumber
                        where ids.Contains(a.ID)
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.FK_ControllerID,
                            ControllerName = ControllerInfo.Name,
                            ControllerAreaName = ControllerInfo.AreaName,
                            a.Type,
                            a.SortNumber,
                            a.Description,
                            a.LimitedAttribute
                        };

            return query.ToArray().Select(s => s.CastTo<ActionListModel>());
        }

        /// <summary>
        /// 检查行为是否已经存在
        /// </summary>
        /// <param name="actionName">行为名称</param>
        /// <param name="controllerId">所属控制器ID</param>
        /// <param name="id">需要忽略的主键值。新增判定时可传空，更新判定时则需传当前数据的主键值</param>
        /// <returns>行为存在返回 true ，否则返回 false。</returns>
        public bool CheckActionExist(string actionName, string controllerId, string id)
        {
            return this.Any(a => a.Name == actionName && a.FK_ControllerID == controllerId && a.ID != id);
        }

        /// <summary>
        /// 检查指定的控制器ID下是否存在行为数据
        /// </summary>
        /// <param name="controllerId">控制器ID</param>
        /// <returns>存在行为返回 true ，否则返回 false 。</returns>
        public bool CheckActionExist(string controllerId)
        {
            return this.Any(a => a.FK_ControllerID == controllerId);
        }

        /// <summary>
        /// 检验指定ID的行为是否存在直接关联的菜单（即菜单的FK_ActionID是否为actionId）
        /// </summary>
        /// <param name="actionId">行为ID</param>
        /// <returns>存在直接关联的菜单返回 true ，否则返回 false 。</returns>
        public bool CheckDirectRelatedMenuExist(string actionId)
        {
            return new SysMenuRepository(this.UnitOfWork).Any(a => a.FK_ActionID == actionId);
        }

        /// <summary>
        /// 根据行为ID删除行为及被关联的数据
        /// </summary>
        /// <param name="id">行为ID</param>
        /// <returns>返回影响的行数</returns>
        public int DeleteActionWithRelation(string id)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append(" delete from SysAction where ID = @ID; ");
            sql.Append(@" update SysMenuPermission 
                            set ActionIDs = REPLACE(ActionIDs,@ID,'')
                            where ActionIDs != @ID and ActionIDs like @ID_0; ");

            sql.Append(@" delete SysMenuPermission where ActionIDs = @ID;");
            sql.Append(@" delete a 
                            from SysUserPermission a 
                            left join SysMenuPermission b on a.FK_PermissionID = b.ID 
                            where b.ActionIDs = @ID; ");
            sql.Append(@" delete a 
                            from SysRolePermission a 
                            left join SysMenuPermission b on a.FK_PermissionID = b.ID 
                            where b.ActionIDs = @ID; ");

            try
            {
                return this.Database.ExecuteSqlCommand(System.Data.Entity.TransactionalBehavior.EnsureTransaction, sql.ToString(), new System.Data.SqlClient.SqlParameter("@ID", id), new System.Data.SqlClient.SqlParameter("@ID_0", "%" + id + "%"));
            }
            catch (Exception e)
            {
                Log4Helper.Error(e, "数据访问层异常");
                return -1;
            }
        }
    }
}

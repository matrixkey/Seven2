﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Utilities;

namespace SF.Framework.DataAchieve.EntityFramework.Repositories.Master
{
    partial class SysLogRepository
    {
        /// <summary>
        /// 写入系统日志
        /// </summary>
        /// <param name="type">日志类型</param>
        /// <param name="content">日志信息</param>
        /// <param name="operationUserName">操作人用户名</param>
        /// <param name="operationRealName">操作人姓名</param>
        /// <param name="operationIpAddress">操作人IP</param>
        /// <param name="immediatelySave">是否立即保存，默认否</param>
        public void WriteLog(SystemLogType type, string content, string operationUserName, string operationRealName, string operationIpAddress, bool immediatelySave = false)
        {
            SysLog log = new SysLog();
            log.ID = GuidHelper.GetNewFormatGuid();
            log.Type = type;
            log.Content = content;
            log.OperationUserName = operationUserName;
            log.OperationRealName = operationRealName;
            log.OperationDateTime = DateTime.Now;
            log.OperationIpAddress = operationIpAddress;

            this.Add(log);
            if (immediatelySave)
            {
                this.UnitOfWork.Commit();
            }
        }
    }
}

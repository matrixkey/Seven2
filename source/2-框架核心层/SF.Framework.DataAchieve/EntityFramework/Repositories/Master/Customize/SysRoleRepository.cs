﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.DataAchieve.EntityFramework.Repositories.Master
{
    partial class SysRoleRepository
    {
        /// <summary>
        /// 获取指定角色ID的角色数据编辑模型
        /// </summary>
        /// <param name="id">角色ID</param>
        /// <returns>角色数据编辑模型</returns>
        public RoleFormModel GetRoleFormModel(string id)
        {
            var roleQuery = this.Query();
            
            var query = from a in roleQuery
                        where a.ID == id
                        orderby a.SortNumber
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.SortNumber
                        };

            return query.FirstOrDefault().CastTo<RoleFormModel>();
        }

        /// <summary>
        /// 获取角色列表分页数据
        /// </summary>
        /// <param name="queryModel">查询条件模型</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">每页条数</param>
        /// <returns>角色列表分页数据</returns>
        public SF.ComponentModel.Paging.PagingData GetRoleListPagerData(string queryModel, int pageIndex, int pageSize)
        {
            var roleQuery = this.Query();
            
            var query = from a in roleQuery
                        orderby a.SortNumber
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.SortNumber
                        };

            return query.ToPagingDataObject(pageIndex - 1, pageSize);
        }

        /// <summary>
        /// 获取角色列表数据，供角色选择器使用
        /// </summary>
        /// <param name="roleIds">角色ID集合</param>
        /// <param name="roleName">角色名称，模糊查询</param>
        /// <returns>角色列表数据</returns>
        public IEnumerable<RoleListModel> GetRoleSelectorModels(IEnumerable<string> roleIds, string roleName)
        {
            var roleQuery = this.Query();
            if (!roleIds.IsNullOrEmpty())
            {
                roleQuery = roleQuery.Where(w => roleIds.Contains(w.ID));
            }
            if (!string.IsNullOrWhiteSpace(roleName))
            {
                roleQuery = roleQuery.Where(w => w.Name.Contains(roleName));
            }

            var query = from a in roleQuery
                        orderby a.SortNumber
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.SortNumber
                        };

            return query.ToArray().Select(s => s.CastTo<RoleListModel>());
        }
    }
}

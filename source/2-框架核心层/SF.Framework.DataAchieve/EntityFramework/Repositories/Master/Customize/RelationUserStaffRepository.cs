﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;

namespace SF.Framework.DataAchieve.EntityFramework.Repositories.Master
{
    partial class RelationUserStaffRepository
    {
        /// <summary>
        /// 获取指定用户ID的用户员工关系实体对象
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns>用户员工关系实体对象</returns>
        public RelationUserStaff FindByUserID(string userId)
        {
            return this.Query(w => w.FK_UserID == userId).FirstOrDefault();
        }

        /// <summary>
        /// 根据员工ID检查是否存在与其关联的用户关系
        /// </summary>
        /// <param name="staffId">员工ID</param>
        /// <returns>存在关联返回 true ，否则返回 false 。</returns>
        public bool CheckRelationExistByStaffID(string staffId)
        {
            return this.Any(a => a.FK_StaffID == staffId);
        }
    }
}

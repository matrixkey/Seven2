﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.DataAchieve.EntityFramework.Repositories.Master
{
    partial class HrOrganizationRepository
    {
        /// <summary>
        /// 获取指定ID的机构编辑数据模型
        /// </summary>
        /// <param name="id">机构ID</param>
        /// <returns>机构编辑数据模型</returns>
        public OrganizationFormModel GetOrganFormModel(string id)
        {
            var organQuery = this.Query();
            var staffQuery = new HrStaffRepository(this.UnitOfWork).Query();

            var query = from a in organQuery
                        join b in organQuery on a.ParentID equals b.ID into ab
                        from ParentInfo in ab.DefaultIfEmpty()
                        join c in staffQuery on a.FK_ResponsiblePersonsStaffID equals c.ID into ac
                        from ResponsiblePersonsStaffInfo in ac.DefaultIfEmpty()
                        where a.ID == id
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.ShortName,
                            a.PrefixName,
                            a.Type,
                            a.ParentID,
                            ParentName = ParentInfo == null ? "" : ParentInfo.Name,
                            a.SortNumber,
                            a.FK_ResponsiblePersonsStaffID,
                            a.PhoneNumber,
                            a.Fax
                        };

            return query.FirstOrDefault().CastTo<OrganizationFormModel>();
        }

        /// <summary>
        /// 获取新的机构Code
        /// Code严格遵循 父级Code + 自带排序号（不足两位前面补0） 的格式
        /// 若Code已存在，则当前排序号自增1（不能超过99），重新取Code
        /// </summary>
        /// <param name="parentId">父级机构ID</param>
        /// <param name="sortNumber">自带排序号</param>
        /// <returns>新的机构Code</returns>
        public string GetNewOrganCode(string parentId, int sortNumber)
        {
            string result = ""; int resultSortNumber = 0;

            if (parentId == EntityContract.ParentIDDefaultValueWhenIDIsString)
            {
                resultSortNumber = sortNumber;
                result = resultSortNumber > 9 ? resultSortNumber.ToString() : "0" + resultSortNumber;
                while (this.OrganCodeExist(result))
                {
                    resultSortNumber = (resultSortNumber + 1) > 99 ? 1 : (resultSortNumber + 1);
                    result = resultSortNumber > 9 ? resultSortNumber.ToString() : "0" + resultSortNumber;
                }
            }
            else
            {
                var parentCode = this.GetOrganCode(parentId); resultSortNumber = sortNumber;
                result = parentCode + (resultSortNumber > 9 ? resultSortNumber.ToString() : "0" + resultSortNumber);
                while (this.OrganCodeExist(result))
                {
                    resultSortNumber = (resultSortNumber + 1) > 99 ? 1 : (resultSortNumber + 1);
                    result = parentCode + (resultSortNumber > 9 ? resultSortNumber.ToString() : "0" + resultSortNumber);
                }
            }

            return result;
        }

        /// <summary>
        /// 根据机构code获取其所有子级机构集合
        /// </summary>
        /// <param name="code">机构code</param>
        /// <returns>子级机构集合</returns>
        public IEnumerable<HrOrganization> GetChildrenOrgansByOrganCode(string code)
        {
            return this.Query(w => !w.IsDeleted && w.Code.StartsWith(code) && w.Code != code).OrderBy(o => o.SortNumber).ToArray();
        }

        /// <summary>
        /// 根据父级ID获取下一级机构模型集合
        /// </summary>
        /// <param name="parentId">父级ID</param>
        /// <returns>下一级机构模型集合</returns>
        public IEnumerable<OrganListModel> GetSonOrganModels(string parentId)
        {
            var organQuery = this.Query();

            var query = from a in organQuery
                        where a.ParentID == parentId && !a.IsDeleted
                        orderby a.Type descending, a.SortNumber
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.ShortName,
                            a.PrefixName,
                            a.Code,
                            a.SortNumber,
                            a.ParentID,
                            a.Type
                        };

            return query.ToArray().Select(s => s.CastTo<OrganListModel>());
        }

        /// <summary>
        /// 根据机构主键ID获取机构名称
        /// </summary>
        /// <param name="id">机构主键</param>
        /// <returns>机构名称</returns>
        public string GetOrganName(string id)
        {
            return this.Query(w => w.ID == id).Select(s => s.Name).FirstOrDefault();
        }

        /// <summary>
        /// 根据机构ID集合，获取他们的下一级机构的数量，以Dic形式返回数据，其中Key为ids的元素，Value为其下一级机构数量
        /// </summary>
        /// <param name="ids">机构ID集合</param>
        /// <returns>Dictionary结果，其中 Key 为 ids 的元素，Value 为其下一级机构数量。</returns>
        public IDictionary<string, int> GetSonsCount(IEnumerable<string> ids)
        {
            var result = new Dictionary<string, int>();
            if (ids.IsNullOrEmpty()) { return result; }

            var data = this.Query(w => !w.IsDeleted && ids.Contains(w.ParentID)).GroupBy(g => g.ParentID).Select(s => new { s.Key, Value = s.Count() }).ToArray();
            foreach (var item in data)
            {
                result.Add(item.Key, item.Value);
            }

            return result;
        }

        /// <summary>
        /// 检查指定ID的机构是否含有未删除的下级机构
        /// </summary>
        /// <param name="id">机构ID</param>
        /// <returns>若存在未删除的下级机构，返回 true ，否则返回 false 。</returns>
        public bool CheckChildrenExist(string id)
        {
            return this.Any(a => a.ParentID == id && !a.IsDeleted);
        }

        /// <summary>
        /// 获取集团、公司级别的组织机构列表数据模型
        /// </summary>
        /// <returns>集团、公司级别的组织机构列表数据模型</returns>
        public IEnumerable<OrganListModel> GetGroupCompanyModels()
        {
            var organQuery = this.Query(w => !w.IsDeleted && (w.Type == OrganizationType.TopGroup || w.Type == OrganizationType.ChildrenGroup || w.Type == OrganizationType.ParentCompany || w.Type == OrganizationType.ConstituentCompany));

            var query = from a in organQuery
                        orderby a.SortNumber
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.ShortName,
                            a.PrefixName,
                            a.Code,
                            a.SortNumber,
                            a.ParentID,
                            a.Type
                        };

            return query.ToArray().Select(s => s.CastTo<OrganListModel>());
        }

        /// <summary>
        /// 获取子集组织机构列表数据模型
        /// </summary>
        /// <param name="parentId">父级机构ID，只查询该机构下的子集机构</param>
        /// <returns>获取所有可用组织机构列表数据模型</returns>
        public IEnumerable<OrganListModel> GetChildrenOrganModels(string parentId)
        {
            if (!string.IsNullOrWhiteSpace(parentId))
            {
                string parentCode = this.GetOrganCode(parentId);
                var organQuery = this.Query(w => !w.IsDeleted);

                var query = from a in organQuery
                            where a.Code.StartsWith(parentCode)
                            orderby a.Type descending, a.SortNumber
                            select new
                            {
                                a.ID,
                                a.Name,
                                a.ShortName,
                                a.PrefixName,
                                a.Code,
                                a.SortNumber,
                                a.ParentID,
                                a.Type
                            };

                return query.ToArray().Select(s => s.CastTo<OrganListModel>());
            }
            else
            {
                var organQuery = this.Query(w => !w.IsDeleted);

                var query = from a in organQuery
                            orderby a.Type descending, a.SortNumber
                            select new
                            {
                                a.ID,
                                a.Name,
                                a.ShortName,
                                a.PrefixName,
                                a.Code,
                                a.SortNumber,
                                a.ParentID,
                                a.Type
                            };

                return query.ToArray().Select(s => s.CastTo<OrganListModel>());
            }
        }




        /// <summary>
        /// 根据机构主键ID获取机构code
        /// </summary>
        /// <param name="id">机构主键ID</param>
        /// <returns>机构code</returns>
        private string GetOrganCode(string id)
        {
            return this.Query(w => w.ID == id).Select(s => s.Code).FirstOrDefault();
        }

        /// <summary>
        /// 根据机构code判定该code是否已经存在
        /// </summary>
        /// <param name="code">机构code</param>
        /// <returns>存在返回true，不存在返回false。</returns>
        private bool OrganCodeExist(string code)
        {
            return this.Any(w => w.Code == code);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.DataAchieve.EntityFramework.Repositories.Master
{
    partial class BasicDataDictionaryRepository
    {
        /// <summary>
        /// 获取指定主键ID的数据字典编辑数据模型
        /// </summary>
        /// <param name="id">数据字典ID</param>
        /// <returns>数据字典编辑数据模型</returns>
        public DataDictionaryFormModel GetDataDictionaryFormModel(string id)
        {
            var dicQuery = this.Query();
            var parentQuery = this.Query();

            var query = from a in dicQuery
                        join b in parentQuery on a.ParentID equals b.ID into ab
                        from ParentInfo in ab.DefaultIfEmpty()
                        where a.ID == id
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.BusinessCode,
                            a.SortNumber,
                            a.ParentID,
                            ParentName = ParentInfo == null ? "" : ParentInfo.Name,
                            a.TypeSymbol,
                            a.IsShow,
                            a.IsSystem
                        };

            return query.FirstOrDefault().CastTo<DataDictionaryFormModel>();
        }

        /// <summary>
        /// 获取指定父级的子集数据字典模型集合
        /// </summary>
        /// <param name="parentId">父级ID</param>
        /// <returns>子集数据字典模型集合</returns>
        public IEnumerable<DataDictionaryListModel> GetChildrenDataDictionary(string parentId)
        {
            var query = this.Query(w => w.ParentID == parentId).OrderBy(o => o.SortNumber).Select(s => new { s.ID, s.Name, s.BusinessCode, s.SortNumber, s.TypeSymbol, s.IsShow, s.IsSystem });

            return query.ToArray().Select(s => s.CastTo<DataDictionaryListModel>());
        }

        /// <summary>
        /// 检查指定父级是否存在子级数据字典
        /// </summary>
        /// <param name="parentId">父级数据字典ID</param>
        /// <returns>存在子级返回 true ，否则返回 false 。</returns>
        public bool CheckChildrenExist(string parentId)
        {
            return this.Any(a => a.ParentID == parentId);
        }

        /// <summary>
        /// 检查父级数据字典的名称是否存在
        /// </summary>
        /// <param name="name">父级数据字典的名称</param>
        /// <param name="id">需要忽略的主键值。新增判定时可传空，更新判定时则需传当前数据的主键值</param>
        /// <returns>存在则返回 true ，否则返回 false 。</returns>
        public bool CheckParentNameExist(string name, string id)
        {
            if (id == null) { id = ""; }
            return this.Any(a => a.Name == name && a.ID != id);
        }

        /// <summary>
        /// 检查子级数据字典的名称是否存在
        /// </summary>
        /// <param name="name">子级数据字典的名称</param>
        /// <param name="id">需要忽略的主键值。新增判定时可传空，更新判定时则需传当前数据的主键值</param>
        /// <returns>存在则返回 true ，否则返回 false 。</returns>
        public bool CheckChildNameExist(string name, string id)
        {
            if (id == null) { id = ""; }
            string parentId = this.Query(w => w.ID == id).Select(s => s.ParentID).FirstOrDefault();
            return this.Any(a => a.Name == name && a.ParentID == parentId && a.ID != id);
        }
    }
}

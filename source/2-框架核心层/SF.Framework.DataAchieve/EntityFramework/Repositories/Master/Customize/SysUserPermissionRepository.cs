﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Utilities;

namespace SF.Framework.DataAchieve.EntityFramework.Repositories.Master
{
    partial class SysUserPermissionRepository
    {
        /// <summary>
        /// 根据用户ID和菜单ID集合获取用户权限模型集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="menuIds">菜单ID集合</param>
        /// <param name="filterUncheck">是否过滤掉“未选中”数据</param>
        /// <returns>用户权限模型集合</returns>
        public IEnumerable<UserPermissionListModel> GetUserPermissionModels(string userId, IEnumerable<string> menuIds, bool filterUncheck)
        {
            var userPermissionQuery = this.Query(w => w.FK_UserID == userId);
            if (filterUncheck) { userPermissionQuery = userPermissionQuery.Where(w => w.Check); }

            var menuPermissionQuery = new SysMenuPermissionRepository(this.UnitOfWork).Query();
            var menuQuery = new SysMenuRepository(this.UnitOfWork).Query();

            var query = (from a in userPermissionQuery
                         join b in menuPermissionQuery on a.FK_PermissionID equals b.ID into ab
                         from MenuPermissionInfo in ab.DefaultIfEmpty()
                         join c in menuQuery on MenuPermissionInfo.FK_MenuID equals c.ID into ac
                         from MenuInfo in ac.DefaultIfEmpty()
                         select new
                         {
                             a.ID,
                             UserID = a.FK_UserID,
                             a.Check,
                             a.Indeterminate,
                             MenuPermissionID = a.FK_PermissionID,
                             MenuPermissionName = MenuPermissionInfo == null ? "" : MenuPermissionInfo.Name,
                             MenuPermissionSortNumber = MenuPermissionInfo == null ? 0 : MenuPermissionInfo.SortNumber,
                             MenuID = MenuInfo == null ? "" : MenuInfo.ID,
                             MenuSortNumber = MenuInfo == null ? 0 : MenuInfo.SortNumber
                         });

            if (menuIds.Count() > 0) { query = query.Where(w => menuIds.Contains(w.MenuID)).OrderBy(o => o.MenuSortNumber).ThenBy(o => o.MenuPermissionSortNumber); }
            else { query = query.OrderBy(o => o.MenuSortNumber).ThenBy(o => o.MenuPermissionSortNumber); }

            return query.ToArray().Select(s => s.CastTo<UserPermissionListModel>(false));
        }

        /// <summary>
        /// 根据用户ID获取该用户的用户权限实体集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns>用户权限实体集合</returns>
        public IEnumerable<SysUserPermission> GetUserPermissions(string userId)
        {
            return this.Query(w => w.FK_UserID== userId).ToArray();
        }
    }
}

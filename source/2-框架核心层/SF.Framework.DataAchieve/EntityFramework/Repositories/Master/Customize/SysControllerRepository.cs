﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.DataAchieve.EntityFramework.Repositories.Master
{
    partial class SysControllerRepository
    {
        /// <summary>
        /// 获取所有控制器的数据模型集合
        /// </summary>
        /// <returns>控制器的数据模型集合</returns>
        public IEnumerable<ControllerListModel> GetAllControllerModels()
        {
            var data = this.Query().OrderBy(o => o.SortNumber).Select(s => new { s.ID, s.Name, s.AreaName, s.SortNumber, s.Description }).ToArray();

            return data.Select(s => s.CastTo<ControllerListModel>());
        }

        /// <summary>
        /// 获取指定主键的控制器的编辑数据模型
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns>控制器的编辑数据模型</returns>
        public ControllerFormModel GetControllerFormModel(string id)
        {
            var data = this.Query(w => w.ID == id).Select(s => new { s.ID, s.Name, s.AreaName, s.SortNumber, s.Description, s.LimitedAttribute }).FirstOrDefault();

            return data.CastTo<ControllerFormModel>();
        }

        /// <summary>
        /// 检查控制器名称是否已经存在
        /// </summary>
        /// <param name="name">控制器名称</param>
        /// <param name="areaName">区域名称</param>
        /// <param name="id">需要忽略的主键值。新增判定时可传空，更新判定时则需传当前数据的主键值</param>
        /// <returns>存在返回 true ，否则返回 false 。</returns>
        public bool CheckControllerNameExist(string name, string areaName, string id)
        {
            if (id == null) { id = ""; }

            return this.Any(a => a.Name == name && a.AreaName == areaName && a.ID != id);
        }
    }
}

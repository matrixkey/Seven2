﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;

namespace SF.Framework.DataAchieve.EntityFramework.Repositories.Master
{
    partial class RelationStaffPositionRepository
    {
        /// <summary>
        /// 检验指定的岗位下是否存在员工
        /// </summary>
        /// <param name="positionId">岗位ID</param>
        /// <returns>存在员工返回 true ，否则返回 false 。</returns>
        public bool CheckStaffExistInPosition(string positionId)
        {
            return this.Any(a => a.FK_PositionID == positionId);
        }

        /// <summary>
        /// 根据员工ID获取对应的岗位关系实体集合
        /// </summary>
        /// <param name="staffId">员工ID</param>
        /// <returns>岗位关系实体集合</returns>
        public IEnumerable<RelationStaffPosition> GetRelationsByStaffID(string staffId)
        {
            return this.Query(w => w.FK_StaffID == staffId).ToArray();
        }
    }
}

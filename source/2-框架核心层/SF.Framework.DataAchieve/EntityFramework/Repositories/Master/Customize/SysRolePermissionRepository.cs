﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.DataAchieve.EntityFramework.Repositories.Master
{
    partial class SysRolePermissionRepository
    {
        /// <summary>
        /// 根据角色ID和菜单ID集合获取角色权限模型集合
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <param name="menuIds">菜单ID集合</param>
        /// <param name="filterUncheck">是否过滤掉“未选中”数据</param>
        /// <returns>角色权限模型集合</returns>
        public IEnumerable<RolePermissionListModel> GetRolePermissionModels(string roleId, IEnumerable<string> menuIds, bool filterUncheck)
        {
            var rolePermissionQuery = this.Query().Where(w => w.FK_RoleID == roleId);
            if (filterUncheck) { rolePermissionQuery = rolePermissionQuery.Where(w => w.Check); }
            var menuPermissionQuery = new SysMenuPermissionRepository(this.UnitOfWork).Query();
            var menuQuery = new SysMenuRepository(this.UnitOfWork).Query();

            var query = (from a in rolePermissionQuery
                         join b in menuPermissionQuery on a.FK_PermissionID equals b.ID into ab
                         from menuPermissionInfo in ab.DefaultIfEmpty()
                         join c in menuQuery on menuPermissionInfo.FK_MenuID equals c.ID
                         select new
                         {
                             a.ID,
                             RoleID = a.FK_RoleID,
                             MenuPermissionID = a.FK_PermissionID,
                             MenuPermissionName = menuPermissionInfo.Name,
                             MenuPermissionSortNumber = menuPermissionInfo.SortNumber,
                             MenuID = c.ID,
                             MenuSortNumber = c.SortNumber,
                             a.Check
                         });

            if (menuIds.Count() > 0) { query = query.Where(w => menuIds.Contains(w.MenuID)).OrderBy(o => o.MenuSortNumber).ThenBy(o => o.MenuPermissionSortNumber); }
            else { query = query.OrderBy(o => o.MenuSortNumber).ThenBy(o => o.MenuPermissionSortNumber); }

            return query.ToArray().Select(s => s.CastTo<RolePermissionListModel>(false));
        }

        /// <summary>
        /// 根据角色ID获取该角色的角色权限实体集合
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <returns>角色权限实体集合</returns>
        public IEnumerable<SysRolePermission> GetRolePermissions(string roleId)
        {
            return this.Query(w => w.FK_RoleID == roleId).ToArray();
        }

        /// <summary>
        /// 根据用户ID获取该用户的主角色的角色菜单权限集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        public IEnumerable<SysRolePermission> GetRolePermissionsByUserID(string userId)
        {
            var rolePermissionQuery = this.Query();
            var relationQuery = new RelationUserRoleRepository(this.UnitOfWork).Query(w => w.IsMaster);

            var data = (from a in rolePermissionQuery
                        join b in relationQuery on a.FK_RoleID equals b.FK_RoleID
                        where b.FK_UserID == userId
                        select a).ToArray();

            return data;
        }

        /// <summary>
        /// 根据用户ID获取该用户的主角色的有效菜单权限集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        public IEnumerable<SysMenuPermission> GetUseableMenuPermissions(string userId)
        {
            var rolePermissionQuery = this.Query();
            var relationQuery = new RelationUserRoleRepository(this.UnitOfWork).Query(w => w.IsMaster);
            var userQuery = new SysUserRepository(this.UnitOfWork).Query();

            var rolePermissionIDs = (from a in rolePermissionQuery
                                     join b in relationQuery on a.FK_RoleID equals b.FK_RoleID
                                     where b.FK_UserID == userId && a.Check
                                     select a.FK_PermissionID);

            return new SysMenuPermissionRepository(this.UnitOfWork).Query(w => rolePermissionIDs.Contains(w.ID)).ToArray();
        }
    }
}

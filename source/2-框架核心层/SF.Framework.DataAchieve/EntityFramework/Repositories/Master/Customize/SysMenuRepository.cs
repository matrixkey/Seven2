﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.DataAchieve.EntityFramework.Repositories.Master
{
    partial class SysMenuRepository
    {
        /// <summary>
        /// 获取所有菜单实体集合
        /// </summary>
        /// <returns>所有菜单实体集合</returns>
        public IEnumerable<SysMenu> GetAllMenus()
        {
            return this.Query().OrderBy(o => o.SortNumber).ToArray();
        }

        /// <summary>
        /// 获取所有根目录菜单实体集合
        /// </summary>
        /// <returns>根目录菜单实体集合</returns>
        public IEnumerable<SysMenu> GetRootMenus()
        {
            return this.Query(w => w.ParentID == EntityContract.ParentIDDefaultValueWhenIDIsString && w.IsShow).OrderBy(o => o.SortNumber).ToArray();
        }

        /// <summary>
        /// 获取指定菜单ID的菜单编辑数据模型
        /// </summary>
        /// <param name="id">菜单ID</param>
        /// <returns>菜单编辑数据模型</returns>
        public MenuFormModel GetMenuFormModel(string id)
        {
            var menuQuery = this.Query();
            var actionQuery = new SysActionRepository(this.UnitOfWork).Query();
            var controllerQuery = new SysControllerRepository(this.UnitOfWork).Query();

            var query = from a in menuQuery
                        join b in actionQuery on a.FK_ActionID equals b.ID into ab
                        from ActionInfo in ab.DefaultIfEmpty()
                        join c in controllerQuery on ActionInfo.FK_ControllerID equals c.ID into bc
                        from ControllerInfo in bc.DefaultIfEmpty()
                        where a.ID == id
                        orderby a.SortNumber
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.Code,
                            a.SortNumber,
                            a.IconClass,
                            a.ParentID,
                            a.FK_ActionID,
                            ControllerName = ControllerInfo == null ? "" : ControllerInfo.Name,
                            ActionName = ActionInfo == null ? "" : ActionInfo.Name,
                            a.IsShow
                        };

            return query.FirstOrDefault().CastTo<MenuFormModel>();
        }

        /// <summary>
        /// 根据菜单主键ID获取菜单名称
        /// </summary>
        /// <param name="id">菜单主键</param>
        /// <returns>菜单名称</returns>
        public string GetMenuName(string id)
        {
            return this.Query(w => w.ID == id).Select(s => s.Name).FirstOrDefault();
        }

        /// <summary>
        /// 获取新的菜单Code
        /// Code严格遵循 父级Code + 自带排序号（不足两位前面补0） 的格式
        /// 若Code已存在，则当前排序号自增1（不能超过99），重新取Code
        /// </summary>
        /// <param name="parentId">父级菜单ID</param>
        /// <param name="sortNumber">自带排序号</param>
        /// <returns>新的菜单Code</returns>
        public string GetNewMenuCode(string parentId, int sortNumber)
        {
            string result = ""; int resultSortNumber = 0;

            if (parentId == EntityContract.ParentIDDefaultValueWhenIDIsString)
            {
                resultSortNumber = sortNumber;
                result = resultSortNumber > 9 ? resultSortNumber.ToString() : "0" + resultSortNumber;
                while (this.MenuCodeExist(result))
                {
                    resultSortNumber = (resultSortNumber + 1) > 99 ? 1 : (resultSortNumber + 1);
                    result = resultSortNumber > 9 ? resultSortNumber.ToString() : "0" + resultSortNumber;
                }
            }
            else
            {
                var parentCode = this.GetMenuCode(parentId); resultSortNumber = sortNumber;
                result = parentCode + (resultSortNumber > 9 ? resultSortNumber.ToString() : "0" + resultSortNumber);
                while (this.MenuCodeExist(result))
                {
                    resultSortNumber = (resultSortNumber + 1) > 99 ? 1 : (resultSortNumber + 1);
                    result = parentCode + (resultSortNumber > 9 ? resultSortNumber.ToString() : "0" + resultSortNumber);
                }
            }

            return result;
        }

        /// <summary>
        /// 根据菜单code获取其所有子级菜单集合
        /// </summary>
        /// <param name="menuCode">菜单code</param>
        /// <returns>子级菜单集合</returns>
        public IEnumerable<SysMenu> GetChildrenMenusByMenuCode(string menuCode)
        {
            return this.Query(w => w.Code.StartsWith(menuCode) && w.Code != menuCode).OrderBy(o => o.SortNumber).ToArray();
        }

        /// <summary>
        /// 根据父级ID获取下一级菜单模型集合
        /// </summary>
        /// <param name="parentId">父级ID</param>
        /// <returns>下一级菜单模型集合</returns>
        public IEnumerable<MenuListModel> GetSonMenuModels(string parentId)
        {
            var menuQuery = this.Query();
            var actionQuery = new SysActionRepository(this.UnitOfWork).Query();
            var controllerQuery = new SysControllerRepository(this.UnitOfWork).Query();

            var query = from a in menuQuery
                        join b in actionQuery on a.FK_ActionID equals b.ID into ab
                        from ActionInfo in ab.DefaultIfEmpty()
                        join c in controllerQuery on ActionInfo.FK_ControllerID equals c.ID into bc
                        from ControllerInfo in bc.DefaultIfEmpty()
                        where a.ParentID == parentId
                        orderby a.SortNumber
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.Code,
                            a.SortNumber,
                            a.IconClass,
                            a.ParentID,
                            ControllerName = ControllerInfo == null ? "" : ControllerInfo.Name,
                            ControllerAreaName = ControllerInfo == null ? "" : ControllerInfo.AreaName,
                            ActionName = ActionInfo == null ? "" : ActionInfo.Name,
                            a.IsShow
                        };

            return query.ToArray().Select(s => s.CastTo<MenuListModel>());
        }

        /// <summary>
        /// 根据父级code获取子级菜单模型集合
        /// </summary>
        /// <param name="menuCode">父级code</param>
        /// <returns>子菜单模型集合</returns>
        public IEnumerable<MenuListModel> GetChildrenMenuModels(string menuCode)
        {
            var menuQuery = this.Query();
            var actionQuery = new SysActionRepository(this.UnitOfWork).Query();
            var controllerQuery = new SysControllerRepository(this.UnitOfWork).Query();

            var query = from a in menuQuery
                        join b in actionQuery on a.FK_ActionID equals b.ID into ab
                        from ActionInfo in ab.DefaultIfEmpty()
                        join c in controllerQuery on ActionInfo.FK_ControllerID equals c.ID into bc
                        from ControllerInfo in bc.DefaultIfEmpty()
                        where a.Code.StartsWith(menuCode) && a.Code != menuCode
                        orderby a.SortNumber
                        select new
                        {
                            a.ID,
                            a.Name,
                            a.Code,
                            a.SortNumber,
                            a.IconClass,
                            a.ParentID,
                            ControllerName = ControllerInfo == null ? "" : ControllerInfo.Name,
                            ControllerAreaName = ControllerInfo == null ? "" : ControllerInfo.AreaName,
                            ActionName = ActionInfo == null ? "" : ActionInfo.Name,
                            a.IsShow
                        };

            return query.ToArray().Select(s => s.CastTo<MenuListModel>());
        }

        /// <summary>
        /// 根据菜单ID集合，获取他们的下一级菜单的数量，以Dic形式返回数据，其中Key为ids的元素，Value为其下一级菜单数量
        /// </summary>
        /// <param name="ids">菜单ID集合</param>
        /// <returns>Dictionary结果，其中 Key 为 ids 的元素，Value 为其下一级菜单数量。</returns>
        public IDictionary<string, int> GetSonsCount(IEnumerable<string> ids)
        {
            var result = new Dictionary<string, int>();
            if (ids.IsNullOrEmpty()) { return result; }

            var data = this.Query(w => ids.Contains(w.ParentID)).GroupBy(g => g.ParentID).Select(s => new { s.Key, Value = s.Count() }).ToArray();
            foreach (var item in data)
            {
                result.Add(item.Key, item.Value);
            }

            return result;
        }

        /// <summary>
        /// 检验下级菜单是否存在
        /// </summary>
        /// <param name="parentId">父级菜单ID</param>
        /// <returns>存在下级菜单返回 true ，否则返回 false 。</returns>
        public bool CheckSonMenuExist(string parentId)
        {
            return this.Any(a => a.ParentID == parentId);
        }

        /// <summary>
        /// 根据菜单ID删除菜单及被关联的数据
        /// </summary>
        /// <param name="id">菜单ID</param>
        /// <returns>返回影响的行数</returns>
        public int DeleteMenuWithRelation(string id)
        {
            StringBuilder sql = new StringBuilder();
            sql.Append(" delete from SysMenu where ID = @ID; ");
            sql.Append(" delete from SysMenuPermission where FK_MenuID = @ID; ");
            sql.Append(@" delete a 
                            from SysUserPermission a 
                            left join SysMenuPermission b on a.FK_PermissionID = b.ID 
                            where b.FK_MenuID = @ID; ");
            sql.Append(@" delete a 
                            from SysRolePermission a 
                            left join SysMenuPermission b on a.FK_PermissionID = b.ID 
                            where b.FK_MenuID = @ID; ");

            try
            {
                return this.Database.ExecuteSqlCommand(System.Data.Entity.TransactionalBehavior.EnsureTransaction, sql.ToString(), new System.Data.SqlClient.SqlParameter("@ID", id));
            }
            catch (Exception e)
            {
                Log4Helper.Error(e, "数据访问层异常");
                return -1;
            }
        }








        /// <summary>
        /// 根据菜单主键ID获取菜单code
        /// </summary>
        /// <param name="id">菜单主键ID</param>
        /// <returns>菜单code</returns>
        private string GetMenuCode(string id)
        {
            return this.Query(w => w.ID == id).Select(s => s.Code).FirstOrDefault();
        }

        /// <summary>
        /// 根据菜单code判定该code是否已经存在
        /// </summary>
        /// <param name="menuCode">菜单code</param>
        /// <returns>存在返回true，不存在返回false。</returns>
        private bool MenuCodeExist(string menuCode)
        {
            return this.Any(w => w.Code == menuCode);
        }
    }
}

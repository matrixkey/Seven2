﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.PermissionCache;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.DataAchieve.EntityFramework.Repositories.Master
{
    partial class SysUserRepository
    {
        /// <summary>
        /// 获取指定用户ID的用户数据编辑模型
        /// </summary>
        /// <param name="id">用户ID</param>
        /// <returns>用户数据编辑模型</returns>
        public UserFormModel GetUserFormModel(string id)
        {
            var userQuery = this.Query();

            var query = from a in userQuery
                        where a.ID == id
                        select new
                        {
                            a.ID,
                            a.UserName
                        };

            return query.FirstOrDefault().CastTo<UserFormModel>();
        }

        /// <summary>
        /// 获取用户列表分页数据
        /// </summary>
        /// <param name="queryModel">序列化的查询条件</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">每页条数</param>
        /// <returns>用户列表分页数据</returns>
        public SF.ComponentModel.Paging.PagingData GetUserListPagerData(string queryModel, int pageIndex, int pageSize)
        {
            var userQuery = this.Query(queryModel);

            var query = from a in userQuery
                        orderby a.UserName
                        select new
                        {
                            a.ID,
                            a.UserName
                        };

            return query.ToPagingDataObject(pageIndex - 1, pageSize);
        }

        /// <summary>
        /// 检查用户名是否存在
        /// </summary>
        /// <param name="userName">要检查的用户名</param>
        /// <param name="id">需要忽略的主键值。新增判定时可传空，更新判定时则需传当前数据的主键值</param>
        /// <returns>一个 bool 值，已存在则返回 true ，不存在则返回 false 。</returns>
        public bool CheckUserNameExist(string userName, string id)
        {
            return this.Any(w => w.UserName == userName && w.ID != id);
        }

        /// <summary>
        /// 获取指定账号的账号数据模型
        /// </summary>
        /// <param name="account">账号</param>
        /// <returns>账号数据模型</returns>
        public UserAccountModel GetAccountModelByAccount(string account)
        {
            var userQuery = this.Query();

            var query = from a in userQuery
                        where a.UserName == account
                        select new
                        {
                            UserEntity = a
                        };

            return query.FirstOrDefault().CastTo<UserAccountModel>();
        }
    }
}

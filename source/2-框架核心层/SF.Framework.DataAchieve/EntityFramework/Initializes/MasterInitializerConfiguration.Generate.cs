﻿// <autogenerated>
//   This file was generated by T4 code generator MultiTemplate.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataAchieve.EntityFramework.Initializes.Master
{
    /// <summary>
    /// Master 数据库迁移策略
    /// </summary>
    internal sealed class MigrateConfiguration : SF.Data.Entity.Migrations.MigrationConfiguration<SF.Framework.DataCore.EntityFramework.EntityContexts.MasterEntityContext>
    {
        protected override void Seed(SF.Framework.DataCore.EntityFramework.EntityContexts.MasterEntityContext context)
        { }
    }


    /// <summary>
    /// Master 数据库初始化操作类
    /// </summary>
    public class DatabaseInitializer : SF.Framework.DataInterface.IInitialize.IDatabaseInitializer
    {
        /// <summary>
        /// 设置数据库初始化策略
        /// </summary>
        /// <param name="migrate">是否合并（自动迁移）。若是，则会检查数据库是否存在，若不存在则创建，若存在则进行自动迁移。若否，则不进行初始化操作（这样能避开EF访问sys.databases检测数据库是否存在，项目稳定后可将参数设置为false）。该参数仅在数据驱动值为 EntityFramework 时有效。</param>
        public void Initialize(bool migrate)
        {
            if (!migrate)
            {
                System.Data.Entity.Database.SetInitializer<SF.Framework.DataCore.EntityFramework.EntityContexts.MasterEntityContext>(null);
            }
            else
            {
                //测试版本，既调用SF.Data.Entity.Migrations.MigrationDatabaseIfModelChanges，也调用当前类文件中的MigrateConfiguration
                System.Data.Entity.Database.SetInitializer<SF.Framework.DataCore.EntityFramework.EntityContexts.MasterEntityContext>(new SF.Data.Entity.Migrations.MigrationDatabaseIfModelChanges<SF.Framework.DataCore.EntityFramework.EntityContexts.MasterEntityContext, MigrateConfiguration>(true));
            }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.DataInterface;
using SF.Framework.DataInterface.IRepositoryFactories;

namespace SF.Framework.DataAchieve.EntityFramework
{
    public partial class UnitOfWork : Disposable, IUnitOfWork
    {
        /// <summary>
        /// 仓储工厂缓存
        /// </summary>
        private Dictionary<Type, object> repositoryFactoryCache = new Dictionary<Type, object>();

        /// <summary>
        /// 获取仓储工厂
        /// </summary>
        /// <typeparam name="R">仓储工厂接口</typeparam>
        /// <returns>仓储工厂实例</returns>
        private R GetRepositoryFactoryByInstance<R>()
            where R : IBaseRepositoryFactory, new()
        {
            Type t = typeof(R);
            if (!repositoryFactoryCache.ContainsKey(t))
            {
                var repositoryFactory = new R();
                repositoryFactory.SetUnitOfWork(this);

                repositoryFactoryCache.Add(t, repositoryFactory);

                return repositoryFactory;
            }
            else { return (R)repositoryFactoryCache[t]; }
        }
    }
}

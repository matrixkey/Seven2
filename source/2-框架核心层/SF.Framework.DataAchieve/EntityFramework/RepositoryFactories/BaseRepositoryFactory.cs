﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.DataInterface;
using SF.Framework.DataInterface.IRepositoryFactories;
using SF.Utilities;

namespace SF.Framework.DataAchieve.EntityFramework.RepositoryFactories
{
    public abstract class BaseRepositoryFactory : IBaseRepositoryFactory
    {
        /// <summary>
        /// 当前仓储工厂所在的工作单元对象
        /// </summary>
        protected UnitOfWork UnitOfWork { get; set; }

        /// <summary>
        /// 设置工作单元对象
        /// <param name="unit">工作单元对象</param>
        /// </summary>
        public void SetUnitOfWork(object unit)
        {
            if (unit is UnitOfWork) { this.UnitOfWork = unit as UnitOfWork; }
            else
            {
                throw ExceptionHelper.ThrowDataAccessException("给仓储工厂设置工作单元时发生异常，参数 unit 不是一个工作单元对象。");
            }
        }

        /// <summary>
        /// 仓储缓存
        /// </summary>
        private Dictionary<Type, object> repositoryCache = new Dictionary<Type, object>();

        /// <summary>
        /// 获取仓储
        /// </summary>
        /// <typeparam name="TEntity">实体类型</typeparam>
        /// <typeparam name="R">仓储接口</typeparam>
        /// <param name="db">数据库上下文</param>
        /// <returns>仓储实例</returns>
        protected R GetRepositoryByInstance<TEntity, R>(System.Data.Entity.DbContext db)
            where TEntity : EntityModelBaseForReflection
            where R : class, IBaseRepository<TEntity>, new()
        {
            if (!repositoryCache.ContainsKey(typeof(TEntity)))
            {
                var repository = new R();
                repository.SetUnitOfWork(this.UnitOfWork);
                repository.SetDataContext(db);

                repositoryCache.Add(typeof(TEntity), repository);

                return repository;
            }
            else { return (R)repositoryCache[typeof(TEntity)]; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataInterface.IRepositoryFactories
{
    public interface IBaseRepositoryFactory
    {
        /// <summary>
        /// 设置工作单元对象。本方法设置了编辑器不可见，但只有跨解决方案时才有用。当前解决方案下，还是会被智能提示捕捉到。
        /// <param name="unit">工作单元对象</param>
        /// </summary>
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        void SetUnitOfWork(object unit);
    }
}

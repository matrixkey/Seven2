﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataInterface
{
    /// <summary>
    /// 工作单元接口
    /// </summary>
    public partial interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// 开始数据事务
        /// </summary>
        void BeginTransaction();

        /// <summary>
        /// 提交工作单元
        /// </summary>
        /// <returns>受影响行数</returns>
        int Commit();

        /// <summary>
        /// 执行回滚事务
        /// </summary>
        void Rollback();
    }
}

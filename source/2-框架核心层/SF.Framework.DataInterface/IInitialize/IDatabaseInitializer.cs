﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataInterface.IInitialize
{
    public interface IDatabaseInitializer
    {
        /// <summary>
        /// 设置数据库初始化策略
        /// </summary>
        /// <param name="migrate">是否合并（自动迁移）。若是，则会检查数据库是否存在，若不存在则创建，若存在则进行自动迁移。若否，则不进行初始化操作（这样能避开EF访问sys.databases检测数据库是否存在，项目稳定后可将参数设置为false。）。该参数仅在数据驱动值为 EntityFramework 时有效。</param>
        void Initialize(bool migrate);
    }
}

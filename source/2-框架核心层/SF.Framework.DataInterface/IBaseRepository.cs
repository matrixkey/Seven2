﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataInterface
{
    /// <summary>
    /// 仓储基本接口
    /// </summary>
    /// <typeparam name="TEntity">实体类型</typeparam>
    public interface IBaseRepository<TEntity> where TEntity : EntityModelBaseForReflection
    {
        /// <summary>
        /// 设置工作单元对象。设置了编辑器不可见，但只有跨解决方案时才有用。当前解决方案下，还是会被智能提示捕捉到。
        /// <param name="unit">工作单元对象</param>
        /// </summary>
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        void SetUnitOfWork(object unit);

        /// <summary>
        /// 设置数据库上下文对象。设置了编辑器不可见，但只有跨解决方案时才有用。当前解决方案下，还是会被智能提示捕捉到。
        /// <param name="db">数据库上下文对象</param>
        /// </summary>
        [System.ComponentModel.EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        void SetDataContext(object db);

        #region 数据查询

        /// <summary>
        /// 主键查询
        /// </summary>
        /// <param name="keyValues">键值</param>
        /// <returns>实体</returns>
        TEntity Find(IEnumerable<object> keyValues);

        /// <summary>
        /// 主键查询
        /// </summary>
        /// <param name="keyValues">键值</param>
        /// <returns>实体</returns>
        TEntity Find(params object[] keyValues);

        #endregion

        #region 数据操作

        #region 添加

        /// <summary>
        /// 添加实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Add(TEntity entity);

        /// <summary>
        /// 批量添加实体
        /// </summary>
        /// <param name="entities">实体集合</param>
        void AddRange(IEnumerable<TEntity> entities);

        #endregion

        #region 更改

        /// <summary>
        /// 更改实体
        /// </summary>
        /// <param name="entity">实体对象</param>
        void Update(TEntity entity);

        /// <summary>
        /// 更改实体，可指定要更新的属性
        /// </summary>
        /// <param name="entity">实体对象</param>
        /// <param name="properties">要更新的属性名称集合</param>
        void Update(TEntity entity, IEnumerable<string> properties);

        /// <summary>
        /// 批量更改实体
        /// </summary>
        /// <param name="entities">实体集合</param>
        void UpdateRange(IEnumerable<TEntity> entities);

        /// <summary>
        /// 批量更改实体，可指定要更新的属性
        /// </summary>
        /// <param name="entities">实体集合</param>
        /// <param name="properties">要更新的属性名称集合</param>
        void UpdateRange(IEnumerable<TEntity> entities, IEnumerable<string> properties);

        #endregion

        #region 删除

        /// <summary>
        /// 主键删除实体
        /// </summary>
        /// <param name="key">键值</param>
        void Delete(object key);

        /// <summary>
        /// 删除实体
        /// </summary>
        /// <param name="entity">实体</param>
        void Delete(TEntity entity);

        /// <summary>
        /// 批量删除实体
        /// </summary>
        /// <param name="entities">实体集合</param>
        void DeleteRange(IEnumerable<TEntity> entities);

        /// <summary>
        /// 根据条件删除实体
        /// </summary>
        /// <param name="predicate">查询条件</param>
        void DeleteRange(Expression<Func<TEntity, bool>> predicate);

        /// <summary>
        /// 根据条件删除实体
        /// </summary>
        /// <param name="predicate">查询条件</param>
        void DeleteRange(Expression<Func<TEntity, int, bool>> predicate);

        #endregion

        #endregion
    }
}

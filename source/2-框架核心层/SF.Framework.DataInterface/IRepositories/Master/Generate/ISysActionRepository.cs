﻿// <autogenerated>
//   This file was generated by T4 code generator MultiTemplate.tt.
//   Any changes made to this file manually will be lost next time the file is regenerated.
// </autogenerated>


using System;

using SF.Framework.Entity.Master;

namespace SF.Framework.DataInterface.IRepositories.Master
{
	/// <summary>
    /// 仓储接口
    /// </summary>
    public partial interface ISysActionRepository : IBaseRepository<SysAction>
    {
        
    }
}

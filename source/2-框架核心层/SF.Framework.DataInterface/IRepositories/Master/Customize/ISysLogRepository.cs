﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.DataInterface.IRepositories.Master
{
    partial interface ISysLogRepository
    {
        /// <summary>
        /// 写入系统日志
        /// </summary>
        /// <param name="type">日志类型</param>
        /// <param name="content">日志信息</param>
        /// <param name="operationUserName">操作人用户名</param>
        /// <param name="operationRealName">操作人姓名</param>
        /// <param name="operationIpAddress">操作人IP</param>
        /// <param name="immediatelySave">是否立即保存，默认否</param>
        void WriteLog(SystemLogType type, string content, string operationUserName, string operationRealName, string operationIpAddress, bool immediatelySave = false);
    }
}

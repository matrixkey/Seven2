﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;

namespace SF.Framework.DataInterface.IRepositories.Master
{
    partial interface IHrStaffRepository
    {
        /// <summary>
        /// 获取指定id的员工的编辑数据模型
        /// </summary>
        /// <param name="id">员工ID</param>
        /// <returns>员工的编辑数据模型</returns>
        StaffFormModel GetStaffFormModel(string id);

        /// <summary>
        /// 获取员工列表分页数据
        /// </summary>
        /// <param name="staffIds">员工ID集合</param>
        /// <param name="realName">员工名称，模糊查询</param>
        /// <param name="organId">机构ID</param>
        /// <param name="queryModel">序列化的查询条件</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">每页条数</param>
        /// <returns>员工列表分页数据</returns>
        SF.ComponentModel.Paging.PagingData GetStaffListPagerData(IEnumerable<string> staffIds, string realName, string organId, string queryModel, int pageIndex, int pageSize);
    }
}

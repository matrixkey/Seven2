﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.PermissionCache;
using SF.Framework.DataModel.Views;

namespace SF.Framework.DataInterface.IRepositories.Master
{
    partial interface ISysUserRepository
    {
        /// <summary>
        /// 获取指定用户ID的用户数据编辑模型
        /// </summary>
        /// <param name="id">用户ID</param>
        /// <returns>用户数据编辑模型</returns>
        UserFormModel GetUserFormModel(string id);

        /// <summary>
        /// 获取用户列表分页数据
        /// </summary>
        /// <param name="queryModel">查询条件模型</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">每页条数</param>
        /// <returns>用户列表分页数据</returns>
        SF.ComponentModel.Paging.PagingData GetUserListPagerData(string queryModel, int pageIndex, int pageSize);

        /// <summary>
        /// 检查用户名是否存在
        /// </summary>
        /// <param name="userName">要检查的用户名</param>
        /// <param name="id">需要忽略的主键值。新增判定时可传空，更新判定时则需传当前数据的主键值</param>
        /// <returns>一个 bool 值，已存在则返回 true ，不存在则返回 false 。</returns>
        bool CheckUserNameExist(string userName, string id);

        /// <summary>
        /// 获取指定账号的账号数据模型
        /// </summary>
        /// <param name="account">账号</param>
        /// <returns>账号数据模型</returns>
        UserAccountModel GetAccountModelByAccount(string account);
    }
}

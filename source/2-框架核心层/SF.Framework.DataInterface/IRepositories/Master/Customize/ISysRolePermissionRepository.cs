﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;

namespace SF.Framework.DataInterface.IRepositories.Master
{
    partial interface ISysRolePermissionRepository
    {
        /// <summary>
        /// 根据角色ID和菜单ID集合获取角色权限模型集合
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <param name="menuIds">菜单ID集合</param>
        /// <param name="filterUncheck">是否过滤掉“未选中”数据</param>
        /// <returns>角色权限模型集合</returns>
        IEnumerable<RolePermissionListModel> GetRolePermissionModels(string roleId, IEnumerable<string> menuIds, bool filterUncheck);

        /// <summary>
        /// 根据角色ID获取该角色的角色权限实体集合
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <returns>角色权限实体集合</returns>
        IEnumerable<SysRolePermission> GetRolePermissions(string roleId);

        /// <summary>
        /// 根据用户ID获取该用户的主角色的角色菜单权限集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        IEnumerable<SysRolePermission> GetRolePermissionsByUserID(string userId);

        /// <summary>
        /// 根据用户ID获取该用户的主角色的有效菜单权限集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        IEnumerable<SysMenuPermission> GetUseableMenuPermissions(string userId);
    }
}

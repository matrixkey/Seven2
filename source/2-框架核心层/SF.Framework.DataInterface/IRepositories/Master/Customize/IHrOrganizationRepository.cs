﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;

namespace SF.Framework.DataInterface.IRepositories.Master
{
    partial interface IHrOrganizationRepository
    {
        /// <summary>
        /// 获取指定ID的机构编辑数据模型
        /// </summary>
        /// <param name="id">机构ID</param>
        /// <returns>机构编辑数据模型</returns>
        OrganizationFormModel GetOrganFormModel(string id);

        /// <summary>
        /// 获取新的机构Code
        /// Code严格遵循 父级Code + 自带排序号（不足两位前面补0） 的格式
        /// 若Code已存在，则当前排序号自增1（不能超过99），重新取Code
        /// </summary>
        /// <param name="parentId">父级机构ID</param>
        /// <param name="sortNumber">自带排序号</param>
        /// <returns>新的机构Code</returns>
        string GetNewOrganCode(string parentId, int sortNumber);

        /// <summary>
        /// 根据机构code获取其所有子级机构集合
        /// </summary>
        /// <param name="code">机构code</param>
        /// <returns>子级机构集合</returns>
        IEnumerable<HrOrganization> GetChildrenOrgansByOrganCode(string code);

        /// <summary>
        /// 根据父级ID获取下一级机构模型集合
        /// </summary>
        /// <param name="parentId">父级ID</param>
        /// <returns>下一级机构模型集合</returns>
        IEnumerable<OrganListModel> GetSonOrganModels(string parentId);

        /// <summary>
        /// 根据机构主键ID获取机构名称
        /// </summary>
        /// <param name="id">机构主键</param>
        /// <returns>机构名称</returns>
        string GetOrganName(string id);

        /// <summary>
        /// 根据机构ID集合，获取他们的下一级机构的数量，以Dic形式返回数据，其中Key为ids的元素，Value为其下一级机构数量
        /// </summary>
        /// <param name="ids">机构ID集合</param>
        /// <returns>Dictionary结果，其中 Key 为 ids 的元素，Value 为其下一级机构数量。</returns>
        IDictionary<string, int> GetSonsCount(IEnumerable<string> ids);

        /// <summary>
        /// 检查指定ID的机构是否含有未删除的下级机构
        /// </summary>
        /// <param name="id">机构ID</param>
        /// <returns>若存在未删除的下级机构，返回 true ，否则返回 false 。</returns>
        bool CheckChildrenExist(string id);

        /// <summary>
        /// 获取集团、公司级别的组织机构列表数据模型
        /// </summary>
        /// <returns>集团、公司级别的组织机构列表数据模型</returns>
        IEnumerable<OrganListModel> GetGroupCompanyModels();

        /// <summary>
        /// 获取子集组织机构列表数据模型
        /// </summary>
        /// <param name="parentId">父级机构ID，只查询该机构下的子集机构</param>
        /// <returns>获取所有可用组织机构列表数据模型</returns>
        IEnumerable<OrganListModel> GetChildrenOrganModels(string parentId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;

namespace SF.Framework.DataInterface.IRepositories.Master
{
    partial interface IHrPositionRepository
    {
        /// <summary>
        /// 获取指定ID的岗位编辑数据模型
        /// </summary>
        /// <param name="id">岗位ID</param>
        /// <returns>岗位编辑数据模型</returns>
        PositionFormModel GetPositionFormModel(string id);

        /// <summary>
        /// 根据机构ID获取该机构下的岗位数据模型集合
        /// </summary>
        /// <param name="organId">机构ID</param>
        /// <returns>该机构下的岗位数据模型集合</returns>
        IEnumerable<PositionListModel> GetPositionModels(string organId);

        /// <summary>
        /// 获取岗位列表数据，供岗位选择器使用，带岗位的完整所属信息
        /// </summary>
        /// <param name="positionIds">岗位ID集合</param>
        /// <param name="positionName">岗位名称，模糊查询</param>
        /// <param name="organId">机构ID</param>
        /// <returns></returns>
        IEnumerable<PositionSelectorListModel> GetPositionSelectorModels(IEnumerable<string> positionIds, string positionName, string organId);

        /// <summary>
        /// 检查指定机构ID下是否存在岗位
        /// </summary>
        /// <param name="organId">机构ID</param>
        /// <returns>若存在岗位，返回 true ，否则返回 false 。</returns>
        bool CheckPositionExistInOrgan(string organId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;

namespace SF.Framework.DataInterface.IRepositories.Master
{
    partial interface ISysUserPermissionRepository
    {
        /// <summary>
        /// 根据用户ID和菜单ID集合获取用户权限模型集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="menuIds">菜单ID集合</param>
        /// <param name="filterUncheck">是否过滤掉“未选中”数据</param>
        /// <returns>用户权限模型集合</returns>
        IEnumerable<UserPermissionListModel> GetUserPermissionModels(string userId, IEnumerable<string> menuIds, bool filterUncheck);

        /// <summary>
        /// 根据用户ID获取该用户的用户权限实体集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns>用户菜单权限实体集合</returns>
        IEnumerable<SysUserPermission> GetUserPermissions(string userId);
    }
}

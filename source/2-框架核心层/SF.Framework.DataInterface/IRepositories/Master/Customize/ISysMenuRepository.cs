﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;

namespace SF.Framework.DataInterface.IRepositories.Master
{
    partial interface ISysMenuRepository
    {
        /// <summary>
        /// 获取所有菜单实体集合
        /// </summary>
        /// <returns>所有菜单实体集合</returns>
        IEnumerable<SysMenu> GetAllMenus();

        /// <summary>
        /// 获取所有根目录菜单实体集合
        /// </summary>
        /// <returns>根目录菜单实体集合</returns>
        IEnumerable<SysMenu> GetRootMenus();

        /// <summary>
        /// 获取指定菜单ID的菜单编辑数据模型
        /// </summary>
        /// <param name="id">菜单ID</param>
        /// <returns>菜单编辑数据模型</returns>
        MenuFormModel GetMenuFormModel(string id);

        /// <summary>
        /// 根据菜单主键ID获取菜单名称
        /// </summary>
        /// <param name="id">菜单主键</param>
        /// <returns>菜单名称</returns>
        string GetMenuName(string id);

        /// <summary>
        /// 获取新的菜单Code
        /// Code严格遵循 父级Code + 自带排序号（不足两位前面补0） 的格式
        /// 若Code已存在，则当前排序号自增1（不能超过99），重新取Code
        /// </summary>
        /// <param name="parentId">父级菜单ID</param>
        /// <param name="sortNumber">自带排序号</param>
        /// <returns>新的菜单Code</returns>
        string GetNewMenuCode(string parentId, int sortNumber);

        /// <summary>
        /// 根据菜单code获取其所有子级菜单集合
        /// </summary>
        /// <param name="menuCode">菜单code</param>
        /// <returns>子级菜单集合</returns>
        IEnumerable<SysMenu> GetChildrenMenusByMenuCode(string menuCode);

        /// <summary>
        /// 根据父级ID获取下一级菜单模型集合
        /// </summary>
        /// <param name="parentId">父级ID</param>
        /// <returns>下一级菜单模型集合</returns>
        IEnumerable<MenuListModel> GetSonMenuModels(string parentId);

        /// <summary>
        /// 根据父级code获取子级菜单模型集合
        /// </summary>
        /// <param name="menuCode">父级code</param>
        /// <returns>子菜单模型集合</returns>
        IEnumerable<MenuListModel> GetChildrenMenuModels(string menuCode);

        /// <summary>
        /// 根据菜单ID集合，获取他们的下一级菜单的数量，以Dic形式返回数据，其中Key为ids的元素，Value为其下一级菜单数量
        /// </summary>
        /// <param name="ids">菜单ID集合</param>
        /// <returns>Dictionary结果，其中 Key 为 ids 的元素，Value 为其下一级菜单数量。</returns>
        IDictionary<string, int> GetSonsCount(IEnumerable<string> ids);

        /// <summary>
        /// 检验下级菜单是否存在
        /// </summary>
        /// <param name="parentId">父级菜单ID</param>
        /// <returns>存在下级菜单返回 true ，否则返回 false 。</returns>
        bool CheckSonMenuExist(string parentId);

        /// <summary>
        /// 根据菜单ID删除菜单及被关联的数据
        /// </summary>
        /// <param name="id">菜单ID</param>
        /// <returns>返回影响的行数</returns>
        int DeleteMenuWithRelation(string id);
    }
}

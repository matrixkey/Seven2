﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;

namespace SF.Framework.DataInterface.IRepositories.Master
{
    partial interface ISysMenuPermissionRepository
    {
        /// <summary>
        /// 获取所有菜单权限实体集合
        /// </summary>
        /// <returns>菜单权限实体集合</returns>
        IEnumerable<SysMenuPermission> GetAllMenuPermissions();

        /// <summary>
        /// 根据菜单权限ID集合获取菜单权限集合
        /// </summary>
        /// <param name="ids">菜单权限ID集合</param>
        /// <returns>菜单权限实体集合</returns>
        IEnumerable<SysMenuPermission> GetMenuPermissions(IEnumerable<string> ids);

        /// <summary>
        /// 获取除指定菜单权限ID集合之外的菜单权限集合
        /// </summary>
        /// <param name="ids">要排除的菜单权限ID集合</param>
        /// <param name="filterAlwaysUseable">是否过滤掉总是可用的菜单权限数据</param>
        /// <returns>菜单权限实体集合</returns>
        IEnumerable<SysMenuPermission> GetMenuPermissionsExcept(IEnumerable<string> ids, bool filterAlwaysUseable);

        /// <summary>
        /// 获取总是可用的菜单权限集合
        /// </summary>
        /// <returns>菜单权限集合</returns>
        IEnumerable<SysMenuPermission> GetAlwaysUseablePermissions();

        /// <summary>
        /// 获取除指定id之外的类型为按钮的菜单权限集合
        /// </summary>
        /// <param name="ids">要排除的菜单权限ID集合</param>
        /// <returns>按钮菜单权限集合</returns>
        IEnumerable<SysMenuPermission> GetButtonMenuPermissionsExcept(IEnumerable<string> ids);

        /// <summary>
        /// 获取指定ID的菜单权限数据编辑模型
        /// </summary>
        /// <param name="id">菜单权限ID</param>
        /// <returns>菜单权限数据编辑模型</returns>
        MenuPermissionFormModel GetMenuPermissionEditModel(string id);

        /// <summary>
        /// 获取指定菜单的菜单权限列表数据模型集合
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <returns>菜单权限列表数据模型集合</returns>
        IEnumerable<MenuPermissionListModel> GetMenuPermissionModels(string menuId);

        /// <summary>
        /// 根据菜单权限ID删除菜单权限及被关联的数据
        /// </summary>
        /// <param name="id">菜单权限ID</param>
        /// <returns>返回影响的行数</returns>
        int DeleteMenuPermissionWithRelation(string id);
    }
}

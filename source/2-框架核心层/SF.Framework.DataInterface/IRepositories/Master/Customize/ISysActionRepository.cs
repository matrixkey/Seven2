﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;

namespace SF.Framework.DataInterface.IRepositories.Master
{
    partial interface ISysActionRepository
    {
        /// <summary>
        /// 获取指定主键的行为的编辑数据模型
        /// </summary>
        /// <param name="id">行为主键</param>
        /// <returns>行为的编辑数据模型</returns>
        ActionFormModel GetActionFormModel(string id);

        /// <summary>
        /// 获取指定控制器 <see cref="controllerId"/> 的所有行为数据模型集合
        /// </summary>
        /// <param name="controllerId">控制器ID</param>
        /// <param name="actionName">行为名称，用于模糊查询，空则不进行匹配查询</param>
        /// <returns>行为数据模型集合</returns>
        IEnumerable<ActionListModel> GetActionModels(string controllerId, string actionName);

        /// <summary>
        /// 获取指定ID集合的行为数据模型集合
        /// </summary>
        /// <param name="ids">行为ID集合</param>
        /// <returns>行为数据模型集合</returns>
        IEnumerable<ActionListModel> GetActionModels(IEnumerable<string> ids);

        /// <summary>
        /// 检查行为是否已经存在
        /// </summary>
        /// <param name="actionName">行为名称</param>
        /// <param name="controllerId">所属控制器ID</param>
        /// <param name="id">需要忽略的主键值。新增判定时可传空，更新判定时则需传当前数据的主键值</param>
        /// <returns>行为存在返回 true ，否则返回 false。</returns>
        bool CheckActionExist(string actionName, string controllerId, string id);

        /// <summary>
        /// 检查指定的控制器ID下是否存在行为数据
        /// </summary>
        /// <param name="controllerId">控制器ID</param>
        /// <returns>存在行为返回 true ，否则返回 false 。</returns>
        bool CheckActionExist(string controllerId);

        /// <summary>
        /// 检验指定ID的行为是否存在直接关联的菜单（即菜单的FK_ActionID是否为actionId）
        /// </summary>
        /// <param name="actionId">行为ID</param>
        /// <returns>存在直接关联的菜单返回 true ，否则返回 false 。</returns>
        bool CheckDirectRelatedMenuExist(string actionId);

        /// <summary>
        /// 根据行为ID删除行为及被关联的数据
        /// </summary>
        /// <param name="id">行为ID</param>
        /// <returns>返回影响的行数</returns>
        int DeleteActionWithRelation(string id);
    }
}

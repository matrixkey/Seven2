﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;

namespace SF.Framework.DataInterface.IRepositories.Master
{
    partial interface IBasicDataDictionaryRepository
    {
        /// <summary>
        /// 获取指定主键ID的数据字典编辑数据模型
        /// </summary>
        /// <param name="id">数据字典ID</param>
        /// <returns>数据字典编辑数据模型</returns>
        DataDictionaryFormModel GetDataDictionaryFormModel(string id);

        /// <summary>
        /// 获取指定父级的子集数据字典模型集合
        /// </summary>
        /// <param name="parentId">父级ID</param>
        /// <returns>子集数据字典模型集合</returns>
        IEnumerable<DataDictionaryListModel> GetChildrenDataDictionary(string parentId);

        /// <summary>
        /// 检查指定父级是否存在子级数据字典
        /// </summary>
        /// <param name="parentId">父级数据字典ID</param>
        /// <returns>存在子级返回 true ，否则返回 false 。</returns>
        bool CheckChildrenExist(string parentId);

        /// <summary>
        /// 检查父级数据字典的名称是否存在
        /// </summary>
        /// <param name="name">父级数据字典的名称</param>
        /// <param name="id">需要忽略的主键值。新增判定时可传空，更新判定时则需传当前数据的主键值</param>
        /// <returns>存在则返回 true ，否则返回 false 。</returns>
        bool CheckParentNameExist(string name, string id);

        /// <summary>
        /// 检查子级数据字典的名称是否存在
        /// </summary>
        /// <param name="name">子级数据字典的名称</param>
        /// <param name="id">需要忽略的主键值。新增判定时可传空，更新判定时则需传当前数据的主键值</param>
        /// <returns>存在则返回 true ，否则返回 false 。</returns>
        bool CheckChildNameExist(string name, string id);
    }
}

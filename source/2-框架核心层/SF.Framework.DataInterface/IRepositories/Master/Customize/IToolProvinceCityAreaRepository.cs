﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;

namespace SF.Framework.DataInterface.IRepositories.Master
{
    partial interface IToolProvinceCityAreaRepository
    {
        /// <summary>
        /// 检查指定的省市区编号是否存在
        /// </summary>
        /// <param name="code">省市区编号</param>
        /// <returns>存在返回 true ，否则返回 false 。</returns>
        bool CheckCodeExist(string code);

        /// <summary>
        /// 获取所有省市区实体集合
        /// </summary>
        /// <returns>所有省市区实体集合</returns>
        IEnumerable<ToolProvinceCityArea> GetAll();
    }
}

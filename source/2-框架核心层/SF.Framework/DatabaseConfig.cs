﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Utilities;

namespace SF.Framework
{
    public class DatabaseConfig
    {
        /// <summary>
        /// web.config文件中数据库连接字符串的名称
        /// </summary>
        public static string ConnectionStringName = "matrixkey";

        /// <summary>
        /// 默认的数据库管道名称
        /// </summary>
        public static readonly string DefaultProviderName = "System.Data.SqlClient";

        /// <summary>
        /// 默认的数据库驱动方式
        /// </summary>
        private static readonly string DefaultDataDriven = DataDrivenType.EntityFramework.ToString();

        /// <summary>
        /// 默认的用来加密解密 数据库连接字符串 的密钥
        /// </summary>
        private static readonly string DefaultConnectionStringEncryptKey = "angle";

        /// <summary>
        /// 数据库驱动方式
        /// </summary>
        public enum DataDrivenType
        {
            /// <summary>
            /// EF驱动方式
            /// </summary>
            EntityFramework,

            /// <summary>
            /// ADO.NET驱动方式
            /// </summary>
            ADO_NET
        }



        private static string _connectioonString = string.Empty;

        /// <summary>
        /// 数据连接字符串
        /// </summary>
        public static string ConnectionString
        {
            get
            {
                if (string.IsNullOrEmpty(_connectioonString))
                {
                    string tempConnectionString = string.Empty, tempProviderName = string.Empty;
                    if (ConfigHelper.TryGetConnectionInfo(ConnectionStringName, out tempConnectionString, out tempProviderName))
                    {
                        _connectioonString = NeedDecrypt ? SF.Security.Cryptography.DESCrypto.Decrypt(tempConnectionString, DefaultConnectionStringEncryptKey) : tempConnectionString;
                        _providerName = tempProviderName;
                    }
                }
                if (string.IsNullOrWhiteSpace(_connectioonString))
                {
                    throw ExceptionHelper.ThrowConfigException("不存在名为“" + ConnectionStringName + "”的数据库连接信息，请检查web.config文件中的设置。");
                }
                return _connectioonString;
            }
        }

        private static string _providerName = string.Empty;

        /// <summary>
        /// 数据提供程序
        /// </summary>
        public static string ProviderName
        {
            get
            {
                if (string.IsNullOrEmpty(_providerName))
                {
                    _providerName = ConfigHelper.ProviderName(ConnectionStringName);
                }
                if (string.IsNullOrWhiteSpace(_providerName))
                {
                    _providerName = DefaultProviderName;
                }
                return _providerName;
            }
        }

        /// <summary>
        /// 表示WEB应用程序在连接数据库时，是否需要对 数据库连接字符串 进行解密。
        /// 该属性配置项由 Web.config/configuration/appSettings/Database:ConnectionSafety 参数指定。
        /// </summary>
        public static bool NeedDecrypt
        {
            get
            {
                string tempNeedDecrypt = string.Empty;
                if (ConfigHelper.TryGetAppSettingInfo("Database:ConnectionSafety", out tempNeedDecrypt))
                { }
                return string.IsNullOrWhiteSpace(tempNeedDecrypt) ? false : tempNeedDecrypt.ToBoolean(false);
            }
        }

        private static string _dataDriven = string.Empty;

        /// <summary>
        /// 表示WEB应用程序在连接数据库时，应该使用的数据库驱动方式（将决定业务逻辑层中使用的工作单元实例源）。
        /// 该属性配置项由 Web.config/configuration/appSettings/Database:DataDriven 参数指定。
        /// </summary>
        public static string DataDriven
        {
            get
            {
                if (string.IsNullOrEmpty(_dataDriven))
                {
                    string tempDataDriven = string.Empty;
                    if (ConfigHelper.TryGetAppSettingInfo("Database:DataDriven", out tempDataDriven))
                    { }
                    _dataDriven = string.IsNullOrWhiteSpace(tempDataDriven) ? DefaultDataDriven : tempDataDriven;
                }
                return _dataDriven;
            }
        }

        /// <summary>
        /// 表示WEB应用程序在初次使用数据库时，是否自动合并/迁移数据库。该属性只在 <see cref="DataDriven"/> 值为 EntityFramework 时有效。
        /// 该属性配置项由 Web.config/configuration/appSettings/Database:Migrate 参数指定。
        /// </summary>
        public static bool IsMigrateDatabase
        {
            get
            {
                string tempIsMigrateDatabase = string.Empty;
                if (ConfigHelper.TryGetAppSettingInfo("Database:Migrate", out tempIsMigrateDatabase))
                { }
                return string.IsNullOrEmpty(tempIsMigrateDatabase) ? false : tempIsMigrateDatabase.ToBoolean(false);
            }
        }

        //private static bool? _databaseInit = null;

        ///// <summary>
        ///// 初始化数据库时是否执行初始数据的插入，默认为false
        ///// </summary>
        //public static bool DatabaseInit
        //{
        //    get
        //    {
        //        if (!_databaseInit.HasValue)
        //        {
        //            string temp = ConfigHelper.AppSetting("Seven:DatabaseInit");
        //            _databaseInit = string.IsNullOrEmpty(temp) ? false : temp.ToBoolean(false);
        //        }
        //        return _databaseInit.Value;
        //    }
        //}
    }
}

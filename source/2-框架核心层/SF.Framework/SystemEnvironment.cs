﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Utilities;

namespace SF.Framework
{
    /// <summary>
    /// 表示当前应用程序运行环境
    /// </summary>
    public class SystemEnvironment
    {
        /// <summary>
        /// 获取或设置当前应用程序的默认配置文件名称。
        /// </summary>
        public static string CurrentConfigurationFileName
        {
            get { return AppDomain.CurrentDomain.SetupInformation.ConfigurationFile; }
            set { AppDomain.CurrentDomain.SetupInformation.ConfigurationFile = value; }
        }

        /// <summary>
        /// 表示当前 WEB 应用程序在前端页面资源输出时，是否启用 WEB 资源文件(仅 JavaScript 和 Css 文件)自动压缩功能。
        /// 该属性配置项由 Web.config/configuration/appSettings/BundleTable.EnableOptimizations 参数指定。
        /// </summary>
        public static bool EnableOptimizations
        {
            get
            {
                string temp = string.Empty;
                ConfigHelper.TryGetAppSettingInfo("BundleTable.EnableOptimizations", out temp);

                return temp.ToBoolean(false, true);
            }
        }

        /// <summary>
        /// 表示当前 WEB 应用程序中，系统自带的超级管理员帐号。
        /// </summary>
        public static string SuperAdminAccountName
        {
            get { return "admin"; }
        }

        /// <summary>
        /// 表示当前 WEB 应用程序中，启动项的程序集名称
        /// </summary>
        public static string ProjectStartItemAssemblyName
        {
            get { return "SF.Framework.WebClient"; }
        }

        /// <summary>
        /// 表示当前 WEB 应用程序的标题名。
        /// </summary>
        public static string SystemTitle
        {
            get { return "企业 MIS 系统快速开发平台(SF)"; }
        }

        /// <summary>
        /// 表示当前 WEB 应用程序的标题名缩写。
        /// </summary>
        public static string SystemTitleAbbreviation
        {
            get { return "企业 MIS 系统快速开发平台(SF)"; }
        }

        /// <summary>
        /// 表示当前 WEB 应用程序的版权声明信息.
        /// </summary>
        public static string Copyright
        {
            get { return "@2013-2014 Copyright: 落阳."; }
        }
    }
}

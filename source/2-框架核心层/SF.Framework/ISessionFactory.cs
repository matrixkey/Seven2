﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework
{
    public interface ISessionFactory
    {
        /// <summary>
        /// 当前登录用户的用户名
        /// </summary>
        string UserName { get; }
    }
}

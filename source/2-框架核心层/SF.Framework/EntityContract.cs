﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework
{
    /// <summary>
    /// 实体契约定义
    /// </summary>
    public static class EntityContract
    {
        /// <summary>
        /// 当可递归表的主键是 string 类型时，约定其顶级 parentID 的默认值。
        /// </summary>
        public const string ParentIDDefaultValueWhenIDIsString = "root";

        /// <summary>
        /// 当可递归表的主键是 string 类型时，约定其顶级 parentID 对应的默认显示值。
        /// </summary>
        public const string ParentNameDefualtValueWhenIDIsString = "根目录";

        /// <summary>
        /// 用来给账号的密码进行加密的默认密钥，一旦系统投入使用，请勿修改。
        /// </summary>
        public const string AccountPasswordEncryptionDefaultKey = "pinMingSoft.Net";
    }
}

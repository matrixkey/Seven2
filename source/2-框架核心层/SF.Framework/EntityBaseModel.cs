﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace SF.Framework
{
    public abstract class EntityBaseModel : EntityModelBaseForReflection
    {
        #region 映射到数据库的属性

        /// <summary>
        /// 获取或设置一个 <see cref="string"/> 值，该值表示实体对象的数据创建者。
        /// </summary>
        public virtual string CreateUser { get; set; }

        /// <summary>
        /// 获取或设置一个 <see cref="DateTime"/> 值，该值表示实体对象的数据创建时间。
        /// </summary>
        public virtual DateTime CreateDate { get; set; }

        /// <summary>
        /// 获取或设置一个 <see cref="string"/> 值，该值表示实体对象的数据最后修改者。
        /// </summary>
        public virtual string LastModifyUser { get; set; }

        /// <summary>
        /// 获取或设置一个 <see cref="DateTime"/> 值，该值表示实体对象的数据最后修改时间。
        /// </summary>
        public virtual DateTime? LastModifyDate { get; set; }

        /// <summary>
        /// 获取或设置当前 实体数据模型 对象在数据库中存储 的行版本标识。
        /// </summary>
        /// <remarks>在用于并发处理环境中时，该版本标识非常有用。</remarks>
        [ConcurrencyCheck]
        [Timestamp]
        public virtual byte[] Timestamp { get; set; }

        #endregion
    }
}

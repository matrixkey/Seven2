﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework
{
    public class SystemIconClassDefine
    {
        /// <summary>
        /// 默认的icon样式名称
        /// </summary>
        public static string DefaultIconClassName
        {
            get { return "icon-unknow"; }
        }

        /// <summary>
        /// 默认的“大集团”图标样式名称
        /// </summary>
        public static string DefaultGroupIconClassName
        {
            get { return "icon-group"; }
        }

        /// <summary>
        /// 默认的“小集团”图标样式名称
        /// </summary>
        public static string DefaultSonGroupIconClassName
        {
            get { return "icon-songroup"; }
        }

        /// <summary>
        /// 默认的“公司”图标样式名称
        /// </summary>
        public static string DefaultCompanyIconClassName
        {
            get { return "icon-company"; }
        }

        /// <summary>
        /// 默认的“部门”图标样式名称
        /// </summary>
        public static string DefaultDeptIconClassName
        {
            get { return "icon-dept"; }
        }
    }
}

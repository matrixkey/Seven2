﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace SF.Framework
{
    /// <summary>
    /// 行为类型
    /// </summary>
    public enum ActionType
    {
        /// <summary>
        /// 页面
        /// </summary>
        [Description("页面")]
        [EnumMember(Value = "页面")]
        View = 0,

        /// <summary>
        /// 提供数据
        /// </summary>
        [Description("提供数据")]
        [EnumMember(Value = "提供数据")]
        Data = 1,

        /// <summary>
        /// 操作
        /// </summary>
        [Description("操作")]
        [EnumMember(Value = "操作")]
        Operation = 2
    }

    /// <summary>
    /// 行为限制标记
    /// </summary>
    public enum ActionLimitedAttribute
    {
        /// <summary>
        /// 允许匿名访问
        /// </summary>
        [Description("允许匿名访问")]
        [EnumMember(Value = "允许匿名访问")]
        AllowAnonymous = 0,

        /// <summary>
        /// 允许登录后访问
        /// </summary>
        [Description("允许登录后访问")]
        [EnumMember(Value = "允许登录后访问")]
        AllowLogined = 1,

        /// <summary>
        /// 只允许超管访问
        /// </summary>
        [Description("只允许超管访问")]
        [EnumMember(Value = "只允许超管访问")]
        AllowOnlyAdmin = 2,

        /// <summary>
        /// 跟随其他设置
        /// </summary>
        [Description("跟随其他设置")]
        [EnumMember(Value = "跟随其他设置")]
        Follow = 10
    }

    /// <summary>
    /// 菜单权限类型
    /// </summary>
    public enum MenuPermissionType
    {
        /// <summary>
        /// 访问
        /// </summary>
        [Description("访问")]
        [EnumMember(Value = "访问")]
        Scan = 0,

        /// <summary>
        /// 按钮
        /// </summary>
        [Description("按钮")]
        [EnumMember(Value = "按钮")]
        Button = 1,

        /// <summary>
        /// 数据
        /// </summary>
        [Description("数据")]
        [EnumMember(Value = "数据")]
        Data = 2
    }

    /// <summary>
    /// 系统操作日志类型
    /// </summary>
    public enum SystemLogType
    {
        /// <summary>
        /// 登录
        /// </summary>
        [Description("登录")]
        [EnumMember(Value = "登录")]
        Login = 1,

        /// <summary>
        /// 注销退出
        /// </summary>
        [Description("注销退出")]
        [EnumMember(Value = "注销退出")]
        Logout = 2,

        /// <summary>
        /// 密码修改
        /// </summary>
        [Description("密码修改")]
        [EnumMember(Value = "密码修改")]
        PasswordModify = 3,

        /// <summary>
        /// 数据添加
        /// </summary>
        [Description("数据添加")]
        [EnumMember(Value = "数据添加")]
        Add = 10,

        /// <summary>
        /// 数据更新
        /// </summary>
        [Description("数据更新")]
        [EnumMember(Value = "数据更新")]
        Update = 20,

        /// <summary>
        /// 数据删除
        /// </summary>
        [Description("数据删除")]
        [EnumMember(Value = "数据删除")]
        Delete = 30
    }

    /// <summary>
    /// 组织结构类型
    /// </summary>
    public enum OrganizationType
    {
        /// <summary>
        /// 部门
        /// </summary>
        [Description("部门")]
        [EnumMember(Value = "部门")]
        Department = 0,

        /// <summary>
        /// 分公司
        /// </summary>
        [Description("分公司")]
        [EnumMember(Value = "分公司")]
        ConstituentCompany = 1,

        /// <summary>
        /// 总公司
        /// </summary>
        [Description("总公司")]
        [EnumMember(Value = "总公司")]
        ParentCompany = 2,

        /// <summary>
        /// 子集团
        /// </summary>
        [Description("子集团")]
        [EnumMember(Value = "子集团")]
        ChildrenGroup = 3,

        /// <summary>
        /// 顶级集团
        /// </summary>
        [Description("顶级集团")]
        [EnumMember(Value = "顶级集团")]
        TopGroup = 4
    }
}

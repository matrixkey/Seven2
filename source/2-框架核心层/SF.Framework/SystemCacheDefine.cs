﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework
{
    public class SystemCacheDefine
    {
        /// <summary>
        /// 表示当前 WEB 应用程序的英文标记
        /// </summary>
        public static string SystemEnglishSymbol
        {
            get { return "SevenSystem"; }
        }


        /// <summary>
        /// 表示当前 WEB 应用程序在存储“登录名”时的 cookie 名称
        /// </summary>
        public static string LoginNameCookieName
        {
            get { return SystemEnglishSymbol + "_LoginName"; }
        }

        /// <summary>
        /// 表示当前 WEB 应用程序在存储“主题皮肤名”时的 cookie 名称前缀
        /// </summary>
        public static string ThemeCookieNamePrefix
        {
            get { return SystemEnglishSymbol + "_Theme_"; }
        }

        /// <summary>
        /// 表示当前 WEB 应用程序在存储“组织结构范围”时的 session 名称前缀
        /// </summary>
        public static string OrganRangeCookieNamePrefix
        {
            get { return SystemEnglishSymbol + "_RangeOrgans_"; }
        }

        /// <summary>
        /// 表示当前 WEB 应用程序在存储“菜单范围”时的 session 名称前缀
        /// </summary>
        public static string MenuRangeCookieNamePrefix
        {
            get { return SystemEnglishSymbol + "_RangeMenus_"; }
        }

        /// <summary>
        /// 表示当前 WEB 应用程序在存储“控制器行为范围”时的 session 名称前缀
        /// </summary>
        public static string ControllerActionRangeCookieNamePrefix
        {
            get { return SystemEnglishSymbol + "_RangeControllerActions_"; }
        }

        /// <summary>
        /// 表示当前 WEB 应用程序在存储“菜单权限范围”时的 session 名称前缀
        /// </summary>
        public static string MenuPermissionRangeCookieNamePrefix
        {
            get { return SystemEnglishSymbol + "_RangeMenuPermissions_"; }
        }

        /// <summary>
        /// 表示当前 WEB 应用程序在存储“菜单按钮权限范围”时的 session 名称前缀
        /// </summary>
        public static string MenuButtonPermissionRangeCookieNamePrefix
        {
            get { return SystemEnglishSymbol + "_RangeMenuButtonPermissions_"; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Export
{
    /// <summary>
    /// 单元格合并规则模型
    /// </summary>
    public class MergerCellParamModel
    {
        /// <summary>
        /// 开始行的索引
        /// </summary>
        public int RowStartIndex { get; set; }

        /// <summary>
        /// 开始列的索引
        /// </summary>
        public int ColStartIndex { get; set; }

        /// <summary>
        /// 结束行的索引
        /// </summary>
        public int RowEndIndex { get; set; }

        /// <summary>
        /// 结束列的索引
        /// </summary>
        public int ColEndIndex { get; set; }

        /// <summary>
        /// 是否重复
        /// </summary>
        public bool Repeat { get; set; }

        /// <summary>
        /// 间隔行数，小于0则表示在 主体规则的行范围内重复合并
        /// </summary>
        public int IntervalRowCount { get; set; }

        /// <summary>
        /// 间隔列数，小于0则表示在 主体规则的列范围内重复合并
        /// </summary>
        public int IntervalColCount { get; set; }
    }
}

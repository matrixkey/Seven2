﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Collections.Specialized;
using System.Linq.Expressions;

namespace SF.Utilities
{
    /// <summary>
    /// 提供一组对 基础对象类型 <see cref="System.Object"/> 操作方法的扩展。
    /// </summary>
    public static partial class ObjectExtensions
    {

        /// <summary>
        /// 判断指定的对象是否为 Null 或者等于 DBNull.Value 值。
        /// </summary>
        /// <param name="_this"></param>
        /// <returns></returns>
        public static bool IsNull(this Object _this)
        {
            return _this == null || Convert.IsDBNull(_this);
        }


        /// <summary>
        /// 将当前对象转换成一个包含键值对集合的 <see cref="NameValueCollection"/> 对象。键值对中的 Key 值表示对象中的属性名，Value 值表示该属性名所对应的值。
        /// 当前对象中所有具有 get 属性器的公共属性名及属性值都将包含在返回的键值对枚举列表中。
        /// </summary>
        /// <param name="_this">要被转换的对象。</param>
        /// <returns>返回一个 <see cref="NameValueCollection"/>。该对象键值对中的 Key 值表示对象中的属性名，Value 值表示该属性名所对应的值。</returns>
        public static NameValueCollection ToNameValueCollection(this Object _this)
        {
            Check.NotNull(_this);

            Dictionary<string, object> dictionary = _this.ToDictionary();
            NameValueCollection collection = new NameValueCollection();
            foreach (var item in dictionary)
            {
                collection.Add(item.Key, Convert.ToString(item.Value));
            }
            return collection;
        }


        /// <summary>
        /// 将当前对象转换成一个包含键值对集合的 Dictionary 对象。键值对中的 Key 值表示对象中的属性名，Value 值表示该属性名所对应的值。
        /// 当前对象中所有具有 get 属性器的公共属性名及属性值都将包含在返回的键值对枚举列表中。
        /// </summary>
        /// <param name="_this">要被转换的对象。</param>
        /// <returns>返回一个 Dictionary。该对象键值对中的 Key 值表示对象中的属性名，Value 值表示该属性名所对应的值。</returns>
        public static Dictionary<string, object> ToDictionary(this object _this)
        {
            Check.NotNull(_this);
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            var properies = _this.GetType().GetProperties().Where(p => p.GetMethod != null);
            foreach (PropertyInfo property in properies)
            {
                dictionary.Add(property.Name, property.GetValue(_this));
            }
            return dictionary;
        }

        /// <summary>
        /// 将当前对象转换成一个包含键值对集合的 Dictionary 对象。键值对中的 Key 值表示对象中的属性名，Value 值表示该属性名所对应的值。
        /// 当前对象中所有指定作用域范围的属性名及属性值都将包含在返回的键值对枚举列表中。
        /// </summary>
        /// <param name="_this">要被转换的对象。</param>
        /// <param name="bindingAttr">指定搜索 <paramref name="_this"/> 中属性的反射搜索范围。</param>
        /// <returns>返回一个 Dictionary。该对象键值对中的 Key 值表示对象中的属性名，Value 值表示该属性名所对应的值。</returns>
        public static Dictionary<string, object> ToDictionary(this object _this, BindingFlags bindingAttr)
        {
            Check.NotNull(_this);
            Dictionary<string, object> dictionary = new Dictionary<string, object>();
            PropertyInfo[] properies = _this.GetType().GetProperties(bindingAttr);
            foreach (PropertyInfo property in properies)
            {
                dictionary.Add(property.Name, property.GetValue(_this));
            }
            return dictionary;
        }

        #region 反射方式的模型转换

        /// <summary>
        /// 将当前对象中所有属性的值按照属性名称和类型定义的匹配关系复制到另一对象中。
        /// </summary>
        /// <param name="_this">
        /// 表示将要用于复制数据到另一对象的元数据对象。
        /// 如果该参数值为 Null，将不会执行复制操作。
        /// </param>
        /// <param name="targetElement">表示一个目标对象，该对象中的相应属性值将会被更改。</param>
        /// <param name="abortOnFailed">一个布尔类型值，该值指示在复制数据过程中如果出现异常，是否立即停止并抛出异常，默认为 false。</param>
        public static void CopyTo(this object _this, object targetElement, bool abortOnFailed = false)
        {
            if (_this == null)
                return;

            Check.NotNull(targetElement);
            Type thisType = _this.GetType(), elemType = targetElement.GetType();

            var thisProps = thisType.GetProperties().Where(p => p.GetMethod != null);
            var elemProps = elemType.GetProperties().Where(p => p.SetMethod != null);
            foreach (PropertyInfo thisProperty in thisProps)
            {
                PropertyInfo elemProperty = elemProps.FirstOrDefault(p => p.Name == thisProperty.Name);
                if (elemProperty != null && elemProperty.PropertyType.IsAssignableFrom(thisProperty.PropertyType))
                {
                    if (abortOnFailed)
                        elemProperty.SetValue(targetElement, thisProperty.GetValue(_this));
                    else
                        Trying.Try(() => elemProperty.SetValue(targetElement, thisProperty.GetValue(_this)));
                }
            }

            var thisFields = thisType.GetFields();
            var elemFields = elemType.GetFields();
            foreach (FieldInfo thisField in thisFields)
            {
                FieldInfo elemField = elemFields.FirstOrDefault(f => f.Name == thisField.Name);
                if (elemField != null && elemField.FieldType.IsAssignableFrom(thisField.FieldType))
                {
                    if (abortOnFailed)
                        elemField.SetValue(targetElement, thisField.GetValue(_this));
                    else
                        Trying.Try(() => elemField.SetValue(targetElement, thisField.GetValue(_this)));
                }
            }
        }


        /// <summary>
        /// 将当前对象转换成一个指定类型的新对象。
        /// 该方法首先判断 <paramref name="_this"/> 是否为 <paramref name="resultType"/> 指定的类型，如果是，则直接返回 <paramref name="_this"/>；
        /// 否则将通过 Activator.CreateInstance(resultType) 方法创建一个 <paramref name="resultType"/> 类型的对象，然后查找出该类型对象有哪些公共属性值，将 <paramref name="_this"/> 对象的对应属性值复制到新创建的对象中。
        /// </summary>
        /// <param name="_this">要转换的对象。</param>
        /// <param name="resultType">要转换的目标实体类型。</param>
        /// <param name="abortOnFailed">一个布尔类型值，该值表示在复制数据过程中如果出现异常，是否立即停止并抛出异常，默认为 false。</param>
        /// <returns>
        /// 该方法首先判断 <paramref name="_this"/> 是否为 <paramref name="resultType"/> 指定的类型，如果是，则直接返回 <paramref name="_this"/>；
        /// 否则将通过 Activator.CreateInstance(resultType) 方法创建一个 <paramref name="resultType"/> 类型的对象，然后查找出该类型对象有哪些公共属性值，将 <paramref name="_this"/> 对象的对应属性值复制到新创建的对象中。
        /// </returns>
        public static object CastTo(this object _this, Type resultType, bool abortOnFailed = false)
        {
            Check.NotNull(_this);
            Check.NotNull(resultType);
            if (resultType.IsInstanceOfType(_this))
            {
                return _this;
            }
            object ret = Activator.CreateInstance(resultType);
            _this.CopyTo(ret, abortOnFailed);
            return ret;
        }

        /// <summary>
        /// 将当前对象转换成一个指定类型的新对象。
        /// 该方法首先判断 <paramref name="_this"/> 是否为 <typeparamref name="TResult"/> 指定的类型，如果是，则直接返回 <paramref name="_this"/>；
        /// 否则将通过 Activator.CreateInstance(resultType) 方法创建一个 <typeparamref name="TResult"/> 类型的对象，然后查找出该类型对象有哪些公共属性值，将 <paramref name="_this"/> 对象的对应属性值复制到新创建的对象中。
        /// </summary>
        /// <typeparam name="TResult">要转换的目标实体类型。</typeparam>
        /// <param name="_this">要转换的对象。</param>
        /// <param name="abortOnFailed">一个布尔类型值，该值表示在复制数据过程中如果出现异常，是否立即停止并抛出异常，默认为 false。</param>
        /// <returns>
        /// 该方法首先判断 <paramref name="_this"/> 是否为 <typeparamref name="TResult"/> 指定的类型，如果是，则直接返回 <paramref name="_this"/>；
        /// 否则将通过 Activator.CreateInstance(resultType) 方法创建一个 <typeparamref name="TResult"/> 类型的对象，然后查找出该类型对象有哪些公共属性值，将 <paramref name="_this"/> 对象的对应属性值复制到新创建的对象中。
        /// </returns>
        public static TResult CastTo<TResult>(this object _this, bool abortOnFailed = false) where TResult : class,new()
        {
            if (_this is TResult)
            {
                return (TResult)_this;
            }
            //Check.NotNull(_this);
            if (_this.IsNull()) { return null; }

            TResult ret = (TResult)Activator.CreateInstance(typeof(TResult));
            _this.CopyTo(ret, abortOnFailed);
            return ret;
        }



        /// <summary>
        /// 创建一个当前对象的新副本，该副本和当前对象为同一类型，且其中各个属性的值均等同于原对象中各个属性的值。
        /// 相当于浅表复制操作 Object.MemberwiseClone，但和 Object.MemberwiseClone 不同的是该操作只对公共 Public 的可 set 属性进行复制，并且不会复制其他字段或私有成员的值。
        /// </summary>
        /// <param name="_this">要复制的对象。</param>
        /// <param name="abortOnFailed">一个布尔类型值，该值表示在复制数据过程中如果出现异常，是否立即停止并抛出异常，默认为 false。</param>
        /// <returns>一个对象新副本</returns>
        public static object Duplicate(this object _this, bool abortOnFailed = false)
        {
            if (_this == null)
                return null;

            Type thisType = _this.GetType();
            if (thisType.IsValueType)
                return _this;

            if (_this is ICloneable)
                return ((ICloneable)_this).Clone();

            Object obj = Activator.CreateInstance(thisType);
            _this.CopyTo(obj, abortOnFailed);
            return obj;
        }

        /// <summary>
        /// 创建一个当前对象的新副本，该副本和当前对象为同一类型，且其中各个属性的值均等同于原对象中各个属性的值。
        /// 相当于浅表复制操作 Object.MemberwiseClone，但和 Object.MemberwiseClone 不同的是该操作只对公共 Public 的可 set 属性进行复制，并且不会复制其他字段或私有成员的值。
        /// </summary>
        /// <typeparam name="TSource">要转换的目标实体类型。</typeparam>
        /// <param name="_this">要复制的对象。</param>
        /// <param name="abortOnFailed">一个布尔类型值，该值表示在复制数据过程中如果出现异常，是否立即停止并抛出异常，默认为 false。</param>
        /// <returns>一个对象新副本</returns>
        public static TSource Duplicate<TSource>(this TSource _this, bool abortOnFailed = false) where TSource : class, new()
        {
            if (_this == null)
                return null;

            Type thisType = typeof(TSource);
            if (thisType.IsAbstract)
            {
                Object obj = Duplicate(_this as object, abortOnFailed);
                return obj == null ? null : obj as TSource;
            }

            if (thisType.IsValueType)
                return _this;

            if (_this is ICloneable)
                return ((ICloneable)_this).Clone() as TSource;

            TSource target = new TSource();
            _this.CopyTo(target, abortOnFailed);
            return target;
        }

        /// <summary>
        /// 创建当前 Object 的浅表副本。同 Object.MemberwiseClone 方法。
        /// 该方法创建一个浅表副本，方法是创建一个新对象，然后将当前对象的非静态字段复制到该新对象。
        /// 如果字段是值类型的，则对该字段执行逐位复制。 如果字段是引用类型，则复制引用但不复制引用的对象；因此，原始对象及其复本引用同一对象。
        /// </summary>
        /// <param name="_this">要执行复制操作的对象。</param>
        /// <returns></returns>
        public static TSource Clone<TSource>(this TSource _this)
        {
            if (_this == null)
                return default(TSource);

            if (_this.GetType().IsValueType)
                return _this;

            return (TSource)MemberwiseCloneMethod.Invoke(_this, null);
        }

        #endregion

        #region 委托和表达式树方式的模型转换

        /// <summary>
        /// 指定 Type 与其他 Type 的赋值委托缓存
        /// </summary>
        private static Dictionary<Type, Dictionary<Type, Delegate>> copyCache = new Dictionary<Type, Dictionary<Type, Delegate>>();

        /// <summary>
        /// 将当前对象中所有属性的值按照属性名称和类型定义的匹配关系复制到另一对象中。
        /// </summary>
        /// <typeparam name="TEntity">要执行复制操作的对象的类型。</typeparam>
        /// <typeparam name="TResult">目标类型</typeparam>
        /// <param name="_this">要执行复制操作的对象。</param>
        /// <returns>返回一个目标类型对象</returns>
        public static TResult CopyByDelegate<TEntity, TResult>(this TEntity _this)
            where TEntity : class
            where TResult : class
        {
            if (_this is TResult)
            {
                return _this as TResult;
            }
            TResult ret = (TResult)Activator.CreateInstance(typeof(TResult));

            Type sourceType = typeof(TEntity), targetType = typeof(TResult);
            Dictionary<Type, Delegate> typeDelegate;
            if (!copyCache.TryGetValue(sourceType, out typeDelegate))
            {
                typeDelegate = new Dictionary<Type, Delegate>();
                Delegate del = CreateCopyDelegate<TEntity, TResult>();
                typeDelegate.Add(targetType, del);
                copyCache.Add(sourceType, typeDelegate);

                ret = ((Func<TEntity, TResult>)del)(_this);
            }
            else
            {
                Delegate del;
                if (!typeDelegate.TryGetValue(targetType, out del))
                {
                    del = CreateCopyDelegate<TEntity, TResult>();
                    typeDelegate = new Dictionary<Type, Delegate>();
                    typeDelegate.Add(targetType, del);
                }
                ret = ((Func<TEntity, TResult>)del)(_this);
            }

            return ret;
        }

        /// <summary>
        /// 创建复制委托
        /// source =>
        /// {
        ///     var target;
        ///     target = new TResult();
        ///     target.A = source.A;
        ///     target.B = source.B;
        ///     return target;
        /// }
        /// </summary>
        /// <typeparam name="TEntity"></typeparam>
        /// <typeparam name="TResult"></typeparam>
        /// <returns>一个委托</returns>
        private static Func<TEntity, TResult> CreateCopyDelegate<TEntity, TResult>()
        {
            Type sourceType = typeof(TEntity), targetType = typeof(TResult);
            //lambda的方法定义中的参数
            var param = Expression.Parameter(sourceType, "e");
            // Lambda方法体中的变量集
            var methodVars = new List<ParameterExpression>();
            // Lambda方法体中的语句集
            var methodBodies = new List<Expression>();
            methodBodies.Add(FieldAssign(sourceType, targetType, param, methodBodies, methodVars));
            // 将语句集组合成一个语句块
            var MethodBlock = Expression.Block(targetType, methodVars, methodBodies);
            // 将表达式树生成委托
            return Expression.Lambda<Func<TEntity, TResult>>(MethodBlock, param).Compile();
        }

        /// <summary>
        /// 组建表达式赋值语句
        /// var target;
        /// target = new TResult();
        /// target.A = source.A;
        /// target.B = source.B;
        /// </summary>
        /// <param name="sourceType"></param>
        /// <param name="targetType"></param>
        /// <param name="sourceExp"></param>
        /// <param name="bodies"></param>
        /// <param name="paramList"></param>
        /// <returns>一个表达式树</returns>
        private static Expression FieldAssign(Type sourceType, Type targetType, Expression sourceExp, List<Expression> bodies, List<ParameterExpression> paramList)
        {
            var newEntityVar = Expression.Variable(targetType);
            paramList.Add(newEntityVar);//var a;
            bodies.Add(Expression.Assign(newEntityVar, Expression.New(targetType)));// a = new T();

            var sourceProps = sourceType.GetProperties().Where(p => p.GetMethod != null);
            var targetProps = targetType.GetProperties().Where(p => p.SetMethod != null);
            foreach (PropertyInfo thisProperty in sourceProps)
            {
                PropertyInfo elemProperty = targetProps.FirstOrDefault(p => p.Name == thisProperty.Name);
                if (elemProperty != null && elemProperty.PropertyType.IsAssignableFrom(thisProperty.PropertyType))
                {
                    var p1 = Expression.Property(newEntityVar, elemProperty);
                    var p2 = Expression.Property(sourceExp, thisProperty);

                    bodies.Add(Expression.Assign(p1, p2));// a.X = e.X
                }
            }

            return newEntityVar;
        }

        #endregion

        #region Internal CodeBlock

        private static MethodInfo _memberwiseClone;

        /// <summary>
        /// 获取 System.Obejct 类的 MemberwiseClone 方法对象
        /// </summary>
        internal static MethodInfo MemberwiseCloneMethod
        {
            get
            {
                if (_memberwiseClone == null)
                    _memberwiseClone = typeof(Object).GetMethod("MemberwiseClone", BindingFlags.Instance | BindingFlags.NonPublic);
                return _memberwiseClone;
            }
        }

        #endregion


    }
}

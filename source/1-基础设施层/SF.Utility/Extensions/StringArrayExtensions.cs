﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Utilities
{
    public static class StringArrayExtensions
    {
        /// <summary>
        /// 将字符串数组转换为其所表示的 int 值集合。
        /// </summary>
        /// <param name="value"></param>
        /// <param name="distinct">是否去掉重复值，默认是。</param>
        /// <returns></returns>
        public static IEnumerable<int> ToInt(this string[] value, bool distinct = true)
        {
            List<int> result = new List<int>();
            foreach (var item in value)
            {
                result.Add(item.ToInt());
            }
            return distinct ? result.ToArray().Distinct() : result.ToArray();
        }

        /// <summary>
        /// 将字符串数组转换为其所表示的 bool 值集合。
        /// </summary>
        /// <param name="value"></param>
        /// <param name="distinct">是否去掉重复值，默认是。</param>
        /// <returns></returns>
        public static IEnumerable<bool> ToBoolean(this string[] value, bool distinct = true)
        {
            List<bool> result = new List<bool>();
            foreach (var item in value)
            {
                result.Add(item.ToBoolean());
            }
            return distinct ? result.ToArray().Distinct() : result.ToArray();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Data;

namespace SF.Utilities
{
    /// <summary>
    /// 提供一组对 数据行对象 <see cref="System.Data.DataRow"/> 操作方法的扩展。
    /// </summary>
    public static class DataRowExtensions
    {
        /// <summary>
        /// 将一个数据行对象转换为一个动态运行时对象。
        /// <paramref name="row"/> 数据行对象中的所有数据项将作为属性值反映在返回的动态对象中。
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public static dynamic ToObject(this DataRow row)
        {
            Check.NotNull(row);
            dynamic obj = new DynamicObject();
            foreach (DataColumn column in row.Table.Columns)
            {
                obj[column.ColumnName] = row[column];
            }
            return obj;
        }

        /// <summary>
        /// 将一个数据行对象转换成一个指定类型的 CLR 对象。
        /// <paramref name="row"/> 数据行对象中的所有数据项将作为属性值反映在返回的 CLR 对象中。
        /// 对于不能转换的数据项（例如指定的 <typeparamref name="TResult"/> 类型中没有该数据项所示名称的属性、或者数据类型不匹配），将不会包含在返回的 CLR 对象中。
        /// </summary>
        /// <typeparam name="TResult">指定的 CLR 类型。</typeparam>
        /// <param name="row"></param>
        /// <returns></returns>
        public static TResult ToObject<TResult>(this DataRow row) where TResult : class, new()
        {
            Check.NotNull(row);

            Type type = typeof(TResult);
            TResult ret = new TResult();
            IEnumerable<PropertyInfo> properties = type.GetProperties().Where(p => p.SetMethod != null);
            foreach (string name in row.Table.GetColumnNames())
            {
                PropertyInfo property = properties.FirstOrDefault(p => p.Name == name);
                if (property == null)
                    continue;

                object value = row[name];
                if (value.IsNull() && !property.PropertyType.IsValueType)
                {
                    Trying.Try(() => property.SetValue(ret, property.PropertyType == typeof(DBNull) ? DBNull.Value : value));
                }
                else
                {
                    if (property.PropertyType.IsAssignableFrom(value.GetType()))
                    {
                        Trying.Try(() => property.SetValue(ret, value));
                    }
                    else
                    {
                        if (value is IConvertible && property.PropertyType.IsImplementOf(typeof(IConvertible)))
                        {
                            object val = Convert.ChangeType(value, property.PropertyType);
                            Trying.Try(() => property.SetValue(ret, val));
                        }
                    }
                }
            }
            return ret;
        }

        /// <summary>
        /// 将一个数据行对象转换成一个指定类型的 CLR 对象。
        /// <paramref name="row"/> 数据行对象中的所有数据项将作为属性值反映在返回的 CLR 对象中。
        /// 对于不能转换的数据项（例如指定的 <typeparamref name="TResult"/> 类型中没有该数据项所示名称的属性、或者数据类型不匹配），将不会包含在返回的 CLR 对象中。
        /// </summary>
        /// <typeparam name="TResult">指定的 CLR 类型。</typeparam>
        /// <param name="row"></param>
        /// <param name="accordingToDescriptionName">是否根据 属性中的 Description 值来对比 Row 中的列名</param>
        /// <returns></returns>
        public static TResult ToObject<TResult>(this DataRow row, bool accordingToDescriptionName) where TResult : class, new()
        {
            if (!accordingToDescriptionName) { return row.ToObject<TResult>(); }


            Check.NotNull(row);

            Type type = typeof(TResult);
            TResult ret = new TResult();
            IEnumerable<PropertyInfo> properties = type.GetProperties().Where(p => p.SetMethod != null);
            foreach (string name in row.Table.GetColumnNames())
            {
                PropertyInfo property = properties.FirstOrDefault(p =>
                {
                    var description = p.GetDescription();
                    if (description == name) { return true; }
                    return false;
                });
                if (property == null)
                { continue; }

                object value = row[name];
                if (value.IsNull() && !property.PropertyType.IsValueType)
                {
                    Trying.Try(() => property.SetValue(ret, property.PropertyType == typeof(DBNull) ? DBNull.Value : value));
                }
                else
                {
                    if (property.PropertyType.IsAssignableFrom(value.GetType()))
                    {
                        Trying.Try(() => property.SetValue(ret, value));
                    }
                    else
                    {
                        if (value is IConvertible && property.PropertyType.IsImplementOf(typeof(IConvertible)))
                        {
                            object val = Convert.ChangeType(value, property.PropertyType);
                            Trying.Try(() => property.SetValue(ret, val));
                        }
                    }
                }
            }
            return ret;
        }

        /// <summary>
        /// 为DataRow的各列赋值
        /// </summary>
        /// <param name="row">要赋值的DataRow对象</param>
        /// <param name="value">要赋的值</param>
        /// <param name="startIndex">赋值列起始位置的索引</param>
        /// <param name="len">从赋值列起始位置的索引开始算，要赋值的列数</param>
        public static void SetValue(this DataRow row, object value, int startIndex, int len)
        {
            int total = row.Table.Columns.Count;
            if (startIndex + 1 > total)
            {
                throw new Exception("赋值起始位置的索引超出了DataTable列的数量。");
            }
            if (len <= 0)
            {
                throw new Exception("赋值的列数必须大于0。");
            }

            int endIndex = Math.Min(total, startIndex + len);
            for (int k = startIndex; k < endIndex; k++)
            {
                row[k] = value;
            }
        }

        /// <summary>
        /// 为DataRow的各列赋值
        /// </summary>
        /// <param name="row">要赋值的DataRow对象</param>
        /// <param name="value">要赋的值</param>
        /// <param name="startIndex">赋值列起始位置的索引</param>
        public static void SetValue(this DataRow row, object value, int startIndex)
        {
            int total = row.Table.Columns.Count;
            if (startIndex + 1 > total)
            {
                throw new Exception("赋值起始位置的索引超出了DataTable列的数量。");
            }

            int endIndex = total;
            for (int k = startIndex; k < endIndex; k++)
            {
                row[k] = value;
            }
        }

        /// <summary>
        /// 为DataRow的各列赋值，从索引0开始赋值
        /// </summary>
        /// <param name="row">要赋值的DataRow对象</param>
        /// <param name="values">要赋的值集合</param>
        public static void SetValues(this DataRow row, IEnumerable<object> values)
        {
            if (values.IsNullOrEmpty()) { return; }
            int total = row.Table.Columns.Count;

            int startIndex = 0;
            int endIndex = Math.Min(total, startIndex + values.Count());

            for (int k = startIndex; k < endIndex; k++)
            {
                row[k] = values.ElementAt(k - startIndex);
            }
        }

        /// <summary>
        /// 为DataRow的各列赋值，从索引0开始赋值
        /// </summary>
        /// <param name="row">要赋值的DataRow对象</param>
        /// <param name="values">要赋的值集合</param>
        /// <param name="startIndex">赋值列起始位置的索引</param>
        public static void SetValues(this DataRow row, IEnumerable<object> values, int startIndex)
        {
            if (values.IsNullOrEmpty()) { return; }
            int total = row.Table.Columns.Count;
            if (startIndex + 1 > total)
            {
                throw new Exception("赋值起始位置的索引超出了DataTable列的数量。");
            }

            int endIndex = Math.Min(total, startIndex + values.Count());

            for (int k = startIndex; k < endIndex; k++)
            {
                row[k] = values.ElementAt(k - startIndex);
            }
        }

        /// <summary>
        /// 为DataRow的各列赋值，从索引0开始赋值
        /// </summary>
        /// <param name="row">要赋值的DataRow对象</param>
        /// <param name="values">要赋的值集合</param>
        /// <param name="startIndex">赋值列起始位置的索引</param>
        public static void SetValues(this DataRow row, IEnumerable<int> values, int startIndex)
        {
            if (values.IsNullOrEmpty()) { return; }
            int total = row.Table.Columns.Count;
            if (startIndex + 1 > total)
            {
                throw new Exception("赋值起始位置的索引超出了DataTable列的数量。");
            }

            int endIndex = Math.Min(total, startIndex + values.Count());

            for (int k = startIndex; k < endIndex; k++)
            {
                row[k] = values.ElementAt(k - startIndex);
            }
        }

        /// <summary>
        /// 为DataRow的各列赋值，从索引0开始赋值
        /// </summary>
        /// <param name="row">要赋值的DataRow对象</param>
        /// <param name="values">要赋的值集合</param>
        /// <param name="startIndex">赋值列起始位置的索引</param>
        public static void SetValues(this DataRow row, IEnumerable<decimal> values, int startIndex)
        {
            if (values.IsNullOrEmpty()) { return; }
            int total = row.Table.Columns.Count;
            if (startIndex + 1 > total)
            {
                throw new Exception("赋值起始位置的索引超出了DataTable列的数量。");
            }

            int endIndex = Math.Min(total, startIndex + values.Count());

            for (int k = startIndex; k < endIndex; k++)
            {
                row[k] = values.ElementAt(k - startIndex);
            }
        }
    }
}

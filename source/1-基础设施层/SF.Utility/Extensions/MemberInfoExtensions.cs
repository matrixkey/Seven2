﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.ComponentModel;
using System.Runtime.Serialization;

namespace SF.Utilities
{
    public static class MemberInfoExtensions
    {
        /// <summary>
        ///  获取成员元数据的Description特性描述信息
        /// </summary>
        /// <param name="member">成员元数据对象</param>
        /// <param name="inherit">是否搜索成员的继承链以查找描述特性</param>
        /// <returns>返回Description特性描述信息，如不存在则返回如不存在则返回空字符串</returns>
        public static string GetDescription(this MemberInfo member, bool inherit = false)
        {
            return GetDescription(member, "", inherit);
        }

        /// <summary>
        ///  获取成员元数据的Description特性描述信息
        /// </summary>
        /// <param name="member">成员元数据对象</param>
        /// <param name="defaultValue">如Description特性不存在则返回指定默认值</param>
        /// <param name="inherit">是否搜索成员的继承链以查找描述特性</param>
        /// <returns>返回Description特性描述信息</returns>
        public static string GetDescription(this MemberInfo member, string defaultValue, bool inherit = false)
        {
            DescriptionAttribute desc = member.GetAttribute<DescriptionAttribute>(inherit);
            return desc == null ? defaultValue : desc.Description;
        }

        /// <summary>
        ///  获取成员元数据的EnumMember特性描述信息
        /// </summary>
        /// <param name="member">成员元数据对象</param>
        /// <param name="inherit">是否搜索成员的继承链以查找描述特性</param>
        /// <returns>返回EnumMember特性信息，如不存在则返回成员的名称</returns>
        public static string GetEnumMember(this MemberInfo member, bool inherit = false)
        {
            System.Runtime.Serialization.EnumMemberAttribute info = member.GetAttribute<System.Runtime.Serialization.EnumMemberAttribute>(inherit);
            return info == null ? null : info.Value;
        }

        /// <summary>
        /// 从类型成员获取指定Attribute特性
        /// </summary>
        /// <typeparam name="T">Attribute特性类型</typeparam>
        /// <param name="memberInfo">成员元数据对象</param>
        /// <param name="inherit">是否从继承中查找</param>
        /// <returns>存在返回第一个，不存在返回null</returns>
        public static T GetAttribute<T>(this MemberInfo memberInfo, bool inherit) where T : Attribute
        {
            return memberInfo.GetAttribute(typeof(T), inherit) as T;
        }

        /// <summary>
        /// 从类型成员获取指定Attribute特性
        /// </summary>
        /// <param name="memberInfo">成员元数据对象</param>
        /// <param name="attributeType">Attribute特性类型</param>
        /// <param name="inherit">是否从继承中查找</param>
        /// <returns>存在返回第一个，不存在返回null</returns>
        public static object GetAttribute(this MemberInfo memberInfo, Type attributeType, bool inherit)
        {
            return memberInfo.GetCustomAttributes(attributeType, inherit).FirstOrDefault();
        }

        /// <summary>
        /// 获取成员中是否存在指定特性
        /// </summary>
        /// <param name="memberInfo">成员元数据对象</param>
        /// <param name="attr">指定特性</param>
        /// <param name="inherit">是否从继承中查找</param>
        /// <returns></returns>
        public static bool HasAttribute(this MemberInfo memberInfo, Type attrType, bool inherit = false)
        {
            return memberInfo.GetCustomAttributes(attrType, inherit).Count() > 0;
        }
    }
}

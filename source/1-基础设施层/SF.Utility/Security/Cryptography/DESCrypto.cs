﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace SF.Security.Cryptography
{
    public static class DESCrypto
    {
        /// <summary>
        /// 系统默认密钥
        /// </summary>
        private readonly static string EncryptKey = "matrixkey";

        #region 根据系统默认密钥对数据进行加密解密

        /// <summary>
        /// 根据系统默认密钥对数据进行加密
        /// </summary>
        /// <param name="original">明文</param>
        /// <returns>密文</returns>
        public static string Encrypt(string original)
        {
            return Encrypt(original, EncryptKey);
        }
        /// <summary>
        /// 根据系统默认密钥对数据进行解密
        /// </summary>
        /// <param name="original">密文</param>
        /// <returns>明文</returns>
        public static string Decrypt(string original)
        {
            return Decrypt(original, EncryptKey, Encoding.Default);
        }

        #endregion

        #region 根据指定密钥（string格式）对数据（string格式）进行加密解密

        /// <summary>
        /// 根据指定密钥（string格式）对数据（string格式）加密
        /// </summary>
        /// <param name="original">原始数据</param>
        /// <param name="key">密钥</param>
        /// <returns>密文</returns>
        public static string Encrypt(string original, string key)
        {
            if (string.IsNullOrWhiteSpace(original)) { return ""; }

            byte[] buff = System.Text.Encoding.Default.GetBytes(original);
            byte[] kb = System.Text.Encoding.Default.GetBytes(key);
            return Convert.ToBase64String(Encrypt(buff, kb));
        }

        /// <summary>
        /// 根据指定密钥（string格式）对密文（string格式）解密
        /// </summary>
        /// <param name="original">密文</param>
        /// <param name="key">密钥</param>
        /// <returns>明文</returns>
        public static string Decrypt(string original, string key)
        {
            return Decrypt(original, key, System.Text.Encoding.Default);
        }

        /// <summary>
        /// 根据指定密钥（string格式）对密文（string格式）解密，返回指定编码的明文
        /// </summary>
        /// <param name="encrypted">密文</param>
        /// <param name="key">密钥</param>
        /// <param name="encoding">字符编码方案</param>
        /// <returns>明文</returns>
        public static string Decrypt(string encrypted, string key, Encoding encoding)
        {
            byte[] buff = Convert.FromBase64String(encrypted);
            byte[] kb = System.Text.Encoding.Default.GetBytes(key);
            return encoding.GetString(Decrypt(buff, kb));
        }

        #endregion

        #region  根据指定密钥（byte[]格式）对数据（byte[]格式）进行加密解密，最终结果为byte[]

        /// <summary>
        /// 生成MD5摘要
        /// </summary>
        /// <param name="original">数据源</param>
        /// <returns>摘要</returns>
        private static byte[] MakeMD5(byte[] original)
        {
            MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
            byte[] keyhash = hashmd5.ComputeHash(original);
            hashmd5 = null;
            return keyhash;
        }

        /// <summary>
        /// 根据指定密钥加密
        /// </summary>
        /// <param name="original">byte[]格式的要加密的明文数据</param>
        /// <param name="key">byte[]格式的密钥明文数据</param>
        /// <returns>byte[]格式的密文</returns>
        private static byte[] Encrypt(byte[] original, byte[] key)
        {
            TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
            des.Key = MakeMD5(key);
            des.Mode = CipherMode.ECB;

            return des.CreateEncryptor().TransformFinalBlock(original, 0, original.Length);
        }

        /// <summary>
        /// 根据指定密钥解密
        /// </summary>
        /// <param name="encrypted">byte[]格式的已加密的密文数据</param>
        /// <param name="key">byte[]格式的密钥明文数据</param>
        /// <returns>byte[]格式的明文</returns>
        private static byte[] Decrypt(byte[] encrypted, byte[] key)
        {
            TripleDESCryptoServiceProvider des = new TripleDESCryptoServiceProvider();
            des.Key = MakeMD5(key);
            des.Mode = CipherMode.ECB;

            return des.CreateDecryptor().TransformFinalBlock(encrypted, 0, encrypted.Length);
        }

        #endregion
    }
}

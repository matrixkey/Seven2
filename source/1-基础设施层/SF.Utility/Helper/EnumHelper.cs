﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Utilities
{
    public static class EnumHelper
    {
        /// <summary>
        /// 获取枚举键值对集合，以Value,Value格式返回 
        /// </summary>
        /// <typeparam name="TEnum">枚举类型</typeparam>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<string, string>> GetEnumItemsOfVV<TEnum>() where TEnum : struct
        {
            CheckTEnum<TEnum>();

            Type enumType = typeof(TEnum);
            IEnumerable<TEnum> values = Enum.GetValues(enumType).Cast<TEnum>();
            IEnumerable<KeyValuePair<string, string>> items =
                values.Select(value => new KeyValuePair<string, string>(value.ToString(), value.ToString()));
            return items;
        }

        /// <summary>
        /// 获取枚举键值对集合，以Value,Description格式返回
        /// </summary>
        /// <typeparam name="TEnum">枚举类型</typeparam>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<string, string>> GetEnumItemsOfVD<TEnum>() where TEnum : struct
        {
            CheckTEnum<TEnum>();

            Type enumType = typeof(TEnum);
            List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>();
            foreach (int key in Enum.GetValues(enumType))
            {
                string Value = Enum.GetName(enumType, key);
                var Field = enumType.GetField(Value);
                string Description = Field.GetDescription();
                string Text = string.IsNullOrWhiteSpace(Description) ? Value : Description;

                items.Add(new KeyValuePair<string, string>(Value, Text));
            }

            return items;
        }

        /// <summary>
        /// 获取枚举键值对集合，以Key,Description格式返回
        /// </summary>
        /// <typeparam name="TEnum">枚举类型</typeparam>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<string, string>> GetEnumItemsOfKD<TEnum>() where TEnum : struct
        {
            CheckTEnum<TEnum>();

            Type enumType = typeof(TEnum);
            List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>();
            foreach (int key in Enum.GetValues(enumType))
            {
                string Value = Enum.GetName(enumType, key);
                var Field = enumType.GetField(Value);
                string Description = Field.GetDescription();
                string Text = string.IsNullOrWhiteSpace(Description) ? Value : Description;

                items.Add(new KeyValuePair<string, string>(key.ToString(), Text));
            }

            return items;
        }

        /// <summary>
        /// 获取枚举键值对集合，以Key,Value格式返回
        /// </summary>
        /// <typeparam name="TEnum">枚举类型</typeparam>
        /// <returns></returns>
        public static IEnumerable<KeyValuePair<string, string>> GetEnumItemsOfKV<TEnum>() where TEnum : struct
        {
            CheckTEnum<TEnum>();

            Type enumType = typeof(TEnum);
            List<KeyValuePair<string, string>> items = new List<KeyValuePair<string, string>>();
            foreach (int key in Enum.GetValues(enumType))
            {
                items.Add(new KeyValuePair<string, string>(key.ToString(), Enum.GetName(enumType, key)));
            }

            return items;
        }

        /// <summary>
        /// 获取text在枚举内的值
        /// </summary>
        /// <typeparam name="TEnum">枚举类型</typeparam>
        /// <param name="enumText">枚举的text值</param>
        /// <returns></returns>
        public static TEnum GetKey<TEnum>(string enumText) where TEnum : struct
        {
            CheckTEnum<TEnum>();

            Type enumType = typeof(TEnum);
            return (TEnum)Enum.Parse(enumType, enumText);
        }

        /// <summary>
        /// 检查类型参数 TEnum 是否是枚举类型，若不是枚举类型则抛出异常
        /// </summary>
        /// <typeparam name="TEnum">要判定的类型参数</typeparam>
        private static void CheckTEnum<TEnum>() where TEnum : struct
        {
            if (!(typeof(TEnum).IsEnum))
            {
                throw new ArgumentException("类型参数 TEnum 必须是枚举类型！");
            }
        }
    }
}

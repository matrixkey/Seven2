﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Utilities
{
    public static class SqlParamsHelper
    {
        /// <summary>
        /// 获取数据库提供程序名称与其其 SQL 脚本环境下的查询参数名称前缀字符的关联关系映射集合。
        /// </summary>
        /// <returns></returns>
        private static Dictionary<string, string> GetParameterTokenMappings()
        {
            Dictionary<string, string> parameterTokenMappings = new Dictionary<string, string>();

            parameterTokenMappings.Add("System.Data.Odbc", "@");
            parameterTokenMappings.Add("System.Data.OleDb", "@");
            parameterTokenMappings.Add("System.Data.OracleClient", ":");
            parameterTokenMappings.Add("System.Data.SqlClient", "@");
            parameterTokenMappings.Add("System.Data.SqlServerCe.3.5", "@");
            parameterTokenMappings.Add("System.Data.SqlServerCe.4.0", "@");
            parameterTokenMappings.Add("MySql.Data.MySqlClient", "@");
            parameterTokenMappings.Add("Oracle.ManagedDataAccess.Client", "@");
            parameterTokenMappings.Add("IBM.Data.DB2", "@");

            return parameterTokenMappings;
        }

        /// <summary>
        /// In 参数中的元素嵌套间隔标记字符
        /// </summary>
        private static string InParameterNestSplitToken { get { return "'"; } }

        /// <summary>
        /// 将参数值集合转换成In参数可识别的字符串，如'a','b','c'
        /// </summary>
        /// <param name="ps">参数值集合</param>
        /// <param name="trim">是否移除两边的嵌套间隔标记字符</param>
        /// <returns>In参数可识别的字符串</returns>
        public static string BuildInParam(IEnumerable<string> ps, bool trim = false)
        {
            string result = (InParameterNestSplitToken + string.Join(InParameterNestSplitToken + "," + InParameterNestSplitToken, ps) + InParameterNestSplitToken);
            if (trim)
            { result = result.Trim(InParameterNestSplitToken[0]); }

            return result;
        }
    }
}

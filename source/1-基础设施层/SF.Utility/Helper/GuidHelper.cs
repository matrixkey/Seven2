﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Utilities
{
    public static class GuidHelper
    {
        /// <summary>
        /// 获取新的guid
        /// </summary>
        /// <returns></returns>
        public static Guid GetNewGuid()
        {
            return Guid.NewGuid();
        }

        /// <summary>
        /// 获取特定格式的新Guid
        /// </summary>
        /// <param name="format">guid格式化方式。N表示32位无其他符号格式；D表示由连字符“-”分隔的32位数字；B表示括在大括号中、由连字符“-”分隔的32位数字；P表示括在圆括号中、由连字符“-”分隔的32位数字。默认为N格式。</param>
        /// <returns></returns>
        public static string GetNewFormatGuid(string format = "N")
        {
            if (!new string[] { "N", "D", "B", "P" }.Contains(format))
            {
                format = "N";
            }
            return GetNewGuid().ToString("N");
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Utilities
{
    public static class MvcUrlHelper
    {
        /// <summary>
        /// 根据指定的 控制器名称 和 行为名称 组装成可用的URL
        /// </summary>
        /// <param name="controller">控制器名称</param>
        /// <param name="action">动作名称</param>
        /// <returns>URL</returns>
        public static string GetRealUrl(string controller, string action)
        {
            return GetRealUrl("", controller, action);
        }

        /// <summary>
        /// 根据指定的 控制器名称 和 行为名称 组装成可用的URL
        /// </summary>
        /// <param name="area">区域名称</param>
        /// <param name="controller">控制器名称</param>
        /// <param name="action">动作名称</param>
        /// <returns>URL</returns>
        public static string GetRealUrl(string area, string controller, string action)
        {
            if (string.IsNullOrWhiteSpace(controller) || string.IsNullOrWhiteSpace(action)) { return string.Empty; }

            return (string.IsNullOrWhiteSpace(area) ? "" : "/" + area) + "/" + controller + "/" + action;
        }
    }
}

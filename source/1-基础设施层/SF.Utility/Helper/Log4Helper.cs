﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using log4net;
using log4net.Config;

namespace SF.Utilities
{
    /// <summary>
    /// 日志记录工具
    /// </summary>
    public static class Log4Helper
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        static Log4Helper()
        {
            XmlConfigurator.ConfigureAndWatch(new FileInfo(AppDomain.CurrentDomain.SetupInformation.ApplicationBase + @"\log4net.config"));
        }

        /// <summary>
        /// 记录Info日志
        /// </summary>
        /// <param name="msg">消息</param>
        public static void Info(string msg)
        {
            LogManager.GetLogger("系统消息").Info(msg);
        }

        /// <summary>
        /// 记录Info日志
        /// </summary>
        /// <param name="obj">某个类对象</param>
        /// <param name="msg">消息</param>
        public static void Info(object obj, string msg)
        {
            LogManager.GetLogger(obj.GetType()).Info(msg);
        }

        /// <summary>
        /// 记录Error日志
        /// </summary>
        /// <param name="msg">消息</param>
        public static void Error(string msg)
        {
            LogManager.GetLogger("系统错误").Error(msg);
        }

        /// <summary>
        /// 记录Error日志
        /// </summary>
        /// <param name="ex">异常</param>
        /// <param name="msg">消息</param>
        public static void Error(Exception ex, string msg)
        {
            LogManager.GetLogger("系统错误").Error(msg, ex);
        }

        /// <summary>
        /// 记录Error日志
        /// </summary>
        /// <param name="obj">某个类对象</param>
        /// <param name="msg">消息</param>
        public static void Error(object obj, string msg)
        {
            LogManager.GetLogger(obj.GetType()).Error(msg);
        }

        /// <summary>
        /// 记录Error日志
        /// </summary>
        /// <param name="obj">某个类对象</param>
        /// <param name="ex">异常</param>
        public static void Error(object obj, Exception ex)
        {
            LogManager.GetLogger(obj.GetType()).Error(ex);
        }

        /// <summary>
        /// 记录Error日志
        /// </summary>
        /// <param name="obj">某个类对象</param>
        /// <param name="ex">异常</param>
        /// <param name="msg">消息</param>
        public static void Error(object obj, Exception ex, string msg)
        {
            LogManager.GetLogger(obj.GetType()).Error(msg, ex);
        }
    }
}

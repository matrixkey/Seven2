﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SF.Web.Mvc.UI.EasyUI
{
    public class EasyUiFactory : UiFactory
    {
        public EasyUiFactory(HtmlHelper htmlHelper)
            : base(htmlHelper)
        { }

        public static new EasyUiFactory Create(HtmlHelper htmlHelper)
        {
            return new EasyUiFactory(htmlHelper);
        }
    }

    public class EasyUiFactory<TModel> : EasyUiFactory
    {
        public EasyUiFactory(HtmlHelper<TModel> htmlHelper)
            : base(htmlHelper)
        { }

        public new HtmlHelper<TModel> Html
        {
            get { return base.Html as HtmlHelper<TModel>; }
            protected internal set { base.Html = value; }
        }

        public static EasyUiFactory<TModel> Create(HtmlHelper<TModel> htmlHelper)
        {
            return new EasyUiFactory<TModel>(htmlHelper);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Utilities;

namespace SF.Web.Mvc.UI.EasyUI
{
    public static class EasyUiTypeUtils
    {
        public static EasyUiTypeTagTextAttribute GetTagTextAttribute(this EasyUiType uiType)
        {
            return uiType.GetCustomeAttributes<EasyUiTypeTagTextAttribute>().FirstOrDefault();
        }

        public static string GetTagName(this EasyUiType uiType)
        {
            var attr = uiType.GetTagTextAttribute();
            return attr == null ? null : attr.TagName;
        }

        public static string GetHtmlClass(this EasyUiType uiType)
        {
            var attr = uiType.GetTagTextAttribute();
            return attr == null ? null : attr.Class;
        }

        public static bool IsFormControl(this EasyUiType uiType)
        {
            var attr = uiType.GetTagTextAttribute();
            return attr == null ? false : attr.IsFormControl;
        }

        public static bool ValueToInnerHtml(this EasyUiType uiType)
        {
            var attr = uiType.GetTagTextAttribute();
            return attr == null ? false : attr.ValueToInnerHtml;
        }

        public static bool GetInitializable(this EasyUiType uiType)
        {
            var attr = uiType.GetTagTextAttribute();
            return attr == null ? EasyUiTypeTagTextAttribute.DefaultInitializable : attr.Initializable;
        }

        public static string GetInputType(this EasyUiType uiType)
        {
            var attr = uiType.GetTagTextAttribute();
            return attr == null ? null : attr.InputType;
        }

        public static string[] GetDefaultClass(this EasyUiType uiType)
        {
            List<string> list = new List<string>();
            var attr = uiType.GetTagTextAttribute();
            var defaultClass = attr == null ? null : attr.DefaultClass;
            if (!string.IsNullOrWhiteSpace(defaultClass))
            {
                var array = defaultClass.Split(new string[] { }, StringSplitOptions.RemoveEmptyEntries);
                list.AddRange(array);
            }
            return list.Distinct().ToArray();
        }
    }
}

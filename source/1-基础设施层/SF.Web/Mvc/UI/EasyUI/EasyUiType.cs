﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Web.Mvc.UI.EasyUI
{
    /// <summary>
    /// 用于表示 jquery-easyui 的 HTML 控件类型。
    /// </summary>
    public enum EasyUiType
    {
        [EasyUiTypeTagText(TagName = "div", Class = "easyui-draggable")]
        Draggable = 0,

        [EasyUiTypeTagText(TagName = "div", Class = "easyui-droppable")]
        Droppable,

        [EasyUiTypeTagText(TagName = "div", Class = "easyui-resizable")]
        Resizable,

        [EasyUiTypeTagText(TagName = "div", Class = "easyui-pagination")]
        Pagination,

        [EasyUiTypeTagText(TagName = "input", Class = "easyui-searchbox")]
        SearchBox,

        [EasyUiTypeTagText(TagName = "div", Class = "easyui-progressbar")]
        ProgressBar,

        [EasyUiTypeTagText(TagName = "span", Class = "easyui-tooltip")]
        Tooltip,



        [EasyUiTypeTagText(TagName = "div", Class = "easyui-panel")]
        Panel,

        [EasyUiTypeTagText(TagName = "div", Class = "easyui-tabs")]
        Tabs,

        [EasyUiTypeTagText(TagName = "div", Class = "easyui-accordion")]
        Accordion,

        [EasyUiTypeTagText(TagName = "div", Class = "easyui-layout")]
        Layout,



        [EasyUiTypeTagText(TagName = "div", Class = "easyui-menu")]
        Menu,

        [EasyUiTypeTagText(TagName = "a", Class = "easyui-linkbutton")]
        LinkButton,

        [EasyUiTypeTagText(TagName = "a", Class = "easyui-menubutton")]
        MenuButton,

        [EasyUiTypeTagText(TagName = "a", Class = "easyui-splitbutton")]
        SplitButton,



        [EasyUiTypeTagText(TagName = "div", Class = "easyui-form")]
        Form,

        [EasyUiTypeTagText(TagName = "input", Class = "easyui-validatebox", IsFormControl = true, DefaultClass = "textbox")]
        ValidateBox,

        [EasyUiTypeTagText(TagName = "input", Class = "easyui-combo", Initializable = false, IsFormControl = true)]
        Combo,

        [EasyUiTypeTagText(TagName = "input", Class = "easyui-combobox", IsFormControl = true)]
        ComboBox,

        [EasyUiTypeTagText(TagName = "input", Class = "easyui-combotree", IsFormControl = true)]
        ComboTree,

        [EasyUiTypeTagText(TagName = "input", Class = "easyui-combogrid", IsFormControl = true)]
        ComboGrid,

        [EasyUiTypeTagText(TagName = "input", Class = "easyui-numberbox", IsFormControl = true)]
        NumberBox,

        [EasyUiTypeTagText(TagName = "input", Class = "easyui-datebox", IsFormControl = true)]
        DateBox,

        [EasyUiTypeTagText(TagName = "input", Class = "easyui-datetimebox", IsFormControl = true)]
        DateTimeBox,

        [EasyUiTypeTagText(TagName = "div", Class = "easyui-calendar", IsFormControl = true)]
        Calendar,

        [EasyUiTypeTagText(TagName = "input", Class = "easyui-spinner", Initializable = false, IsFormControl = true)]
        Spinner,

        [EasyUiTypeTagText(TagName = "input", Class = "easyui-numberspinner", IsFormControl = true)]
        NumberSpinner,

        [EasyUiTypeTagText(TagName = "input", Class = "easyui-timespinner", IsFormControl = true)]
        TimeSpinner,

        [EasyUiTypeTagText(TagName = "div", Class = "easyui-slider", IsFormControl = true)]
        Slider,



        [EasyUiTypeTagText(TagName = "div", Class = "easyui-window")]
        Window,

        [EasyUiTypeTagText(TagName = "div", Class = "easyui-dialog")]
        Dialog,



        [EasyUiTypeTagText(TagName = "table", Class = "easyui-datagrid")]
        DataGrid,

        [EasyUiTypeTagText(TagName = "table", Class = "easyui-propertygrid")]
        PropertyGrid,

        [EasyUiTypeTagText(TagName = "table", Class = "easyui-tree")]
        Tree,

        [EasyUiTypeTagText(TagName = "table", Class = "easyui-treegrid")]
        TreeGrid,


        [EasyUiTypeTagText(TagName = "div", Class = "easyui-portal")]
        Portal,

        [EasyUiTypeTagText(TagName = "div", Class = "easyui-toolbar")]
        Toolbar,



        [EasyUiTypeTagText(TagName = "textarea", Class = "easyui-codemirror", IsFormControl = true, ValueToInnerHtml = true)]
        CodeMirror,

        [EasyUiTypeTagText(TagName = "input", Class = "easyui-comboicons", IsFormControl = true)]
        ComboIcons,

        [EasyUiTypeTagText(TagName = "input", Class = "easyui-comboselector", IsFormControl = true)]
        ComboSelector,

        [EasyUiTypeTagText(TagName = "input", Class = "easyui-euploadify", IsFormControl = true)]
        EUploadify,

        [EasyUiTypeTagText(TagName = "input", Class = "easyui-my97", IsFormControl = true)]
        My97,

        [EasyUiTypeTagText(TagName = "textarea", Class = "easyui-ueditor", IsFormControl = true, ValueToInnerHtml = true)]
        UEditor,
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Web.Mvc.UI.EasyUI;

namespace System.Web.Mvc
{
    public static class EasyUiExtensions
    {
        public static EasyUiFactory EasyUi(this HtmlHelper htmlHelper)
        {
            return EasyUiFactory.Create(htmlHelper);
        }

        public static EasyUiFactory<TModel> EasyUi<TModel>(this HtmlHelper<TModel> htmlHelper)
        {
            return EasyUiFactory<TModel>.Create(htmlHelper);
        }
    }
}

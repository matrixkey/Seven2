﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

using SF.Web.Mvc.UI.EasyUI;

namespace System.Web.Mvc
{
    public static class ValidateboxExtensions
    {
        public static MvcHtmlString ValidateBox(this EasyUiFactory factory, string name)
        {
            return ValidateBoxHelper(factory, null, name, null, null, null);
        }

        public static MvcHtmlString ValidateBox(this EasyUiFactory factory, string name, object value)
        {
            return ValidateBoxHelper(factory, null, name, value, null, null);
        }

        public static MvcHtmlString ValidateBox(this EasyUiFactory factory, string name, object value, object dataOptions)
        {
            return ValidateBoxHelper(factory, null, name, value, dataOptions, null);
        }

        public static MvcHtmlString ValidateBox(this EasyUiFactory factory, string name, object value, object dataOptions, object htmlAttributes)
        {
            return ValidateBoxHelper(factory, null, name, value, dataOptions, htmlAttributes);
        }

        public static MvcHtmlString ValidateBoxFor<TModel, TProperty>(this EasyUiFactory<TModel> factory, Expression<Func<TModel, TProperty>> expression)
        {
            return ValidateBoxFor(factory, expression, null, null);
        }

        public static MvcHtmlString ValidateBoxFor<TModel, TProperty>(this EasyUiFactory<TModel> factory, Expression<Func<TModel, TProperty>> expression, object dataOptions)
        {
            return ValidateBoxFor(factory, expression, dataOptions, null);
        }

        public static MvcHtmlString ValidateBoxFor<TModel, TProperty>(this EasyUiFactory<TModel> factory, Expression<Func<TModel, TProperty>> expression, object dataOptions, object htmlAttributes)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, factory.Html.ViewData);
            string name = ExpressionHelper.GetExpressionText(expression);
            return ValidateBoxHelper(factory,
                                     metadata,
                                     name,
                                     metadata.Model,
                                     dataOptions,
                                     htmlAttributes);
        }



        private static MvcHtmlString ValidateBoxHelper(this EasyUiFactory factory, ModelMetadata metadata, string name, object value, object dataOptions, object htmlAttributes)
        {
            return factory.EasyUiHelper(EasyUiType.ValidateBox, metadata, name, value, false, dataOptions, htmlAttributes);
        }
    }
}

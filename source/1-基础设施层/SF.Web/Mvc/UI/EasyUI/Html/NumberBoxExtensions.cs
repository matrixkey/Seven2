﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

using SF.Web.Mvc.UI.EasyUI;

namespace System.Web.Mvc
{
    public static class NumberBoxExtensions
    {
        public static MvcHtmlString NumberBox(this EasyUiFactory factory, string name)
        {
            return NumberBoxHelper(factory, null, name, null, null, null);
        }

        public static MvcHtmlString NumberBox(this EasyUiFactory factory, string name, object value)
        {
            return NumberBoxHelper(factory, null, name, value, null, null);
        }

        public static MvcHtmlString NumberBox(this EasyUiFactory factory, string name, object value, object dataOptions)
        {
            return NumberBoxHelper(factory, null, name, value, dataOptions, null);
        }

        public static MvcHtmlString NumberBox(this EasyUiFactory factory, string name, object value, object dataOptions, object htmlAttributes)
        {
            return NumberBoxHelper(factory, null, name, value, dataOptions, htmlAttributes);
        }




        public static MvcHtmlString NumberBoxFor<TModel, TProperty>(this EasyUiFactory<TModel> factory, Expression<Func<TModel, TProperty>> expression)
        {
            return NumberBoxFor(factory, expression, null, null);
        }

        public static MvcHtmlString NumberBoxFor<TModel, TProperty>(this EasyUiFactory<TModel> factory, Expression<Func<TModel, TProperty>> expression, object dataOptions)
        {
            return NumberBoxFor(factory, expression, dataOptions, null);
        }

        public static MvcHtmlString NumberBoxFor<TModel, TProperty>(this EasyUiFactory<TModel> factory, Expression<Func<TModel, TProperty>> expression, object dataOptions, object htmlAttributes)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, factory.Html.ViewData);
            string name = ExpressionHelper.GetExpressionText(expression);
            return NumberBoxHelper(factory,
                                     metadata,
                                     name,
                                     metadata.Model,
                                     dataOptions,
                                     htmlAttributes);
        }



        private static MvcHtmlString NumberBoxHelper(this EasyUiFactory factory, ModelMetadata metadata, string name, object value, object dataOptions, object htmlAttributes)
        {
            return factory.EasyUiHelper(EasyUiType.NumberBox, metadata, name, value, false, dataOptions, htmlAttributes);
        }
    }
}

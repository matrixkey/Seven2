﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Web.Mvc.UI.EasyUI
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
    public class EasyUiTypeTagTextAttribute : Attribute
    {
        public static bool DefaultInitializable
        {
            get { return true; }
        }

        public static string DefaultInputType
        {
            get { return "text"; }
        }


        public EasyUiTypeTagTextAttribute()
        {
            this.Initializable = DefaultInitializable;
            this.InputType = DefaultInputType;
        }


        /// <summary>
        /// 指示该 jquery-easyui 控件所使用的 HTML 标签名称。
        /// </summary>
        public string TagName { get; set; }

        /// <summary>
        /// 指示该 jquery-easyui 控件所使用的 HTML 标签 class 属性。
        /// </summary>
        public string Class { get; set; }

        /// <summary>
        /// 指示该 jquery-easyui 控件的 HTML Input Type 值；
        /// 仅当该控件的的 HTML 标签为 INPUT 时，该属性才生效。
        /// </summary>
        public string InputType { get; set; }

        /// <summary>
        /// 指示该 jquery-easyui 控件是否是表单输入控件(表单输入控件兼容 easyui-form)。
        /// </summary>
        public bool IsFormControl { get; set; }

        /// <summary>
        /// 指示该 jquery-easyui 控件是否自动将其 Value 属性赋值给 HTML 标签的 InnerHtml 中。
        /// </summary>
        public bool ValueToInnerHtml { get; set; }

        /// <summary>
        /// 指示 jquery-easyui 是否能在页面加载完成时自动初始化。
        /// </summary>
        public bool Initializable { get; set; }



        public string DefaultClass { get; set; }

        public object DefaultOptions { get; set; }
    }
}

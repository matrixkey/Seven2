﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SF.Web.Mvc.UI
{
    public class UiFactory
    {
        public UiFactory(HtmlHelper htmlHelper)
        {
            this.Html = htmlHelper;
        }


        public virtual HtmlHelper Html
        {
            get;
            protected internal set;
        }


        public static UiFactory Create(HtmlHelper htmlHelper)
        {
            return new UiFactory(htmlHelper);
        }
    }

    public class UiFactory<TModel> : UiFactory
    {
        public UiFactory(HtmlHelper<TModel> htmlHelper)
            : base(htmlHelper)
        { }

        public new HtmlHelper<TModel> Html
        {
            get { return base.Html as HtmlHelper<TModel>; }
            protected internal set { base.Html = value; }
        }

        public static UiFactory<TModel> Create(HtmlHelper<TModel> htmlHelper)
        {
            return new UiFactory<TModel>(htmlHelper);
        }
    }
}

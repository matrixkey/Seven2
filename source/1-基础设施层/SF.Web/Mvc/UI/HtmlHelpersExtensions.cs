﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Web.Mvc.UI;

namespace System.Web.Mvc
{
    public static class HtmlHelpersExtensions
    {
        public static UiFactory UI(this HtmlHelper htmlHelper)
        {
            return UiFactory.Create(htmlHelper);
        }

        public static UiFactory<TModel> UI<TModel>(this HtmlHelper<TModel> htmlHelper)
        {
            return UiFactory<TModel>.Create(htmlHelper);
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace System.Web
{
    /// <summary>
    /// 为常见的 HTML 解析操作提供一组工具方法。
    /// </summary>
    public static class HtmlUtils
    {
        private static IEnumerable<string> selfClosingTags;

        static HtmlUtils()
        {
            if (HtmlUtils.selfClosingTags == null)
            {
                var list = new List<string>();
                list.Add("input");
                HtmlUtils.selfClosingTags = list;
            }
        }

        /// <summary>
        /// 获取一个序列，序列中的每项都表示一个自结束 HTML 标记名字符串。
        /// </summary>
        public static IEnumerable<string> SelfClosingTags
        {
            get { return HtmlUtils.selfClosingTags; }
        }


        /// <summary>
        /// 判断指定的 HTML 标记是否为自结束标记。
        /// </summary>
        /// <param name="tagName"></param>
        /// <returns></returns>
        public static bool IsSelfClosingTag(string tagName)
        {
            if (string.IsNullOrWhiteSpace(tagName)) { return false; }
            return HtmlUtils.SelfClosingTags.Contains(tagName, StringComparer.CurrentCultureIgnoreCase);
        }
    }
}

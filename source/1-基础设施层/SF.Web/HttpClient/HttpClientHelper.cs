﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;

namespace SF.Web.Utilities
{
    public class HttpClientHelper
    {
        /// <summary>
        /// 用指定的基url初始化HttpClient对象
        /// </summary>
        /// <returns></returns>
        private HttpClient ClientInit(string urlBase)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri(urlBase);
            // 为JSON格式添加一个Accept报头
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return client;
        }

        public T Test1<T>(object param)
        {
            HttpClient client = ClientInit("http://localhost:57252/");
            HttpResponseMessage response = client.PostAsJsonAsync("/api/Account/NormalLogin", param).Result;

            if (response.IsSuccessStatusCode)
            {
                var info = response.Content.ReadContentAsString();
                T obj = SF.Utilities.Serialization.ToObject<T>(info);

                return obj;
            }
            else
            {
                var s1 = response.StatusCode;
                var s2 = response.ReasonPhrase;
                throw SF.Utilities.ExceptionHelper.ThrowBusinessException("WebApi访问异常。");
            }
        }
    }
}

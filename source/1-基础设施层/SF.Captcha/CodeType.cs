﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Captcha
{
    /// <summary>
    /// 验证码类型
    /// </summary>
    public enum EnumCodeType
    {
        /// <summary>
        /// 纯数字
        /// </summary>
        onlyNumber,

        /// <summary>
        /// 纯字母
        /// </summary>
        onlyLetter,

        /// <summary>
        /// 数字结合字母
        /// </summary>
        both
    }
}

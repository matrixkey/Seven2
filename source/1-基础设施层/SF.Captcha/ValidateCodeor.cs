﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Captcha
{
    public static class ValidateCodeor
    {
        /// <summary>
        /// 生成验证码，将验证码图片输出到页面，并返回验证码字符串
        /// </summary>
        /// <param name="codeLength">验证码长度</param>
        /// <returns></returns>
        public static string VerifyCode(int codeLength)
        {
            ValidateCode vc = new ValidateCode();
            vc.Length = codeLength;

            string code = vc.CreateVerifyCode();

            vc.CreateImageOnPage(code);

            return code;
        }

        /// <summary>
        /// 生成验证码，将验证码图片输出到页面，并返回验证码字符串
        /// </summary>
        /// <param name="codeLength">验证码长度</param>
        /// <param name="codetype">验证码类型</param>
        /// <returns></returns>
        public static string VerifyCode(int codeLength, EnumCodeType codetype)
        {
            ValidateCode vc = new ValidateCode();
            vc.Length = codeLength;
            vc.CodeType = codetype;

            string code = vc.CreateVerifyCode();

            vc.CreateImageOnPage(code);

            return code;
        }

        /// <summary>
        /// 生成验证码，将验证码图片输出到页面，并返回验证码字符串
        /// </summary>
        /// <param name="codeLength">验证码长度</param>
        /// <param name="codetype">验证码类型</param>
        /// <param name="isTwist">是否使用扭曲效果</param>
        /// <returns></returns>
        public static string VerifyCode(int codeLength, EnumCodeType codetype, bool isTwist)
        {
            ValidateCode vc = new ValidateCode();
            vc.Length = codeLength;
            vc.CodeType = codetype;
            vc.IsTwist = isTwist;

            string code = vc.CreateVerifyCode();

            vc.CreateImageOnPage(code);

            return code;
        }

        /// <summary>
        /// 生成验证码，将验证码图片输出到页面，并返回验证码字符串
        /// </summary>
        /// <param name="codeLength">验证码长度</param>
        /// <param name="codetype">验证码类型</param>
        /// <param name="isTwist">是否使用扭曲效果</param>
        /// <param name="fontSize">验证码字体大小</param>
        /// <returns></returns>
        public static string VerifyCode(int codeLength, EnumCodeType codetype, bool isTwist, int fontSize)
        {
            ValidateCode vc = new ValidateCode();
            vc.Length = codeLength;
            vc.CodeType = codetype;
            vc.IsTwist = isTwist;
            vc.FontSize = fontSize;

            string code = vc.CreateVerifyCode();

            vc.CreateImageOnPage(code);

            return code;
        }

        /// <summary>
        /// 生成验证码，将验证码图片输出到页面，并返回验证码字符串
        /// </summary>
        /// <param name="codeLength">验证码长度</param>
        /// <param name="codetype">验证码类型</param>
        /// <param name="isTwist">是否使用扭曲效果</param>
        /// <param name="fontSize">验证码字体大小</param>
        /// <param name="padding">边框补的像素大小</param>
        /// <returns></returns>
        public static string VerifyCode(int codeLength, EnumCodeType codetype, bool isTwist, int fontSize, int padding)
        {
            ValidateCode vc = new ValidateCode();
            vc.Length = codeLength;
            vc.CodeType = codetype;
            vc.IsTwist = isTwist;
            vc.FontSize = fontSize;
            vc.Padding = padding;

            string code = vc.CreateVerifyCode();

            vc.CreateImageOnPage(code);

            return code;
        }

        /// <summary>
        /// 生成验证码，将验证码图片输出到页面，并返回验证码字符串
        /// </summary>
        /// <param name="codeLength">验证码长度</param>
        /// <param name="codetype">验证码类型</param>
        /// <param name="isTwist">是否使用扭曲效果</param>
        /// <param name="fontSize">验证码字体大小</param>
        /// <param name="padding">边框补的像素大小</param>
        /// <param name="chaos">是否输出燥点</param>
        /// <returns></returns>
        public static string VerifyCode(int codeLength, EnumCodeType codetype,bool isTwist, int fontSize, int padding, bool chaos)
        {
            ValidateCode vc = new ValidateCode();
            vc.Length = codeLength;
            vc.CodeType = codetype;
            vc.IsTwist = isTwist;
            vc.FontSize = fontSize;
            vc.Padding = padding;
            vc.Chaos = chaos;

            string code = vc.CreateVerifyCode();

            vc.CreateImageOnPage(code);

            return code;
        }
    }
}

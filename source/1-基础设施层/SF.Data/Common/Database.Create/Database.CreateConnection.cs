﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;
using SF.Data.EnterpriseLibrary;

namespace SF.Data.Common
{
    public abstract partial class Database
    {
        /// <summary>
        /// 创建并返回与当前 <see cref="DbProviderFactory"/> 关联的 <see cref="DbConnection"/> 对象。
        /// </summary>
        /// <returns>与当前 <see cref="DbProviderFactory"/> 关联的 <see cref="DbConnection"/> 对象。</returns>
        public virtual DbConnection CreateConnection()
        {
            return DatabaseExtensions.CreateConnection(this.PrimitiveDatabase);
        }
    }
}

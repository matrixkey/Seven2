﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Data.Common;

namespace SF.Data.Generic
{
    /// <summary>
    /// 定义解析普通 SQL(适用于 Access 等 OleDb 接口数据库) 脚本中参数名称列表的方法。
    /// </summary>
    [DbProvider("System.Data.OleDb")]
    public class GenericScriptParameterParser : DbScriptParameterParser
    {
        /// <summary>
        /// 以 <paramref name="providerName"/> 作为数据库提供程序名称初始化 <see cref="GenericScriptParameterParser"/> 类型的实例。
        /// <remarks>既用 <see cref="SF.Data.Common.DbProvider"/> 特性设置了默认的数据库提供程序名称，又提供“以给定数据库提供程序名称初始化”的构造函数，其原因是 <see cref="GenericScriptParameterParser"/> 的实现还可以支持其他的数据源类型，如Obdc、sqlite。</remarks>
        /// </summary>
        /// <param name="providerName">数据库提供程序名称。</param>
        public GenericScriptParameterParser(string providerName) : base(providerName) { }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Data.Test.EF;

namespace SF.Data.Test.Common
{
    public static class EntityHelper
    {
        public static IEnumerable<Role> GetRoles(int count)
        {
            List<Role> r = new List<Role>();

            for (int k = 0; k < count; k++)
            {
                r.Add(new Role { Name = "角色-" + count, Level = k });
            }

            return r;
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data;

using SF.Data.Sql;
using SF.Data.Common;

namespace SF.Data.Test
{
    [TestClass]
    public class SqlServerDatabase
    {
        /// <summary>
        /// 以指定数据库连接字符串的方式连接sqlserver数据库，并获取数据填充到DataTable
        /// </summary>
        [TestMethod]
        public void TestMethod1()
        {
            SqlDatabase database = new SqlDatabase(@"server=MATRIXKEY\MATRIXKEYSQL2012;database=NDF-Test1;uid=sa;password=1;");
            DataTable table = database.ExecuteDataTable("SELECT top 5 * FROM dbo.Roles");
            foreach (DataRow row in table.Rows)
            {
                Console.WriteLine(row["Name"]);
            }
        }

        /// <summary>
        /// 以指定数据库连接字符串名称的方式连接数据库，并获取数据填充到DataTable
        /// </summary>
        [TestMethod]
        public void TestMethod2()
        {
            Database database = DatabaseFactories.CreateDatabase("matrixkey");
            DataTable table = database.ExecuteDataTable("SELECT top 10 * FROM dbo.Roles WHERE Name LIKE N'%' + @name + N'%'", "角色1");
            foreach (DataRow row in table.Rows)
            {
                Console.WriteLine(row["Name"]);
            }

            DataTable table2 = database.ExecuteDataTable("SELECT top 10 * FROM dbo.Roles WHERE Name LIKE @name", "%角色1%");
            foreach (DataRow row in table2.Rows)
            {
                Console.WriteLine(row["Name"]);
            }
        }

        /// <summary>
        /// 以配置文件中最后一项数据库连接信息的方式连接数据库，并获取数据填充到DataTable
        /// </summary>
        [TestMethod]
        public void TestMethod3()
        {
            Database database = DatabaseFactory.Default.CreateDatabase();
            DataTable table = database.ExecuteDataTable("SELECT top 5 * FROM dbo.Roles");
            foreach (DataRow row in table.Rows)
            {
                Console.WriteLine(row["Name"]);
            }
        }

        /// <summary>
        /// 以指定数据库连接字符串名称的方式连接sqlServer数据库，并获取数据填充到DataTable
        /// </summary>
        [TestMethod]
        public void TestMethod4()
        {
            Database database = new SqlDatabaseFactory().CreateDatabaseByName("matrixkey");
            DataTable table = database.ExecuteDataTable("SELECT top 10 * FROM dbo.Roles WHERE Name LIKE N'%' + @name + N'%'", "角色1");
            foreach (DataRow row in table.Rows)
            {
                Console.WriteLine(row["Name"]);
            }
        }
    }
}

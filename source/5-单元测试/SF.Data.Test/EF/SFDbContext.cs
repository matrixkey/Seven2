﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using SF.Data.Entity.Migrations;

namespace SF.Data.Test.EF
{
    public class SFDbContext:SF.Data.Entity.DbContext
    {
        public SFDbContext():this("matrixkey")
        { }

        public SFDbContext(string connectionStringOrName):base(connectionStringOrName)
        { }

        public virtual DbSet<Role> Roles { get; set; }

        protected override void Initialize()
        {
            Database.SetInitializer<SFDbContext>(new MigrationDatabaseIfModelChanges<SFDbContext, MigrationConfiguration<SFDbContext>>());
        }
    }
}

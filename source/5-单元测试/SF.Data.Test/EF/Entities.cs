﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Data.Test.EF
{
    public class Role : SF.Framework.EntityBaseModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public int Level { get; set; }

        public string Code { get; set; }
    }
}

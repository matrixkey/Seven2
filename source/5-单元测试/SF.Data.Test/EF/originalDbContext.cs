﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace SF.Data.Test.EF
{
    public class OriginalDbContext : System.Data.Entity.DbContext
    {
        public OriginalDbContext()
            : this("matrixkey")
        { }

        public OriginalDbContext(string connectionStringOrName)
            : base(connectionStringOrName)
        { }

        public virtual DbSet<Role> Roles { get; set; }
    }
}

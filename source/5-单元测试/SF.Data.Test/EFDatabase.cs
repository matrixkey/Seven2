﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using System.Linq;

using SF.Data.Entity;
using SF.Data.Test.EF;
using SF.Data.Test.Common;

namespace SF.Data.Test
{
    [TestClass]
    public class EFDatabase
    {
        [TestMethod]
        public void TestMethod1()
        {
            Stopwatch sw = new Stopwatch(); string msg = string.Empty; Stopwatch sw1 = new Stopwatch();
            sw.Start(); sw1.Start();
            using (var db = new OriginalDbContext())
            {
                sw.Stop();
                msg += "完成TheDataContext初始化，花费时间：" + sw.ElapsedMilliseconds + "毫秒。";

                sw.Restart();
                int count = 10000;
                var roles = EntityHelper.GetRoles(count);
                sw.Stop();
                msg += "完成" + count + "条数据组装，花费时间：" + sw.ElapsedMilliseconds + "毫秒。";

                sw.Restart();
                db.Roles.AddRange(roles);

                db.SaveChanges();

                sw.Stop();

                msg += "完成" + count + "条数据插入，花费时间：" + sw.ElapsedMilliseconds + "毫秒。";
            }
            sw1.Stop();
            Console.WriteLine(msg);
            Console.WriteLine("总共花费时间：" + sw1.ElapsedMilliseconds + "毫秒。");
        }

        [TestMethod]
        public void TestMethod2()
        {
            Stopwatch sw = new Stopwatch(); string msg = string.Empty; Stopwatch sw1 = new Stopwatch();
            sw.Start(); sw1.Start();
            using (var db = new SFDbContext())
            {
                sw.Stop();
                msg += "完成TheDataContext初始化，花费时间：" + sw.ElapsedMilliseconds + "毫秒。";

                sw.Restart();
                int count = 10000;
                var roles = EntityHelper.GetRoles(count);
                sw.Stop();
                msg += "完成" + count + "条数据组装，花费时间：" + sw.ElapsedMilliseconds + "毫秒。";

                sw.Restart();
                db.Roles.AddRange(roles);

                db.SaveChanges();

                sw.Stop();

                msg += "完成" + count + "条数据插入，花费时间：" + sw.ElapsedMilliseconds + "毫秒。";
            }
            sw1.Stop();
            Console.WriteLine(msg);
            Console.WriteLine("总共花费时间：" + sw1.ElapsedMilliseconds + "毫秒。");
        }

        [TestMethod]
        public void TestSelectAll2()
        {
            Stopwatch sw = new Stopwatch();
            using (var db = new OriginalDbContext())
            {
                sw.Start();
                var roles = db.Roles.ToArray();
                sw.Stop();
                Console.WriteLine("获取数据量共" + roles.Count() + "条，耗费时间：" + sw.ElapsedMilliseconds + "毫秒");
            }
        }

        [TestMethod]
        public void TestSelectAll3()
        {
            Stopwatch sw = new Stopwatch();
            using (var db = new SFDbContext())
            {
                sw.Start();
                var roles = db.Roles.ToArray();
                sw.Stop();
                Console.WriteLine("获取数据量共" + roles.Count() + "条，耗费时间：" + sw.ElapsedMilliseconds + "毫秒");
            }
        }

        /// <summary>
        /// sql方式获取角色表数据，并且不跟踪
        /// </summary>
        [TestMethod]
        public void TestSelectAll4()
        {
            Stopwatch sw = new Stopwatch();
            using (var db = new OriginalDbContext())
            {
                sw.Start();
                var roles = db.Roles.SqlQuery("select * from Roles").AsNoTracking().ToArray();
                sw.Stop();
                Console.WriteLine("获取数据量共" + roles.Count() + "条，耗费时间：" + sw.ElapsedMilliseconds + "毫秒");
            }
        }

        /// <summary>
        /// sql方式获取角色表数据，并且跟踪
        /// </summary>
        [TestMethod]
        public void TestSelectAll5()
        {
            Stopwatch sw = new Stopwatch();
            using (var db = new OriginalDbContext())
            {
                sw.Start();
                var roles = db.Roles.SqlQuery("select * from Roles").ToArray();
                sw.Stop();
                Console.WriteLine("获取数据量共" + roles.Count() + "条，耗费时间：" + sw.ElapsedMilliseconds + "毫秒");
            }
        }

        /// <summary>
        /// 用扩展的方法执行sql获取角色表数据，并且是DataTable格式的数据
        /// </summary>
        [TestMethod]
        public void TestSelectAll6()
        {
            Stopwatch sw = new Stopwatch();
            using (var db = new OriginalDbContext())
            {
                sw.Start();
                var roles = db.Database.ExecuteDataTable("select * from Roles");
                sw.Stop();
                Console.WriteLine("获取数据量共" + roles.Rows.Count + "条，耗费时间：" + sw.ElapsedMilliseconds + "毫秒");
            }
        }

        /// <summary>
        /// sql方式获取角色表数据，转化成Role模型，并且不跟踪
        /// </summary>
        [TestMethod]
        public void TestSelectAll7()
        {
            Stopwatch sw = new Stopwatch();
            using (var db = new OriginalDbContext())
            {
                sw.Start();
                var roles = db.Database.SqlQuery<Role>("select * from Roles").ToArray();
                sw.Stop();
                Console.WriteLine("获取数据量共" + roles.Count() + "条，耗费时间：" + sw.ElapsedMilliseconds + "毫秒");
            }
        }

        [TestMethod]
        public void TTTT2()
        {
            var ssss = SF.Utilities.ConfigHelper.GetConnectionStringWithoutCredentials(@"server=MATRIXKEY\MATRIXKEYSQL2012;database=NDF-Test1;uid=sa;  password  =  1;");
            Console.WriteLine(ssss);
        }
    }
}

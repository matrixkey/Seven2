﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using System.Data;
using System.ComponentModel;

using SF.Utilities;

namespace SF.Utility.Test
{
    [TestClass]
    public class TypeExtensions
    {
        [TestMethod]
        public void TestMethod1()
        {
            Type c1 = typeof(MarshalByValueComponent);
            Type c2 = typeof(DataTable);
            Type c3 = typeof(IDisposable);

            DataTable t = new DataTable();

            Console.WriteLine(c1.IsInhertOf(c2));           //false
            Console.WriteLine(c2.IsInhertOf(c1));           //true

            Console.WriteLine(c3.IsImplementOf(c2));        //false
            Console.WriteLine(c2.IsImplementOf(c3));        //true

            Console.WriteLine(c3.IsInhertOrImplementOf(c1));//false
            Console.WriteLine(c1.IsInhertOrImplementOf(c3));//true

            Console.WriteLine(c1.IsAssignableFrom(c3));//false
            Console.WriteLine(c3.IsAssignableFrom(c1));//true
        }
    }
}

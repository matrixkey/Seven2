﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using SF.Security.Cryptography;

namespace SF.Utility.Test
{
    [TestClass]
    public class Cryptography
    {
        [TestMethod]
        public void HashAlgorithms()
        {
            string oldStr1 = "123456";

            //MD516
            string newStr1 = SF.Security.Cryptography.HashAlgorithms.MD516(oldStr1);
            bool flag = SF.Security.Cryptography.HashAlgorithms.CompareMD516(oldStr1, newStr1);

            Console.WriteLine("加密方式：MD5-16，密钥：默认");
            Console.WriteLine("明文：" + oldStr1);
            Console.WriteLine("密文：" + newStr1);
            Console.WriteLine("*************************************");
            //Console.WriteLine("明文与密文是否匹配：" + flag);


            //MD5
            newStr1 = SF.Security.Cryptography.HashAlgorithms.MD5(oldStr1);
            flag = SF.Security.Cryptography.HashAlgorithms.CompareMD5(oldStr1, newStr1);

            Console.WriteLine("加密方式：MD5-32，密钥：默认");
            Console.WriteLine("明文：" + oldStr1);
            Console.WriteLine("密文：" + newStr1);
            Console.WriteLine("*************************************");



            //RIPEMD160
            newStr1 = SF.Security.Cryptography.HashAlgorithms.RIPEMD160(oldStr1);
            flag = SF.Security.Cryptography.HashAlgorithms.CompareRIPEMD160(oldStr1, newStr1);

            Console.WriteLine("加密方式：RIPEMD160，密钥：默认");
            Console.WriteLine("明文：" + oldStr1);
            Console.WriteLine("密文：" + newStr1);
            Console.WriteLine("*************************************");



            //SHA1
            newStr1 = SF.Security.Cryptography.HashAlgorithms.SHA1(oldStr1);
            flag = SF.Security.Cryptography.HashAlgorithms.CompareSHA1(oldStr1, newStr1);

            Console.WriteLine("加密方式：SHA1，密钥：默认");
            Console.WriteLine("明文：" + oldStr1);
            Console.WriteLine("密文：" + newStr1);
            Console.WriteLine("*************************************");



            //SHA256
            newStr1 = SF.Security.Cryptography.HashAlgorithms.SHA256(oldStr1);
            flag = SF.Security.Cryptography.HashAlgorithms.CompareSHA256(oldStr1, newStr1);

            Console.WriteLine("加密方式：SHA256，密钥：默认");
            Console.WriteLine("明文：" + oldStr1);
            Console.WriteLine("密文：" + newStr1);
            Console.WriteLine("*************************************");
        }

        [TestMethod]
        public void DESCrypto()
        {
            string oldStr1 = "123456";

            string newStr1 = SF.Security.Cryptography.DESCrypto.Encrypt(oldStr1);
            string os = SF.Security.Cryptography.DESCrypto.Decrypt(newStr1);

            Console.WriteLine("加密方式：DESCrypto，密钥：默认");
            Console.WriteLine("明文：" + oldStr1);
            Console.WriteLine("密文：" + newStr1);
            Console.WriteLine("解密后的明文：" + os);
            Console.WriteLine("*************************************");
        }

        [TestMethod]
        public void DESCrypto00000()
        {
            string oldStr1 = @"server=MATRIXKEY\MATRIXKEYSQL2012;database=SF-SevenMaster;uid=sa;password=1;";
            string key = "angle";
            string newStr1 = SF.Security.Cryptography.DESCrypto.Encrypt(oldStr1, key);
            string os = SF.Security.Cryptography.DESCrypto.Decrypt(newStr1, key);

            Console.WriteLine("加密方式：DESCrypto，密钥：" + key);
            Console.WriteLine("明文：" + oldStr1);
            Console.WriteLine("密文：" + newStr1);
            Console.WriteLine("解密后的明文：" + os);
            Console.WriteLine("*************************************");
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using SF.Utilities;

namespace SF.Utility.Test
{
    [TestClass]
    public class StringHelper
    {
        [TestMethod]
        public void IsIPv4Test()
        {
            string str1 = "a.b.c.d";
            string str2 = "1.0.a0.1";
            string str3 = "0.0.0.1";
            string str4 = "0.0.0.0";
            string str5 = "1.0.0.1";
            Console.WriteLine(str1.IsIPv4());
            Console.WriteLine(str2.IsIPv4());
            Console.WriteLine(str3.IsIPv4());
            Console.WriteLine(str4.IsIPv4());
            Console.WriteLine(str5.IsIPv4());
        }

        [TestMethod]
        public void IsTelTest()
        {
            string str = "0791-1301471";
            Console.WriteLine(str.IsTel());
        }

        [TestMethod]
        public void IsMobileTest()
        {
            string str = "15879151882";
            Console.WriteLine(str.IsMobile());
        }

        [TestMethod]
        public void IsDate()
        {
            string str = "2001-01-01";
            Console.WriteLine(str.IsDate());
        }

        [TestMethod]
        public void IsColorTest()
        {
            Console.WriteLine("111".IsColor());
            Console.WriteLine("#111".IsColor());
            Console.WriteLine("#1111".IsColor());
            Console.WriteLine("#111111".IsColor());
        }

        [TestMethod]
        public void IsSafePasswordTest()
        {
            Console.WriteLine("123456".IsSafePassword());
            Console.WriteLine("a<1@_".IsSafePassword());
            Console.WriteLine("abcdefg".IsSafePassword());
            Console.WriteLine("1234567".IsSafePassword());
            Console.WriteLine("12~!@".IsSafePassword());
            Console.WriteLine("ab~!@".IsSafePassword());
            Console.WriteLine("12~!@".IsSafePassword());
            Console.WriteLine("abc~!@".IsSafePassword());
            Console.WriteLine("abcde123".IsSafePassword());
            Console.WriteLine("abcdefg!".IsSafePassword());
            Console.WriteLine("123456!".IsSafePassword());
            Console.WriteLine("pas2word".IsSafePassword());
            Console.WriteLine("pas2word@cjw".IsSafePassword());
        }

        [TestMethod]
        public void ToCaseFullCharTest()
        {
            Console.WriteLine("abc张三.,,<,，".ToCaseFullChar());
        }





        [TestMethod]
        public void GenericTest()
        {
            var array = Enums.GetFields(typeof(StringSplitOptions));
            foreach (var item in array)
            {
                Console.WriteLine(item);
            }
        }

        [TestMethod]
        public void Test11()
        {
            string a = string.Concat("a","cc");

            Console.WriteLine(a);
        }
    }
}

﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using SF.Utilities;

namespace SF.Utility.Test
{
    [TestClass]
    public class ExpressionsHelper
    {
        [TestMethod]
        public void TestMethod1()
        {
            A a1 = new A { Name = "123" };

            var properties = a1.GetType().GetProperties();

            Type t1;

            var lab1 = SF.Utilities.Expressions.Lambda(a1.GetType(), "Name", out t1);
            Delegate d1 = lab1.Compile();
            object r1 = d1.DynamicInvoke(a1);

            if (r1.IsNull()) {  }

            Console.WriteLine(r1.ToString());
        }
    }

    internal class A
    {
        public string ID { get; set; }

        public string Name { get; set; }

        public string LastModityUser { get; set; }

        public DateTime LastModityDate { get; set; }
    }
}

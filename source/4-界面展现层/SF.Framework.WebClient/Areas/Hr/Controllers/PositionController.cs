﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;
using SF.Framework.BLL;
using SF.Framework.Web;

namespace SF.Framework.WebClient.Areas.Hr.Controllers
{
    [Description("岗位控制器")]
    public class PositionController : BaseController
    {
        #region 视图

        /// <summary>
        /// 岗位新建页面
        /// </summary>
        /// <param name="organId">机构ID</param>
        /// <param name="organName">机构名称</param>
        /// <returns></returns>
        [Description("岗位新建页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult PositionCreatePage(string organId, string organName)
        {
            var model = BllFactory.Master.HrPosition.GetPositionCreateModel(organId, organName);

            return PartialView(model);
        }

        /// <summary>
        /// 岗位编辑页面
        /// </summary>
        /// <param name="id">岗位ID</param>
        /// <returns></returns>
        [Description("岗位编辑页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult PositionEditPage(string id)
        {
            var model = BllFactory.Master.HrPosition.GetPositionEditModel(id);

            return PartialView("PositionCreatePage", model);
        }

        #endregion

        #region 数据

        /// <summary>
        /// 获取指定机构下的岗位列表json数据
        /// </summary>
        /// <param name="organId">机构ID</param>
        /// <returns></returns>
        [Description("获取指定机构下的岗位列表json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetPositionJsonData(string organId)
        {
            return BllFactory.Master.HrPosition.GetPositionModelsData(organId).SerializeToJsonResult();
        }

        #endregion

        #region 操作

        /// <summary>
        /// 岗位保存操作
        /// </summary>
        /// <param name="model">岗位数据模型</param>
        /// <returns></returns>
        [Description("岗位保存操作")]
        [CatchException(ActionType.Operation)]
        public JsonResult SavePosition(PositionFormModel model)
        {
            return BllFactory.Master.HrPosition.SavePosition(model).SerializeToJsonResult();
        }

        /// <summary>
        /// 岗位移除操作
        /// </summary>
        /// <param name="id">岗位ID</param>
        /// <returns></returns>
        [Description("岗位移除操作")]
        [CatchException(ActionType.Operation)]
        public JsonResult RemovePosition(string id)
        {
            return BllFactory.Master.HrPosition.RemovePosition(id).SerializeToJsonResult();
        }

        #endregion
    }
}
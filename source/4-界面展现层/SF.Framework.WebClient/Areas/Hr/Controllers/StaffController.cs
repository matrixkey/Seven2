﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using SF.Framework.DataModel.Views;
using SF.Framework.BLL;
using SF.Framework.Web;
using SF.Utilities;

namespace SF.Framework.WebClient.Areas.Hr.Controllers
{
    [Description("员工控制器")]
    public class StaffController : BaseController
    {
        #region 视图

        /// <summary>
        /// 员工管理页面
        /// </summary>
        /// <returns></returns>
        [Description("员工管理页面")]
        [CatchException(ActionType.View)]
        [AutoSerializeButtonPermissions]
        public ViewResult ManageIndex()
        {
            return View();
        }

        /// <summary>
        /// 员工新建页面
        /// </summary>
        /// <returns></returns>
        [Description("员工新建页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult StaffCreatePage()
        {
            var model = BllFactory.Master.HrStaff.GetStaffCreateModel();

            return PartialView(model);
        }

        /// <summary>
        /// 员工编辑页面
        /// </summary>
        /// <param name="id">员工ID</param>
        /// <returns></returns>
        [Description("员工编辑页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult StaffEditPage(string id)
        {
            var model = BllFactory.Master.HrStaff.GetStaffEditModel(id);

            return PartialView("StaffCreatePage", model);
        }

        #endregion

        #region 数据

        /// <summary>
        /// 获取员工列表信息分页json数据
        /// </summary>
        /// <param name="organId">机构ID</param>
        /// <param name="queryModel">序列化的查询条件</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <returns></returns>
        [Description("获取员工列表信息分页json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetStaffListJsonData(string organId, string queryModel, int? page, int? rows)
        {
            return BllFactory.Master.HrStaff.GetStaffListPagerData(null, organId, queryModel, page ?? 1, rows ?? 10).SerializeToDatagridJsonResult(DateFormatType.yyyyMMdd);
        }

        #endregion

        #region 操作

        /// <summary>
        /// 保存员工
        /// </summary>
        /// <param name="model">员工数据模型</param>
        /// <returns></returns>
        [Description("保存员工")]
        [CatchException(ActionType.Operation)]
        public JsonResult SaveStaff(StaffFormModel model)
        {
            return BllFactory.Master.HrStaff.SaveStaff(model).SerializeToJsonResult();
        }

        /// <summary>
        /// 删除员工
        /// </summary>
        /// <param name="id">员工ID</param>
        /// <returns></returns>
        [Description("删除员工")]
        [CatchException(ActionType.Operation)]
        public JsonResult RemoveStaff(string id)
        {
            return BllFactory.Master.HrStaff.RemoveStaff(id).SerializeToJsonResult();
        }

        #endregion
    }
}
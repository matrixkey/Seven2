﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using SF.Framework.DataModel.Views;
using SF.Framework.BLL;
using SF.Framework.Web;

namespace SF.Framework.WebClient.Areas.Hr.Controllers
{
    [Description("组织机构控制器")]
    public class OrganizationController : BaseController
    {
        #region 视图

        /// <summary>
        /// 组织机构管理页面
        /// </summary>
        /// <returns></returns>
        [Description("组织机构管理页面")]
        [CatchException(ActionType.View)]
        [AutoSerializeButtonPermissions("organ,position")]
        public ViewResult ManageIndex()
        {
            return View();
        }

        /// <summary>
        /// 组织机构新建页面
        /// </summary>
        /// <param name="type">创建类型</param>
        /// <param name="parentId">父级机构ID</param>
        /// <returns></returns>
        [Description("组织机构新建页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult OrganCreatePage(OrganizationType type, string parentId = EntityContract.ParentIDDefaultValueWhenIDIsString)
        {
            var model = BllFactory.Master.HrOrganization.GetOrganCreateModel(parentId, type);

            return PartialView(this.GetCreateOrEditViewPageName(type), model);
        }

        /// <summary>
        /// 组织机构编辑页面
        /// </summary>
        /// <param name="id">组织机构ID</param>
        /// <returns></returns>
        [Description("组织机构编辑页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult OrganEditPage(string id)
        {
            var model = BllFactory.Master.HrOrganization.GetOrganEditModel(id);

            return PartialView(this.GetCreateOrEditViewPageName(model.Type), model);
        }

        /// <summary>
        /// 选择组织机构类型页面
        /// </summary>
        /// <param name="parentId">父级id</param>
        /// <param name="parentName">父级名称</param>
        /// <returns></returns>
        [Description("选择组织机构类型页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult ChooseOrganTypePage(string parentId, string parentName)
        {

            return PartialView();
        }

        #endregion

        #region 数据

        

        #endregion

        #region 操作

        /// <summary>
        /// 机构保存操作
        /// </summary>
        /// <param name="model">机构数据模型</param>
        /// <returns></returns>
        [Description("机构保存操作")]
        [CatchException(ActionType.Operation)]
        public JsonResult SaveOrganization(OrganizationFormModel model)
        {
            return BllFactory.Master.HrOrganization.SaveOrganization(model).SerializeToJsonResult();
        }

        /// <summary>
        /// 删除组织机构（假删除）
        /// </summary>
        /// <param name="id">组织机构ID</param>
        /// <returns>业务操作结果</returns>
        [Description("删除组织机构（假删除）")]
        [CatchException(ActionType.Operation)]
        public JsonResult RemoveOrganization(string id)
        {
            return BllFactory.Master.HrOrganization.RemoveOrganization(id).SerializeToJsonResult();
        }

        #endregion

        #region 其他

        /// <summary>
        /// 根据 机构类型 返回机构创建/编辑的视图名称
        /// </summary>
        /// <param name="type">机构类型</param>
        /// <returns>机构创建/编辑的视图名称</returns>
        private string GetCreateOrEditViewPageName(OrganizationType type)
        {
            string viewName = string.Empty;
            switch (type)
            {
                case OrganizationType.TopGroup:
                case OrganizationType.ChildrenGroup:
                    viewName = "GroupCreatePage";
                    break;
                case OrganizationType.ParentCompany:
                case OrganizationType.ConstituentCompany:
                    viewName = "CompanyCreatePage";
                    break;
                case OrganizationType.Department:
                    viewName = "DepartmentCreatePage";
                    break;
                default:
                    viewName = "DepartmentCreatePage";
                    break;
            }
            return viewName;
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;
using System.Xml;

using SF.Framework.BLL;
using SF.Framework.Web;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.WebClient.Areas.Admin.Controllers
{
    [Description("权限底层控制器，用来维护控制器、行为数据。")]
    [AllowOnlySuperAdmin]
    public class PermissionDefinitionController : BaseController
    {
        #region 视图

        /// <summary>
        /// 控制器行为管理页面
        /// </summary>
        /// <returns></returns>
        [Description("控制器行为管理页面")]
        [CatchException(ActionType.View)]
        public ViewResult ManageIndex()
        {
            return View();
        }

        #region 控制器

        /// <summary>
        /// 控制器新建页面
        /// </summary>
        /// <returns></returns>
        [Description("控制器新建页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult ControllerCreatePage()
        {
            var model = BllFactory.Master.SysController.GetControllerCreateModel();
            return PartialView(model);
        }

        /// <summary>
        /// 控制器编辑页面
        /// </summary>
        /// <param name="id">控制器主键</param>
        /// <returns></returns>
        [Description("控制器编辑页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult ControllerEditPage(string id)
        {
            var model = BllFactory.Master.SysController.GetControllerEditModel(id);

            return PartialView("ControllerCreatePage", model);
        }

        #endregion

        #region 行为

        /// <summary>
        /// 行为新建页面
        /// </summary>
        /// <param name="controllerId">控制器ID</param>
        /// <param name="controllerName">控制器名称</param>
        /// <param name="controllerAreaName">控制器区域名称</param>
        /// <returns></returns>
        [Description("行为新建页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult ActionCreatePage(string controllerId, string controllerName, string controllerAreaName)
        {
            var model = BllFactory.Master.SysAction.GetActionCreateModel(controllerId, controllerName, controllerAreaName);
            return PartialView(model);
        }

        /// <summary>
        /// 行为编辑页面
        /// </summary>
        /// <param name="id">行为主键</param>
        /// <returns></returns>
        [Description("行为编辑页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult ActionEditPage(string id)
        {
            var model = BllFactory.Master.SysAction.GetActionEditModel(id);

            return PartialView("ActionCreatePage", model);
        }

        #endregion

        /// <summary>
        /// 数据转换页面
        /// </summary>
        /// <returns></returns>
        [Description("数据转换页面")]
        [CatchException(ActionType.View)]
        public ViewResult TransferIndex()
        {
            return View();
        }

        #endregion

        #region 数据

        /// <summary>
        /// 获取可维护的控制器列表数据
        /// </summary>
        /// <returns></returns>
        [Description("获取可维护的控制器列表数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetControllerJsonData()
        {
            return BllFactory.Master.SysController.GetControllerListData().SerializeToJsonResult();
        }

        /// <summary>
        /// 获取指定控制器的可维护的控制器列表数据
        /// </summary>
        /// <param name="controllerId">控制器ID</param>
        /// <returns></returns>
        [Description("获取指定控制器的可维护的控制器列表数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetActionJsonData(string controllerId)
        {
            return BllFactory.Master.SysAction.GetActionListData(controllerId).SerializeToJsonResult(true);
        }

        #endregion

        #region 操作

        /// <summary>
        /// 控制器保存操作
        /// </summary>
        /// <param name="model">控制器数据模型</param>
        /// <returns></returns>
        [Description("控制器保存操作")]
        [CatchException(ActionType.Operation)]
        public JsonResult SaveController(ControllerFormModel model)
        {
            return BllFactory.Master.SysController.SaveController(model).SerializeToJsonResult();
        }

        /// <summary>
        /// 控制器移除操作
        /// </summary>
        /// <param name="id">控制器ID</param>
        /// <returns></returns>
        [Description("控制器移除操作")]
        [CatchException(ActionType.Operation)]
        public JsonResult RemoveController(string id)
        {
            return BllFactory.Master.SysController.RemoveController(id).SerializeToJsonResult();
        }

        /// <summary>
        /// 行为保存操作
        /// </summary>
        /// <param name="model">行为数据模型</param>
        /// <returns></returns>
        [Description("行为保存操作")]
        [CatchException(ActionType.Operation)]
        public JsonResult SaveAction(ActionFormModel model)
        {
            return BllFactory.Master.SysAction.SaveAction(model).SerializeToJsonResult();
        }

        /// <summary>
        /// 行为移除操作
        /// </summary>
        /// <param name="id">行为ID</param>
        /// <returns></returns>
        [Description("行为移除操作")]
        [CatchException(ActionType.Operation)]
        public JsonResult RemoveAction(string id)
        {
            return BllFactory.Master.SysAction.RemoveAction(id).SerializeToJsonResult();
        }

        /// <summary>
        /// 将数据库中的控制器、行为信息生成Xml文件
        /// </summary>
        /// <returns></returns>
        [Description("将数据库中的控制器、行为信息生成Xml文件")]
        [CatchException(ActionType.Operation)]
        public JsonResult TransferToXml()
        {
            //初始化一个xml实例
            XmlDocument xmlDoc = new XmlDocument();

            //加入XML的声明段落,<?xml version="1.0" encoding="gb2312"?>
            XmlDeclaration xmlDecl = xmlDoc.CreateXmlDeclaration("1.0", "utf-8", null);
            xmlDoc.AppendChild(xmlDecl);

            string rootName = SF.Framework.SystemEnvironment.ProjectStartItemAssemblyName;
            //加入一个根元素
            XmlElement xmlElem = xmlDoc.CreateElement("", rootName, "");
            xmlDoc.AppendChild(xmlElem);

            string assemblyName = SF.Framework.SystemEnvironment.ProjectStartItemAssemblyName;
            var controllers = SF.Framework.Web.Reflection.ControllerActionGetter.GetControllersByAssembly(assemblyName);
            XmlNode root = xmlDoc.SelectSingleNode(rootName);

            foreach (var item in controllers)
            {
                XmlElement xe1 = xmlDoc.CreateElement("Controller");//创建一个 控制器 节点
                xe1.SetAttribute("Name", item.ControllerName);//设置该节点 Name 属性
                xe1.SetAttribute("Limited", ((int)item.LimitedAttribute).ToString());//设置该节点 Limited 属性
                xe1.SetAttribute("Description", item.Description);//设置该节点 Description 属性

                var actions = new List<SF.Framework.Web.Reflection.ActionModel>();
                if (!string.IsNullOrWhiteSpace(item.ControllerName))
                {
                    actions = SF.Framework.Web.Reflection.ControllerActionGetter.GetActionsByAssembly(assemblyName, item.ControllerName, item.AreaName);
                }
                foreach (var action in actions)
                {
                    XmlElement xesub1 = xmlDoc.CreateElement("Action");//创建一个 行为 节点
                    xesub1.InnerText = action.ActionName;//设置文本节点
                    xesub1.SetAttribute("Type", action.Type.ToString());//设置该节点 Type 属性
                    xesub1.SetAttribute("Limited", ((int)action.LimitedAttribute).ToString());//设置该节点 Limited 属性
                    xesub1.SetAttribute("Description", action.Description);//设置该节点 Description 属性

                    xe1.AppendChild(xesub1);//添加到 控制器 节点中
                }

                root.AppendChild(xe1);
            }

            //保存创建好的XML文档
            string path = Server.MapPath("/Files/Security/PermissionDefinition-" + DateTime.Now.ToString("yyyy-MM-dd HH-mm-ss") + ".xml");
            xmlDoc.Save(path);

            return Json(new { success = true });
        }

        #endregion

        #region 其他



        #endregion
    }
}
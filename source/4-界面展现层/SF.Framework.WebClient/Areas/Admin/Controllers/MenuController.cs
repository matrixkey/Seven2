﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using SF.Framework.DataModel.Views;
using SF.Framework.BLL;
using SF.Framework.Web;

namespace SF.Framework.WebClient.Areas.Admin.Controllers
{
    [Description("菜单控制器")]
    [AllowOnlySuperAdmin]
    public class MenuController : BaseController
    {
        #region 视图

        /// <summary>
        /// 菜单管理页面
        /// </summary>
        /// <returns></returns>
        [Description("菜单管理页面")]
        [CatchException(ActionType.View)]
        public ViewResult ManageIndex()
        {
            return View();
        }

        #region 菜单

        /// <summary>
        /// 菜单新建页面
        /// </summary>
        /// <param name="parentId">父级菜单ID</param>
        /// <returns></returns>
        [Description("菜单新建页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult MenuCreatePage(string parentId = EntityContract.ParentIDDefaultValueWhenIDIsString)
        {
            var model = BllFactory.Master.SysMenu.GetMenuCreateModel(parentId);

            return PartialView(model);
        }

        /// <summary>
        /// 菜单编辑页面
        /// </summary>
        /// <param name="id">菜单ID</param>
        /// <returns></returns>
        [Description("菜单编辑页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult MenuEditPage(string id)
        {
            var model = BllFactory.Master.SysMenu.GetMenuEditModel(id);

            return PartialView("MenuCreatePage", model);
        }

        #endregion

        #endregion

        #region 数据

        /// <summary>
        /// 获取指定父级菜单ID的下一级菜单信息json数据
        /// </summary>
        /// <param name="id">父级菜单ID</param>
        /// <returns></returns>
        [Description("获取指定父级菜单ID的下一级菜单信息json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetAsynMenuJsonData(string id = EntityContract.ParentIDDefaultValueWhenIDIsString)
        {
            return BllFactory.Master.SysMenu.GetSonMenuData(id).SerializeToJsonResult();
        }

        #endregion

        #region 操作

        /// <summary>
        /// 菜单保存操作
        /// </summary>
        /// <param name="model">菜单数据模型</param>
        /// <returns></returns>
        [Description("菜单保存操作")]
        [CatchException(ActionType.Operation)]
        public JsonResult SaveMenu(MenuFormModel model)
        {
            return BllFactory.Master.SysMenu.SaveMenu(model).SerializeToJsonResult();
        }

        /// <summary>
        /// 菜单移除操作
        /// </summary>
        /// <param name="id">菜单ID</param>
        /// <returns></returns>
        [Description("菜单移除操作")]
        [CatchException(ActionType.Operation)]
        public JsonResult RemoveMenu(string id)
        {
            return BllFactory.Master.SysMenu.RemoveMenu(id).SerializeToJsonResult();
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using SF.Framework.DataModel.Views;
using SF.Framework.BLL;
using SF.Framework.Web;
using SF.Utilities;

namespace SF.Framework.WebClient.Areas.Admin.Controllers
{
    [Description("菜单权限控制器")]
    [AllowOnlySuperAdmin]
    public class MenuPermissionController : BaseController
    {
        #region 视图

        /// <summary>
        /// 菜单权限新建页面
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <param name="menuName">菜单名称</param>
        /// <returns></returns>
        [Description("菜单权限新建页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult MenuPermissionCreatePage(string menuId, string menuName)
        {
            var model = BllFactory.Master.SysMenuPermission.GetMenuPermissionCreateModel(menuId, menuName);

            return PartialView(model);
        }

        /// <summary>
        /// 菜单权限编辑页面
        /// </summary>
        /// <param name="id">菜单权限ID</param>
        /// <returns></returns>
        [Description("菜单权限编辑页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult MenuPermissionEditPage(string id)
        {
            var model = BllFactory.Master.SysMenuPermission.GetMenuPermissionEditModel(id);

            return PartialView("MenuPermissionCreatePage", model);
        }

        #endregion

        #region 数据

        /// <summary>
        /// 获取指定菜单的菜单权限列表数据
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <returns></returns>
        [Description("获取指定菜单的菜单权限列表数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetMenuPermissionJsonData(string menuId)
        {
            return BllFactory.Master.SysMenuPermission.GetMenuPermissionData(menuId).SerializeToJsonResult(true);
        }

        #endregion

        #region 操作

        /// <summary>
        /// 菜单权限保存操作
        /// </summary>
        /// <param name="model">菜单权限数据模型</param>
        /// <returns></returns>
        [Description("菜单权限保存操作")]
        [CatchException(ActionType.Operation)]
        public JsonResult SaveMenuPermission(MenuPermissionFormModel model)
        {
            return BllFactory.Master.SysMenuPermission.SaveMenuPermission(model).SerializeToJsonResult();
        }

        /// <summary>
        /// 菜单权限删除操作
        /// </summary>
        /// <param name="id">菜单权限ID</param>
        /// <returns></returns>
        [Description("菜单权限删除操作")]
        [CatchException(ActionType.Operation)]
        public JsonResult RemoveMenuPermission(string id)
        {
            return BllFactory.Master.SysMenuPermission.RemoveMenuPermission(id).SerializeToJsonResult();
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using SF.Framework.DataModel.Views;
using SF.Framework.BLL;
using SF.Framework.Web;

namespace SF.Framework.WebClient.Areas.System.Controllers
{
    [Description("角色控制器")]
    public class RoleController : BaseController
    {
        #region 视图

        /// <summary>
        /// 角色管理页面
        /// </summary>
        /// <returns></returns>
        [Description("角色管理页面")]
        [CatchException(ActionType.View)]
        [AutoSerializeButtonPermissions]
        public ViewResult ManageIndex()
        {
            return View();
        }

        /// <summary>
        /// 角色新建页面
        /// </summary>
        /// <returns></returns>
        [Description("角色新建页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult RoleCreatePage()
        {
            var model = BllFactory.Master.SysRole.GetRoleCreateModel();

            return PartialView(model);
        }

        /// <summary>
        /// 角色编辑页面
        /// </summary>
        /// <param name="id">角色ID</param>
        /// <returns></returns>
        [Description("角色编辑页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult RoleEditPage(string id)
        {
            var model = BllFactory.Master.SysRole.GetRoleEditModel(id);

            return PartialView("RoleCreatePage", model);
        }

        #endregion

        #region 数据

        /// <summary>
        /// 获取角色列表信息分页json数据
        /// </summary>
        /// <param name="queryModel">序列化的查询条件</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <returns></returns>
        [Description("获取角色列表信息分页json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetRoleJsonData(string queryModel, int? page, int? rows)
        {
            return BllFactory.Master.SysRole.GetRoleListPagerData(queryModel, page ?? 1, rows ?? 10).SerializeToDatagridJsonResult(true);
        }

        #endregion

        #region 操作

        /// <summary>
        /// 保存角色操作
        /// </summary>
        /// <param name="model">角色数据模型</param>
        /// <returns></returns>
        [Description("保存角色操作")]
        [CatchException(ActionType.Operation)]
        public JsonResult SaveRole(RoleFormModel model)
        {
            return BllFactory.Master.SysRole.SaveRole(model).SerializeToJsonResult();
        }

        /// <summary>
        /// 删除角色操作
        /// </summary>
        /// <param name="id">角色ID</param>
        /// <returns></returns>
        [Description("删除角色操作")]
        [CatchException(ActionType.Operation)]
        public JsonResult RemoveRole(string id)
        {
            return BllFactory.Master.SysRole.RemoveRole(id).SerializeToJsonResult();
        }

        #endregion
    }
}
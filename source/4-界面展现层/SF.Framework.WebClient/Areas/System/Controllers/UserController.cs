﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using SF.Framework.DataModel.Views;
using SF.Framework.BLL;
using SF.Framework.Web;

namespace SF.Framework.WebClient.Areas.System.Controllers
{
    [Description("用户控制器")]
    public class UserController : BaseController
    {
        #region 视图

        /// <summary>
        /// 用户管理页面
        /// </summary>
        /// <returns></returns>
        [Description("用户管理页面")]
        [CatchException(ActionType.View)]
        [AutoSerializeButtonPermissions]
        public ViewResult ManageIndex()
        {
            return View();
        }

        [Description("用户新建页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult UserCreatePage()
        {
            var model = BllFactory.Master.SysUser.GetUserCreateModel();

            return PartialView(model);
        }

        /// <summary>
        /// 用户编辑页面
        /// </summary>
        /// <param name="id">用户ID</param>
        /// <returns></returns>
        [Description("用户编辑页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult UserEditPage(string id)
        {
            var model = BllFactory.Master.SysUser.GetUserEditModel(id);

            return PartialView("UserCreatePage", model);
        }

        #endregion

        #region 数据

        [Description("获取用户列表信息分页json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetUserJsonData(string queryModel, int? page, int? rows)
        {
            return BllFactory.Master.SysUser.GetUserListPagerData(queryModel, page ?? 1, rows ?? 10).SerializeToDatagridJsonResult(true);
        }

        #endregion

        #region 操作

        /// <summary>
        /// 保存用户操作
        /// </summary>
        /// <param name="model">用户数据模型</param>
        /// <returns></returns>
        [Description("保存用户操作")]
        [CatchException(ActionType.Operation)]
        public JsonResult SaveUser(UserFormModel model)
        {
            return BllFactory.Master.SysUser.SaveUser(model).SerializeToJsonResult();
        }

        #endregion
    }
}
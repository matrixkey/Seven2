﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using SF.Framework.DataModel.Views;
using SF.Framework.BLL;
using SF.Framework.Web;

namespace SF.Framework.WebClient.Areas.System.Controllers
{
    [Description("数据字典控制器")]
    public class DataDictionaryController : BaseController
    {
        #region 视图

        /// <summary>
        /// 数据字典管理页面
        /// </summary>
        /// <returns></returns>
        [Description("数据字典管理页面")]
        [CatchException(ActionType.View)]
        [AutoSerializeButtonPermissions("parent,children")]
        public ViewResult ManageIndex()
        {
            return View();
        }

        /// <summary>
        /// 父级数据字典创建页面
        /// </summary>
        /// <returns></returns>
        [Description("父级数据字典创建页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult ParentCreatePage()
        {
            var model = BllFactory.Master.BasicDataDictionary.GetDataDictionaryCreateModel(EntityContract.ParentIDDefaultValueWhenIDIsString, EntityContract.ParentNameDefualtValueWhenIDIsString);

            return PartialView(model);
        }

        /// <summary>
        /// 父级数据字典编辑页面
        /// </summary>
        /// <param name="id">父级数据字典ID</param>
        /// <returns></returns>
        [Description("父级数据字典编辑页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult ParentEditPage(string id)
        {
            var model = BllFactory.Master.BasicDataDictionary.GetDataDictionaryEditModel(id);

            return PartialView("ParentCreatePage", model);
        }

        /// <summary>
        /// 子级数据字典创建页面
        /// </summary>
        /// <param name="parentId">父级数据字典ID</param>
        /// <param name="parentName">父级数据字典名称</param>
        /// <param name="typeSymbol">父级数据字典类别标识</param>
        /// <returns></returns>
        [Description("子级数据字典创建页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult ChildCreatePage(string parentId, string parentName, string typeSymbol)
        {
            var model = BllFactory.Master.BasicDataDictionary.GetDataDictionaryCreateModel(parentId, parentName, typeSymbol);

            return PartialView(model);
        }

        /// <summary>
        /// 子级数据字典编辑页面
        /// </summary>
        /// <param name="id">子级数据字典ID</param>
        /// <returns></returns>
        [Description("子级数据字典编辑页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult ChildEditPage(string id)
        {
            var model = BllFactory.Master.BasicDataDictionary.GetDataDictionaryEditModel(id);

            return PartialView("ChildCreatePage", model);
        }

        #endregion

        #region 数据

        /// <summary>
        /// 获取父级数据字典树形json数据
        /// </summary>
        /// <returns></returns>
        [Description("获取父级数据字典树形json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetParentDataDictionaryTreeJsonData()
        {
            return BllFactory.Master.BasicDataDictionary.GetParentDataDictionaryTreeData().SerializeToJsonResult();
        }

        /// <summary>
        /// 获取指定父级的子集数据字典json数据
        /// </summary>
        /// <returns></returns>
        [Description("获取指定父级的子集数据字典json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetChildrenDataDictionaryJsonData(string parentId)
        {
            return BllFactory.Master.BasicDataDictionary.GetChildrenDataDictionaryData(parentId).SerializeToJsonResult();
        }

        #endregion

        #region 操作

        /// <summary>
        /// 保存数据字典
        /// </summary>
        /// <param name="model">数据字典数据模型</param>
        /// <returns></returns>
        [Description("保存数据字典")]
        [CatchException(ActionType.Operation)]
        public JsonResult SaveDataDictionary(DataDictionaryFormModel model)
        {
            return BllFactory.Master.BasicDataDictionary.SaveDataDictionary(model).SerializeToJsonResult();
        }

        /// <summary>
        /// 删除数据字典
        /// </summary>
        /// <param name="id">数据字典ID</param>
        /// <returns></returns>
        [Description("删除数据字典")]
        [CatchException(ActionType.Operation)]
        public JsonResult RemoveDataDictionary(string id)
        {
            return BllFactory.Master.BasicDataDictionary.RemoveDataDictionary(id).SerializeToJsonResult();
        }

        #endregion
    }
}
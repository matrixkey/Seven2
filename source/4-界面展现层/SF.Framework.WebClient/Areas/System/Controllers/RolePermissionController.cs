﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using SF.Framework.DataModel.Views;
using SF.Framework.BLL;
using SF.Framework.Web;
using SF.Utilities;

namespace SF.Framework.WebClient.Areas.System.Controllers
{
    [Description("角色权限控制器")]
    public class RolePermissionController : BaseController
    {
        #region 视图

        /// <summary>
        /// 分配角色权限页面
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <returns></returns>
        [Description("分配角色权限页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult AssignRolePermissionPage(string roleId)
        {
            ViewBag.RoleID = roleId;

            return PartialView();
        }

        #endregion

        #region 数据

        /// <summary>
        /// 获取角色权限json数据
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <param name="rangeFilter">是否根据管理范围过滤，默认过滤</param>
        /// <returns></returns>
        [Description("获取角色权限json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetRolePermissionJsonData0(string roleId, bool rangeFilter = true)
        {
            rangeFilter = false;
            return BllFactory.Master.SysRolePermission.GetRolePermissionDataList(roleId, rangeFilter).SerializeToJsonResult();
        }

        #endregion

        #region 操作

        /// <summary>
        /// 保存角色权限
        /// </summary>
        /// <param name="permission">角色权限数据模型</param>
        /// <param name="roleId">角色ID</param>
        /// <param name="otherRoleIds">应用到其他角色的id</param>
        /// <returns></returns>
        [Description("保存角色权限")]
        [CatchException(ActionType.Operation)]
        public JsonResult SaveRolePermission([ModelBinder(typeof(JsonBinder<RolePermissionFormModel>))]IEnumerable<RolePermissionFormModel> permission, string roleId, string otherRoleIds)
        {
            return BllFactory.Master.SysRolePermission.SaveRolePermission(permission, roleId, otherRoleIds).SerializeToJsonResult();
        }

        #endregion
    }
}
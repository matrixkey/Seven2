﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using SF.Framework.DataModel.Views;
using SF.Framework.BLL;
using SF.Framework.Web;

namespace SF.Framework.WebClient.Areas.System.Controllers
{
    [Description("用户权限控制器")]
    public class UserPermissionController : BaseController
    {
        #region 视图

        /// <summary>
        /// 分配用户权限页面
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <returns></returns>
        [Description("分配用户权限页面")]
        [CatchException(ActionType.View)]
        public PartialViewResult AssignUserPermissionPage(string userId)
        {
            ViewBag.UserID = userId;

            return PartialView();
        }

        #endregion

        #region 数据

        /// <summary>
        /// 获取用户权限json数据
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="rangeFilter">是否根据管理范围过滤，默认过滤</param>
        /// <returns></returns>
        [Description("提供用户权限json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetUserPermissionJsonData(string userId, bool rangeFilter = true)
        {
            rangeFilter = false;
            return BllFactory.Master.SysUserPermission.GetUserPermissionDataList(userId, rangeFilter).SerializeToJsonResult();
        }

        #endregion

        #region 操作

        /// <summary>
        /// 保存用户权限
        /// </summary>
        /// <param name="permission">用户权限数据模型</param>
        /// <param name="userId">用户ID</param>
        /// <param name="otherUserIds">应用到其他用户的id</param>
        /// <returns></returns>
        [Description("保存用户权限")]
        [CatchException(ActionType.Operation)]
        public JsonResult SaveUserPermission([ModelBinder(typeof(JsonBinder<UserPermissionFormModel>))]IEnumerable<UserPermissionFormModel> permission, string userId, string otherUserIds)
        {
            return BllFactory.Master.SysUserPermission.SaveUserPermission(permission, userId, otherUserIds).SerializeToJsonResult();
        }

        #endregion
    }
}
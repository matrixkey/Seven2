﻿/*
==============================================================================
//  公共方法库代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("comlib");

    window.comlib.loginPageUrl = "/Account/Login";

    //整个项目的textare的所占行数和列数
    window.comlib.textAreaAttr = { cols: "3", rows: "3" };
    window.comlib.textAreaReadonlyAttr = { cols: "3", rows: "3", readonly: "readonly" };
    window.comlib.textAreaAttrBig = { cols: "4", rows: "5" };
    window.comlib.textAreaAttrBigger = { cols: "4", rows: "7" };

    //整个项目的表示颜色
    window.comlib.color = { lightGreen: "#66CD00;", green: "green;", red: "red;", purple: "#6600FF", sky: "#3366FF", pink: "#FF99FF" };

    //整个项目的状态表示颜色
    window.comlib.stateColor = { lightGreen: "color:#66CD00;", green: "color:green;", red: "color:red;", purple: "color:#6600FF", sky: "color:#3366FF" };




    //显示图标
    window.comlib.showIcon = function (iconCls) {
        if ($.string.isNullOrWhiteSpace(iconCls)) { return ""; }
        return "<span class=\"showIcon-span " + iconCls + "\"></span>";
    }

    // 模拟a标签提交至url
    // url：提交的url
    // args：提交的参数
    window.comlib.simulateAlink = function (url, args) {
        $("body").children("a[id^='simulateA_']").remove();
        var id = "simulateA_" + $.util.guid("N"), href = url + "?" + $.param(args), id2 = "simulateASpan_" + $.util.guid("N");
        var al = "<a id=\"" + id + "\" target=\"_self\" href=\"" + href + "\"><span id=\"" + id2 + "\">模拟A标签</span></a>";
        $("body").append(al);
        $("#" + id2).click();
    };

    // 以拼接html的方式格式化出 easyui-linkbutton 按钮
    // htmlAttributes：object类型，为a标签定义html属性，如id，name，class等
    // inconCls：string类型，icon图标的css名
    // text：string类型，按钮的中文信息
    // disabled：boolean类型，是否禁用按钮，禁用效果为 置灰。注意，click事件在某些浏览器还是能触发，所以请自己在click事件中判定disabled来决定是否要执行方法。
    window.comlib.formatLinkButton = function (htmlAttributes, iconCls, text, disabled) {
        var a;
        if (typeof htmlAttributes == "object") {
            //IE7下设置name后无法获取到，因此以html的方式写name
            if (htmlAttributes.name != undefined) {
                a = $("<a name='" + htmlAttributes.name + "'></a>");
            }
            else {
                a = $("<a />");
            }
            a.attr(htmlAttributes);
        } else {
            a = $("<a />");
        }

        a.addClass("toolbar-item-button l-btn l-btn-small l-btn-plain toolbar-item");
        if (disabled) {
            a.addClass("l-btn-disabled l-btn-plain-disabled");
        }
        var spanOutside = $("<span />"), spanInside1 = $("<span />"), spanInside2 = $("<span />");
        spanOutside.addClass("l-btn-left l-btn-icon-left");

        spanInside1.addClass("l-btn-text");
        if (typeof text == "string" && !$.string.isNullOrWhiteSpace(text)) {
            spanInside1.text(text);
        }

        spanInside2.addClass("l-btn-icon");
        if (typeof iconCls == "string") {
            spanInside2.addClass(iconCls);
        }

        var lb = a.append(spanOutside.append(spanInside1).append(spanInside2));
        var divDom = $("<div />").append(lb);

        return divDom.html();
    };

    // window.open的简易封装
    // url:目标地址
    // param:传递参数，支持object和string格式
    // size:窗口大小，object格式，属性为width和height
    window.comlib.windowOpen = function (url, param, size) {
        if ($.string.isNullOrWhiteSpace(url)) { return; }
        if (typeof param == "object") {
            url = url + "?" + $.param(param);
        } else if (typeof param == "string") {
            url = url + "?" + param;
        }
        if (typeof size == "object") {
            if (size.width == undefined) { size.width = screen.width - 150 }
            if (size.height == undefined) { size.height = screen.height - 150 }
        } else { size = { width: screen.width - 150, height: screen.height - 150 }; }
        var sFeatures = "top=" + ((screen.height - size.height - 50) / 2) + ",left=" + ((screen.width - size.width) / 2) + ",width=" + size.width + ",height=" + size.height + ",location=yes,menubar=no,resizable=yes,scrollbars=yes,status=no,toolbar=no";
        var tmp = window.open(url, "", sFeatures);
        //tmp.moveTo((screen.width - size.width) / 2, (screen.height - size.height) / 2);
        //tmp.resizeTo(size.width, size.height);
        //tmp.focus();
        //tmp.location = url;
    };

    // 给查询表单的text input控件添加回车查询事件
    // form:查询条件中的表单jquery对象，若未定义，则默认取id为queryFm的表单
    // query:查询按钮的jquery对象，若未定义，则默认取id为aquery的按钮
    window.comlib.bindKeyDownToQueryFormInput = function (form, query) {
        form = form == undefined ? $("#queryFm") : form;
        query = query == undefined ? $("#aquery", form) : query;
        var el = $("input[type=text]", form);
        el.bind("keydown", function (event) {
            if (event.keyCode == 13) {
                query.click();
                event.preventDefault();
            }
        });
        if (el.length > 0) { el.first().focus().select(); }
    };

    // 将 Array 转换成 Object 对象，最终返回的对象中必定包含属性 length，其值为 Array 对象的长度
    // array:要转换的 Array 对象
    // propertyNameCallback:一个 Function，会为 array 中的每个元素调用该函数，若函数返回值不为 undefined，则将该返回值当做最终要返回的 Object 对象的一个属性名。
    // propertyValueCallback:一个 Function，会为 array 中的每个元素调用该函数，若函数返回值不为 undefined，则将该返回值当做
    //      最终要返回的 Object 对象中 属性名为“propertyNameCallback函数返回值”的属性的属性值。
    window.comlib.arrayParseToObject = function (array, propertyNameCallback, propertyValueCallback) {
        array = $.util.isArray(array) ? array : [];
        propertyNameCallback = $.isFunction(propertyNameCallback) ? propertyNameCallback : function (item) { return item.ID; };
        propertyValueCallback = $.isFunction(propertyValueCallback) ? propertyValueCallback : function (item) { return item; };

        var obj = { length: array.length };
        $.array.forEach(array, function (item) {
            var propertyName = propertyNameCallback(item);
            if (propertyName) {
                var propertyValue = propertyValueCallback(item);
                if (propertyValue) {
                    obj[String(propertyName)] = propertyValue;
                }
            }
        });

        return obj;
    };












    // 以一个 easyui-tree 和两个 easyui-datagrid 的方式显示 控制器-行为 选择器。
    // 其中，控制器信息在 easyui-tree 中呈现，指定控制器的行为列表信息在 easyui-datagrid（第一个待选）中呈现，已选的数据在 easyui-datagrid（第二个已选）中呈现。
    // 参数说明：
    //       onEnter：“确定”按钮的触发事件，参数签名为 row 或 rows ，表示被选择的行。参数签名取决于该选择器是单选还是多选（由dataOptions中的singleSelect控制）。
    //       selected：已选项的id串，逗号相隔，或者数组。
    //       singleSelect：是否单选，默认为 true ，参数值决定 onEnter 方法中的参数签名。
    window.comlib.showControllerActionSelector = function (onEnter, selected, singleSelect) {
        singleSelect = singleSelect != undefined ? String(singleSelect).toBoolean() : true;
        var btnId = "btn" + $.util.guid("N"),
            onSelectParamBuild = function (node) {
                var param = { controllerId: node.id };
                return param;
            };
        var toolbar = [
                { type: "label", options: { text: "行为：" } },
                {
                    type: "textbox", options: {
                        name: "actionName", width: 120, keydown: function (event, t) {
                            if (event.keyCode == 13) {
                                $(t).find("#" + btnId).click();
                                event.preventDefault();
                            }
                        }
                    }
                },
                {
                    type: "button", options: {
                        text: "查询", iconCls: "icon-search", id: btnId,
                        onclick: function (t) {
                            window.jeasyui.helper.searchForSelector($(t), "TreeDblDataGrid");
                        }
                    }
                }
        ];

        var opts = {
            onEnter: onEnter,
            extToolbar: true,
            selected: selected,
            width: 900,
            treeWidth: 200,
            treeTitle: "控制器列表",
            treeOptions: {
                url: "/Public/GetControllerSelectorTreeJson",
                onSelectParamBuild: onSelectParamBuild
            },
            datagridOptions: {
                singleSelect: singleSelect,
                toolbar: toolbar,
                url: "/Public/GetActionSelectorJson",
                selectedUrl: "/Public/GetActionSelectedJson",
                queryParams: { first: true },
                idField: "ID",
                columns: [[
                    {
                        field: "ControllerName", title: "控制器", width: 130,
                        formatter: function (val, row) {
                            return ($.string.isNullOrWhiteSpace(row.ControllerAreaName) ? "" : "[" + row.ControllerAreaName + "]") + val;
                        }
                    },
                    { field: "Name", title: "行为", width: 130 },
                    { field: "Type", title: "类型", width: 90 },
                    { field: "Description", title: "行为描述", width: 250 }
                ]],
                pagination: true,
                onBeforeLoad: function (param) {
                    return !param.first;
                }
            }
        };

        var dia = $.easyui.showTreeDblDataGridSelector(opts);
    };


    // 以一个 easyui-tree 的方式显示 菜单 选择器。此选择器为单选。
    // 参数说明：
    //       onEnter：“确定”按钮的触发事件，参数签名为 node ，表示被选择的树节点。
    //       selected：已选项的id串，逗号相隔，或者数组。
    //       removeId：要移除的node的id。
    //       showRoot：是否显示“根目录”，默认为false。
    window.comlib.showMenuSelector = function (onEnter, selected, removeId, showRoot) {
        showRoot = showRoot ? Boolean(showRoot) : false;
        var opts = {
            onEnter: onEnter,
            selected: selected,
            treeOptions: {
                url: "/Public/GetMenuSelectorTreeJson",
                queryParams: { showRoot: showRoot },
                dataPlain: true,
                parentField: "ParentID",
                onLoadSuccess: function (node, data) {
                    if (removeId && data.length > 0) {
                        var tree = $(this);
                        if (String(removeId) != "0") {
                            var node = tree.tree("find", removeId);
                            if (node) { tree.tree("remove", node.target); }
                        }
                    }
                    //console.warn($.util.$(this).isEasyUI("tree"));
                }
            }
        };

        var dia = $.easyui.showTreeSelector(opts);
    };


    // 以一个 easyui-tree 和两个 easyui-datagrid 的方式显示 组织机构-岗位 选择器。
    // 其中，组织机构信息在 easyui-tree 中呈现，指定部门的岗位列表信息在 easyui-datagrid（第一个待选）中呈现，已选的数据在 easyui-datagrid（第二个已选）中呈现。
    // 参数说明：
    //       onEnter：“确定”按钮的触发事件，参数签名为 row 或 rows ，表示被选择的行。参数签名取决于该选择器是单选还是多选（由dataOptions中的singleSelect控制）。
    //       selected：已选项的id串，逗号相隔，或者数组。
    //       checked：已选中项的id串，逗号相隔，或者数组。该参数表示在 已选datagrid中是否对selected的row进行check。
    //       singleSelect：是否单选，默认为 true ，参数值决定 onEnter 方法中的参数签名。
    window.comlib.showOrganPositionSelector = function (onEnter, selected, checked, singleSelect) {
        singleSelect = singleSelect != undefined ? String(singleSelect).toBoolean() : true;
        var btnId = "btn" + $.util.guid("N"),
            onSelectParamBuild = function (node) {
                var param = { organId: node.id };
                return param;
            };
        var toolbar = [
                { type: "label", options: { text: "岗位：" } },
                {
                    type: "textbox", options: {
                        name: "positionName", width: 120, keydown: function (event, t) {
                            if (event.keyCode == 13) {
                                $(t).find("#" + btnId).click();
                                event.preventDefault();
                            }
                        }
                    }
                },
                {
                    type: "button", options: {
                        text: "查询", iconCls: "icon-search", id: btnId,
                        onclick: function (t) {
                            window.jeasyui.helper.searchForSelector($(t), "TreeDblDataGrid");
                        }
                    }
                }
        ];

        var opts = {
            onEnter: onEnter,
            extToolbar: true,
            selected: selected,
            checked: checked,
            width: 900,
            treeWidth: 200,
            treeTitle: "组织机构列表",
            treeOptions: {
                url: "/Public/GetSyncOrganTreeJson",
                onSelectParamBuild: onSelectParamBuild,
                parentField: "ParentID",
                dataPlain: true
            },
            datagridOptions: {
                singleSelect: singleSelect,
                selectOnCheck: singleSelect,
                toolbar: toolbar,
                url: "/Public/GetPositionSelectorJson",
                selectedUrl: "/Public/GetPositionSelectedJson",
                selectedShowCheckBox: !singleSelect,
                selectedCheckFirstSelected: true,
                queryParams: { first: true },
                idField: "PositionID",
                columns: [[
                    { field: "PositionName", title: "岗位", width: 100 },
                    { field: "OrganName", title: "所属机构", width: 100 }
                ]],
                pagination: false,
                fitColumns: true,
                onBeforeLoad: function (param) {
                    return !param.first;
                }
            }
        };

        var dia = $.easyui.showTreeDblDataGridSelector(opts);
    };


    // 以一个 easyui-datagrid 的方式显示 指定公司的角色 选择器。
    // 参数说明：
    //       onEnter：“确定”按钮的触发事件，参数签名为 row 或 rows ，表示被选择的行。参数签名取决于该选择器是单选还是多选（由dataOptions中的singleSelect控制）。
    //       selected：已选项的id串，逗号相隔，或者数组。
    //       checked：已选中项的id串，逗号相隔，或者数组。该参数表示在 已选datagrid中是否对selected的row进行check。
    //       singleSelect：是否单选，默认为 true ，参数值决定 onEnter 方法中的参数签名。
    window.comlib.showRoleSelector = function (onEnter, selected, checked, singleSelect) {
        //生成查询按钮的id，为回车查询功能定位按钮
        var btnId = "btn" + $.util.guid("N");
        var toolbar = [
                { type: "label", options: { text: "名称：" } },
                {
                    type: "textbox", options: {
                        name: "roleName", width: 120,
                        keydown: function (event, t) {
                            if (event.keyCode == 13) {
                                $(t).find("#" + btnId).click();
                                event.preventDefault();
                            }
                        }
                    }
                },
                {
                    type: "button", options: {
                        text: "查询", iconCls: "icon-search", id: btnId,
                        onclick: function (t) {
                            window.jeasyui.helper.searchForSelector($(t), "SingleDataGrid");
                        }
                    }
                }
        ];

        var opts = {
            title: "角色选择",
            onEnter: onEnter,
            extToolbar: true,
            selected: selected,
            checked: checked,
            datagridOptions: {
                singleSelect: singleSelect,
                selectOnCheck: true,
                checkOnSelect: false,
                selectedShowCheckBox: !singleSelect,
                selectedCheckFirstSelected: true,
                toolbar: toolbar,
                fitColumns: true,
                url: "/Public/GetRoleSelectorJson",
                idField: 'ID',
                columns: [[
                    { field: 'Name', title: '角色', width: 120 }
                ]]
            }
        };

        //dia是选择器的dialog对象，其中datagrid对象被放在dia对象的datagrid属性中
        var dia = $.easyui.showSingleDataGridSelector(opts);
    };



    // 以一个 easyui-tree 和两个 easyui-datagrid 的方式显示 组织机构-员工 选择器。
    // 其中，组织机构信息在 easyui-tree 中呈现，指定机构的员工列表信息在 easyui-datagrid（第一个待选）中呈现，已选的数据在 easyui-datagrid（第二个已选）中呈现。
    // 参数说明：
    //       onEnter：“确定”按钮的触发事件，参数签名为 row 或 rows ，表示被选择的行。参数签名取决于该选择器是单选还是多选（由dataOptions中的singleSelect控制）。
    //       selected：已选项的id串，逗号相隔，或者数组。
    //       singleSelect：是否单选，默认为 true ，参数值决定 onEnter 方法中的参数签名。
    //       parentOrganId：父级机构的ID，若参数存在值，则在 easyui-tree 中呈现的组织机构信息只包含该机构下的子集机构
    window.comlib.showStaffSelector = function (onEnter, selected, singleSelect, parentOrganId) {
        singleSelect = singleSelect != undefined ? String(singleSelect).toBoolean() : true;
        var btnId = "btn" + $.util.guid("N"),
            onSelectParamBuild = function (node) {
                var param = {};
                if (node.attributes && node.attributes.IsGroup) { param.groupId = node.id; }
                else if (node.attributes && node.attributes.IsCompany) { param.companyId = node.id; }
                else if (node.attributes && node.attributes.IsDept) { param.departmentId = node.id; }
                return param;
            };
        var toolbar = [
                { type: "label", options: { text: "姓名：" } },
                {
                    type: "textbox", options: {
                        name: "realName", width: 120, keydown: function (event, t) {
                            if (event.keyCode == 13) {
                                $(t).find("#" + btnId).click();
                                event.preventDefault();
                            }
                        }
                    }
                },
                {
                    type: "button", options: {
                        text: "查询", iconCls: "icon-search", id: btnId,
                        onclick: function (t) {
                            window.jeasyui.helper.searchForSelector($(t), "TreeDblDataGrid");
                        }
                    }
                }
        ];

        var opts = {
            onEnter: onEnter,
            extToolbar: true,
            selected: selected,
            width: 900,
            treeWidth: 200,
            treeTitle: "组织机构列表",
            treeOptions: {
                url: "/Public/GetSyncOrganTreeJson?parentId=" + parentOrganId,
                onSelectParamBuild: onSelectParamBuild,
                parentField: "ParentID",
                dataPlain: true
            },
            datagridOptions: {
                singleSelect: singleSelect,
                toolbar: toolbar,
                url: "/Public/GetStaffSelectorJson",
                selectedUrl: "/Public/GetStaffSelectedJson",
                queryParams: { first: true },
                idField: "ID",
                columns: [[
                    { field: "RealName", title: "姓名", width: 100 },
                    { field: "PositionName", title: "岗位", width: 100 },
                    { field: "DepartmentName", title: "部门", width: 100 },
                    {
                        field: "CompanyName", title: "公司", width: 100, formatter: function (val,row) {
                            return $.string.isNullOrWhiteSpace(row.GroupName) ? val : row.GroupName + "-" + val;
                        }
                    }
                ]],
                pagination: true,
                onBeforeLoad: function (param) {
                    return !param.first;
                }
            }
        };

        var dia = $.easyui.showTreeDblDataGridSelector(opts);
    };

})(jQuery);
﻿/*
==============================================================================
//  菜单权限新建编辑 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.MenuPermission.MenuPermissionCreatePage");

    window.views.MenuPermission.MenuPermissionCreatePage.initPage = function (formId, buttonType) {
        var form = $("#" + formId),
            key = $("#ID", form).val(),
            create = $.string.isNullOrWhiteSpace(key);

        var controlInit = function () {

            $("#Name", form).validatebox({
                autovalidate: false,
                required: true
            });

            var setButtonInfo = function (show) {
                if (show)
                {
                    $("#buttonInfo", form).show();
                    $("#ButtonName,#ButtonSymbol", form).validatebox("enableValidation");
                }
                else
                {
                    $("#buttonInfo", form).hide();
                    $("#ButtonName,#ButtonSymbol", form).validatebox("disableValidation");
                }
            };

            $("#Type", form).combobox({
                url: "/Public/GetMenuPermissionTypeJson",
                valueField: "id",
                textField: "text",
                editable: false,
                required: true,
                onChange: function (newValue, oldValue) {
                    setButtonInfo(newValue == buttonType);
                },
                onLoadSuccess: function () {
                    var defaultValue = $(this).combobox("getValue");
                    setButtonInfo(defaultValue == buttonType);
                }
            });

            $("#ButtonName", form).validatebox({ autovalidate: false, required: true });
            $("#ButtonSymbol", form).validatebox({
                autovalidate: false, required: true
            });

            $("#ButtonIconClass,#ButtonHandler,#GroupMark", form).validatebox({ required: false });

            $("#SortNumber", form).validatebox({
                autovalidate: false,
                required: true,
                validType: ['integerRange[0,99]'],
                width: 110
            });
        };

        var buttonInit = function () {

            $("#select_actions", form).click(function () {
                var enter = function (rows) {
                    if (rows && rows.length) {
                        var ids = $.array.map(rows, function (row) { return row.ID; }).join(",");
                        var texts = $.array.map(rows, function (row) { return ($.string.isNullOrWhiteSpace(row.ControllerAreaName) ? "" : "/"+ row.ControllerAreaName) + "/" + row.ControllerName + "/" + row.Name + "[" + row.Type + "] " + row.Description; });
                        var html = "";
                        texts.forEach(function (item) {
                            html += "<li style=\"list-style-type:none;line-height:24px;white-space:nowrap;font-weight:bolder;\">";
                            html += item;
                            html += "</li>";
                        });

                        $("#ActionIDs", form).val(ids);
                        $("#ulActionInfos", form).html(html);
                    }
                    else {
                        $.easyui.messager.alert("提醒", "请先选择一行", "warning");
                        return false;
                    }
                };
                var selected = $("#ActionIDs", form).val();

                window.comlib.showControllerActionSelector(enter, selected, false);
            });
        };

        controlInit();
        buttonInit();
    };


})(jQuery);
﻿/*
==============================================================================
//  员工新建编辑 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.Staff.StaffCreatePage");

    window.views.Staff.StaffCreatePage.initPage = function (formId) {
        var form = $("#" + formId),
            key = $("#ID", form).val(),
            create = $.string.isNullOrWhiteSpace(key);

        var controlInit = function () {

            $("#RealName", form).validatebox({
                autovalidate: false,
                required: true
            });
        };

        var buttonInit = function () {
            $("#select_Position", form).click(function () {
                var enter = function (rows) {
                    if (rows && rows.length) {
                        var texts = [], positionInfos = [];
                        rows.forEach(function (row) {
                            var text = row.OrganName + "-" + row.PositionName + (row.checked ? "<span class=\"required\"> [主要]</span>" : " [次要]");
                            if (row.checked == true) {
                                texts.insert(0, text);
                            }
                            else {
                                texts.push(text);
                            }
                            positionInfos.push({ FK_PositionID: row.PositionID, IsMaster: row.checked == true });
                        });

                        var html = "";
                        texts.forEach(function (item) {
                            html += "<li style=\"list-style-type:none;line-height:24px;white-space:nowrap;font-weight:bolder;\">";
                            html += item;
                            html += "</li>";
                        });

                        $("#PositionIDs", form).val($.array.map(positionInfos, function (item) { return item.FK_PositionID; }));
                        $("#MasterPositionIDs", form).val($.array.map($.array.filter(positionInfos, function (item0) { return item0.IsMaster; }), function (item) { return item.FK_PositionID; }));
                        $("#StringPositionRelations", form).val($.array.map(positionInfos, function (item) { return item.FK_PositionID + "," + item.IsMaster }).join(";"));
                        $("#ulPositionInfos", form).html(html);
                    }
                };
                var selected = $("#PositionIDs", form).val();
                var checked = $("#MasterPositionIDs", form).val();

                window.comlib.showOrganPositionSelector(enter, selected, checked, false);
            });
        };

        controlInit();
        buttonInit();
    };

})(jQuery);
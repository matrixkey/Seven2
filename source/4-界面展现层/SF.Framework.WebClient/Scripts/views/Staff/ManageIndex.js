﻿/*
==============================================================================
//  员工管理 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.Staff.ManageIndex");

    var treeOrganization = "#treeOrganization", dgStaff = "#dgStaff"; var queryFm = "#queryFm";

    window.views.Staff.ManageIndex.initPage = function (permissions) {

        var totalButtonDatas = window.jeasyui.helper.buildToolbarUsePermissionJsonForFormatter(permissions, true, false);

        var organTreeInit = function () {
            $(treeOrganization).tree({
                url: "/Public/GetAsynOrganTreeJson",
                onSelect: function (node) {
                    var panelTitle = "机构[" + node.text + "] - 员工列表", params = { organId: node.id };
                    $(dgStaff).datagrid("getPanel").panel("setTitle", panelTitle);
                    $(dgStaff).datagrid("clearSelections");
                    $(dgStaff).datagrid("load", params);
                }
            });
        };

        var queryFormInit = function () {
            $("#aquery", queryFm).click(function () {
                var queryParam = { queryModel: $(queryFm).serialize() };
                $(dgStaff).datagrid('load', queryParam);
            });
        };

        var userDatagridInit = function () {

            var otherButtonDatas = window.jeasyui.helper.filterGetNotAddButton(totalButtonDatas, "add");

            var getToolbar = function () {
                var buttonData = [],
                    addButtonData = window.jeasyui.helper.filterGetAddButton(totalButtonDatas, "add");
                if (addButtonData) {
                    buttonData.push(addButtonData);
                }
                buttonData.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.Staff.reloadStaff(); } } });

                return $("<div />").appendTo("body").toolbar({
                    data: buttonData
                }).toolbar("toolbar");
            };

            var getFrozenColumns = function () {
                var result = [];
                result.push({ field: "RealName", title: "姓名", width: 150 });
                return result;
            };

            var getColumns = function () {
                var result = [];
                result.push({ field: "PositionName", title: "所属岗位", width: 100 });
                result.push({
                    field: 'operator', title: '操作', width: 150,
                    formatter: function (val, row, index) {
                        var formatterButtons = window.jeasyui.helper.formatterLinkButtons(otherButtonDatas, row, "ID");
                        return formatterButtons.join("&nbsp;");
                    }
                });
                return result;
            };

            var options = {
                title: "员工列表",
                url: "/Hr/Staff/GetStaffListJsonData",
                idField: "ID",
                toolbar: getToolbar(),
                rownumbers: false,
                fit: true,
                border: false,
                singleSelect: true,
                pagination: true,
                frozenColumns: [getFrozenColumns()],
                columns: [getColumns()],
                onLoadSuccess: function (data) {
                    $.fn.datagrid.extensions.onLoadSuccess.apply(this, arguments);//调用扩展的事件防止覆盖
                    var t = $(this), cols = t.datagrid("getColumnDom", "operator"), rows = data.rows;

                    window.jeasyui.helper.bindLinkButtonsEvent(cols, rows, otherButtonDatas);
                }
            };

            $(dgStaff).datagrid(options);
        };

        organTreeInit();
        queryFormInit();
        userDatagridInit();
    };

    window.views.Staff.createStaff = function (btn) {
        $.easyui.showDialog({
            title: "新建员工",
            href: "/Hr/Staff/StaffCreatePage",
            onSave: function (dialog) {
                var model = window.jeasyui.helper.formValidate(dialog);
                if (model != null) {
                    window.views.Staff.saveStaff(model);
                } else { return false; }
            },
            width: 800,
            height: 280,
            topMost: true,
            enableApplyButton: false
        });
    };

    window.views.Staff.editStaff = function (btn, row) {
        row = row == undefined ? $(dgStaff).datagrid("getSelected") : row;
        if (row) {
            var param = $.param({ id: row.ID });
            $.easyui.showDialog({
                title: "编辑员工",
                href: "/Hr/Staff/StaffEditPage?" + param,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.Staff.saveStaff(model);
                    } else { return false; }
                },
                width: 800,
                height: 280,
                topMost: true,
                enableApplyButton: false
            });
        }
    };

    window.views.Staff.saveStaff = function (model) {
        $.easyui.loading({ locale: top.window.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/Hr/Staff/SaveStaff", model, function (result) {
            $.easyui.loaded(top.window.$("body"));
            window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.Staff.reloadStaff);
        });
    };

    window.views.Staff.removeStaff = function (btn, row) {
        row = row == undefined ? $(dgStaff).datagrid("getSelected") : row;
        if (row) {
            var callback = function () {
                $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                $.post("/Hr/Staff/RemoveStaff", { id: row.ID }, function (result) {
                    $.easyui.loaded($("body"));
                    window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.Staff.reloadStaff);
                }, "json");
            };
            window.jeasyui.helper.confirmCallBack("确认删除 " + row.RealName + " 吗?", callback);
        }
    };

    window.views.Staff.reloadStaff = function () {
        $(dgStaff).datagrid("clearSelections");
        $(dgStaff).datagrid("load");
    };

})(jQuery);
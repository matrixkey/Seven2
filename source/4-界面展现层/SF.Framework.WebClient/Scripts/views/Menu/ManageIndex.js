﻿/*
==============================================================================
//  菜单管理 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.Menu.ManageIndex");

    var tgMenu = "#tgMenu", dgPermission = "#dgPermission";

    window.views.Menu.ManageIndex.initPage = function (buttonType) {

        var menuTreegridInit = function () {

            var getToolbar = function () {
                var buttonData = [];
                buttonData.push({ type: "button", options: { text: "新增", iconCls: "icon-add", onclick: function (t) { window.views.Menu.createMenu(); } } });
                buttonData.push({ type: "button", options: { text: "编辑", iconCls: "icon-edit", onclick: function (t) { window.views.Menu.editMenu(); } } });
                buttonData.push({ type: "button", options: { text: "删除", iconCls: "icon-remove", onclick: function (t) { window.views.Menu.removeMenu(); } } });
                buttonData.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.Menu.reloadMenu(); } } });

                return $("<div />").appendTo("body").toolbar({
                    data: buttonData
                }).toolbar("toolbar");
            };

            var getFrozenColumns = function () {
                var result = [];
                result.push({ field: "Name", title: "名称", width: 200 });
                return result;
            };

            var getColumns = function () {
                var result = [];
                result.push({ field: "Code", title: "编号", width: 80 });
                result.push({ field: "SortNumber", title: "排序号", width: 80 });
                result.push({ field: "ControllerName", title: "控制器", width: 200 });
                result.push({ field: "ActionName", title: "行为", width: 200 });
                result.push({
                    field: "IsShow", title: "是否显示", width: 100,
                    formatter: function (val) {
                        return val ? "<b>显示</b>" : "<b>隐藏</b>";
                    },
                    styler: function (val, row) {
                        if (val)
                        { return window.comlib.stateColor.green; }
                        else
                        { return window.comlib.stateColor.red; }
                    }
                });
                return result;
            };

            var options = {
                title: "菜单列表",
                url: "/Admin/Menu/GetAsynMenuJsonData",
                idField: "ID",
                treeField: "Name",
                parentField: "ParentID",
                toolbar: getToolbar(),
                rownumbers: false,
                fit: true,
                border: false,
                singleSelect: true,
                frozenColumns: [getFrozenColumns()],
                columns: [getColumns()],
                onSelect: function (row) {
                    if (!$.string.isNullOrWhiteSpace(row.ControllerName)) {
                        $(dgPermission).datagrid("clearSelections");
                        $(dgPermission).datagrid("load", { menuID: row.ID });
                    }
                    else {
                        $(dgPermission).datagrid("clearSelections");
                        $(dgPermission).datagrid("loadData", []);
                    }
                }
            };

            $(tgMenu).treegrid(options);
        };

        var permissionDatagridInit = function () {

            var getToolbar = function () {
                var buttonData = [];
                buttonData.push({ type: "button", options: { text: "新增", iconCls: "icon-add", onclick: function (t) { window.views.Menu.createMenuPermission(); } } });
                buttonData.push({ type: "button", options: { text: "编辑", iconCls: "icon-edit", onclick: function (t) { window.views.Menu.editMenuPermission(); } } });
                buttonData.push({ type: "button", options: { text: "删除", iconCls: "icon-remove", onclick: function (t) { window.views.Menu.removeMenuPermission(); } } });
                buttonData.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.Menu.reloadMenuPermission(); } } });

                return $("<div />").appendTo("body").toolbar({
                    data: buttonData
                }).toolbar("toolbar");
            };

            var getColumns = function () {
                var result = [];
                result.push({
                    field: "Name", title: "名称", width: 150,
                    formatter: function (val, row) {
                        var str = "<b>" + val + "</b>" + ("&nbsp;&nbsp;" + window.comlib.showIcon(row.ButtonIconClass));
                        return str;
                    }
                });
                result.push({ field: "SortNumber", title: "排序号", width: 100 });
                result.push({
                    field: "Type", title: "类型", width: 100,
                    formatter: function (val, row) { return val == buttonType ? "<b>" + val + "</b>" : val; },
                    styler: function (val, row) {
                        if (val == buttonType)
                        { return window.comlib.stateColor.green; }
                    }
                });
                return result;
            };

            var options = {
                url: "/Admin/MenuPermission/GetMenuPermissionJsonData",
                title: "菜单权限列表",
                queryParams: { first: true },
                idField: "ID",
                toolbar: getToolbar(),
                rownumbers: false,
                fit: true,
                border: false,
                singleSelect: true,
                columns: [getColumns()],
                onBeforeLoad: function (param) {
                    return !param.first;
                }
            };

            $(dgPermission).datagrid(options);
        };

        menuTreegridInit();
        permissionDatagridInit();

    };

    window.views.Menu.createMenu = function () {
        var row = $(tgMenu).treegrid("getSelected");
        var param = $.param(row ? { parentId: row.ID } : {});
        $.easyui.showDialog({
            title: "新建菜单",
            href: "/Admin/Menu/MenuCreatePage?" + param,
            onSave: function (dialog) {
                var model = window.jeasyui.helper.formValidate(dialog);
                if (model != null) {
                    window.views.Menu.saveMenu(model);
                } else { return false; }
            },
            width: 800,
            height: 280,
            topMost: true,
            enableApplyButton: false
        });
    };

    window.views.Menu.editMenu = function () {
        var row = $(tgMenu).treegrid("getSelected");
        if (row) {
            var param = $.param({ id: row.ID });
            $.easyui.showDialog({
                title: "编辑菜单 - " + row.Name,
                href: "/Admin/Menu/MenuEditPage?" + param,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.Menu.saveMenu(model);
                    } else { return false; }
                },
                width: 800,
                height: 280,
                topMost: true,
                enableApplyButton: false
            });
        }
    };

    window.views.Menu.saveMenu = function (model) {
        $.easyui.loading({ locale: top.window.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/Admin/Menu/SaveMenu", model, function (result) {
            $.easyui.loaded(top.window.$("body"));
            window.jeasyui.helper.actionDoneCallBackForTreeGrid.call(this, result, $(tgMenu));
        });
    };

    window.views.Menu.removeMenu = function () {
        var row = $(tgMenu).treegrid("getSelected");
        if (row) {
            var callback = function () {
                $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                $.post("/Admin/Menu/RemoveMenu", { id: row.ID }, function (result) {
                    $.easyui.loaded($("body"));
                    window.jeasyui.helper.actionDoneCallBack.call(this, result, function () { $(tgMenu).treegrid("remove", row.ID); $(dgPermission).datagrid("loadData", []); });
                }, "json");
            };
            window.jeasyui.helper.confirmCallBack("删除菜单将同时删除其权限信息，确认删除 " + row.Name + " 吗?", callback);
        }
    };

    window.views.Menu.reloadMenu = function () {
        $(tgMenu).treegrid("clearSelections");
        $(tgMenu).treegrid("load");
    };



    window.views.Menu.createMenuPermission = function () {
        var row = $(tgMenu).treegrid("getSelected");
        if (row && !$.string.isNullOrWhiteSpace(row.ControllerName)) {
            var param = $.param({ menuId: row.ID, menuName: row.Name });
            $.easyui.showDialog({
                title: "新建菜单 - " + row.Name + " 的权限",
                href: "/Admin/MenuPermission/MenuPermissionCreatePage?" + param,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        return window.views.Menu.saveMenuPermission(model);
                    } else { return false; }
                },
                width: 800,
                height: 400,
                topMost: true,
                enableApplyButton: false
            });
        }
    };

    window.views.Menu.editMenuPermission = function () {
        var row = $(dgPermission).datagrid("getSelected");
        if (row) {
            var param = $.param({ id: row.ID });
            $.easyui.showDialog({
                title: "编辑菜单权限 - " + row.Name,
                href: "/Admin/MenuPermission/MenuPermissionEditPage?" + param,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        return window.views.Menu.saveMenuPermission(model);
                    } else { return false; }
                },
                width: 800,
                height: 400,
                topMost: true,
                enableApplyButton: false
            });
        }
    };

    window.views.Menu.saveMenuPermission = function (model) {
        if ($.string.isNullOrWhiteSpace(model.ActionIDs)) { $.easyui.messager.alert("提醒", "请选择关联的行为", "warning"); return false; }

        $.easyui.loading({ locale: top.window.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/Admin/MenuPermission/SaveMenuPermission", model, function (result) {
            $.easyui.loaded(top.window.$("body"));
            window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.Menu.reloadMenuPermission);
        });
    };

    window.views.Menu.removeMenuPermission = function () {
        var row = $(dgPermission).datagrid("getSelected");
        if (row) {
            var callback = function () {
                $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                $.post("/Admin/MenuPermission/RemoveMenuPermission", { id: row.ID }, function (result) {
                    $.easyui.loaded($("body"));
                    window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.Menu.reloadMenuPermission);
                }, "json");
            };
            window.jeasyui.helper.confirmCallBack("确认删除 " + row.Name + " 吗?", callback);
        }
    };

    window.views.Menu.reloadMenuPermission = function () {
        $(dgPermission).datagrid("load");
    };

})(jQuery);
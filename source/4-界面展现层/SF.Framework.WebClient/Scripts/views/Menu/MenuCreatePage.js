﻿/*
==============================================================================
//  菜单新建编辑 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.Menu.MenuCreatePage");

    window.views.Menu.MenuCreatePage.initPage = function (formId) {
        var form = $("#" + formId),
            key = $("#ID", form).val(),
            create = $.string.isNullOrWhiteSpace(key);

        var controlInit = function () {

            $("#Name", form).validatebox({
                autovalidate: false,
                required: true
            });

            $("#SortNumber", form).numberbox({
                autovalidate: false,
                required: true,
                validType: ['length[1,2]'],
                width: 110
            });
        };

        var buttonInit = function () {

            $("#select_parent", form).click(function () {
                var enter = function (node) {
                    if (node) {
                        $("#ParentID", form).val(node.id);
                        $("#labParentInfo", form).text(node.text);
                        var children = $(this).tree("getNearChildren", node.target),
                            sortNumbersOfChildren = $.array.map(children, function (item) { if ($.util.isNumeric(item.attributes.SortNumber)) { return item.attributes.SortNumber; } }),
                            sortNumber = $.array.max(sortNumbersOfChildren);
                        
                        $("#SortNumber", form).numberbox("setValue", sortNumber == 99 ? sortNumber : (sortNumber + 1));
                    }
                    else { $.easyui.messager.alert("提醒", "请先选择一条", "warning"); return false; }
                };
                var selected = $("#ParentID", form).val();

                window.comlib.showMenuSelector(enter, selected, key, true);
            });

            $("#select_action", form).click(function () {
                var enter = function (row) {
                    if (row) {
                        var fullName = ($.string.isNullOrWhiteSpace(row.ControllerAreaName) ? "" : "[" + row.ControllerAreaName + "]") + row.ControllerName;
                        $("#spanActionInfo", form).html(fullName + "<b>[控制器]</b>&nbsp;&nbsp;" + row.Name + "<b>[行为]</b>");
                        $("#FK_ActionID", form).val(row.ID);
                        $("#ControllerName", form).val(fullName);
                        $("#ActionName", form).val(row.Name);
                    }
                    else {
                        $("#spanActionInfo", form).html("<b>无关联</b>");
                        $("#FK_ActionID", form).val("");
                        $("#ControllerName", form).val("");
                        $("#ActionName", form).val("");
                    }
                };
                var selected = $("#FK_ActionID", form).val();

                window.comlib.showControllerActionSelector(enter, selected);
            });
        };

        controlInit();
        buttonInit();
    };

})(jQuery);
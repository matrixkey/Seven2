﻿/*
==============================================================================
//  角色管理 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.Role.ManageIndex");

    var dgRole = "#dgRole"; var queryFm = "#queryFm";

    window.views.Role.ManageIndex.initPage = function (permissions) {
        var totalButtonDatas = window.jeasyui.helper.buildToolbarUsePermissionJsonForFormatter(permissions, true, false);

        var queryFormInit = function () { };

        var roleDatagridInit = function () {

            var otherButtonDatas = window.jeasyui.helper.filterGetNotAddButton(totalButtonDatas, "add");

            var getToolbar = function () {
                var buttonData = [],
                    addButtonData = window.jeasyui.helper.filterGetAddButton(totalButtonDatas, "add");
                if (addButtonData) {
                    buttonData.push(addButtonData);
                }
                buttonData.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.Role.reloadRole(); } } });

                return $("<div />").appendTo("body").toolbar({
                    data: buttonData
                }).toolbar("toolbar");
            };

            var getFrozenColumns = function () {
                var result = [];
                result.push({ field: "Name", title: "名称", width: 150 });
                return result;
            };

            var getColumns = function () {
                var result = [];
                result.push({ field: "SortNumber", title: "排序号", width: 100 });
                result.push({
                    field: 'operator', title: '操作', width: 350,
                    formatter: function (val, row, index) {
                        var formatterButtons = window.jeasyui.helper.formatterLinkButtons(otherButtonDatas, row, "ID");
                        return formatterButtons.join("&nbsp;");
                    }
                });
                return result;
            };

            var options = {
                title: "角色列表",
                url: "/System/Role/GetRoleJsonData",
                idField: "ID",
                toolbar: getToolbar(),
                rownumbers: false,
                fit: true,
                border: false,
                singleSelect: true,
                pagination: true,
                frozenColumns: [getFrozenColumns()],
                columns: [getColumns()],
                onLoadSuccess: function (data) {
                    $.fn.datagrid.extensions.onLoadSuccess.apply(this, arguments);//调用扩展的事件防止覆盖
                    var t = $(this), cols = t.datagrid("getColumnDom", "operator"), rows = data.rows;

                    window.jeasyui.helper.bindLinkButtonsEvent(cols, rows, otherButtonDatas);
                }
            };

            $(dgRole).datagrid(options);
        };

        queryFormInit();
        roleDatagridInit();
    };

    window.views.Role.createRole = function (btn) {
        $.easyui.showDialog({
            title: "新建角色",
            href: "/System/Role/RoleCreatePage",
            onSave: function (dialog) {
                var model = window.jeasyui.helper.formValidate(dialog);
                if (model != null) {
                    window.views.Role.saveRole(model);
                } else { return false; }
            },
            width: 800,
            height: 240,
            topMost: true,
            enableApplyButton: false
        });
    };

    window.views.Role.editRole = function (btn, row) {
        row = row == undefined ? $(dgRole).datagrid("getSelected") : row;
        if (row) {
            var param = $.param({ id: row.ID });
            $.easyui.showDialog({
                title: "编辑角色",
                href: "/System/Role/RoleEditPage?" + param,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.Role.saveRole(model);
                    } else { return false; }
                },
                width: 800,
                height: 240,
                topMost: true,
                enableApplyButton: false
            });
        }
    };

    window.views.Role.saveRole = function (model) {
        $.easyui.loading({ locale: top.window.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/System/Role/SaveRole", model, function (result) {
            $.easyui.loaded(top.window.$("body"));
            window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.Role.reloadRole);
        });
    };

    window.views.Role.removeRole = function (btn, row) {
        row = row == undefined ? $(dgRole).datagrid("getSelected") : row;
        if (row) {
            var callback = function () {
                $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                $.post("/System/Role/RemoveRole", { id: row.ID }, function (result) {
                    $.easyui.loaded($("body"));
                    window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.Role.reloadRole);
                }, "json");
            };
            window.jeasyui.helper.confirmCallBack("确认删除 " + row.Name + " 吗?", callback);
        }
    };

    window.views.Role.reloadRole = function () {
        $(dgRole).datagrid("clearSelections");
        $(dgRole).datagrid("load");
    };

    window.views.Role.assignPermission = function (btn, row) {
        row = row == undefined ? $(dgRole).datagrid("getSelected") : row;
        if (row) {
            var param = $.param({ roleId: row.ID });
            var currentWindowSize = $.util.windowSize();
            $.easyui.showDialog({
                title: "分配角色 [" + row.Name + "] 的权限",
                href: "/System/RolePermission/AssignRolePermissionPage?" + param,
                width: currentWindowSize.width - 100,
                height: currentWindowSize.height - 80,
                topMost: true,
                enableApplyButton: false,
                enableSaveButton: false,
                closeButtonText: "关闭"
            });
        }
    };

})(jQuery);
﻿/*
==============================================================================
//  组织机构管理 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.Organization.ManageIndex");
    $.util.namespace("views.Position");

    var treeOrganization = "#treeOrganization", dgPosition = "#dgPosition";

    window.views.Organization.ManageIndex.initPage = function (permissions, organGroupName, positionGroupName) {
        var totalButtonDatas = window.jeasyui.helper.buildToolbarUsePermissionJsonForFormatter(permissions, true, true);

        var organTreeToolbarInit = function () {
            var data = window.jeasyui.helper.splitToolbar(totalButtonDatas, organGroupName, true);
            data.push({
                type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.Organization.reloadOrgan(); } }
            });
            var toolbar = $("<div />").insertBefore(treeOrganization).toolbar({
                data: data
            }).toolbar("toolbar");
        };

        var organTreeInit = function () {

            $(treeOrganization).tree({
                url: "/Public/GetAsynOrganTreeJson",
                parentField: "ParentID",
                onSelect: function (node) {
                    $(dgPosition).datagrid("load", { organId: node.id });
                }
            });

        };

        var positionDatagridInit = function () {
            var positionButtonDatas = window.jeasyui.helper.splitToolbar(totalButtonDatas, positionGroupName, false);
            var otherButtonDatas = window.jeasyui.helper.filterGetNotAddButton(positionButtonDatas, "add");

            var getToolbar = function () {
                var buttonData = [];
                var addButtonData = window.jeasyui.helper.filterGetAddButton(positionButtonDatas, "add");
                if (addButtonData) {
                    buttonData.push(addButtonData);
                    buttonData.push("-");
                }
                buttonData.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.Position.reloadPosition(); } } });

                return $("<div />").appendTo("body").toolbar({
                    data: buttonData
                }).toolbar("toolbar");
            };

            var getColumns = function () {
                var result = [];
                result.push({
                    field: "Name", title: "名称", width: 150,
                    formatter: function (val, row) {
                        var str = "<b>" + val + "</b>";
                        return str;
                    }
                });
                result.push({ field: "SortNumber", title: "排序号", width: 100 });
                result.push({
                    field: 'operator', title: '操作', width: 350,
                    formatter: function (val, row, index) {
                        var formatterButtons = window.jeasyui.helper.formatterLinkButtons(otherButtonDatas, row, "ID");
                        return formatterButtons.join("&nbsp;");
                    }
                });
                return result;
            };

            var options = {
                url: "/Hr/Position/GetPositionJsonData",
                title: "岗位列表",
                queryParams: { first: true },
                idField: "ID",
                toolbar: getToolbar(),
                rownumbers: false,
                fit: true,
                border: false,
                singleSelect: true,
                columns: [getColumns()],
                onBeforeLoad: function (param) {
                    return !param.first;
                },
                onLoadSuccess: function (data) {
                    $.fn.datagrid.extensions.onLoadSuccess.apply(this, arguments);//调用扩展的事件防止覆盖
                    var t = $(this), cols = t.datagrid("getColumnDom", "operator"), rows = data.rows;

                    window.jeasyui.helper.bindLinkButtonsEvent(cols, rows, otherButtonDatas);
                }
            };

            $(dgPosition).datagrid(options);

        };

        organTreeToolbarInit();
        organTreeInit();
        positionDatagridInit();
    }

    window.views.Organization.createOrgan = function (btn) {
        var selected = $(treeOrganization).tree("getSelected");
        var param = $.param(selected ? { parentId: selected.id, parentName: selected.text } : {});

        var callback = function (type) {
            $.easyui.showDialog({
                title: "新建组织机构",
                href: "/Hr/Organization/OrganCreatePage?type=" + type + "&" + param,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        return window.views.Organization.saveOrgan(model);
                    } else { return false; }
                },
                width: 800,
                height: 250,
                topMost: true,
                enableApplyButton: false
            });
        };

        var dia = $.easyui.showDialog({
            title: "请选择要新建的组织机构类型",
            //content: "<div><input type=\"text\" id=\"OrganType\" name=\"OrganType\" / ></div>",
            href: "/Hr/Organization/ChooseOrganTypePage?" + param,
            onSave: function (dialog) {
                var model = window.jeasyui.helper.formValidate(dialog);
                if (model != null) {
                    return callback(model.OrganType);
                } else { return false; }
            },
            width: 400,
            height: 200,
            topMost: true,
            enableApplyButton: false
        });

        //$.util.exec(function () {
        //    var typeCombobox = dia.find("#OrganType"), container = typeCombobox.parent(), panelHeight = dia.dialog("contentPanel").panel("body").height();
        //    var comboboxOptions = {};
        //    dia.combobox = typeCombobox.combobox(comboboxOptions);
        //    console.log(container[0].outerHTML);
        //    container.css({ "text-align": "center", "padding-top": "" + (panelHeight - 26) / 2 + "px" });
        //    console.log(container[0].outerHTML);
        //});
    };

    window.views.Organization.editOrgan = function (btn) {
        var selected = $(treeOrganization).tree("getSelected");
        if (selected) {
            $.easyui.showDialog({
                title: "编辑组织机构-" + selected.text,
                href: "/Hr/Organization/OrganEditPage?id=" + selected.id,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        return window.views.Organization.saveOrgan(model);
                    } else { return false; }
                },
                width: 800,
                height: 250,
                topMost: true,
                enableApplyButton: false
            });
        }
    };

    window.views.Organization.saveOrgan = function (model) {
        $.easyui.loading({ locale: top.window.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/Hr/Organization/SaveOrganization", model, function (result) {
            $.easyui.loaded(top.window.$("body"));
            window.jeasyui.helper.actionDoneCallBackForTree.call(this, result, $(treeOrganization));
        });
    };

    window.views.Organization.removeOrgan = function (btn) {
        var selected = $(treeOrganization).tree("getSelected");
        if (selected) {
            var callback = function () {
                $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                $.post("/Hr/Organization/RemoveOrganization", { id: selected.id }, function (result) {
                    $.easyui.loaded($("body"));
                    window.jeasyui.helper.actionDoneCallBack.call(this, result, function () { $(treeOrganization).tree("remove", selected.target); });
                }, "json");
            };
            window.jeasyui.helper.confirmCallBack("确认删除 " + selected.text + " 吗?", callback);
        }
    };

    window.views.Organization.reloadOrgan = function () {
        var oldNode = $(treeOrganization).tree("getSelected");
        if (oldNode) { lastNodeId = oldNode.id; }
        $(treeOrganization).tree("reload");
    };



    window.views.Position.createPosition = function (btn) {
        var selected = $(treeOrganization).tree("getSelected");
        if (selected) {
            var params = $.param({ organId: selected.id, organName: selected.text });
            $.easyui.showDialog({
                title: "新建[" + selected.text + "]的岗位",
                href: "/Hr/Position/PositionCreatePage?" + params,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        return window.views.Position.savePosition(model);
                    } else { return false; }
                },
                width: 800,
                height: 250,
                topMost: true,
                enableApplyButton: false
            });
        }
    };

    window.views.Position.editPosition = function (btn, row) {
        row = row == undefined ? $(dgPosition).datagrid("getSelected") : row;
        if (row) {
            var params = $.param({ id: row.ID });
            $.easyui.showDialog({
                title: "编辑岗位 - " + row.Name,
                href: "/Hr/Position/PositionEditPage?" + params,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        return window.views.Position.savePosition(model);
                    } else { return false; }
                },
                width: 800,
                height: 250,
                topMost: true,
                enableApplyButton: false
            });
        }
    };

    window.views.Position.savePosition = function (model) {
        $.easyui.loading({ locale: top.window.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/Hr/Position/SavePosition", model, function (result) {
            $.easyui.loaded(top.window.$("body"));
            window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.Position.reloadPosition);
        });
    };

    window.views.Position.removePosition = function (btn, row) {
        row = row == undefined ? $(dgPosition).datagrid("getSelected") : row;
        if (row) {
            var callback = function () {
                $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                $.post("/Hr/Position/RemovePosition", { id: row.ID }, function (result) {
                    $.easyui.loaded($("body"));
                    window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.Position.reloadPosition);
                }, "json");
            };
            window.jeasyui.helper.confirmCallBack("确认删除 " + row.Name + " 吗?", callback);
        }
    };

    window.views.Position.reloadPosition = function () {
        $(dgPosition).datagrid("load");
    };

})(jQuery);
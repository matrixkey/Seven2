﻿/*
==============================================================================
//  控制器行为管理 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.PermissionDefinition.ManageIndex");

    var dgController = "#dgController", dgAction = "#dgAction";

    window.views.PermissionDefinition.ManageIndex.initPage = function () {

        var controllerDatagridInit = function () {

            var getToolbar = function () {
                var buttonData = [];
                buttonData.push({ type: "button", options: { text: "新增", iconCls: "icon-add", onclick: function (t) { window.views.PermissionDefinition.createController(); } } });
                buttonData.push({ type: "button", options: { text: "编辑", iconCls: "icon-edit", onclick: function (t) { window.views.PermissionDefinition.editController(); } } });
                buttonData.push({ type: "button", options: { text: "删除", iconCls: "icon-remove", onclick: function (t) { window.views.PermissionDefinition.removeController(); } } });
                buttonData.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.PermissionDefinition.reloadController(); } } });

                return $("<div />").appendTo("body").toolbar({
                    data: buttonData
                }).toolbar("toolbar");
            };

            var getFrozenColumns = function () {
                var result = [];
                result.push({
                    field: "Name", title: "名称", width: 180,
                    formatter: function (val, row) {
                        return ($.string.isNullOrWhiteSpace(row.AreaName) ? "" : "[" + row.AreaName + "]") + row.Name;
                    }
                });
                return result;
            };

            var getColumns = function () {
                var result = [];
                result.push({ field: "SortNumber", title: "排序号", width: 90 });
                result.push({ field: "Description", title: "描述", width: 200 });
                return result;
            };

            var options = {
                title: "控制器列表",
                url: "/Admin/PermissionDefinition/GetControllerJsonData",
                idField: "ID",
                toolbar: getToolbar(),
                rownumbers: false,
                fit: true,
                border: false,
                singleSelect: true,
                frozenColumns: [getFrozenColumns()],
                columns: [getColumns()],
                onSelect: function (index, row) {
                    $(dgAction).datagrid("getPanel").panel("setTitle", "控制器[" + row.Name + "] - 行为列表");
                    $(dgAction).datagrid("clearSelections");
                    $(dgAction).datagrid("load", { controllerId: row.ID });
                }
            };

            $(dgController).datagrid(options);
        };

        var actionDatagridInit = function () {

            var getToolbar = function () {
                var buttonData = [];
                buttonData.push({ type: "button", options: { text: "新增", iconCls: "icon-add", onclick: function (t) { window.views.PermissionDefinition.createAction(); } } });
                buttonData.push({ type: "button", options: { text: "编辑", iconCls: "icon-edit", onclick: function (t) { window.views.PermissionDefinition.editAction(); } } });
                buttonData.push({ type: "button", options: { text: "删除", iconCls: "icon-remove", onclick: function (t) { window.views.PermissionDefinition.removeAction(); } } });
                buttonData.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.PermissionDefinition.reloadAction(); } } });

                return $("<div />").appendTo("body").toolbar({
                    data: buttonData
                }).toolbar("toolbar");
            };

            var getFrozenColumns = function () {
                var result = [];
                result.push({ field: "Name", title: "名称", width: 150 });
                return result;
            };

            var getColumns = function () {
                var result = [];
                result.push({ field: "ControllerName", title: "所属控制器", width: 150 });
                result.push({ field: "SortNumber", title: "排序号", width: 100 });
                result.push({ field: "Type", title: "类型", width: 100 });
                result.push({ field: "LimitedAttribute", title: "限制标记", width: 100 });
                result.push({ field: "Description", title: "描述", width: 250 });
                return result;
            };

            var options = {
                url: "/Admin/PermissionDefinition/GetActionJsonData",
                queryParams: { first: true },
                title: "行为列表",
                idField: "ID",
                toolbar: getToolbar(),
                rownumbers: false,
                fit: true,
                border: false,
                singleSelect: true,
                frozenColumns: [getFrozenColumns()],
                columns: [getColumns()],
                onBeforeLoad: function (param) {
                    return !param.first;
                }
            };

            $(dgAction).datagrid(options);
        };

        controllerDatagridInit();
        actionDatagridInit();
    };

    window.views.PermissionDefinition.createController = function () {
        $.easyui.showDialog({
            title: "新建控制器",
            href: "/Admin/PermissionDefinition/ControllerCreatePage",
            onSave: function (dialog) {
                var model = window.jeasyui.helper.formValidate(dialog);
                if (model != null) {
                    window.views.PermissionDefinition.saveController(model);
                } else { return false; }
            },
            width: 800,
            height: 260,
            topMost: true,
            enableApplyButton: false
        });
    };

    window.views.PermissionDefinition.editController = function () {
        var row = $(dgController).datagrid("getSelected");
        if (row) {
            $.easyui.showDialog({
                title: "编辑控制器",
                href: "/Admin/PermissionDefinition/ControllerEditPage?id=" + row.ID,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.PermissionDefinition.saveController(model);
                    } else { return false; }
                },
                width: 800,
                height: 240,
                topMost: true,
                enableApplyButton: false
            });
        }
    };

    window.views.PermissionDefinition.saveController = function (model) {
        $.easyui.loading({ locale: top.window.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/Admin/PermissionDefinition/SaveController", model, function (result) {
            $.easyui.loaded(top.window.$("body"));
            window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.PermissionDefinition.reloadController);
        });
    };

    window.views.PermissionDefinition.removeController = function () {
        var row = $(dgController).datagrid("getSelected");
        if (row) {
            var callback = function () {
                $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                $.post("/Admin/PermissionDefinition/RemoveController", { id: row.ID }, function (result) {
                    $.easyui.loaded($("body"));
                    window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.PermissionDefinition.reloadController);
                }, "json");
            };
            window.jeasyui.helper.confirmCallBack("确认删除 " + row.Name + " 吗?", callback);
        }
    };

    window.views.PermissionDefinition.reloadController = function () {
        $(dgController).datagrid("clearSelections");
        $(dgController).datagrid("load");
    };



    window.views.PermissionDefinition.createAction = function (url) {
        var controllerRow = $(dgController).datagrid("getSelected");
        if (controllerRow) {
            var param = $.param({ controllerId: controllerRow.ID, controllerName: controllerRow.Name, controllerAreaName: controllerRow.AreaName });
            $.easyui.showDialog({
                title: "新建 控制器-" + controllerRow.Name + " 的行为",
                href: "/Admin/PermissionDefinition/ActionCreatePage?" + param,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.PermissionDefinition.saveAction(model);
                    } else { return false; }
                },
                width: 800,
                height: 310,
                topMost: true,
                enableApplyButton: false
            });
        }
    };

    window.views.PermissionDefinition.editAction = function (url) {
        var row = $(dgAction).datagrid("getSelected");
        if (row) {
            $.easyui.showDialog({
                title: "编辑行为",
                href: "/Admin/PermissionDefinition/ActionEditPage?id=" + row.ID,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.PermissionDefinition.saveAction(model);
                    } else { return false; }
                },
                width: 800,
                height: 310,
                topMost: true,
                enableApplyButton: false
            });
        }
    };

    window.views.PermissionDefinition.saveAction = function (model) {
        $.easyui.loading({ locale: top.window.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/Admin/PermissionDefinition/SaveAction", model, function (result) {
            $.easyui.loaded(top.window.$("body"));
            window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.PermissionDefinition.reloadAction);
        });
    };

    window.views.PermissionDefinition.removeAction = function () {
        var row = $(dgAction).datagrid("getSelected");
        if (row) {
            var callback = function () {
                $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                $.post("/Admin/PermissionDefinition/RemoveAction", { id: row.ID }, function (result) {
                    $.easyui.loaded($("body"));
                    window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.PermissionDefinition.reloadAction);
                }, "json");
            };
            window.jeasyui.helper.confirmCallBack("删除行为将同时删除其关联信息，确认删除 " + row.Name + " 吗?", callback);
        }
    };

    window.views.PermissionDefinition.reloadAction = function () {
        $(dgAction).datagrid("clearSelections");
        $(dgAction).datagrid("load");
    };

})(jQuery);
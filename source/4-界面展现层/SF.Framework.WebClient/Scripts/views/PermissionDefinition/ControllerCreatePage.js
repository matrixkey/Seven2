﻿/*
==============================================================================
//  控制器新建编辑 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.PermissionDefinition.ControllerCreatePage");

    window.views.PermissionDefinition.ControllerCreatePage.initPage = function (formId) {
        var form = $("#" + formId),
            key = $("#ID", form).val(),
            create = $.string.isNullOrWhiteSpace(key);

        var controlInit = function () {
            var setDescription = function (controllerValue) {
                var record = $("#FullName", form).combobox("findItem", controllerValue);
                if (record) {
                    $("#Name", form).val(record.attributes.ControllerName);
                    $("#spanAreaName", form).text(record.attributes.AreaName);
                    $("#AreaName", form).val(record.attributes.AreaName);
                    $("#Description", form).val(record.attributes.Description);
                    $("#LimitedAttribute", form).combobox("setValue", record.attributes.Limited);
                }

            };

            $("#FullName", form).combobox({
                url: "/Public/GetControllerWithDatabaseInfoJson",
                valueField: "id",
                textField: "text",
                editable: false,
                required: true,
                onChange: function (newValue, oldValue) {
                    setDescription(newValue);
                },
                onLoadSuccess: function () {
                    var defaultValue = $(this).combobox("getValue");
                    if (!$.string.isNullOrWhiteSpace(defaultValue)) {
                        setDescription(defaultValue);
                    }
                    else {
                        var data = $(this).combobox("getData");
                        if (data.length > 0) {
                            $(this).combobox("setValue", data[0].id);
                        }
                    }
                }
            });

            $("#LimitedAttribute", form).combobox({
                url: "/Public/GetActionLimitedAttributeJson",
                valueField: "id",
                textField: "text",
                editable: false,
                required: true
            });

            $("#SortNumber", form).numberbox({
                required: true
            });
        };

        var buttonInit = function () { };

        controlInit();
        buttonInit();
    };

})(jQuery);
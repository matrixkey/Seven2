﻿/*
==============================================================================
//  行为新建编辑 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.PermissionDefinition.ActionCreatePage");

    window.views.PermissionDefinition.ActionCreatePage.initPage = function (formId) {
        var form = $("#" + formId),
            key = $("#ID", form).val(),
            create = $.string.isNullOrWhiteSpace(key),
            controllerName = $("#ControllerName", form).val(),
            controllerAreaName = $("#ControllerAreaName", form).val(),
            hasArea = !$.string.isNullOrWhiteSpace(controllerAreaName);

        var controlInit = function () {
            var setActionInfo = function (actionValue) {
                var record = $("#Name", form).combobox("findItem", actionValue);
                if (record) {
                    $("#Type", form).combobox("setValue", record.attributes.Type);
                    $("#url", form).text((hasArea ? "/" + controllerAreaName : "") + "/" + controllerName + "/" + record.id);
                    $("#Description", form).val(record.attributes.Description);
                    $("#LimitedAttribute", form).combobox("setValue", record.attributes.Limited);
                }
                else {
                    $("#Type", form).combobox("clear");
                    $("#url", form).text("无效的URL");
                    $("#Description", form).val("");
                }
            };

            $("#Name", form).combobox({
                url: "/Public/GetActionJson?controllerName=" + controllerName + "&controllerAreaName=" + controllerAreaName,
                valueField: "id",
                textField: "text",
                editable: false,
                required: true,
                onChange: function (newValue, oldValue) {
                    setActionInfo(newValue);
                },
                onLoadSuccess: function () {
                    var defaultValue = $(this).combobox("getValue");
                    if (!$.string.isNullOrWhiteSpace(defaultValue)) {
                        setActionInfo(defaultValue);
                    }
                    else {
                        var data = $(this).combobox("getData");
                        if (data.length > 0) {
                            $(this).combobox("setValue", data[0].id);
                        }
                    }
                }
            });

            $("#Type", form).combobox({
                url: "/Public/GetActionTypeJson",
                valueField: "id",
                textField: "text",
                editable: false,
                required: true,
                disabled: true
            });

            $("#LimitedAttribute", form).combobox({
                url: "/Public/GetActionLimitedAttributeJson",
                valueField: "id",
                textField: "text",
                editable: false,
                required: true
            });

            $("#SortNumber", form).numberbox({
                required: true
            });
        };

        var buttonInit = function () { };

        controlInit();
        buttonInit();
    };

})(jQuery);
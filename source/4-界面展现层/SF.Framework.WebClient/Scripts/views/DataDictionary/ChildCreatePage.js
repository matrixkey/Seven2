﻿/*
==============================================================================
//  数据字典新建编辑 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.DataDictionary.ChildCreatePage");

    window.views.DataDictionary.ChildCreatePage.initPage = function (formId) {
        var form = $("#" + formId),
            key = $("#ID", form).val(),
            create = $.string.isNullOrWhiteSpace(key);

        var controlInit = function () {

            $("#Name", form).validatebox({
                autovalidate: false,
                required: true
            });

            $("#TypeSymbol", form).validatebox({
                autovalidate: false,
                required: true
            });

            $("#SortNumber", form).numberbox({
                autovalidate: false,
                required: true,
                width: 110
            });
        };

        var buttonInit = function () { };

        controlInit();
        buttonInit();
    };

})(jQuery);
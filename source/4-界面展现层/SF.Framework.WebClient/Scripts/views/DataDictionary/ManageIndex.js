﻿/*
==============================================================================
//  数据字典管理 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.DataDictionary.ManageIndex");

    var treeParent = "#treeParent", dgChildren = "#dgChildren";

    window.views.DataDictionary.ManageIndex.initPage = function (permissions, parentGroupName, childrenGroupName) {
        var totalButtonDatas = window.jeasyui.helper.buildToolbarUsePermissionJsonForFormatter(permissions, true, true);

        var parentTreeToolbarInit = function () {
            var data = window.jeasyui.helper.splitToolbar(totalButtonDatas, parentGroupName, true);
            data.push({
                type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.DataDictionary.reloadParent(); } }
            });
            var toolbar = $("<div />").insertBefore(treeParent).toolbar({
                data: data
            }).toolbar("toolbar");
        };

        var parentTreeInit = function () {
            $(treeParent).tree({
                url: "/System/DataDictionary/GetParentDataDictionaryTreeJsonData",
                onSelect: function (node) {
                    $(dgChildren).datagrid("getPanel").panel("setTitle", "[" + node.text + "] - 数据字典列表");
                    $(dgChildren).datagrid("clearSelections");
                    $(dgChildren).datagrid("load", { parentId: node.id });
                }
            });
        };

        var childrenDatagridInit = function () {
            var childrenButtonDatas = window.jeasyui.helper.splitToolbar(totalButtonDatas, childrenGroupName, false);
            var otherButtonDatas = window.jeasyui.helper.filterGetNotAddButton(childrenButtonDatas, "add");

            var getToolbar = function () {
                var buttonData = [],
                    addButtonData = window.jeasyui.helper.filterGetAddButton(childrenButtonDatas, "add");
                if (addButtonData) {
                    buttonData.push(addButtonData);
                    buttonData.push("-");
                }
                buttonData.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.DataDictionary.reloadChildren(); } } });

                return $("<div />").appendTo("body").toolbar({
                    data: buttonData
                }).toolbar("toolbar");
            };

            var getFrozenColumns = function () {
                var result = [];
                result.push({ field: "Name", title: "名称", width: 150 });
                return result;
            };

            var getColumns = function () {
                var result = [];
                result.push({ field: "SortNumber", title: "排序号", width: 100 });
                result.push({ field: "BusinessCode", title: "业务编号", width: 100 });
                result.push({ field: "TypeSymbol", title: "类别标志", width: 100 });
                result.push({
                    field: "IsShow", title: "是否显示", width: 100,
                    formatter: function (val) {
                        return val ? "<b>显示</b>" : "<b>隐藏</b>";
                    },
                    styler: function (val, row) {
                        if (val)
                        { return window.comlib.stateColor.green; }
                        else
                        { return window.comlib.stateColor.red; }
                    }
                });
                result.push({
                    field: 'operator', title: '操作', width: 350,
                    formatter: function (val, row, index) {
                        var formatterButtons = window.jeasyui.helper.formatterLinkButtons(otherButtonDatas, row, "ID");
                        return formatterButtons.join("&nbsp;");
                    }
                });
                return result;
            };

            var options = {
                title: "数据字典列表",
                url: "/System/DataDictionary/GetChildrenDataDictionaryJsonData",
                queryParams: { first: true },
                idField: "ID",
                toolbar: getToolbar(),
                rownumbers: false,
                fit: true,
                border: false,
                singleSelect: true,
                pagination: false,
                frozenColumns: [getFrozenColumns()],
                columns: [getColumns()],
                onBeforeLoad: function (param) {
                    return !param.first;
                },
                onLoadSuccess: function (data) {
                    $.fn.datagrid.extensions.onLoadSuccess.apply(this, arguments);//调用扩展的事件防止覆盖
                    var t = $(this), cols = t.datagrid("getColumnDom", "operator"), rows = data.rows;

                    window.jeasyui.helper.bindLinkButtonsEvent(cols, rows, otherButtonDatas);
                }
            };

            $(dgChildren).datagrid(options);
        };

        parentTreeToolbarInit();
        parentTreeInit();
        childrenDatagridInit();
    };

    window.views.DataDictionary.createParent = function (btn) {
        $.easyui.showDialog({
            title: "新建父级数据字典",
            href: "/System/DataDictionary/ParentCreatePage",
            onSave: function (dialog) {
                var model = window.jeasyui.helper.formValidate(dialog);
                if (model != null) {
                    window.views.DataDictionary.saveParent(model);
                } else { return false; }
            },
            width: 800,
            height: 240,
            topMost: true,
            enableApplyButton: false
        });
    };

    window.views.DataDictionary.editParent = function (btn) {
        var selected = $(treeParent).tree("getSelected");
        if (selected) {
            var param = $.param({ id: selected.id });
            $.easyui.showDialog({
                title: "编辑父级数据字典",
                href: "/System/DataDictionary/ParentEditPage?" + param,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.DataDictionary.saveParent(model);
                    } else { return false; }
                },
                width: 800,
                height: 240,
                topMost: true,
                enableApplyButton: false
            });
        }
    };

    window.views.DataDictionary.saveParent = function (model) {
        $.easyui.loading({ locale: top.window.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/System/DataDictionary/SaveDataDictionary", model, function (result) {
            $.easyui.loaded(top.window.$("body"));
            window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.DataDictionary.reloadParent);
        });
    };

    window.views.DataDictionary.removeParent = function (btn) {
        var selected = $(treeParent).tree("getSelected");
        if (selected) {
            var callback = function () {
                $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                $.post("/System/DataDictionary/RemoveDataDictionary", { id: selected.id }, function (result) {
                    $.easyui.loaded($("body"));
                    window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.DataDictionary.reloadParent);
                }, "json");
            };
            window.jeasyui.helper.confirmCallBack("确认删除父级数据字典 " + selected.text + " 吗?", callback);
        }
    };

    window.views.DataDictionary.reloadParent = function () {
        $(treeParent).tree("reload");
    };



    window.views.DataDictionary.createChildren = function (btn) {
        var selected = $(treeParent).tree("getSelected");
        if (selected) {
            var params = $.param({ parentId: selected.id, parentName: selected.text, typeSymbol: selected.attributes.TypeSymbol });
            $.easyui.showDialog({
                title: "新建子级数据字典",
                href: "/System/DataDictionary/ChildCreatePage?" + params,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.DataDictionary.saveChildren(model);
                    } else { return false; }
                },
                width: 800,
                height: 280,
                topMost: true,
                enableApplyButton: false
            });
        }
    };

    window.views.DataDictionary.editChildren = function (btn, row) {
        row = row == undefined ? $(dgChildren).datagrid("getSelected") : row;
        if (row) {
            var param = $.param({ id: row.ID });
            $.easyui.showDialog({
                title: "编辑子级数据字典",
                href: "/System/DataDictionary/ChildEditPage?" + param,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.DataDictionary.saveChildren(model);
                    } else { return false; }
                },
                width: 800,
                height: 280,
                topMost: true,
                enableApplyButton: false
            });
        }
    };

    window.views.DataDictionary.saveChildren = function (model) {
        $.easyui.loading({ locale: top.window.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/System/DataDictionary/SaveDataDictionary", model, function (result) {
            $.easyui.loaded(top.window.$("body"));
            window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.DataDictionary.reloadChildren);
        });
    };

    window.views.DataDictionary.removeChildren = function (btn, row) {
        row = row == undefined ? $(dgChildren).datagrid("getSelected") : row;
        if (row) {
            var callback = function () {
                $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                $.post("/System/DataDictionary/RemoveDataDictionary", { id: row.ID }, function (result) {
                    $.easyui.loaded($("body"));
                    window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.DataDictionary.reloadChildren);
                }, "json");
            };
            window.jeasyui.helper.confirmCallBack("确认删除 " + row.Name + " 吗?", callback);
        }
    };

    window.views.DataDictionary.reloadChildren = function () {
        $(dgChildren).datagrid("clearSelections");
        $(dgChildren).datagrid("load");
    };

})(jQuery);
﻿/*
==============================================================================
//  用户管理 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.User.ManageIndex");

    var treeOrganization = "#treeOrganization", dgUser = "#dgUser"; var queryFm = "#queryFm";

    window.views.User.ManageIndex.initPage = function (permissions) {
        var totalButtonDatas = window.jeasyui.helper.buildToolbarUsePermissionJsonForFormatter(permissions, true, false);

        var organTreeInit = function () {
            $(treeOrganization).tree({
                url: "/Public/GetGroupCompanyTreeJson",
                parentField: "ParentID",
                dataPlain: true,
                onSelect: function (node) {
                    $(dgUser).datagrid("getPanel").panel("setTitle", "机构[" + node.text + "] - 用户列表");
                    $(dgUser).datagrid("clearSelections");
                    $(dgUser).datagrid("load");
                }
            });
        };

        var queryFormInit = function () {
            $("#aquery", queryFm).click(function () {
                var queryParam = { queryModel: $(queryFm).serialize() };
                $(dgUser).datagrid('load', queryParam);
            });
        };

        var userDatagridInit = function () {

            var otherButtonDatas = window.jeasyui.helper.filterGetNotAddButton(totalButtonDatas, "add");

            var getToolbar = function () {
                var buttonData = [],
                    addButtonData = window.jeasyui.helper.filterGetAddButton(totalButtonDatas, "add");
                if (addButtonData) {
                    buttonData.push(addButtonData);
                }
                buttonData.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.User.reloadUser(); } } });

                return $("<div />").appendTo("body").toolbar({
                    data: buttonData
                }).toolbar("toolbar");
            };

            var getFrozenColumns = function () {
                var result = [];
                result.push({ field: "UserName", title: "用户名", width: 150 });
                return result;
            };

            var getColumns = function () {
                var result = [];
                result.push({ field: "StaffRealName", title: "员工", width: 100 });
                result.push({
                    field: 'operator', title: '操作', width: 150,
                    formatter: function (val, row, index) {
                        var formatterButtons = window.jeasyui.helper.formatterLinkButtons(otherButtonDatas, row, "ID");
                        return formatterButtons.join("&nbsp;");
                    }
                });
                return result;
            };

            var options = {
                title: "用户列表",
                url: "/System/User/GetUserJsonData",
                idField: "ID",
                toolbar: getToolbar(),
                rownumbers: false,
                fit: true,
                border: false,
                singleSelect: true,
                pagination: true,
                frozenColumns: [getFrozenColumns()],
                columns: [getColumns()],
                onLoadSuccess: function (data) {
                    $.fn.datagrid.extensions.onLoadSuccess.apply(this, arguments);//调用扩展的事件防止覆盖
                    var t = $(this), cols = t.datagrid("getColumnDom", "operator"), rows = data.rows;

                    window.jeasyui.helper.bindLinkButtonsEvent(cols, rows, otherButtonDatas);
                }
            };

            $(dgUser).datagrid(options);
        };

        organTreeInit();
        queryFormInit();
        userDatagridInit();
    };

    window.views.User.createUser = function (btn) {
        $.easyui.showDialog({
            title: "新建用户",
            href: "/System/User/UserCreatePage",
            onSave: function (dialog) {
                var model = window.jeasyui.helper.formValidate(dialog);
                if (model != null) {
                    window.views.User.saveUser(model);
                } else { return false; }
            },
            width: 800,
            height: 340,
            topMost: true,
            enableApplyButton: false
        });
    };

    window.views.User.editUser = function (btn, row) {
        row = row == undefined ? $(dgUser).datagrid("getSelected") : row;
        if (row) {
            var param = $.param({ id: row.ID });
            $.easyui.showDialog({
                title: "编辑用户",
                href: "/System/User/UserEditPage?" + param,
                onSave: function (dialog) {
                    var model = window.jeasyui.helper.formValidate(dialog);
                    if (model != null) {
                        window.views.User.saveUser(model);
                    } else { return false; }
                },
                width: 800,
                height: 340,
                topMost: true,
                enableApplyButton: false
            });
        }
    };

    window.views.User.saveUser = function (model) {
        $.easyui.loading({ locale: top.window.$("body"), msg: "数据处理中，请稍等..." });
        $.post("/System/User/SaveUser", model, function (result) {
            $.easyui.loaded(top.window.$("body"));
            window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.User.reloadUser);
        });
    };

    window.views.User.removeUser = function (btn, row) {
        row = row == undefined ? $(dgUser).datagrid("getSelected") : row;
        if (row) {
            var callback = function () {
                //$.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
                //$.post("/System/Role/RemoveRole", { id: row.ID }, function (result) {
                //    $.easyui.loaded($("body"));
                //    window.jeasyui.helper.actionDoneCallBack.call(this, result, window.views.Role.reloadRole);
                //}, "json");
            };
            window.jeasyui.helper.confirmCallBack("确认删除 " + row.UserName + " 吗?", callback);
        }
    };

    window.views.User.reloadUser = function () {
        $(dgUser).datagrid("clearSelections");
        $(dgUser).datagrid("load");
    };

    window.views.User.assignPermission = function (btn, row) {
        row = row == undefined ? $(dgUser).datagrid("getSelected") : row;
        if (row) {
            var param = $.param({ userId: row.ID });
            var currentWindowSize = $.util.windowSize();
            $.easyui.showDialog({
                title: "分配用户 [" + row.UserName + "] 的权限",
                href: "/System/UserPermission/AssignUserPermissionPage?" + param,
                width: currentWindowSize.width - 100,
                height: currentWindowSize.height - 80,
                topMost: true,
                enableApplyButton: false,
                enableSaveButton: false,
                closeButtonText: "关闭"
            });
        }
    };

})(jQuery);
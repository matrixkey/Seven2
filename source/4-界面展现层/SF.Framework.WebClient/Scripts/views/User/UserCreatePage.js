﻿/*
==============================================================================
//  用户新建编辑 的页面控制层代码。
==============================================================================
*/
(function ($) {

    $.util.namespace("views.User.UserCreatePage");

    window.views.User.UserCreatePage.initPage = function (formId) {
        var form = $("#" + formId),
            key = $("#ID", form).val(),
            create = $.string.isNullOrWhiteSpace(key);

        var controlInit = function () {

            $("#UserName", form).validatebox({
                autovalidate: false,
                required: true
            });

            if (create) {
                $("#Password", form).validatebox({
                    autovalidate: false,
                    required: true,
                    validType: ["passwordNew"]
                });

                $("#PasswordRepeat", form).validatebox({
                    autovalidate: false,
                    required: true,
                    validType: ["equals['Password','id']"]
                });
            }
            else {
                $("#Password,#PasswordRepeat", form).prop("disabled", true);
                $("#Password", form).parents("tr:first").hide();
            }
        };

        var buttonInit = function () {

            $("#adefaultpwd", form).click(function () {
                $("#Password,#PasswordRepeat", form).val("111111");
            });
        };

        controlInit();
        buttonInit();
    };

})(jQuery);
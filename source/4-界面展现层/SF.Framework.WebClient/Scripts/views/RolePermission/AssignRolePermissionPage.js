﻿/*
==============================================================================
//  分配角色权限 的页面控制层代码。
==============================================================================
*/
(function () {

    $.util.namespace("views.RolePermission.AssignRolePermissionPage");

    var tgRolePermission = "#tgRolePermission"; var currentArguments = {};

    window.views.RolePermission.AssignRolePermissionPage.initPage = function (roleId) {

        currentArguments.roleId = roleId;

        var rolePermissionTreegridInit = function () {

            var getToolbar = function () {
                var buttonData = [];
                buttonData.push({ type: "button", options: { text: "保存权限设置", iconCls: "icon-save", onclick: function (t) { window.views.RolePermission.saveRolePermission(); } } });
                buttonData.push("-");
                buttonData.push({ type: "button", options: { text: "刷新", iconCls: "icon-reload", onclick: function (t) { window.views.RolePermission.reloadRolePermission(); } } });

                return $("<div />").appendTo("body").toolbar({
                    data: buttonData
                }).toolbar("toolbar");
            };

            var getFrozenColumns = function () {
                var result = [];
                result.push({ field: "Name", title: "菜单", width: 200 });
                return result;
            };

            var getColumns = function () {
                var result = [];
                result.push({
                    field: 'Permission', title: '权限', width: 550, align: "left",
                    formatter: function (val, row) {
                        var html = "";
                        if (row.Permissions && row.Permissions.length && row.Permissions.length > 0) {
                            $.each(row.Permissions, function (i, p) {
                                html += "<span id=\"checkspan_" + p.MenuPermissionID + "_end\" name=\"checkspan_" + row.ID + "_end\" class=\"tree-checkbox tree-checkbox" + p.State + "\" key=\"" + p.MenuPermissionID + "\" value=\"" + row.ID + "\" onclick=\"window.views.RolePermission.checkState(this, event);\" ></span><b>" + p.MenuPermissionName + "</b>&nbsp;&nbsp;&nbsp;&nbsp;";
                            });
                        }
                        return html;
                    }
                });
                return result;
            };

            var options = {
                url: "/System/RolePermission/GetRolePermissionJsonData0",
                queryParams: { roleId: roleId, rangeFilter: true },
                idField: "ID",
                treeField: "Name",
                dataPlain: true,
                parentField: "ParentID",
                toolbar: getToolbar(),
                rownumbers: true,
                fit: true,
                fitColumns: true,
                border: false,
                singleSelect: true,
                checkOnSelect: false,
                selectOnCheck: false,
                cascadeCheck: true,
                enableHeaderClickMenu: false,
                frozenColumns: [getFrozenColumns()],
                columns: [getColumns()],
                onCheck: function (rowData) {
                    var dom = $(this).treegrid("getRowDom", rowData.ID);
                    $("span[name='checkspan_" + rowData.ID + "_end']", dom).removeClass("tree-checkbox0 tree-checkbox2").addClass("tree-checkbox1");
                },
                onUncheck: function (rowData) {
                    var dom = $(this).treegrid("getRowDom", rowData.ID);
                    $("span[name='checkspan_" + rowData.ID + "_end']", dom).removeClass("tree-checkbox1 tree-checkbox2").addClass("tree-checkbox0");
                },
                onCheckAll: function (rows) {
                    var dgPanel = $(this).treegrid("getPanel");
                    $("span[id^='checkspan_']", dgPanel).removeClass("tree-checkbox0 tree-checkbox2").addClass("tree-checkbox1");
                },
                onUncheckAll: function (rows) {
                    var dgPanel = $(this).treegrid("getPanel");
                    $("span[id^='checkspan_']", dgPanel).removeClass("tree-checkbox1 tree-checkbox2").addClass("tree-checkbox0");
                }
            };

            $(tgRolePermission).treegrid(options);

        };

        rolePermissionTreegridInit();
    };

    window.views.RolePermission.checkState = function (ck, e) {
        ck = $(ck);
        if (ck.hasClass("tree-checkbox0")) {
            ck.removeClass("tree-checkbox0").addClass("tree-checkbox1");
        }
        else if (ck.hasClass("tree-checkbox1")) {
            ck.removeClass("tree-checkbox1").addClass("tree-checkbox0");
        }
        e.stopPropagation();
    };

    window.views.RolePermission.saveRolePermission = function (showOther) {
        var panel = $(tgRolePermission).treegrid("getPanel");
        var currentRowCheckboxs = $("span[id^='checkspan_']", panel);
        var arrayPermission = [];
        currentRowCheckboxs.each(function (i, el) {
            var check = !$(el).hasClass("tree-checkbox0");
            var indeterminate = $(el).hasClass("tree-checkbox2");
            arrayPermission.push({ MenuID: $(el).attr("value"), PermissionID: $(el).attr("key"), Check: check, Indeterminate: indeterminate });
        });

        var param = { permission: JSON.stringify(arrayPermission), roleId: currentArguments.roleId };
        if (!showOther) {
            //var otherselectRole = $(roleTreeOther).tree("getChecked");
            //var otherRoleId = [];
            //$.array.forEach(otherselectRole, function (val) {
            //    if (val.attributes.IsRole) {
            //        otherRoleId.push(val.attributes.ID);
            //    }
            //});

            //$.extend(param, { otherRoleIds: otherRoleId.join(",") });
        }
        var callback = function () {
            $.easyui.loading({ locale: $("body"), msg: "数据处理中，请稍等..." });
            $.post("/System/RolePermission/SaveRolePermission", param, function (result) {
                $.easyui.loaded($("body"));
                window.jeasyui.helper.actionDoneCallBack.call(this, result);
            });
        };

        window.jeasyui.helper.confirmCallBack("确认保存角色权限吗?", callback);
    };

    window.views.RolePermission.reloadRolePermission = function () {
        $(tgRolePermission).treegrid("reload");
    };

})(jQuery);
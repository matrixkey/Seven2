﻿/*
* 更改 jQuery.ajax 函数的error属性
* 
*
* 李溪林
* 最近更新：2014-11-20
*
*/
(function ($) {
    var _limitedStatusCode = 318;
    var _timeoutStatusCode = 319;
    var _unLogin = 320;
    var _ajaxOpt = $.ajaxSetup();
    var _error = _ajaxOpt.error;
    var _showLimitedMessage = function (msg) {
        $.messager.alert("操作提醒", msg == undefined ? "权限不足，请求失败！" : unescape(msg), "info");
    };

    var _showTimeOutMessage = function () {
        var id = "span_" + $.util.guid("N");
        var span = "#" + id;
        var timeout = 5;
        var jump = function () { $.util.top.location.href = window.comlib.loginPageUrl; };
        $.easyui.messager.alert("操作提醒", "登录已超时，系统将会在 <span id='" + id + "'>" + timeout + "</span> 秒后跳转到登录页面", "info");
        window.top.$(span).currentPanel().panel("options").onClose = jump;
        var c = function (value) { window.top.$(span).text(value); if (value == 0) { jump(); } };
        var run = function () {
            c(timeout);
            timeout--;
            if (timeout >= 0) { $.util.exec(run, 1000); }
        };
        $.util.exec(run);
    };

    var onLoadError = function (XMLHttpRequest, textStatus, errorThrown) {
        if (XMLHttpRequest && XMLHttpRequest.status == _limitedStatusCode) {
            XMLHttpRequest = $.extend({}, XMLHttpRequest, { responseText: "无权访问！" });
            $.messager.progress("close");
            if ($.easyui.messager != $.messager) { $.easyui.messager.progress("close"); }
            _showLimitedMessage(errorThrown);
        }
        else if (XMLHttpRequest && (XMLHttpRequest.status == _timeoutStatusCode || XMLHttpRequest.status == _unLogin)) {
            XMLHttpRequest = $.extend({}, XMLHttpRequest, { responseText: "登录超时！" });
            $.messager.progress("close");
            if ($.easyui.messager != $.messager) { $.easyui.messager.progress("close"); }
            _showTimeOutMessage();
        }
        else {
            _error.call(this, XMLHttpRequest, textStatus, errorThrown);
        }
    };

    $.fn.form.defaults.onLoadError = onLoadError;
    $.fn.combobox.defaults.onLoadError = onLoadError;
    $.fn.combotree.defaults.onLoadError = onLoadError;
    $.fn.combogrid.defaults.onLoadError = onLoadError;
    $.fn.datagrid.defaults.onLoadError = onLoadError;
    $.fn.propertygrid.defaults.onLoadError = onLoadError;
    $.fn.tree.defaults.onLoadError = onLoadError;
    $.fn.treegrid.defaults.onLoadError = onLoadError;

    $.ajaxSetup({
        error: onLoadError
    });

})(jQuery);
﻿/**
* jQuery EasyUI 1.3.6
* Copyright (c) 2009-2013 www.jeasyui.com. All rights reserved.
*
* Licensed under the GPL or commercial licenses
* To use it on other terms please contact us: jeasyui@gmail.com
* http://www.gnu.org/licenses/gpl.txt
* http://www.jeasyui.com/license_commercial.php
*
* jQuery EasyUI validatebox Extensions 1.0 beta
* jQuery EasyUI validatebox 组件扩展
* jeasyui.extensions.validatebox.self.js
* 开发 李溪林
* 最近更新：2013-07-31
*
* 依赖项：
*   1、jquery.jdirk.js v1.0 beta late
*   2、jeasyui.extensions.js v1.0 beta late
*
* Copyright (c) 2013 Lixilin personal All rights reserved.
* http://www.chenjianwei.org
*/

/*
功能说明：
*/
(function ($, undefined) {

    var rules = {
        //这种验证不需要后台用sql查询，参数不传入 查询的目标字段，只传入查询的目标字段的值，后台的参数名只能是Value和Key
        // param参数对象中各参数次序：
        //      0:内容名称
        //      1:提交地址
        //      2:可不传，若传则是key，或者key所在的控件的id
        //      3:与2一起传，若是string，则表示2是key；若是id，则表示2是id
        ajaxAddValidWithoutField: {
            validator: function (value, param) {
                var url = param[1];         //提交地址
                var va = value;             //验证值
                var type = param.length > 3 ? param[3] : "string";
                var key = type == "id" ? $("#" + param[2]).val() : "";
                var pa = {
                    Key: key,
                    Value: va
                };
                var result = $.util.requestAjaxBoolean(url, pa);
                return !result;
            },
            message: '{0}有重复记录'
        },
        //这种验证不需要后台用sql查询，参数不传入 查询的目标字段，只传入查询的目标字段的值，注意2个参数的次序，后台的参数名只能是Value和Key
        ajaxUpdateValidWithoutField: {
            validator: function (value, param) {
                var url = param[1];         //提交地址
                var va = value;             //验证值
                var key = param[2];
                var pa = {
                    Key: key,
                    Value: va
                };
                var result = $.util.requestAjaxBoolean(url, pa);
                return !result;
            },
            message: '{0}有重复记录'
        },
        //  必须是安全的密码字符(由字符或数字或特殊字符(~!@#$%^&*.)组成， 6-16 位)格式
        passwordNew: {
            validator: function (value) {
                str = $.string.isNullOrEmpty(value) ? "" : String(value);
                return (/^[\@A-Za-z0-9\!\#\$\%\^\&\*\.\~]{6,16}$/.test(str));
            },
            message: "输入的内容必须是安全的密码字符(由字符、数字或特殊字符(~!@#$%^&*.)组成，6 至 16 位)格式."
        },
        //  强制验证
        forceValid: {
            validator: function (value, param) {
                var result = param[1];
                return result;
            },
            message: "{0}"
        }
    };
    $.extend($.fn.validatebox.defaults.rules, rules);



    function setRequired(target, required) {
        var t = $(target), opts = t.validatebox("options");
        opts.required = required;
    };

    //从规则集合中获取“存在于现有validatebox规则集合”中的那部分规则
    function getEffectValidType(rs) {
        var thisRules = [];
        var allRules = $.fn.validatebox.defaults.rules;
        rs.forEach(function (validType) {
            var types = /([a-zA-Z_]+)(.*)/.exec(validType);
            var tempRules = allRules[types[1]];
            //var validParams = eval(types[2]);
            if (tempRules != undefined) { thisRules.push(validType); }
        });

        return thisRules;
    }

    function addValidType(target, rs) {
        var currentRules = [];
        if ($.util.isString(rs)) { currentRules.push(rs); }
        else if ($.util.isArray(rs)) { currentRules = rs; }
        else { return; }
        if (currentRules.length == 0) { return; }

        var thisRules = getEffectValidType(currentRules);
        if (thisRules.length == 0) { return; }

        var t = $(target), opts = t.validatebox("options");
        var _validType = opts.validType;
        if (_validType.length && _validType.length >= 0) {
            thisRules.forEach(function (rule) {
                _validType.push(rule);
            });
        }
        else {
            _validType = thisRules;
        }
        opts.validType = _validType;
    };

    function removeValidType(target, rs) {
        var currentRules = [];
        if ($.util.isString(rs)) { currentRules.push(rs); }
        else if ($.util.isArray(rs)) { currentRules = rs; }
        else { return; }
        if (currentRules.length == 0) { return; }

        var thisRules = getEffectValidType(currentRules);
        if (thisRules.length == 0) { return; }

        var t = $(target), opts = t.validatebox("options");
        var _validType = opts.validType;
        if (_validType.length && _validType.length > 0) {
            thisRules.forEach(function (rule) {
                $.array.remove(_validType, rule, function (item) { return rule == item; });
            });
            opts.validType = _validType;
        }
        else { return; }
    }

    var methods = {

        //  扩展 easyui-validatebox 组件的自定义方法；用于设置启用或者禁用 easyui-validatebox 控件的表单非空验证功能，该方法定义如下参数：
        //      required:   Boolean 类型的值，表示启用或者禁用 easyui-validatebox 控件的表单非空验证功能。
        //  返回值：返回表示当前 easyui-validatebox 控件的 jQuery 链式对象。
        setRequired: function (jq, required) { return jq.each(function () { setRequired(this, required); }); },

        // 扩展 easyui-validatebox 组件的自定义方法；用于动态添加 easyui-validatebox 控件的验证规则，该方法定义如下参数：
        //      rules:   表示要动态添加的规则，该参数可以是如下类型
        //          1、String类型：表示一个要动态添加的规则；
        //          2、Array类型：表示一组要动态添加的规则；
        //  返回值：返回表示当前 easyui-validatebox 控件的 jQuery 链式对象。
        addValidType: function (jq, rules) { return jq.each(function () { addValidType(this, rules); }); },

        // 扩展 easyui-validatebox 组件的自定义方法；用于动态移除 easyui-validatebox 控件的验证规则，该方法定义如下参数：
        //      rules:   表示要动态移除的规则，该参数可以是如下类型
        //          1、String类型：表示一个要动态移除的规则；
        //          2、Array类型：表示一组要动态移除的规则；
        //  返回值：返回表示当前 easyui-validatebox 控件的 jQuery 链式对象。
        removeValidType: function (jq, rules) { return jq.each(function () { removeValidType(this, rules); }); }
    };

    if ($.fn.validatebox.extensions != null && $.fn.validatebox.extensions.methods != null) {
        $.extend($.fn.validatebox.extensions.methods, methods);
    }
    $.extend($.fn.validatebox.methods, methods);

})(jQuery);
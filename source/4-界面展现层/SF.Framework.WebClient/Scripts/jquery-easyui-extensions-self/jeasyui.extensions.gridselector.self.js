﻿/**
* jQuery EasyUI 1.3.6
* Copyright (c) 2009-2013 www.jeasyui.com. All rights reserved.
*
* Licensed under the GPL or commercial licenses
* To use it on other terms please contact author: info@jeasyui.com
* http://www.gnu.org/licenses/gpl.txt
* http://www.jeasyui.com/license_commercial.php
*
* jQuery EasyUI gridselector Extensions 1.0 beta
* jQuery EasyUI gridselector 组件扩展
* jeasyui.extensions.gridselector.js
* 二次开发 李溪林
* 最近更新：2015-01-26
*
* 依赖项：
*   1、jquery.jdirk.js v1.0 beta late
*   2、jeasyui.extensions.js v1.0 beta late
*   3、jeasyui.extensions.menu.js v1.0 beta late
*   4、jeasyui.extensions.panel.js v1.0 beta late
*   5、jeasyui.extensions.window.js v1.0 beta late
*   6、jeasyui.extensions.dialog.js v1.0 beta late
*
* Copyright (c) 2013 ChenJianwei personal All rights reserved.
* http://www.chenjianwei.org
*/
(function ($, undefined) {

    $.util.namespace("$.easyui");



    var getDataGridMinWidth = function (pagination) {
        return pagination ? 300 : 250;
    };

    //对已选项进行重组，这将是调用回调函数onEnter时传递的参数，用来实现“打开选择器之后不进行任何操作就点击确定时依旧可以将原选项返回”的功能
    var getSelected = function (singleSelect, selected) {
        return singleSelect ? (selected ? selected : "") : ($.util.likeArrayNotString(selected) ? selected : (selected ? ($.util.isString(selected) ? selected.split(',').remove("") : [selected]) : []));
    };

    //最初的已选项，用来实现“datagrid1数据首次成功加载后自动选中已选项”的功能
    var getOriginalSelected = function (selected) {
        return $.util.likeArrayNotString(selected) ? $.array.clone(selected) : selected;
    };

    //检查是否需要扩展工具条
    var checkToolbar = function (ext, toolbar) {
        return ext && $.util.likeArrayNotString(toolbar);
    };

    //将toolbar的data对象组装成toolbar后返回
    var getToolbar = function (toolbar) {
        return $("<div class=\"grid-selector-toolbar\"></div>").toolbar({ data: toolbar });
    };

    //将toolbar的data对象组装成toolbar，并返回其div容器对象，可用于赋值datagrid的options的toolbar属性
    var getToolbarDiv = function (toolbar) {
        return getToolbar(toolbar).toolbar("toolbar");
    };

    //检查dialog调整尺寸之后是否需要对dialog内部布局调整尺寸
    var checkResizable = function (dialogOptions, w, h) {
        var minWidth = $.isNumeric(dialogOptions.minWidth) ? dialogOptions.minWidth : $.fn.panel.extensions.defaults.minHeight,
            maxWidth = $.isNumeric(dialogOptions.maxWidth) ? dialogOptions.maxWidth : $.fn.panel.extensions.defaults.maxWidth,
            minHeight = $.isNumeric(dialogOptions.minHeight) ? dialogOptions.minHeight : $.fn.panel.extensions.defaults.minHeight,
            maxHeight = $.isNumeric(dialogOptions.maxHeight) ? dialogOptions.maxHeight : $.fn.panel.extensions.defaults.maxHeight;
        var resizable = true;
        if (w > maxWidth) { resizable = false; }
        if (w < minWidth) { resizable = false; }
        if (h > maxHeight) { resizable = false; }
        if (h < minHeight) { resizable = false; }
        return resizable;
    };

    //移除datagrid1的分页提示信息，如，当前第X页，共N记录
    var removePaginationMessage = function (datagrid1) {
        datagrid1.datagrid("getPager").pagination({ displayMsg: "" });
    };

    //两个datagrid的selector的加载已选数据
    var loadSelectedData = function (selectedUrl, selected, datagrid2, callback) {
        if (!$.string.isNullOrWhiteSpace(selectedUrl) && selected) {
            var str = "";
            if ($.util.isString(selected)) {
                str = selected;
            }
            else if ($.util.isArray(selected)) {
                str = selected.join(",");
            }
            if (!$.string.isNullOrWhiteSpace(str)) {
                $.post(selectedUrl, { selected: str }, function (data) {
                    datagrid2.datagrid("loadData", data);
                    if ($.isFunction(callback)) { callback.call(this, data); }
                });
            }
        }
    };


    //组装查询条件并执行搜索
    var buildParamSearch = function (_this, builder, node, toolbarObj, datagrid1) {
        var param = builder.call(_this, node);
        if (toolbarObj) {
            var ser = toolbarObj.toolbar("getValues");
            $.extend(param, ser);
        }
        datagrid1.datagrid("load", param);
    };



    //两个datagrid的selector的选中行操作
    var selectRow = function (datagrid1Options, row, datagrid2, refresh, checkFirstSelected) {
        var idField = datagrid1Options.idField, idValue = idField ? row[idField] : row;
        var isExists = datagrid2.datagrid("getRowIndex", idValue) > -1;
        if (!isExists) {
            if (datagrid1Options.singleSelect) {
                //从datagrid2中移除所有row
                datagrid2.datagrid("loadData", []);
            }
            datagrid2.datagrid("appendRow", row);
            if ($.isFunction(refresh)) { refresh(); }
            if (checkFirstSelected) {
                if (datagrid2.datagrid("getRows").length == 1)
                { datagrid2.datagrid("checkRow", 0); }
                else
                {
                    var checkedRowIDs = datagrid2.datagrid("getChecked").map(function (item) { return item[idField]; });

                    datagrid2.datagrid("uncheckAll");
                    checkedRowIDs.forEach(function (checkedRowID) {
                        datagrid2.datagrid("checkRow", datagrid2.datagrid("getRowIndex", checkedRowID));
                    });
                }
            }
        }
    };

    //两个datagrid的selector的取消选中行操作
    var unselectRow = function (datagrid1Options, row, datagrid2, refresh, checkFirstSelected) {
        var idField = datagrid1Options.idField, idValue = idField ? row[idField] : row;
        var index = datagrid2.datagrid("getRowIndex", idValue);
        if (index > -1) {
            datagrid2.datagrid("uncheckRow", index);
            datagrid2.datagrid("deleteRow", index);
            if ($.isFunction(refresh)) { refresh(); }
            if (checkFirstSelected && datagrid2.datagrid("getChecked").length == 0 && datagrid2.datagrid("getRows").length > 0) {
                datagrid2.datagrid("checkRow", 0);
            }
        }
    };

    //两个datagrid的selector的首次数据加载完毕后根据“原始已选项”选中行的操作
    var selectRowOnFirst = function (datagrid1, data) {
        if (data) {
            if ($.util.likeArrayNotString(data)) {
                $.each(data, function (i, val) {
                    datagrid1.datagrid("selectRecord", val);
                });
            } else {
                datagrid1.datagrid("selectRecord", data);
            }
        }
    };

    //两个datagrid的selector的非首次数据加载完毕后
    // 1:对 datagrid1 中选中的、而在 datagrid2 中未选中的行进行取消选中操作
    // 2:对 datagrid2 中选中的、而在 datagrid1 中未选中的行进行选中操作
    var selectRowOnNotFirst = function (idField, datagrid1, datagrid2) {
        var selectedRows2 = datagrid2.datagrid("getRows"),
            selectedRowsIDs2 = $.array.map(selectedRows2, function (row) { return idField ? row[idField] : undefined; }),
            rows1 = datagrid1.datagrid("getRows"),
            selectedRows1 = datagrid1.datagrid("getSelections"),
            selectedRowsIDs1 = $.array.map(selectedRows1, function (row) { return idField ? row[idField] : undefined; }),
            moreRows = [], unselectedRows = [], lessRows = [];

        //找到 datagrid1 中选中的、而在 datagrid2 中未选中的行
        moreRows = selectedRows1.length > 0 ? $.array.filter(selectedRows1, function (row) { return idField ? !$.array.contains(selectedRowsIDs2, row[idField]) : true; }) : [];
        //找到 datagrid1 中未选中的行
        unselectedRows = rows1.length > 0 ? $.array.filter(rows1, function (row) { return idField ? !$.array.contains(selectedRowsIDs1, row[idField]) : true; }) : [];
        //找到 datagrid2 中选中的、而在 datagrid1 中未选中的行
        lessRows = unselectedRows.length > 0 ? $.array.filter(unselectedRows, function (row) { return idField ? $.array.contains(selectedRowsIDs2, row[idField]) : true; }) : [];

        $.each(moreRows, function (i, row) {
            var idValue = idField ? row[idField] : row;
            var index = datagrid1.datagrid("getRowIndex", idValue);
            datagrid1.datagrid("unselectRow", index);
        });
        $.each(lessRows, function (i, row) {
            if (idField) { datagrid1.datagrid("selectRecord", row[idField]); }
            else { datagrid1.datagrid("selectRow", datagrid1.datagrid("getRowIndex", row)); }
        });
    };

    //两个datagrid的selector的从已选datagrid中移除行的操作
    var removeRow = function (idField, row, datagrid1, datagrid2, refresh) {
        var idValue = idField ? row[idField] : row;
        var index = datagrid1.datagrid("getRowIndex", idValue);
        if (index > -1) {
            datagrid1.datagrid("unselectRow", index);
        }
        else {
            datagrid2.datagrid("deleteRow", datagrid2.datagrid("getRowIndex", idValue));
            if ($.isFunction(refresh)) { refresh(); }
        }
    }

    //两个datagrid的全部选中操作
    var selectAllRow = function (idField, datagrid1, refresh) {
        var rows = datagrid1.datagrid("getRows"), selectedRows = datagrid1.datagrid("getSelections");
        if (rows.length == selectedRows.length) { return; }
        var selectedRowsIDs = $.array.map(selectedRows, function (row) { return idField ? row[idField] : undefined; }),
            unselectedRows = rows.length > 0 ? $.array.filter(rows, function (row) { return idField ? !$.array.contains(selectedRowsIDs, row[idField]) : true; }) : [];

        $.each(unselectedRows, function (i, val) {
            if (idField) { datagrid1.datagrid("selectRecord", val[idField]); }
            else { datagrid1.datagrid("selectRow", datagrid1.datagrid("getRowIndex", val)); }
        });
        if ($.isFunction(refresh)) { refresh(); }
    };

    //两个datagrid的全部取消选中操作
    var unselectAllRow = function (datagrid1, datagrid2, refresh) {
        datagrid2.datagrid("loadData", []);
        datagrid1.datagrid("clearSelections");
        if ($.isFunction(refresh)) { refresh(); }
    };





    //  增加自定义扩展方法 $.easyui.showSingleDataGridSelector；该方法弹出一个 包含一个 easyui-datagrid 控件的选择框窗口；该方法定义如下参数：
    //      options: 这是一个 JSON-Object 对象；
    //              具体格式参考 $.easyui.showDialog 方法的参数 options 的格式；
    //              该参数格式在 $.easyui.showDialog 参数 options 格式基础上扩展了如下属性：
    //          extToolbar:这是一个boolean值，表示是否向datagrid扩展工具条；
    //          selected:这是一个string格式（多个则以英文逗号相连）或array格式的值，表示已选项；
    //          checked:这是一个string格式（多个则以英文逗号相连）或array格式的值，表示已选项中的选中（checked）项；
    //          onEnter :这是一个 function 对象，表示点击“确定”按钮时回调的函数；
    //          datagridOptions:这是一个 JSON-Object 对象，具体格式参考 easyui-dataGrid 方法的参数 options 的格式；
    //                          该参数格式在 easyui-dataGrid 参数 options 格式基础上扩展了如下属性：
    //                  selectedShowCheckBox:这是一个boolean值，表示 easyui-datagrid 中是否显示checkbox；
    //                      当selectedShowCheckBox为true时，调用onEnter函数时候传入的参数（row 或 rows）中的行对象属性中会存在“checked”属性来表示该行是否被选中；
    //                  selectedCheckFirstSelected:这是一个boolean值，表示在 selectedShowCheckBox 为true的前提下，自动check第一条select的数据行；
    //  返回值：返回弹出窗口的 easyui-dialog 控件对象(jQuery-DOM 格式)。并且在该对象中以属性datagrid表示待选datagrid控件对象。
    $.easyui.showSingleDataGridSelector = function (options) {
        if (options && options.topMost && $ != $.util.$) { return $.util.$.easyui.showSingleDataGridSelector.apply(this, arguments); }

        //datagrid的options
        var datagridOptions = options.datagridOptions ? options.datagridOptions : {};
        //计算 dialog 的最大和最小宽度
        var minDataGrid1Width = getDataGridMinWidth(datagridOptions.pagination),
            diaMinWidth = minDataGrid1Width + 20,
            diaRealWidth = options.width ? (options.width < diaMinWidth ? diaMinWidth : options.width) : diaMinWidth + 50;
        //将options与selectDialog的默认options合并
        var opts = $.extend({
            height: 360, minHeight: 360,
            title: "选择数据，" + (datagridOptions.singleSelect ? "单选" : "多选"),
            iconCls: "icon-hamburg-zoom",
            maximizable: true,
            collapsible: true,
            selected: null,
            checked: null,
            extToolbar: false,
            onEnter: function (val) { }
        }, options, { width: diaRealWidth, minWidth: diaMinWidth });

        var value = getSelected(datagridOptions.singleSelect, opts.selected), tempData = getOriginalSelected(value);
        var checkedValue = getSelected(datagridOptions.singleSelect, opts.checked);
        var dg = null,
            dia = $.easyui.showDialog($.extend({}, opts, {
                content: "<div class=\"grid-selector-container\"></div>",
                saveButtonText: "确定",
                saveButtonIconCls: "icon-ok",
                enableApplyButton: false,
                toolbar: "",
                onSave: function () {
                    if ($.isFunction(opts.onEnter)) { return opts.onEnter.call(dg[0], value); }
                }
            }));

        $.util.exec(function () {
            var container = dia.find(".grid-selector-container"), extToolbar = checkToolbar(opts.extToolbar, datagridOptions.toolbar),
                refreshValue = function () {
                    var tOpts = dg.datagrid("options");
                    if (dgOpts.singleSelect) {
                        var row = dg.datagrid("getSelected");
                        value = row ? row : null;
                    } else {
                        value = dg.datagrid("getSelections");
                    }
                },
                showCheckBox = datagridOptions.selectedShowCheckBox ? $.string.toBoolean(datagridOptions.selectedShowCheckBox) : false,
                checkFirstSelected = datagridOptions.selectedCheckFirstSelected ? $.string.toBoolean(datagridOptions.selectedCheckFirstSelected) : false;

            var dgOpts = $.extend({ striped: true, checkOnSelect: true, selectOnCheck: true, rownumbers: true }, datagridOptions, {
                noheader: true, fit: true, border: false, doSize: true, toolbar: null,
                onSelect: function (index, row) {
                    if (showCheckBox && checkFirstSelected && !row.checked) {
                        // 由于 check要触发select，并且触发后先执行 select 再执行 check ，因此为防止死循环，在此直接修改 checked 属性为 true。
                        // 对 尚未checked的row 进行 select 操作，先执行 onselect ，再调用 checkRow 方法，自动触发 onselect ，最后执行 oncheck 。（onselect执行了两次）
                        if ($(this).datagrid("getChecked").length == 0) { row.checked = true; $(this).datagrid("checkRow", index); }
                        else { refreshValue(); }
                    }
                    else { refreshValue(); }
                },
                onUnselect: function (index, row) {
                    // 由于 check要触发select，并且触发后先执行 select 再执行 check ，因此为防止死循环，在此直接修改 checked 属性为 false。
                    // 对 已经checked的row 进行 unselect 操作，先执行 onUnselect ，再调用 uncheckRow 方法，自动触发 onUnselect ，最后执行 onUncheck 。（onUnselect执行了两次）
                    if (showCheckBox && row.checked == true) { row.checked = false; $(this).datagrid("uncheckRow", index); }
                    else { refreshValue(); }
                },
                onSelectAll: function (rows) { refreshValue(); },
                onUnselectAll: function (rows) { refreshValue(); },
                onCheck: function (index, row) {
                    if (showCheckBox) {
                        row.checked = true;
                    }
                },
                onUncheck: function (index, row) {
                    if (showCheckBox) {
                        row.checked = false;
                        // unselect => 触发 uncheck 之后，若 checkFirstSelected 为 true，则可能需要check 已selected中的第一个row
                        if (checkFirstSelected && $(this).datagrid("getChecked").length == 0) {
                            var nextSelectedRow = $(this).datagrid("getSelected");
                            if (nextSelectedRow != null) {
                                $(this).datagrid("checkRow", $(this).datagrid("getRowIndex", nextSelectedRow));
                            }
                        }
                    }
                },
                onCheckAll: function (rows) {
                    if (showCheckBox) {
                        rows.forEach(function (item) { item.checked = true; });
                    }
                },
                onUncheckAll: function (rows) {
                    if (showCheckBox) {
                        rows.forEach(function (item) { item.checked = false; });
                    }
                },
                onLoadSuccess: function (data) {
                    $.fn.datagrid.defaults.onLoadSuccess.apply(this, arguments);
                    if ($.isFunction(datagridOptions.onLoadSuccess) && datagridOptions.onLoadSuccess != $.fn.datagrid.defaults.onLoadSuccess) {
                        datagridOptions.onLoadSuccess.apply(this, arguments);
                    }
                    if (!tempData) { return; }
                    var t = $(this);

                    //先处理需要check的checkedValue
                    if (showCheckBox) {
                        if ($.util.likeArrayNotString(checkedValue)) {
                            checkedValue.forEach(function (item) {
                                //checkRow会触发select，因此无需额外selectRecord
                                var theRow = t.datagrid("findRow", item);
                                if (theRow) {
                                    var rowIndex = t.datagrid("getRowIndex", theRow);
                                    dg.datagrid("checkRow", rowIndex);
                                }
                            });
                        }
                        else {
                            var theRow = t.datagrid("findRow", checkedValue);
                            if (theRow) {
                                var rowIndex = t.datagrid("getRowIndex", theRow);
                                dg.datagrid("checkRow", rowIndex);
                            }
                        }
                    }

                    //再处理需要select的tempData，可以过滤掉已经check的row
                    if ($.util.likeArrayNotString(tempData)) {
                        $.each(tempData, function (i, val) {
                            if (showCheckBox) {
                                //若初始selected项存在于checkedValue中，则忽略（因为前面已经处理）
                                if ($.array.contains(checkedValue, val)) { }
                                else { dg.datagrid("selectRecord", val); }
                            }
                            else { dg.datagrid("selectRecord", val); }
                        });
                    } else { dg.datagrid("selectRecord", tempData); }
                }
            });
            dg = container.addClass("grid-selector");
            if (extToolbar) {
                dgOpts.toolbar = getToolbarDiv(datagridOptions.toolbar);
            }
            if (showCheckBox) {
                var ckColumn = { field: "ck", title: "选中", checkbox: true, width: 90 };
                if (dgOpts.frozenColumns) {
                    if (dgOpts.frozenColumns.length > 0) {
                        var temp1 = $.array.clone(dgOpts.frozenColumns);
                        var temp2 = $.array.clone(temp1[0]);
                        temp2.insert(0, ckColumn);
                        temp1[0] = temp2;
                        $.extend(dgOpts, { frozenColumns: temp1 });
                    }
                }
                else if (dgOpts.columns) {
                    if (dgOpts.columns.length > 0) {
                        var temp1 = $.array.clone(dgOpts.columns);
                        var temp2 = $.array.clone(temp1[0]);
                        temp2.insert(0, ckColumn);
                        temp1[0] = temp2;
                        $.extend(dgOpts, { columns: temp1 });
                    }
                }
            }
            dia.datagrid = dg.datagrid(dgOpts);
        });

        return dia;
    }



    //  增加自定义扩展方法 $.easyui.showDblDataGridSelector；该方法弹出一个包含两个 easyui-datagrid 控件的选择框窗口；该方法定义如下参数：
    //      options: 这是一个 JSON-Object 对象；
    //              具体格式参考 $.easyui.showDialog 方法的参数 options 的格式；
    //              该参数格式在 $.easyui.showDialog 参数 options 格式基础上扩展了如下属性：
    //          extToolbar:这是一个boolean值，表示是否向datagrid扩展工具条；
    //          selected:这是一个string格式（多个则以英文逗号相连）或array格式的值，表示已选项；
    //          onEnter :这是一个 function 对象，表示点击“确定”按钮时回调的函数；
    //          datagridOptions:这是一个 JSON-Object 对象，具体格式参考 easyui-dataGrid 方法的参数 options 的格式；
    //                          该参数格式在 easyui-dataGrid 参数 options 格式基础上扩展了如下属性：
    //                  selectedUrl:这是一个string格式的值，表示 当 options.selected 参数存在时，已选项datagrid要获取具体数据时要执行的url；
    //  返回值：返回弹出窗口的 easyui-dialog 控件对象(jQuery-DOM 格式)。并且在该对象中以属性datagrid1、datagrid2分别表示待选datagrid控件对象和已选datagrid控件对象。
    $.easyui.showDblDataGridSelector = function (options) {
        if (options && options.topMost && $ != $.util.$) { return $.util.$.easyui.showDblDataGridSelector.apply(this, arguments); }
        //datagrid的options
        var datagridOptions = options.datagridOptions ? options.datagridOptions : {};
        //计算 dialog 的最大和最小宽度
        var defaultCenterWith = 55, minDataGrid1Width = getDataGridMinWidth(datagridOptions.pagination),
            diaMinWidth = (minDataGrid1Width * 2) + defaultCenterWith,
            diaRealWidth = options.width ? (options.width < diaMinWidth ? diaMinWidth : options.width) : diaMinWidth + 50;
        //将options与selectDialog的默认options合并
        var opts = $.extend({
            height: 580, minHeight: 480,
            title: "选择数据，" + (datagridOptions.singleSelect ? "单选" : "多选"),
            iconCls: "icon-hamburg-zoom",
            maximizable: true,
            collapsible: true,
            selected: null,
            extToolbar: false,
            onEnter: function (value) { }
        }, options, { width: diaRealWidth, minWidth: diaMinWidth, centerWidth: defaultCenterWith });

        var value = getSelected(datagridOptions.singleSelect, opts.selected), tempData = getOriginalSelected(value);

        var dg = null,
            dia = $.easyui.showDialog($.extend({}, opts, {
                content: "<div class=\"grid-selector-container\"></div>",
                saveButtonText: "确定",
                saveButtonIconCls: "icon-ok",
                enableApplyButton: false,
                toolbar: "",
                onSave: function () {
                    if ($.isFunction(opts.onEnter)) { return opts.onEnter.call(dg[0], value); }
                }
            }));
        $.util.exec(function () {
            var diaOpts = dia.dialog("options"), onResize = diaOpts.onResize, init = false,
                extToolbar = checkToolbar(opts.extToolbar, datagridOptions.toolbar),
                container = dia.find(".grid-selector-container"),
                width = (($.isNumeric(opts.width) ? opts.width : dia.outerWidth()) - opts.centerWidth) / 2,
                leftPanel = $("<div data-options=\"region: 'west', split: false, border: false\"></div>").width(width).appendTo(container),
                centerPanel = $("<div data-options=\"region: 'center', border: true, bodyCls: 'grid-selector-buttons'\"></div>").appendTo(container),
                rightPanel = $("<div data-options=\"region: 'east', split: false, border: false\"></div>").width(width).appendTo(container),
                dgOpts = $.extend({}, datagridOptions),
                dgOpts1 = $.extend({ striped: true, checkOnSelect: true, selectOnCheck: true, rownumbers: true }, dgOpts, {
                    title: "待选择项", toolbar: null, fit: true, border: false, doSize: true, enableRowContextMenu: false,
                    noheader: false, iconCls: null, collapsible: false, minimizable: false, maximizable: false, closable: false,
                    onSelect: function (rowIndex, rowData) {
                        $.fn.datagrid.defaults.onSelect.apply(this, arguments);
                        if ($.isFunction(dgOpts.onSelect) && dgOpts.onSelect != $.fn.datagrid.defaults.onSelect) {
                            dgOpts.onSelect.apply(this, arguments);
                        }
                        selectRow(dgOpts, rowData, dg2, refreshValue);
                    },
                    onUnselect: function (rowIndex, rowData) {
                        $.fn.datagrid.defaults.onUnselect.apply(this, arguments);
                        if ($.isFunction(dgOpts.onUnselect) && dgOpts.onUnselect != $.fn.datagrid.defaults.onUnselect) {
                            dgOpts.onUnselect.apply(this, arguments);
                        }
                        unselectRow(dgOpts, rowData, dg2, refreshValue);
                    },
                    onLoadSuccess: function (data) {
                        $.fn.datagrid.defaults.onLoadSuccess.apply(this, arguments);
                        if ($.isFunction(dgOpts.onLoadSuccess) && dgOpts.onLoadSuccess != $.fn.datagrid.defaults.onLoadSuccess) {
                            dgOpts.onLoadSuccess.apply(this, arguments);
                        }
                        if (!init) {
                            selectRowOnFirst(dg1, tempData);
                            init = true;
                        }
                        else {
                            selectRowOnNotFirst(dgOpts.idField, dg1, dg2);
                        }
                    }
                }),
                dgOpts2 = $.extend({}, dgOpts1, {
                    url: null, queryParams: {}, remoteSort: false, pagination: false, title: "已选择项", iconCls: null, enableRowContextMenu: false,
                    onSelect: function (rowIndex, rowData) {
                        $.fn.datagrid.defaults.onSelect.apply(this, arguments);
                        removeRow(dgOpts.idField, rowData, dg1, dg2, refreshValue);
                    },
                    onUnselect: $.fn.datagrid.defaults.onUnselect,
                    onLoadSuccess: $.fn.datagrid.defaults.onLoadSuccess
                }),
                dg1 = $("<div></div>").appendTo(leftPanel),
                dg2 = dg = $("<div class=\"grid-selector\"></div>").appendTo(rightPanel),

                btn1 = dgOpts.singleSelect ? null : $("<a></a>").linkbutton({ plain: true, iconCls: "pagination-last" }).tooltip({ content: "选择全部" }).appendTo(centerPanel).click(function () {
                    selectAllRow(dgOpts.idField, dg1, refreshValue);
                }),
                btn4 = dgOpts.singleSelect ? null : $("<a></a>").linkbutton({ plain: true, iconCls: "pagination-first" }).tooltip({ content: "取消全部" }).appendTo(centerPanel).click(function () {
                    unselectAllRow(dg1, dg2, refreshValue);
                }),
                refreshValue = function () {
                    if (dgOpts.singleSelect) {
                        var row = dg2.datagrid("getRows");
                        value = row.length > 0 ? row[0] : null;
                    }
                    else {
                        var rows = dg2.datagrid("getRows");
                        value = $.array.clone(rows);
                    }
                };
            diaOpts.onResize = function (w, h) {
                if ($.isFunction(onResize)) { onResize.apply(this, arguments); }
                $.util.exec(function () {
                    if (checkResizable(diaOpts, w, h)) {
                        var ww = (dia.panel("options").width - diaOpts.centerWidth) / 2;
                        leftPanel.panel("resize", { width: ww });
                        rightPanel.panel("resize", { width: ww });
                        container.layout("resize");
                    }
                });
            };
            if (extToolbar) {
                dgOpts1.toolbar = getToolbarDiv(datagridOptions.toolbar);
            }
            dia.datagrid1 = dg1.datagrid(dgOpts1);
            dia.datagrid2 = dg2.datagrid(dgOpts2);

            if (dgOpts.pagination) { removePaginationMessage(dg1); }
            loadSelectedData(dgOpts.selectedUrl, tempData, dg2, refreshValue);

            container.layout({ fit: true });
        });
        return dia;
    };



    //  增加自定义扩展方法 $.easyui.showTreeSelector；该方法弹出一个 包含一个 easyui-tree 控件的选择框窗口；该方法定义如下参数：
    //      options: 这是一个 JSON-Object 对象；
    //              具体格式参考 $.easyui.showDialog 方法的参数 options 的格式；
    //              该参数格式在 $.easyui.showDialog 参数 options 格式基础上扩展了如下属性：
    //          selected:这是一个string格式（多个则以英文逗号相连）或array格式的值，表示已选项；
    //          onEnter :这是一个 function 对象，表示点击“确定”按钮时回调的函数；
    //          treeOptions:这是一个 JSON-Object 对象，具体格式参考 easyui-tree 方法的参数 options 的格式；
    //  返回值：返回弹出窗口的 easyui-dialog 控件对象(jQuery-DOM 格式)。并且在该对象中以属性tree表示tee控件对象。
    $.easyui.showTreeSelector = function (options) {
        if (options && options.topMost && $ != $.util.$) { options.topMost = false; return $.util.$.easyui.showTreeSelector.call(this, options); }
        //tree的options
        var treeOptions = options.treeOptions ? options.treeOptions : {};
        //计算 dialog 的最大和最小宽度
        var diaMinWidth = 250,
            diaRealWidth = options.width ? (options.width < diaMinWidth ? diaMinWidth : options.width) : diaMinWidth + 50;
        //将options与selectDialog的默认options合并
        var opts = $.extend({
            height: 360, minHeight: 360,
            title: "选择数据，" + (!treeOptions.checkbox ? "单选" : "多选"),
            iconCls: "icon-hamburg-zoom",
            maximizable: true,
            collapsible: true,
            selected: null,
            onEnter: function (val) { }
        }, options, { width: diaRealWidth, minWidth: diaMinWidth });

        var value = getSelected(!treeOptions.checkbox, opts.selected), tempData = getOriginalSelected(value);

        var tree1 = null,
            dia = $.easyui.showDialog($.extend({}, opts, {
                content: "<div class=\"grid-selector-container\"></div>",
                saveButtonText: "确定",
                saveButtonIconCls: "icon-ok",
                enableApplyButton: false,
                toolbar: "",
                onSave: function () {
                    if ($.isFunction(opts.onEnter)) { return opts.onEnter.call(tree1[0], value); }
                }
            }));

        $.util.exec(function () {
            var container = dia.find(".grid-selector-container"), panel = dia.dialog("contentPanel"),
                refreshValue = function () {
                    if (!treeOpts.checkbox) {
                        var node = tree1.tree("getSelected");
                        value = node ? node : null;
                    } else {
                        value = tree1.tree("getChecked");
                    }
                };
            var treeOpts = $.extend({ animate: true, lines: true }, treeOptions, {
                dnd: false, enableContextMenu: false,
                onBeforeSelect: function (node) {
                    $.fn.tree.defaults.onBeforeSelect.apply(this, arguments);
                    if ($.isFunction(treeOptions.onBeforeSelect) && treeOptions.onBeforeSelect != $.fn.tree.defaults.onBeforeSelect) {
                        treeOptions.onBeforeSelect.apply(this, arguments);
                    }
                },
                onSelect: function (node) {
                    $.fn.tree.defaults.onSelect.apply(this, arguments);
                    if ($.isFunction(treeOptions.onSelect) && treeOptions.onSelect != $.fn.tree.defaults.onSelect) {
                        treeOptions.onSelect.apply(this, arguments);
                    }
                    if (!treeOptions.checkbox)
                    { refreshValue(); }
                },
                onBeforeCheck: function (node, check) {
                    $.fn.tree.defaults.onBeforeCheck.apply(this, arguments);
                    if ($.isFunction(treeOptions.onBeforeCheck) && treeOptions.onBeforeCheck != $.fn.tree.defaults.onBeforeCheck) {
                        treeOptions.onBeforeCheck.apply(this, arguments);
                    }
                },
                onCheck: function (node, check) {
                    $.fn.tree.defaults.onCheck.apply(this, arguments);
                    if ($.isFunction(treeOptions.onCheck) && treeOptions.onCheck != $.fn.tree.defaults.onCheck) {
                        treeOptions.onCheck.apply(this, arguments);
                    }
                    if (treeOptions.checkbox)
                    { refreshValue(); }
                },
                onBeforeLoad: function (node, param) {
                    $.fn.tree.defaults.onBeforeLoad.apply(this, arguments);
                    if ($.isFunction(treeOptions.onBeforeLoad) && treeOptions.onBeforeLoad != $.fn.tree.defaults.onBeforeLoad) {
                        treeOptions.onBeforeLoad.apply(this, arguments);
                    }
                    $.easyui.loading({ locale: panel, msg: "数据加载中..." });
                },
                onLoadSuccess: function (node, data) {
                    $.fn.tree.defaults.onLoadSuccess.apply(this, arguments);
                    if ($.isFunction(treeOptions.onLoadSuccess) && treeOptions.onLoadSuccess != $.fn.tree.defaults.onLoadSuccess) {
                        treeOptions.onLoadSuccess.apply(this, arguments);
                    }
                    $.easyui.loaded(panel);
                    if (tempData && data.length > 0) {
                        if (treeOpts.checkbox) {
                            if ($.util.likeArrayNotString(tempData)) {
                                $.each(tempData, function (i, val) {
                                    var tempNode = tree1.tree("find", val);
                                    if (tempNode) { tree1.tree("check", tempNode.target); }
                                });
                            }
                        }
                        else {
                            if ($.util.isString(tempData)) {
                                var tempNode = tree1.tree("find", tempData);
                                if (tempNode) { tree1.tree("select", tempNode.target); }
                            }
                        }
                    }
                }
            });
            tree1 = container.addClass("grid-selector");
            dia.tree = tree1.tree(treeOpts);
        });

        return dia;
    };


    //  增加自定义扩展方法 $.easyui.showTreeDblDataGridSelector；该方法弹出一个包含 一个 easyui-tree 和两个 easyui-datagrid 控件的选择框窗口；该方法定义如下参数：
    //      options: 这是一个 JSON-Object 对象；
    //              具体格式参考 $.easyui.showDialog 方法的参数 options 的格式；
    //              该参数格式在 $.easyui.showDialog 参数 options 格式基础上扩展了如下属性：
    //          treeWidth:这是个数值，表示tree所在panel的宽度；
    //          treeTitle:这是一个string格式的值，表示tree所在panel的标题；
    //          extToolbar:这是一个boolean值，表示是否向datagrid扩展工具条；
    //          selected:这是一个string格式（多个则以英文逗号相连）或array格式的值，表示已选项；
    //          checked:这是一个string格式（多个则以英文逗号相连）或array格式的值，表示已选项中的选中（checked）项；
    //          onEnter :这是一个 function 对象，表示点击“确定”按钮时回调的函数；
    //          treeOptions:这是一个 JSON-Object 对象，具体格式参考 easyui-tree 方法的参数 options 的格式；
    //                      该参数格式在 easyui-tree 参数 options 格式基础上扩展了如下属性：
    //                  onSelectParamBuild:这是一个 function 对象，表示触发“onSelect”事件时，利用该事件参数node对象组装最终param对象的方式。若不指定该参数，则tree的select操作将以参数{ nodeId:node.id }去查询datagrid数据；
    //          datagridOptions:这是一个 JSON-Object 对象，具体格式参考 easyui-dataGrid 方法的参数 options 的格式；
    //                          该参数格式在 easyui-dataGrid 参数 options 格式基础上扩展了如下属性：
    //                  selectedUrl:这是一个string格式的值，表示 当 options.selected 参数存在时，已选项datagrid要获取具体数据时要执行的url；
    //                  selectedShowCheckBox:这是一个boolean值，表示当选中项在“已选datagrid”中时是否显示checkbox；
    //                      当selectedShowCheckBox为true时，调用onEnter函数时候传入的参数（row 或 rows）中的行对象属性中会存在“checked”属性来表示该行是否被选中；
    //                  selectedCheckFirstSelected:这是一个boolean值，表示在 selectedShowCheckBox 为true的前提下，自动check第一条select的数据行；
    //  返回值：返回弹出窗口的 easyui-dialog 控件对象(jQuery-DOM 格式)。并且在该对象中以属性tree、datagrid1、datagrid2分别表示tree控件对象、待选datagrid控件对象和已选datagrid控件对象。
    $.easyui.showTreeDblDataGridSelector = function (options) {
        if (options && options.topMost && $ != $.util.$) { return $.util.$.easyui.showTreeDblDataGridSelector.apply(this, arguments); }

        //tree的options
        var treeOptions = options.treeOptions ? options.treeOptions : {};
        if (!$.isFunction(treeOptions.onSelectParamBuild)) { treeOptions.onSelectParamBuild = function (node) { return { nodeId: node.id }; }; }
        //datagrid的options
        var datagridOptions = options.datagridOptions ? options.datagridOptions : {};
        //计算 dialog 和 tree所在面板 的最大和最小宽度
        var defaultCenterWith = 55, defaultTreeWidth = 200, treeMinWith = 150, treeMaxWith = 300, minDataGrid1Width = getDataGridMinWidth(datagridOptions.pagination),
            treeRealWith = options.treeWidth ? (options.treeWidth < treeMinWith ? treeMinWith : (options.treeWidth > treeMaxWith ? treeMaxWith : options.treeWidth)) : defaultTreeWidth,
            diaMinWidth = (minDataGrid1Width * 2) + defaultCenterWith + treeRealWith,
            diaRealWidth = options.width ? (options.width < diaMinWidth ? diaMinWidth : options.width) : diaMinWidth + 50;
        //将options与selectDialog的默认options合并
        var opts = $.extend({
            height: 580, minHeight: 480,
            title: "选择数据，" + (datagridOptions.singleSelect ? "单选" : "多选"),
            iconCls: "icon-hamburg-zoom",
            maximizable: true,
            collapsible: true,
            selected: null,
            checked: null,
            treeTitle: null,//tree所在面板的标题
            extToolbar: false,
            onEnter: function (value) { }
        }, options, { width: diaRealWidth, minWidth: diaMinWidth, centerWidth: defaultCenterWith, treeWidth: treeRealWith });

        var value = getSelected(datagridOptions.singleSelect, opts.selected), tempData = getOriginalSelected(value);
        var checkedValue = getSelected(datagridOptions.singleSelect, opts.checked);
        var dg = null,
            dia = $.easyui.showDialog($.extend({}, opts, {
                content: "<div class=\"grid-selector-container\"></div>",
                saveButtonText: "确定",
                saveButtonIconCls: "icon-ok",
                enableApplyButton: false,
                toolbar: "",
                onSave: function () {
                    if ($.isFunction(opts.onEnter)) { return opts.onEnter.call(dg[0], value); }
                }
            }));
        $.util.exec(function () {
            var diaOpts = dia.dialog("options"), onResize = diaOpts.onResize, init = false,
                extToolbar = checkToolbar(opts.extToolbar, datagridOptions.toolbar),
                width = (($.isNumeric(opts.width) ? opts.width : dia.outerWidth()) - opts.treeWidth - opts.centerWidth) / 2,
                container = dia.find(".grid-selector-container"),
                leftPanel = $.string.isNullOrWhiteSpace(opts.treeTitle) ? $("<div data-options=\"region: 'west', split: false, border: false\"></div>").width(opts.treeWidth).appendTo(container) : $("<div data-options=\"region: 'west', title: '" + opts.treeTitle + "',split: false, border: false\"></div>").width(opts.treeWidth).appendTo(container),
                centerPanel = $("<div data-options=\"region: 'center', border: true, bodyCls: 'grid-selector-self-center'\"></div>").appendTo(container),
                inLayout = $("<div />").appendTo(centerPanel),
                inLeftPanel = $("<div data-options=\"region: 'west', split: false, border: false\"></div>").width(width).appendTo(inLayout),
                inCenterPanel = $("<div data-options=\"region: 'center', border: true, bodyCls: 'grid-selector-buttons'\"></div>").appendTo(inLayout),
                inRightPanel = $("<div data-options=\"region: 'east', split: false, border: false\"></div>").width(width).appendTo(inLayout),
                treeOpts = $.extend({ animate: true, lines: true }, treeOptions, {
                    checkbox: false, dnd: false, enableContextMenu: false,
                    onBeforeSelect: function (node) {
                        var pass = $.fn.tree.defaults.onBeforeSelect.apply(this, arguments);
                        if (pass == undefined) { pass = true; }
                        if (pass && $.isFunction(treeOptions.onBeforeSelect) && treeOptions.onBeforeSelect != $.fn.tree.defaults.onBeforeSelect) {
                            pass = treeOptions.onBeforeSelect.apply(this, arguments);
                        }
                        return pass;
                    },
                    onSelect: function (node) {
                        $.fn.tree.defaults.onSelect.apply(this, arguments);
                        if ($.isFunction(treeOptions.onSelect) && treeOptions.onSelect != $.fn.tree.defaults.onSelect) {
                            treeOptions.onSelect.apply(this, arguments);
                        }
                        buildParamSearch(this, treeOptions.onSelectParamBuild, node, toolbarObj, dg1);
                    },
                    onBeforeLoad: function (node, param) {
                        $.fn.tree.defaults.onBeforeLoad.apply(this, arguments);
                        if ($.isFunction(treeOptions.onBeforeLoad) && treeOptions.onBeforeLoad != $.fn.tree.defaults.onBeforeLoad) {
                            treeOptions.onBeforeLoad.apply(this, arguments);
                        }
                        $.easyui.loading({ locale: leftPanel, msg: "数据加载中..." });
                    },
                    onLoadSuccess: function (node, data) {
                        $.fn.tree.defaults.onLoadSuccess.apply(this, arguments);
                        if ($.isFunction(treeOptions.onLoadSuccess) && treeOptions.onLoadSuccess != $.fn.tree.defaults.onLoadSuccess) {
                            treeOptions.onLoadSuccess.apply(this, arguments);
                        }
                        $.easyui.loaded(leftPanel);
                    }
                }),
                dgOpts = $.extend({}, datagridOptions),
                showCheckBox = dgOpts.selectedShowCheckBox ? $.string.toBoolean(dgOpts.selectedShowCheckBox) : false,
                checkFirstSelected = dgOpts.selectedCheckFirstSelected ? $.string.toBoolean(dgOpts.selectedCheckFirstSelected) : false,
                toolbarObj = extToolbar ? getToolbar(dgOpts.toolbar) : null,
                toolbarDiv = toolbarObj ? toolbarObj.toolbar("toolbar") : null,
                dgOpts1 = $.extend({ striped: true, checkOnSelect: true, selectOnCheck: true, rownumbers: true }, dgOpts, {
                    title: "待选择项", toolbar: toolbarDiv, fit: true, border: false, doSize: true, enableRowContextMenu: false,
                    noheader: false, iconCls: null, collapsible: false, minimizable: false, maximizable: false, closable: false,
                    onSelect: function (rowIndex, rowData) {
                        $.fn.datagrid.defaults.onSelect.apply(this, arguments);
                        if ($.isFunction(dgOpts.onSelect) && dgOpts.onSelect != $.fn.datagrid.defaults.onSelect) {
                            dgOpts.onSelect.apply(this, arguments);
                        }
                        selectRow(dgOpts, rowData, dg2, refreshValue, (showCheckBox && checkFirstSelected));
                    },
                    onUnselect: function (rowIndex, rowData) {
                        $.fn.datagrid.defaults.onUnselect.apply(this, arguments);
                        if ($.isFunction(dgOpts.onUnselect) && dgOpts.onUnselect != $.fn.datagrid.defaults.onUnselect) {
                            dgOpts.onUnselect.apply(this, arguments);
                        }
                        unselectRow(dgOpts, rowData, dg2, refreshValue, (showCheckBox && checkFirstSelected));
                    },
                    onLoadSuccess: function (data) {
                        $.fn.datagrid.defaults.onLoadSuccess.apply(this, arguments);
                        if ($.isFunction(dgOpts.onLoadSuccess) && dgOpts.onLoadSuccess != $.fn.datagrid.defaults.onLoadSuccess) {
                            dgOpts.onLoadSuccess.apply(this, arguments);
                        }
                        if (!init) {
                            selectRowOnFirst(dg1, tempData);
                            init = true;
                        }
                        else {
                            selectRowOnNotFirst(dgOpts.idField, dg1, dg2);
                        }
                    }
                }),
                dgOpts2 = $.extend({}, dgOpts1, {
                    url: null, queryParams: {}, remoteSort: false, pagination: false, title: "已选择项", iconCls: null, toolbar: null, enableRowContextMenu: false,
                    onSelect: function (rowIndex, rowData) {
                        $.fn.datagrid.defaults.onSelect.apply(this, arguments);
                        removeRow(dgOpts.idField, rowData, dg1, dg2, refreshValue);
                    },
                    onUnselect: $.fn.datagrid.defaults.onUnselect,
                    onCheck: function (rowIndex, rowData) {
                        $.fn.datagrid.defaults.onCheck.apply(this, arguments);
                        if (showCheckBox) {
                            rowData.checked = true;
                        }
                    },
                    onUncheck: function (rowIndex, rowData) {
                        $.fn.datagrid.defaults.onUncheck.apply(this, arguments);
                        if (showCheckBox) {
                            rowData.checked = false;
                        }
                    },
                    onCheckAll: function (rows) {
                        $.fn.datagrid.defaults.onCheckAll.apply(this, arguments);
                        if (showCheckBox) {
                            rows.forEach(function (item) { item.checked = true; });
                        }
                    },
                    onUncheckAll: function (rows) {
                        $.fn.datagrid.defaults.onUncheckAll.apply(this, arguments);
                        if (showCheckBox) {
                            rows.forEach(function (item) { item.checked = false; });
                        }
                    },
                    onLoadSuccess: $.fn.datagrid.defaults.onLoadSuccess
                }),
                tree1 = $("<ul />").appendTo(leftPanel),
                dg1 = $("<div></div>").appendTo(inLeftPanel),
                dg2 = dg = $("<div class=\"grid-selector\"></div>").appendTo(inRightPanel),
                btn1 = dgOpts.singleSelect ? null : $("<a></a>").linkbutton({ plain: true, iconCls: "pagination-last" }).tooltip({ content: "选择全部" }).appendTo(inCenterPanel).click(function () {
                    selectAllRow(dgOpts.idField, dg1, refreshValue);
                }),
                btn4 = dgOpts.singleSelect ? null : $("<a></a>").linkbutton({ plain: true, iconCls: "pagination-first" }).tooltip({ content: "取消全部" }).appendTo(inCenterPanel).click(function () {
                    unselectAllRow(dg1, dg2, refreshValue);
                }),
                refreshValue = function () {
                    if (dgOpts.singleSelect) {
                        var row = dg2.datagrid("getRows");
                        value = row.length > 0 ? row[0] : null;
                    }
                    else {
                        var rows = dg2.datagrid("getRows");
                        value = $.array.clone(rows);
                    }
                };

            if (showCheckBox) {
                var ckColumn = { field: "ck", title: "选中", checkbox: true, width: 90 };
                if (dgOpts2.frozenColumns) {
                    if (dgOpts2.frozenColumns.length > 0) {
                        var temp1 = $.array.clone(dgOpts2.frozenColumns);
                        var temp2 = $.array.clone(temp1[0]);
                        temp2.insert(0, ckColumn);
                        temp1[0] = temp2;
                        $.extend(dgOpts2, { frozenColumns: temp1 });
                    }
                }
                else if (dgOpts2.columns) {
                    if (dgOpts2.columns.length > 0) {
                        var temp1 = $.array.clone(dgOpts2.columns);
                        var temp2 = $.array.clone(temp1[0]);
                        temp2.insert(0, ckColumn);
                        temp1[0] = temp2;
                        $.extend(dgOpts2, { columns: temp1 });
                    }
                }
            }

            diaOpts.onResize = function (w, h) {
                if ($.isFunction(onResize)) { onResize.apply(this, arguments); }
                $.util.exec(function () {
                    if (checkResizable(diaOpts, w, h)) {
                        var ww = (dia.panel("options").width - diaOpts.treeWidth - diaOpts.centerWidth) / 2;
                        inLeftPanel.panel("resize", { width: ww });
                        inRightPanel.panel("resize", { width: ww });
                        inLayout.layout("resize");
                    }
                });
            };

            inLayout.layout({ fit: true });

            dia.tree = tree1.tree(treeOpts);
            dia.datagrid1 = dg1.datagrid(dgOpts1);
            dia.datagrid2 = dg2.datagrid(dgOpts2);

            if (dgOpts.pagination) { removePaginationMessage(dg1); }
            loadSelectedData(dgOpts.selectedUrl, tempData, dg2, function (selectedData) {
                refreshValue();
                var tempCheckData = [];
                if ($.util.isArray(checkedValue)) {
                    tempCheckData = checkedValue;
                }
                else if ($.util.isString(checkedValue)) { tempCheckData.push(checkedValue); }

                tempCheckData.forEach(function (item) {
                    var checkRow = dg2.datagrid("findRow", item);
                    if (checkRow) {
                        var rowIndex = dg2.datagrid("getRowIndex", checkRow);
                        dg2.datagrid("checkRow", rowIndex);
                    }
                });
            });

            container.layout({ fit: true });
        });
        return dia;
    };




    //  增加自定义扩展方法 $.easyui.showAccordionDblDataGridSelector；该方法弹出一个包含 一个 easyui-accordion 和两个 easyui-datagrid 控件的选择框窗口；该方法定义如下参数：
    //      options: 这是一个 JSON-Object 对象；
    //              具体格式参考 $.easyui.showDialog 方法的参数 options 的格式；
    //              该参数格式在 $.easyui.showDialog 参数 options 格式基础上扩展了如下属性：
    //          extToolbar:这是一个boolean值，表示是否向datagrid扩展工具条；
    //          selected:这是一个string格式（多个则以英文逗号相连）或array格式的值，表示已选项；
    //          onEnter :这是一个 function 对象，表示点击“确定”按钮时回调的函数；
    //          accordionOptions:这是一个 JSON-Object 对象，具体格式参考 easyui-accordion 方法的参数 options 的格式；
    //                      该参数格式在 easyui-accordion 参数 options 格式基础上扩展了如下属性：
    //                  panels:这是一个 array 对象，其中每个数组元素都是 JSON-Object 对象，每个元素都表示一个在 easyui-accordion 控件的独立panel中要显示的对象；
    //                      元素具体格式如下：
    //                              panelTitle:这是一个string格式的值，表示面板的标题，若不提供该参数，则默认以“面板[索引号]”为标题；
    //                              type:这是一个string格式的值，表示元素要显示的类型，值可以是 tree、content 其中之一，若不提供该参数则默认为 content；
    //                              typeOptions:当 type 不等于 content 时，这是一个 JSON-Object 对象，具体格式参考 type 参数对应的 easyui 控件的方法的参数 options 的格式；
    //                                          当type为content时，这是一个 string格式对象，将直接以文本方式显示在panel中；
    //          datagridOptions:这是一个 JSON-Object 对象，具体格式参考 easyui-dataGrid 方法的参数 options 的格式；
    //                          该参数格式在 easyui-dataGrid 参数 options 格式基础上扩展了如下属性：
    //                  selectedUrl:这是一个string格式的值，表示 当 options.selected 参数存在时，已选项datagrid要获取具体数据时要执行的url；
    //  返回值：返回弹出窗口的 easyui-dialog 控件对象(jQuery-DOM 格式)。并且在该对象中以属性accordion、datagrid1、datagrid2分别表示accordion控件对象、待选datagrid控件对象和已选datagrid控件对象。
    $.easyui.showAccordionDblDataGridSelector = function (options) {
        if (options && options.topMost && $ != $.util.$) { return $.util.$.easyui.showAccordionDblDataGridSelector.apply(this, arguments); }

        //accordion的options
        var accordionOptions = options.accordionOptions ? $.extend({}, options.accordionOptions) : {};
        //accordion中各panel要显示的对象
        var accordionPanels = accordionOptions.panels ? $.util.isArray(accordionOptions.panels) ? $.array.clone(accordionOptions.panels) : [] : [];
        delete accordionOptions.panels;
        //datagrid的options
        var datagridOptions = options.datagridOptions ? options.datagridOptions : {};
        //计算 dialog 和 accordion 的最大和最小宽度
        var defaultCenterWith = 55, defaultAccordionWidth = 200, accordionMinWith = 150, accordionMaxWith = 300,
            minDataGrid1Width = getDataGridMinWidth(datagridOptions.pagination),
            accordionRealWith = accordionOptions.width ? (accordionOptions.width < accordionMinWith ? accordionMinWith : (accordionOptions.width > accordionMaxWith ? accordionMaxWith : accordionOptions.width)) : defaultAccordionWidth,
            diaMinWidth = (minDataGrid1Width * 2) + defaultCenterWith + accordionRealWith,
            diaRealWidth = options.width ? (options.width < diaMinWidth ? diaMinWidth : options.width) : diaMinWidth + 50;
        //将options与selectDialog的默认options合并
        var opts = $.extend({
            height: 580, minHeight: 480,
            title: "选择数据，" + (datagridOptions.singleSelect ? "单选" : "多选"),
            iconCls: "icon-hamburg-zoom",
            maximizable: true,
            collapsible: true,
            selected: null,
            extToolbar: false,
            onEnter: function (value) { }
        }, options, { width: diaRealWidth, minWidth: diaMinWidth, centerWidth: defaultCenterWith });

        var value = getSelected(datagridOptions.singleSelect, opts.selected), tempData = getOriginalSelected(value);

        var dg = null,
            dia = $.easyui.showDialog($.extend({}, opts, {
                content: "<div class=\"grid-selector-container\"></div>",
                saveButtonText: "确定",
                saveButtonIconCls: "icon-ok",
                enableApplyButton: false,
                toolbar: "",
                onSave: function () {
                    if ($.isFunction(opts.onEnter)) { return opts.onEnter.call(dg[0], value); }
                }
            }));
        $.util.exec(function () {
            var diaOpts = dia.dialog("options"), onResize = diaOpts.onResize, init = false,
                extToolbar = checkToolbar(opts.extToolbar, datagridOptions.toolbar),
                showAccordion = accordionPanels.length > 0 && $.util.isObject(accordionPanels[0]),
                width = (($.isNumeric(opts.width) ? opts.width : dia.outerWidth()) - accordionRealWith - opts.centerWidth) / 2,
                container = dia.find(".grid-selector-container"),
                leftPanel = $("<div data-options=\"region: 'west', split: false, border: false\"></div>").width(accordionRealWith).appendTo(container),
                centerPanel = $("<div data-options=\"region: 'center', border: true, bodyCls: 'grid-selector-self-center'\"></div>").appendTo(container),
                inLayout = $("<div />").appendTo(centerPanel),
                inLeftPanel = $("<div data-options=\"region: 'west', split: false, border: false\"></div>").width(width).appendTo(inLayout),
                inCenterPanel = $("<div data-options=\"region: 'center', border: true, bodyCls: 'grid-selector-buttons'\"></div>").appendTo(inLayout),
                inRightPanel = $("<div data-options=\"region: 'east', split: false, border: false\"></div>").width(width).appendTo(inLayout),
                accordionOpts = showAccordion ? $.extend({ animate: true }, accordionOptions, {
                    fit: true, border: false,
                    onSelect: function (title, index) {
                        $.fn.accordion.defaults.onSelect.apply(this, arguments);
                        if ($.isFunction(accordionOptions.onSelect) && accordionOptions.onSelect != $.fn.accordion.defaults.onSelect) {
                            accordionOptions.onSelect.apply(this, arguments);
                        }
                    },
                    onUnselect: function (title, index) {
                        $.fn.accordion.defaults.onUnselect.apply(this, arguments);
                        if ($.isFunction(accordionOptions.onUnselect) && accordionOptions.onUnselect != $.fn.accordion.defaults.onUnselect) {
                            accordionOptions.onUnselect.apply(this, arguments);
                        }
                    }
                }) : {},
                dgOpts = $.extend({}, datagridOptions),
                toolbarObj = extToolbar ? getToolbar(dgOpts.toolbar) : null,
                toolbarDiv = toolbarObj ? toolbarObj.toolbar("toolbar") : null,
                dgOpts1 = $.extend({ striped: true, checkOnSelect: true, selectOnCheck: true, rownumbers: true }, dgOpts, {
                    title: "待选择项", toolbar: toolbarDiv, fit: true, border: false, doSize: true, enableRowContextMenu: false,
                    noheader: false, iconCls: null, collapsible: false, minimizable: false, maximizable: false, closable: false,
                    onSelect: function (rowIndex, rowData) {
                        $.fn.datagrid.defaults.onSelect.apply(this, arguments);
                        if ($.isFunction(dgOpts.onSelect) && dgOpts.onSelect != $.fn.datagrid.defaults.onSelect) {
                            dgOpts.onSelect.apply(this, arguments);
                        }
                        selectRow(dgOpts, rowData, dg2, refreshValue);
                    },
                    onUnselect: function (rowIndex, rowData) {
                        $.fn.datagrid.defaults.onUnselect.apply(this, arguments);
                        if ($.isFunction(dgOpts.onUnselect) && dgOpts.onUnselect != $.fn.datagrid.defaults.onUnselect) {
                            dgOpts.onUnselect.apply(this, arguments);
                        }
                        unselectRow(dgOpts, rowData, dg2, refreshValue);
                    },
                    onLoadSuccess: function (data) {
                        $.fn.datagrid.defaults.onLoadSuccess.apply(this, arguments);
                        if ($.isFunction(dgOpts.onLoadSuccess) && dgOpts.onLoadSuccess != $.fn.datagrid.defaults.onLoadSuccess) {
                            dgOpts.onLoadSuccess.apply(this, arguments);
                        }
                        if (!init) {
                            selectRowOnFirst(dg1, tempData);
                            init = true;
                        }
                        else {
                            selectRowOnNotFirst(dgOpts.idField, dg1, dg2);
                        }
                    }
                }),
                dgOpts2 = $.extend({}, dgOpts1, {
                    url: null, queryParams: {}, remoteSort: false, pagination: false, title: "已选择项", iconCls: null, toolbar: null, enableRowContextMenu: false,
                    onSelect: function (rowIndex, rowData) {
                        $.fn.datagrid.defaults.onSelect.apply(this, arguments);
                        removeRow(dgOpts.idField, rowData, dg1, dg2, refreshValue);
                    },
                    onUnselect: $.fn.datagrid.defaults.onUnselect,
                    onLoadSuccess: $.fn.datagrid.defaults.onLoadSuccess
                }),
                accordion1 = showAccordion ? $("<div />").appendTo(leftPanel) : null,
                dg1 = $("<div></div>").appendTo(inLeftPanel),
                dg2 = dg = $("<div class=\"grid-selector\"></div>").appendTo(inRightPanel),
                btn1 = dgOpts.singleSelect ? null : $("<a></a>").linkbutton({ plain: true, iconCls: "pagination-last" }).tooltip({ content: "选择全部" }).appendTo(inCenterPanel).click(function () {
                    selectAllRow(dgOpts.idField, dg1, refreshValue);
                }),
                btn4 = dgOpts.singleSelect ? null : $("<a></a>").linkbutton({ plain: true, iconCls: "pagination-first" }).tooltip({ content: "取消全部" }).appendTo(inCenterPanel).click(function () {
                    unselectAllRow(dg1, dg2, refreshValue);
                }),
                refreshValue = function () {
                    if (dgOpts.singleSelect) {
                        var row = dg2.datagrid("getRows");
                        value = row.length > 0 ? row[0] : null;
                    }
                    else {
                        var rows = dg2.datagrid("getRows");
                        value = $.array.clone(rows);
                    }
                };

            diaOpts.onResize = function (w, h) {
                if ($.isFunction(onResize)) { onResize.apply(this, arguments); }
                $.util.exec(function () {
                    if (checkResizable(diaOpts, w, h)) {
                        var ww = (dia.panel("options").width - accordionRealWith - diaOpts.centerWidth) / 2;
                        inLeftPanel.panel("resize", { width: ww });
                        inRightPanel.panel("resize", { width: ww });
                        inLayout.layout("resize");
                    }
                });
            };

            inLayout.layout({ fit: true }); container.layout({ fit: true });

            //以下是先创建各panel的div，然后追加到accordion的div中，再一次性渲染accordion的方式。这种方式需要最后初始化accordion，记得。
            //采用这种方式是因为accordion的add方法存在一个“可能是bug”的现象，accordion在执行add之后，panel的content值并不是马上被渲染完成的，即add之后无法获取content中设定的对象。
            //而因为这个类似bug的存在，导致“tree在加载数据之前的打开遮罩层的位置始终无法定位到其父级的panel”，因为tree还未被及时渲染到panel中，无法通过tree对象获取到父级panel。
            //为了向panel的options中添加额外的属性queryable和queryType，只好创建panel的div时，以动态调整data-options的方式追加给panel的div。
            //            queryable：这是boolean值，表示该panel是否需要组织查询条件，值由“accordionPanels数组中每个元素的type”决定，当type为“tree”时，queryable为true；
            //                  该属性，在“绑定datagrid1的toolbar中的查询按钮事件”时，用于定位“accordion中当前选中的、并且有查询条件可组织的panel”。
            //            queryType：这是string值，继承于“accordionPanels数组中每个元素的type”的值；
            //                  该属性，用于定位panel之后，明确“从哪里获得组装查询条件的对象”。
            if (showAccordion) {
                accordionPanels.forEach(function (item, i) {
                    var title = $.string.isNullOrWhiteSpace(item.panelTitle) ? "面板[" + i + "]" : item.panelTitle,
                        panelProp = { title: title };
                    var accordionPanel = $("<div />").attr(panelProp).appendTo(accordion1);
                    switch (item.type) {
                        case undefined:
                        case "":
                        case "content":
                            accordionPanel.attr({ "data-options": "queryable: false, queryType: '" + item.type + "'" }).html(String(item.typeOptions));
                            break;
                        case "tree":
                            accordionPanel.attr({ "data-options": "queryable: true, queryType: '" + item.type + "'" });
                            if (!$.isFunction(item.typeOptions.onSelectParamBuild)) { item.typeOptions.onSelectParamBuild = function (node) { return { nodeId: node.id }; }; }
                            var treeOpts = $.extend({ animate: true, lines: true }, item.typeOptions, {
                                checkbox: false, dnd: false, enableContextMenu: false,
                                onBeforeSelect: function (node) {
                                    $.fn.tree.defaults.onBeforeSelect.apply(this, arguments);
                                    if ($.isFunction(item.typeOptions.onBeforeSelect) && item.typeOptions.onBeforeSelect != $.fn.tree.defaults.onBeforeSelect) {
                                        item.typeOptions.onBeforeSelect.apply(this, arguments);
                                    }
                                },
                                onSelect: function (node) {
                                    $.fn.tree.defaults.onSelect.apply(this, arguments);
                                    if ($.isFunction(item.typeOptions.onSelect) && item.typeOptions.onSelect != $.fn.tree.defaults.onSelect) {
                                        item.typeOptions.onSelect.apply(this, arguments);
                                    }
                                    buildParamSearch(this, item.typeOptions.onSelectParamBuild, node, toolbarObj, dg1);
                                },
                                onBeforeLoad: function (node, param) {
                                    $.fn.tree.defaults.onBeforeLoad.apply(this, arguments);
                                    if ($.isFunction(item.typeOptions.onBeforeLoad) && item.typeOptions.onBeforeLoad != $.fn.tree.defaults.onBeforeLoad) {
                                        item.typeOptions.onBeforeLoad.apply(this, arguments);
                                    }
                                    $.easyui.loading({ locale: accordionPanel, msg: "数据加载中..." });
                                },
                                onLoadSuccess: function (node, data) {
                                    $.fn.tree.defaults.onLoadSuccess.apply(this, arguments);
                                    if ($.isFunction(item.typeOptions.onLoadSuccess) && item.typeOptions.onLoadSuccess != $.fn.tree.defaults.onLoadSuccess) {
                                        item.typeOptions.onLoadSuccess.apply(this, arguments);
                                    }
                                    $.easyui.loaded(accordionPanel);
                                }
                            });
                            var tree1 = $("<ul/>").appendTo(accordionPanel).tree(treeOpts);
                            break;
                    }
                });
            }

            dia.accordion = showAccordion ? accordion1.accordion(accordionOpts) : null;

            //以下方式暂时不注释，今后若修复问题，将采用以下方式，更“面向对象”一些。
            //以下方式存在问题：tree在加载数据之前的打开遮罩层的位置始终无法定位到其父级的panel；
            //              原因：tree还未被及时渲染到panel中，无法通过tree对象获取到父级panel。
            //              原因的原因：accordion在执行add之后，panel的content值并不是马上被渲染完成的，即add之后无法获取content中设定的对象。
            //以下是先初始化accordion，然后向accordion中追加panel的方式。并不需要创建panel的div，只需要将内容以content形式放在panel的options即可。这种方式需要先初始化accordion，记得。
            //采用这种方式是为了向panel的options中添加额外的属性queryable和queryType
            //                                                  queryable：这是boolean值，表示该panel是否需要组织查询条件，值由“accordionPanels数组中每个元素的type”决定，当type为“tree”时，queryable为true；
            //                                                  queryType：这是string值，继承于“accordionPanels数组中每个元素的type”的值；
            //queryable属性，在“绑定datagrid1的toolbar中的查询按钮事件”时，用于定位“accordion中当前选中的、并且有查询条件可组织的panel”。
            //queryType属性，用于定位panel之后，明确“从哪里获得组装查询条件的对象”。
            if (showAccordion && 1 > 2) {
                accordionPanels.forEach(function (item, i) {
                    var title = $.string.isNullOrWhiteSpace(item.panelTitle) ? "面板[" + i + "]" : item.panelTitle, panelOpts = { title: title, queryable: false, queryType: item.type };

                    switch (item.type) {
                        case undefined:
                        case "":
                        case "content":
                            panelOpts.content = String(item.typeOptions);
                            dia.accordion.accordion("add", panelOpts);
                            break;
                        case "tree":
                            panelOpts.queryable = true;
                            if (!$.isFunction(item.typeOptions.onSelectParamBuild)) { item.typeOptions.onSelectParamBuild = function (node) { return { nodeId: node.id }; }; }
                            var treeOpts = $.extend({ animate: true, lines: true }, item.typeOptions, {
                                checkbox: false, dnd: false, enableContextMenu: false,
                                onBeforeSelect: function (node) {
                                    $.fn.tree.defaults.onBeforeSelect.apply(this, arguments);
                                    if ($.isFunction(item.typeOptions.onBeforeSelect) && item.typeOptions.onBeforeSelect != $.fn.tree.defaults.onBeforeSelect) {
                                        item.typeOptions.onBeforeSelect.apply(this, arguments);
                                    }
                                },
                                onSelect: function (node) {
                                    $.fn.tree.defaults.onSelect.apply(this, arguments);
                                    if ($.isFunction(item.typeOptions.onSelect) && item.typeOptions.onSelect != $.fn.tree.defaults.onSelect) {
                                        item.typeOptions.onSelect.apply(this, arguments);
                                    }
                                    buildParamSearch(this, item.typeOptions.onSelectParamBuild, toolbarObj, dg1);
                                },
                                onBeforeLoad: function (node, param) {
                                    $.fn.tree.defaults.onBeforeLoad.apply(this, arguments);
                                    if ($.isFunction(item.typeOptions.onBeforeLoad) && item.typeOptions.onBeforeLoad != $.fn.tree.defaults.onBeforeLoad) {
                                        item.typeOptions.onBeforeLoad.apply(this, arguments);
                                    }
                                    //var t = $(this);
                                    //console.warn(t[0].outerHTML);
                                    //console.warn($(t[0]).parent().html());
                                    //$.easyui.loading({ locale: $(this).currentPanel(), msg: "数据加载中..." });
                                },
                                onLoadSuccess: function (node, data) {
                                    $.fn.tree.defaults.onLoadSuccess.apply(this, arguments);
                                    if ($.isFunction(item.typeOptions.onLoadSuccess) && item.typeOptions.onLoadSuccess != $.fn.tree.defaults.onLoadSuccess) {
                                        item.typeOptions.onLoadSuccess.apply(this, arguments);
                                    }
                                    //$.easyui.loaded($(this).currentPanel);
                                }
                            });
                            var treeId = $.util.guid("N");
                            panelOpts.content = "<ul id=\"" + treeId + "\"></ul>";
                            dia.accordion.accordion("add", panelOpts);
                            $("#" + treeId).tree(treeOpts);//找不到tree对象
                            break;
                    }
                });
            }

            dia.datagrid1 = dg1.datagrid(dgOpts1);
            dia.datagrid2 = dg2.datagrid(dgOpts2);

            if (dgOpts.pagination) { removePaginationMessage(dg1); }
            loadSelectedData(dgOpts.selectedUrl, tempData, dg2, refreshValue);
        });
        return dia;


    };








})(jQuery);
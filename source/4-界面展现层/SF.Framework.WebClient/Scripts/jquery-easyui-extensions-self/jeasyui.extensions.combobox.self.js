﻿/**
* jQuery EasyUI 1.3.6
* Copyright (c) 2009-2013 www.jeasyui.com. All rights reserved.
*
* Licensed under the GPL or commercial licenses
* To use it on other terms please contact us: jeasyui@gmail.com
* http://www.gnu.org/licenses/gpl.txt
* http://www.jeasyui.com/license_commercial.php
*
* jQuery EasyUI combobox 组件扩展
* jeasyui.extensions.combobox.self.js
* 开发 李溪林
* 最近更新：2015-08-14
*
* 依赖项：
*   1、jquery.jdirk.js v1.0 beta late
*   2、jeasyui.extensions.js v1.0 beta late
*   3、jeasyui.extensions.combo.js v1.0 beta late
*
* Copyright (c) 2015 Lixilin personal All rights reserved.
* http://www.chenjianwei.org
*/

/*
功能说明：
*/
(function ($, undefined) {

    var findItem = function (target, param) {
        var t = $(target), data = t.combobox("getData"), opts = t.combobox("options");
        return $.array.first(data, $.isFunction(param) ? param : function (val) { return val[opts.valueField] == param; });
    };

    var _loadData = $.fn.combobox.methods.loadData;
    var loadData = function (target, data, remainText) {
        var state = $.data(target, 'combobox');
        var opts = state.options;
        state.data = opts.loadFilter.call(target, data);
        state.groups = [];
        data = state.data;

        var selected = $(target).combobox('getValues');
        var dd = [];
        //添加一个记录group历史记录的数组
        var tempGroups = [];
        //添加存放组和组对应的项的对象
        var tempDataForGroups = {}, tempDataForGroupItems = {};
        for (var i = 0; i < data.length; i++) {
            var row = data[i];
            var v = row[opts.valueField] + '';
            var s = row[opts.textField];
            var g = row[opts.groupField];

            if (g) {
                if (!$.array.contains(tempGroups, g)) {
                    tempGroups.push(g);
                    state.groups.push(g);
                    //添加组信息
                    var temp1 = '<div id="' + (state.groupIdPrefix + '_' + (state.groups.length - 1)) + '" class="combobox-group">';
                    temp1 += opts.groupFormatter ? opts.groupFormatter.call(target, g) : g;
                    temp1 += '</div>';
                    tempDataForGroups[g] = temp1;
                }
            } else {
                //无分组
                g = "";
            }

            //添加组-项信息
            var cls = 'combobox-item' + (row.disabled ? ' combobox-item-disabled' : '') + (g ? ' combobox-gitem' : '');
            var temp2 = '<div id="' + (state.itemIdPrefix + '_' + i) + '" class="' + cls + '">';
            temp2 += opts.formatter ? opts.formatter.call(target, row) : s;
            temp2 += '</div>';

            if (tempDataForGroupItems[g] == undefined) {
                tempDataForGroupItems[g] = [];
            }
            tempDataForGroupItems[g].push(temp2);

            if (row['selected'] && $.inArray(v, selected) == -1) {
                selected.push(v);
            }
        }
        //重组tempDataForGroups和tempDataForGroupItems
        var hasGroup = false;
        for (var c in tempDataForGroups) {
            var item = tempDataForGroups[c]; //组信息
            if ($.util.isString(item)) {
                hasGroup = true;
                dd.push(item);
                var items = tempDataForGroupItems[c];
                if (items && $.util.isArray(items)) {
                    items.forEach(function (row) {
                        dd.push(row);
                    });
                }
            }
        }

        if (!hasGroup) {
            //只组装tempDataForGroupItems
            var items = tempDataForGroupItems[""];
            if (items && $.util.isArray(items)) {
                items.forEach(function (row) {
                    dd.push(row);
                });
            }
        }

        $(target).combo('panel').html(dd.join(''));

        if (opts.multiple) {
            setValues(target, selected, remainText);
        } else {
            setValues(target, selected.length ? [selected[selected.length - 1]] : [], remainText);
        }

        opts.onLoadSuccess.call(target, data);
    };

    function setValues(target, values, remainText) {
        var opts = $.data(target, 'combobox').options;
        var panel = $(target).combo('panel');

        if (!$.isArray(values)) { values = values.split(opts.separator) }
        panel.find('div.combobox-item-selected').removeClass('combobox-item-selected');
        var vv = [], ss = [];
        for (var i = 0; i < values.length; i++) {
            var v = values[i];
            var s = v;
            opts.finder.getEl(target, v).addClass('combobox-item-selected');
            var row = opts.finder.getRow(target, v);
            if (row) {
                s = row[opts.textField];
            }
            vv.push(v);
            ss.push(s);
        }

        if (!remainText) {
            $(target).combo('setText', ss.join(opts.separator));
        }
        $(target).combo('setValues', vv);
    }



    var methods = {

        //  扩展 easyui-combobox 的自定义方法；获取符合查找内容的一项；该方法定义如下参数：
        //      param:  表示查找的内容；该方法的参数 param 可以是以下两种类型：
        //          待查找的项数据的 valueField 字段值；
        //          function 类型，该回调函数签名为 function(item, index, items)，其中 item 表示项数据对象、index 表示行索引号、items 表示当前 easyui-combobox 调用 getData 返回的结果集；
        //          如果 param 参数为 function 类型，则 findItem 方法会对当前 easyui-combobox 的每一项数据调用该回调函数；
        //          当回调函数返回 true 时，则表示找到需要查找的结果，立即停止循环调用并返回该项数据；
        //          如果回调函数始终未返回 true，则该回调函数会一直遍历 items 直到最后并返回 null。
        //  返回值：返回一个 JSON-Object，该 JSON-Object 为当前 easyui-combobox 数据源中的一个子项，包含 valueField 和 textField 的值；如果未找到相应数据，则返回 null。
        findItem: function (jq, param) {
            return findItem(jq[0], param);
        },

        //  重写 easyui-combobox 的 loadData 方法；修复其“使用groupField对数据进行分组时，必须将同组数据按顺序排列，否则无法识别同组数据”的bug；该方法定义如下参数：
        //      data:  表示要加载的数据集；
        //  返回值：返回表示当前 easyui-combobox 控件的 jQuery 链式对象。
        //  注意：以“在options中提供url”的方式初始化 easyui-combobox 时，仍旧存在该bug；
        //  原因是，内部源码中请求远程url之后，在 success 中用到的回调函数是内部 loadData function，本次重写只能改变对外暴露的 methods 中的 loadData 方法；
        //  因此，请采用“先ajax远程url获取到数据，然后调用 loadData 方法”的方式来避免受该bug影响。
        loadData: function (jq, data) {
            return jq.each(function () {
                loadData(this, data);
            });
        }
    };
    if ($.fn.combobox.extensions != null && $.fn.combobox.extensions.methods != null) {
        $.extend($.fn.combobox.extensions.methods, methods);
    }
    $.extend($.fn.combobox.methods, methods);

})(jQuery);
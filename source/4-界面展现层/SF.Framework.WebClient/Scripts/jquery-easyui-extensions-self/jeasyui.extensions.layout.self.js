﻿/**
* jQuery EasyUI 1.3.6
* Copyright (c) 2009-2013 www.jeasyui.com. All rights reserved.
*
* Licensed under the GPL or commercial licenses
* To use it on other terms please contact us: jeasyui@gmail.com
* http://www.gnu.org/licenses/gpl.txt
* http://www.jeasyui.com/license_commercial.php
*
* jQuery EasyUI layout 组件扩展
* jeasyui.extensions.layout.self.js
* 开发 李溪林
* 最近更新：2015-05-08
*
* 依赖项：
*   1、jquery.jdirk.js v1.0 beta late
*   2、jeasyui.extensions.js v1.0 beta late
*
* Copyright (c) 2015 Lixilin personal All rights reserved.
* http://www.chenjianwei.org
*/

/*
功能说明：
*/
(function ($, undefined) {

    var _onCollapse = $.fn.layout.paneldefaults.onCollapse;
    var showTitleOnCollapse = function () {
        if ($.isFunction) { _onCollapse.call(this); }

        var layout = $(this).parents(".layout"), layoutOpts = layout.data("layout").options;
        if (!layoutOpts.showTitleOnCollapse) { return; }

        //获取当前region的配置属性
        var opts = $(this).panel("options");
        //获取key
        var expandKey = "expand" + opts.region.substring(0, 1).toUpperCase() + opts.region.substring(1);
        //从layout的缓存对象中取得对应的收缩对象
        var expandPanel = layout.data("layout").panels[expandKey];
        if (expandPanel) {
            //存在，说明是调用Collapse方法触发onCollapse

            var title = opts.collapseTitle == null ? opts.title : opts.collapseTitle;
            //针对横向和竖向的不同处理方式
            if (opts.region == "west" || opts.region == "east") {
                //竖向的文字打竖,其实就是切割文字加br
                var split = [];
                for (var i = 0; i < title.length; i++) {
                    split.push(title.substring(i, i + 1));
                }
                expandPanel.panel("body").addClass("panel-title").css("text-align", "center").html(split.join("<br />"));
            } else {
                expandPanel.panel("setTitle", title);
            }
        }
        else {
            //不存在，说明初始化的options中已设定了Collapse属性为false，暂不处理
            //alert(expandKey);
        }
    };

    var defaults = {
        //  增加 easyui-layout 控件的自定义属性，该属性表示当 easyui-layout 的 panel 折叠时是否显示其 collapseTitle ，默认为 false 。
        showTitleOnCollapse: false
    };

    var paneldefaults = {
        //  增加 easyui-layout 控件 的 panel 的自定义属性，该属性表示在 easyui-layout 的 panel 折叠时要显示的面板标题。
        //  该属性为 null 时表示使用 easyui-layout 的 panel 的 title 作为其值。
        collapseTitle: null,
        //  重写 easyui-layout 控件的 panel 配置中的原生事件 onCollapse，以支持相应扩展功能。
        onCollapse: showTitleOnCollapse
    };

    if ($.fn.layout.extensions != null && $.fn.layout.extensions.defaults != null) {
        $.extend($.fn.layout.extensions.defaults, defaults);
    }
    $.extend($.fn.layout.defaults, defaults);

    if ($.fn.layout.paneldefaults != null && $.fn.layout.extensions.paneldefaults != null) {
        $.extend($.fn.layout.extensions.paneldefaults, paneldefaults);
    }
    $.extend($.fn.layout.paneldefaults, paneldefaults);

})(jQuery);
﻿/**
* jQuery EasyUI 1.3.6
* Copyright (c) 2009-2013 www.jeasyui.com. All rights reserved.
*
* Licensed under the GPL or commercial licenses
* To use it on other terms please contact us: jeasyui@gmail.com
* http://www.gnu.org/licenses/gpl.txt
* http://www.jeasyui.com/license_commercial.php
*
* jQuery EasyUI panel 组件扩展
* jeasyui.extensions.panel.self.js
* 开发 李溪林
* 最近更新：2015-05-08
*
* 依赖项：
*   1、jquery.jdirk.js v1.0 beta late
*   2、jeasyui.extensions.js v1.0 beta late
*
* Copyright (c) 2015 Lixilin personal All rights reserved.
* http://www.chenjianwei.org
*/

/*
功能说明： 还未实现
*/
(function ($, undefined) {

    var _collapsePanel = $.fn.panel.methods.collapsePanel;
    var collapsePanel = function (target, animate) {
        if ($.isFunction(_collapsePanel)) { _collapsePanel.apply(this, arguments); }

        //var state = $.data(target, "panel"), panel = state.panel, isInLayout = $(target).panel("inLayout");
        //if (!isInLayout) { return; }

        //var layout = $(target).parents(".layout"), layoutOpts = layout.data("layout").options;
        //if (!layoutOpts.showTitleOnCollapse) { return; }

        //var opts = state.options;
        //if (opts.region && ["west", "north", "east", "south"].contains(opts.region)) {
        //    var title = opts.collapseTitle == null ? opts.title : opts.collapseTitle;
        //    if (opts.region == "west" || opts.region == "east") {
        //        //竖向的文字打竖,其实就是切割文字加br
        //    }
        //    else {
        //        //panel.panel("setTitle", title);
        //    }
        //}
    };

    var methods = {

        //  重写 easyui-panel 控件的 collapse 方法，支持位于 easyui-layout 中的 layout-panel 部件设置 折叠后显示title 操作；
        //  返回值：返回当前控件 easyui-panel 的 jQuery 链式对象。
        collapse: function (jq, animate) {
            return jq.each(function () {
                collapsePanel(this, animate);
            });
        },
    };
    //if ($.fn.panel.extensions != null && $.fn.panel.extensions.methods != null) {
    //    $.extend($.fn.panel.extensions.methods, methods);
    //}
    //$.extend($.fn.panel.methods, methods);

})(jQuery);
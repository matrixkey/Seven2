﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using SF.Framework.Web;
using SF.Utilities;

namespace SF.Framework.WebClient.Controllers
{
    [Description("工具控制器")]
    [AllowOnlySuperAdmin]
    public class ToolsController : Controller
    {
        #region 视图

        /// <summary>
        /// 省市区信息导入页面
        /// </summary>
        /// <returns></returns>
        [Description("省市区信息导入页面")]
        [CatchException(ActionType.View)]
        public ViewResult ProvinceCityAreaDataImportIndex()
        {
            return View();
        }

        #endregion

        #region 数据

        #endregion

        #region 操作

        /// <summary>
        /// 导入省市区信息
        /// </summary>
        /// <returns></returns>
        [Description("导入省市区信息")]
        [CatchException(ActionType.Operation)]
        public ActionResult ImportProvinceCityArea()
        {
            string message = string.Empty;

            if (Request.Files.Count > 0)
            {
                //先上传文件到服务器
                HttpPostedFileBase file = Request.Files[0];
                double size = double.Parse(file.ContentLength.ToString()) / 1024.00;

                int fileSize = file.ContentLength;
                string fileRealName = file.FileName;
                string saveMark = Guid.NewGuid().ToString("N");
                string fileName = saveMark;
                string fileType = string.Empty;
                if (fileRealName.LastIndexOf('.') > -1)
                {
                    fileType = fileRealName.Substring(fileRealName.LastIndexOf('.') + 1).ToLower();
                    fileRealName = fileRealName.Substring(0, fileRealName.IndexOf('.'));
                }
                string filePath = Server.MapPath(@"..\Temp\ImportExcel\");
                if (!System.IO.Directory.Exists(filePath))
                {
                    System.IO.Directory.CreateDirectory(filePath);
                }
                string wholeFileName;
                if (!string.IsNullOrEmpty(fileType))
                {
                    wholeFileName = filePath + fileName + "." + fileType;
                }
                else
                {
                    wholeFileName = filePath + fileName;
                }
                file.SaveAs(wholeFileName);

                message += "文件[" + fileRealName + "]已经成功上传至服务器，物理地址为[" + wholeFileName + "]";

                var models = SF.Import.ExcelImportHelper.GetExcelData<SF.Framework.DataModel.Import.ProvinceCityAreaExcelSampleModel>(wholeFileName);

                var result = BLL.BllFactory.Master.ToolProvinceCityArea.ImportProvinceCityArea(models);
                message += result.Message;

                return Content(message);
                //return View("CompanyUserImportResultIndex", result.Data as Dictionary<string, string>);
            }
            else
            {
                message += "无文件，请先选择要导入的省市区信息Excel文件。";
                return Content(message);
            }
        }

        #endregion
    }
}
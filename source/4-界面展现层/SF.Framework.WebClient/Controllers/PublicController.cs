﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using SF.Framework.BLL;
using SF.Framework.Web;
using SF.Framework.Web.Reflection;
using SF.Utilities;

namespace SF.Framework.WebClient.Controllers
{
    [NotMapped]
    [AllowLogined]
    public class PublicController : BaseController
    {
        #region 公用的获取 从程序集中反射的 控制器、动作数据

        /// <summary>
        /// 提供控制器列表数据
        /// </summary>
        /// <returns></returns>
        [Description("提供控制器列表数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetControllerJson()
        {
            var controllers = ControllerActionGetter.GetControllersByAssembly(SF.Framework.SystemEnvironment.ProjectStartItemAssemblyName);

            return Json(controllers.Select(s => new { id = (string.IsNullOrWhiteSpace(s.AreaName) ? "" : "[" + s.AreaName + "]") + s.ControllerName, text = (string.IsNullOrWhiteSpace(s.AreaName) ? "" : "[" + s.AreaName + "]") + s.ControllerName, attributes = new { s.ControllerName, AreaName = s.AreaName ?? "", s.Description, Limited = s.LimitedAttribute.ToString() } }));
        }

        /// <summary>
        /// 提供控制器列表数据，含数据库信息
        /// </summary>
        /// <returns></returns>
        [Description("提供控制器列表数据，含数据库信息")]
        [CatchException(ActionType.Data)]
        public JsonResult GetControllerWithDatabaseInfoJson()
        {
            var controllersFromReflection = ControllerActionGetter.GetControllersByAssembly(SF.Framework.SystemEnvironment.ProjectStartItemAssemblyName);
            var controllersFromDatabase = BllFactory.Master.SysController.GetAllControllerModels();

            return Json(controllersFromReflection.Select(s => new { id = (string.IsNullOrWhiteSpace(s.AreaName) ? "" : "[" + s.AreaName + "]") + s.ControllerName, text = (string.IsNullOrWhiteSpace(s.AreaName) ? "" : "[" + s.AreaName + "]") + s.ControllerName + (controllersFromDatabase.Any(a => a.Name == s.ControllerName && a.AreaName == s.AreaName) ? "[已经存在]" : ""), attributes = new { s.ControllerName, AreaName = s.AreaName ?? "", s.Description, Limited = s.LimitedAttribute.ToString() } }));
        }

        /// <summary>
        /// 根据控制器名称提供行为列表数据
        /// </summary>
        /// <param name="controllerName">控制器名称</param>
        /// <param name="controllerAreaName">控制器区域名称</param>
        /// <returns></returns>
        [Description("根据控制器名称提供行为列表数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetActionJson(string controllerName, string controllerAreaName)
        {
            var actions = new List<ActionModel>();
            if (!string.IsNullOrWhiteSpace(controllerName))
            {
                actions = ControllerActionGetter.GetActionsByAssembly(SF.Framework.SystemEnvironment.ProjectStartItemAssemblyName, controllerName, controllerAreaName);
            }
            return Json(actions.Select(s => new { id = s.ActionName, text = s.ActionName + "[" + s.Type.GetDescription() + "]", attributes = new { s.Description, Type = s.Type.ToString(), Limited = s.LimitedAttribute.ToString() } }));
        }

        #endregion

        #region 公用的获取 菜单 数据

        /// <summary>
        /// 获取根目录的菜单json数据
        /// </summary>
        /// <param name="filter">是否根据用户范围过滤，默认是</param>
        /// <returns></returns>
        [Description("获取根目录的菜单json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetRootMenusJson(bool filter = true)
        {
            return BllFactory.Master.SysMenu.GetRootMenuData(filter).SerializeToJsonResult();
        }

        /// <summary>
        /// 根据根目录菜单获取子菜单json数据
        /// </summary>
        /// <param name="parentId">根目录菜单ID</param>
        /// <param name="parentCode">根目录菜单Code</param>
        /// <param name="filter">是否根据用户范围过滤，默认是</param>
        /// <returns></returns>
        [Description("根据根目录菜单获取子菜单json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetChildrenMenusJson(string parentId, string parentCode, bool filter = true)
        {
            return BllFactory.Master.SysMenu.GetChildrenMenuTreeData(parentId, parentCode, filter).SerializeToJsonResult();
        }

        #endregion

        #region 公用的获取 组织机构 数据

        /// <summary>
        /// 获取集团、公司级别的组织机构列表json数据
        /// </summary>
        /// <returns></returns>
        [Description("获取集团、公司级别的组织机构列表json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetGroupCompanyTreeJson()
        {
            return BllFactory.Master.HrOrganization.GetGroupCompanyTreeData().SerializeToJsonResult();
        }

        /// <summary>
        /// 获取指定父级机构ID的下一级机构信息树形json数据
        /// </summary>
        /// <param name="id">父级机构ID</param>
        /// <returns></returns>
        [Description("获取指定父级机构ID的下一级机构信息json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetAsynOrganTreeJson(string id = EntityContract.ParentIDDefaultValueWhenIDIsString)
        {
            return BllFactory.Master.HrOrganization.GetSonOrganTreeData(id).SerializeToJsonResult();
        }

        /// <summary>
        /// 以同步方式获取组织机构信息json数据
        /// </summary>
        /// <param name="parentId">父级机构ID，只显示该机构下的子集机构</param>
        /// <returns></returns>
        [Description("以同步方式获取组织机构信息json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetSyncOrganTreeJson(string parentId)
        {
            return BllFactory.Master.HrOrganization.GetChildrenOrganTreeData(parentId).SerializeToJsonResult();
        }

        #endregion

        #region 枚举类型数据

        /// <summary>
        /// 获取行为类型列表数据，返回格式：枚举Value，枚举Description
        /// </summary>
        /// <returns></returns>
        [Description("获取行为类型列表数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetActionTypeJson()
        {
            var data = SF.Utilities.EnumHelper.GetEnumItemsOfVD<SF.Framework.ActionType>();

            return Json(data.Select(s => new { id = s.Key, text = s.Value }));
        }

        /// <summary>
        /// 获取行为限制标记列表数据，返回格式：枚举Value，枚举Description
        /// </summary>
        /// <returns></returns>
        [Description("获取行为限制标记列表数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetActionLimitedAttributeJson()
        {
            var data = SF.Utilities.EnumHelper.GetEnumItemsOfVD<SF.Framework.ActionLimitedAttribute>();

            return Json(data.Select(s => new { id = s.Key, text = s.Value }));
        }

        /// <summary>
        /// 获取菜单权限类型列表数据，返回格式：枚举Value，枚举Description
        /// </summary>
        /// <returns></returns>
        [Description("获取菜单权限类型列表数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetMenuPermissionTypeJson()
        {
            var data = SF.Utilities.EnumHelper.GetEnumItemsOfVD<SF.Framework.MenuPermissionType>();

            return Json(data.Select(s => new { id = s.Key, text = s.Value }));
        }

        /// <summary>
        /// 获取组织机构类型列表数据，返回格式：枚举Value，枚举Description
        /// </summary>
        /// <returns></returns>
        [Description("获取组织机构类型列表数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetOrganizationTypeJson()
        {
            var data = SF.Utilities.EnumHelper.GetEnumItemsOfVD<SF.Framework.OrganizationType>();

            return Json(data.Select(s => new { id = s.Key, text = s.Value }));
        }

        #endregion

        #region 判定


        #endregion

        #region 选择器

        #region 控制器、行为选择器

        /// <summary>
        /// 获取控制器树形数据
        /// </summary>
        /// <returns></returns>
        [Description("获取控制器树形数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetControllerSelectorTreeJson()
        {
            return BllFactory.Master.SysController.GetControllerTreeData().SerializeToJsonResult();
        }

        /// <summary>
        /// 获取指定控制器的行为数据
        /// </summary>
        /// <param name="controllerId">控制器ID</param>
        /// <param name="actionName">行为名称，用于模糊查询</param>
        /// <returns></returns>
        [Description("获取指定控制器的行为数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetActionSelectorJson(string controllerId, string actionName)
        {
            return BllFactory.Master.SysAction.GetActionListData(controllerId, actionName).SerializeToJsonResult(true);
        }

        /// <summary>
        /// 获取指定ID集合的行为列表数据
        /// </summary>
        /// <param name="selected">行为id集合</param>
        /// <returns></returns>
        [Description("获取指定ID集合的行为列表数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetActionSelectedJson(string selected)
        {
            return BllFactory.Master.SysAction.GetActionListData(selected.ToArray(",", true)).SerializeToJsonResult(true);
        }

        #endregion

        #region 菜单选择器

        /// <summary>
        /// 获取所有菜单的树形数据
        /// </summary>
        /// <param name="showRoot">是否显示“根目录”节点</param>
        /// <returns></returns>
        [Description("获取所有菜单的树形数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetMenuSelectorTreeJson(bool showRoot = false)
        {
            return BllFactory.Master.SysMenu.GetMenuTreeData(showRoot).SerializeToJsonResult();
        }

        #endregion

        #region 岗位选择器

        /// <summary>
        /// 获取指定部门下的岗位列表json数据
        /// </summary>
        /// <param name="positionName">岗位名称，模糊查询</param>
        /// <param name="organId">机构ID</param>
        /// <returns></returns>
        [Description("获取指定部门下的岗位列表json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetPositionSelectorJson(string positionName, string organId)
        {
            return BllFactory.Master.HrPosition.GetPositionSelectorModelsData(null, positionName, organId).SerializeToJsonResult();
        }

        /// <summary>
        /// 获取已选岗位列表json数据
        /// </summary>
        /// <param name="selected">已选岗位的ID串</param>
        /// <returns></returns>
        [Description("获取已选岗位列表json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetPositionSelectedJson(string selected)
        {
            return BllFactory.Master.HrPosition.GetPositionSelectorModelsData(selected.ToArray(",", true), null, null).SerializeToJsonResult();
        }

        #endregion

        #region 角色选择器

        /// <summary>
        /// 获取指定公司下的角色列表json数据
        /// </summary>
        /// <param name="positionName">角色名称，模糊查询</param>
        /// <param name="companyId">公司ID</param>
        /// <returns></returns>
        [Description("获取指定公司下的角色列表json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetRoleSelectorJson(string roleName)
        {
            return BllFactory.Master.SysRole.GetRoleSelectorModelsData(null, roleName).SerializeToJsonResult();
        }

        #endregion

        #region 员工选择器

        /// <summary>
        /// 获取员工列表json数据
        /// </summary>
        /// <param name="realName">员工名称，模糊查询</param>
        /// <param name="organId">子集团ID</param>
        /// <param name="queryModel">序列化的查询条件</param>
        /// <param name="page">页码</param>
        /// <param name="rows">每页条数</param>
        /// <returns></returns>
        [Description("获取指定部门下的员工列表json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetStaffSelectorJson(string realName, string organId, string queryModel, int? page, int? rows)
        {
            return BllFactory.Master.HrStaff.GetStaffListPagerData(realName, organId, queryModel, page ?? 1, rows ?? 10).SerializeToDatagridJsonResult();
        }

        /// <summary>
        /// 获取已选员工列表json数据
        /// </summary>
        /// <param name="selected">已选员工的ID串</param>
        /// <returns></returns>
        [Description("获取已选员工列表json数据")]
        [CatchException(ActionType.Data)]
        public JsonResult GetStaffSelectedJson(string selected)
        {
            return BllFactory.Master.HrStaff.GetStaffListPagerData(selected.ToArray(",", true)).SerializeToJsonResult();
        }

        #endregion

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;

using SF.Framework.Web;
using SF.Web.Utilities;

namespace SF.Framework.WebClient.Controllers
{
    [NotMapped]
    [AllowAnonymous]
    public class BlankController : Controller
    {
        public PartialViewResult ShowMessagePage(string message = "")
        {
            ViewBag.Message = message;

            return PartialView();
        }

        public PartialViewResult NotLoginPage(string message = "")
        {
            ViewBag.Message = message;

            return PartialView();
        }

        public ViewResult NotFoundIndex()
        {
            return View();
        }

        [CatchException(ActionType.View)]
        public ViewResult TestForSelectorIndex1()
        {
            return View();
        }

        [CatchException(ActionType.View)]
        public ViewResult TestIndex()
        {
            return View();
        }

        public JsonResult TestWebApi(string userName, string password)
        {
            var param = new { Account = userName, Password = password };
            LoginResultModel model = new SF.Web.Utilities.HttpClientHelper().Test1<LoginResultModel>(param);
            return Json(new { success = true, data = model });
        }

        /// <summary>
        /// 登录的结果数据模型
        /// </summary>
        public class LoginResultModel
        {
            /// <summary>
            /// 登录是否成功
            /// </summary>
            public bool Success { get; set; }

            /// <summary>
            /// 登录失败的提示信息
            /// </summary>
            public string FailMessage { get; set; }

            /// <summary>
            /// 登录成功后获得的用户数据模型
            /// </summary>
            public SF.Framework.BLL.Security.CurrentUserInfoModel Data { get; set; }
        }
    }
}
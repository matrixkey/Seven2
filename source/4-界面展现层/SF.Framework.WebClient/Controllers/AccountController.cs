﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using SF.Framework.BLL.Security;
using SF.Framework.Web;

namespace SF.Framework.WebClient.Controllers
{
    [Description("登录退出相关控制器")]
    [NotMapped]
    public class AccountController : BaseController
    {
        [Description("登录页面")]
        [CatchException(ActionType.View)]
        [AllowAnonymous]
        public ViewResult Login()
        {
            var model = new LoginModel();
            model.RememberLoginName = false;

            return View(model);
        }

        [Description("登录操作")]
        [CatchException(ActionType.Operation)]
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginModel model)
        {
            //model.Password = SF.Framework.Web.Security.JsSecurity.Decode(model.Password);
            model.Password = "111111";
            model.VerCode = "123";
            SF.Framework.BLL.OperationResult Result = new BLL.Security.Account().Login(model, "");
            if (Result.Success)
            {
                return RedirectToAction("Index", "Home", new { });
            }
            else
            {
                model.Message = Result.Message;
                return View(model);
            }
        }

        /// <summary>
        /// 注销操作
        /// </summary>
        /// <returns></returns>
        [Description("注销操作")]
        [CatchException(ActionType.Operation)]
        [AllowLogined]
        public JsonResult Logout()
        {
            SF.Framework.BLL.OperationResult result = new BLL.Security.Account().Logout(User.Identity.Name);

            return result.SerializeToJsonResult();
        }
    }
}
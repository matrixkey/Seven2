﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel;

using SF.Framework.Web;
using SF.Framework.DataModel.Views;

namespace SF.Framework.WebClient.Controllers
{
    [Description("后台首页控制器")]
    [NotMapped]
    [AllowLogined]
    public class HomeController : BaseController
    {
        /// <summary>
        /// 后台首页
        /// </summary>
        /// <returns></returns>
        [Description("后台首页")]
        [CatchException(ActionType.View)]
        public ViewResult Index()
        {
            MainPageModel model = new MainPageModel();
            model.Account = this.CurrentUserBaseInfo.UserName;
            model.RealName = this.CurrentUserBaseInfo.UserName;

            model.North = false;
            model.West = false;
            model.South = true;
            model.SystemName = "XXX系统";
            model.Copyright = SF.Framework.SystemEnvironment.Copyright;
            ViewBag.Title = model.SystemName;

            return View(model);
        }
    }
}
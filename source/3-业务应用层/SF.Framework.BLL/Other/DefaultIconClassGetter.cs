﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.BLL.Other
{
    public static class DefaultIconClassGetter
    {
        /// <summary>
        /// 根据组织结构类型获取其对应的预定义的图标样式名称
        /// </summary>
        /// <param name="type">组织机构类型</param>
        /// <returns></returns>
        public static string GetOrganIcon(OrganizationType type)
        {
            string result;
            switch (type)
            {
                case OrganizationType.TopGroup:
                    result = SystemIconClassDefine.DefaultGroupIconClassName;
                    break;
                case OrganizationType.ChildrenGroup:
                    result = SystemIconClassDefine.DefaultSonGroupIconClassName;
                    break;
                case OrganizationType.ParentCompany:
                    result = SystemIconClassDefine.DefaultCompanyIconClassName;
                    break;
                case OrganizationType.ConstituentCompany:
                    result = SystemIconClassDefine.DefaultCompanyIconClassName;
                    break;
                case OrganizationType.Department:
                    result = SystemIconClassDefine.DefaultDeptIconClassName;
                    break;
                default:
                    result = SystemIconClassDefine.DefaultDeptIconClassName;
                    break;
            }
            return result;
        }
    }
}

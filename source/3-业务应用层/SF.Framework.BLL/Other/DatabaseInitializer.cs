﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.BLL.Other
{
    /// <summary>
    /// 数据库初始化操作类
    /// </summary>
    public static class DatabaseInitializer
    {
        /// <summary>
        /// 数据库初始化
        /// </summary>
        public static void Initialize()
        {
            var initers = IUnitOfWorkFactory.DbInitializer;
            foreach (var item in initers)
            {
                item.Initialize(DatabaseConfig.IsMigrateDatabase);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.BLL.Other
{
    public class PermissionHelper
    {
        /// <summary>
        /// 计算checkbox状态。0表示，不选中；1表示，选中；2表示，选中不打勾。
        /// </summary>
        /// <param name="check">打勾状态</param>
        /// <param name="indeterminate">选中状态</param>
        /// <returns>一个数字，0表示不选中；1表示选中；2表示选中不打勾。</returns>
        internal static int GetCheckboxState(bool check, bool indeterminate)
        {
            if (!check && !indeterminate) { return 0; }
            else if (check && !indeterminate) { return 1; }
            else if (check && indeterminate) { return 2; }

            return 0;
        }

        /// <summary>
        /// 计算checkbox状态。0表示，不选中；1表示，选中。
        /// </summary>
        /// <param name="check">打勾状态</param>
        /// <returns>一个数字，0表示不选中；1表示选中。</returns>
        internal static int GetCheckboxState(bool check)
        {
            return check ? 1 : 0;
        }
    }
}

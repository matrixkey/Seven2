﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.DataInterface;
using SF.Framework.DataInterface.IInitialize;
using SF.Utilities;

namespace SF.Framework.BLL
{
    /// <summary>
    /// 工作单元工厂
    /// </summary>
    internal static class IUnitOfWorkFactory
    {
        /// <summary>
        /// 获取由 数据驱动方式 决定的 工作单元实例
        /// </summary>
        public static IUnitOfWork UnitOfWork
        {
            get
            {
                return GetIUnitOfWorkByDataDriven(DatabaseConfig.DataDriven, DatabaseConfig.ConnectionString, DatabaseConfig.ProviderName);
            }
        }

        /// <summary>
        /// 根据 指定的数据库连接名称或字符串 和 数据库提供程序名称 获取由 数据驱动方式 决定的 工作单元接口对象
        /// </summary>
        /// <param name="nameOrConnectionString">数据库连接中名称或字符串</param>
        /// <param name="providerName">数据库管道名称，仅在<see cref="nameOrConnString"/>为数据库连接字符串时有效</param>
        /// <returns></returns>
        public static IUnitOfWork SpecialUnitOfWork(string nameOrConnectionString, string providerName = "")
        {
            string tempConnectionString = string.Empty, tempProviderName = string.Empty;
            if (!ConfigHelper.TryGetConnectionInfo(nameOrConnectionString, out tempConnectionString, out tempProviderName))
            {
                tempConnectionString = nameOrConnectionString;
                tempProviderName = string.IsNullOrWhiteSpace(providerName) ? DatabaseConfig.DefaultProviderName : providerName;
            }
            else
            {
                //if (DatabaseConfig.NeedDecrypt)
                //{ tempConnectionString = SF.Security.Cryptography.DESCrypto.Decrypt(tempConnectionString); }
            }

            return GetIUnitOfWorkByDataDriven(DatabaseConfig.DataDriven, tempConnectionString, tempProviderName);
        }

        /// <summary>
        /// 获取由 数据驱动方式 决定的数据库初始化器接口对象
        /// </summary>
        public static IEnumerable<IDatabaseInitializer> DbInitializer
        {
            get
            {
                List<IDatabaseInitializer> result = new List<IDatabaseInitializer>();
                result.Add(GetIDatabaseInitializerByDataDriven(DatabaseConfig.DataDriven));

                return result;
            }
        }

        /// <summary>
        /// 根据 指定的数据库连接字符串、数据库提供程序名称 和 数据驱动方式 获取工作单元接口对象
        /// </summary>
        /// <param name="dataDriven">数据驱动方式</param>
        /// <param name="connectionString">数据库连接字符串</param>
        /// <param name="providerName">数据库提供程序名称</param>
        /// <returns>工作单元接口对象</returns>
        private static IUnitOfWork GetIUnitOfWorkByDataDriven(string dataDriven, string connectionString, string providerName)
        {
            if (dataDriven.Equals(SF.Framework.DatabaseConfig.DataDrivenType.EntityFramework.ToString(), StringComparison.CurrentCultureIgnoreCase))
            {
                return new SF.Framework.DataAchieve.EntityFramework.UnitOfWork(connectionString, providerName);
            }
            else if (dataDriven.Equals(SF.Framework.DatabaseConfig.DataDrivenType.ADO_NET.ToString(), StringComparison.CurrentCultureIgnoreCase))
            {
                return null;
            }

            throw ExceptionHelper.ThrowConfigException("系统参数配置文件中的 数据驱动方式 参数 Database:DataDriven，所设置的值 " + dataDriven + " 不符合系统规定（可设置的值为 " + SF.Framework.DatabaseConfig.DataDrivenType.EntityFramework.ToString() + " 或  " + SF.Framework.DatabaseConfig.DataDrivenType.ADO_NET.ToString() + "），请重新设置。");
        }

        /// <summary>
        /// 根据 给定的数据驱动方式 获取数据库初始化器接口对象
        /// </summary>
        /// <param name="dataDriven">数据驱动方式</param>
        /// <returns>数据库初始化器接口对象</returns>
        private static IDatabaseInitializer GetIDatabaseInitializerByDataDriven(string dataDriven)
        {
            if (dataDriven.Equals(SF.Framework.DatabaseConfig.DataDrivenType.EntityFramework.ToString(), StringComparison.CurrentCultureIgnoreCase))
            {
                return new SF.Framework.DataAchieve.EntityFramework.Initializes.Master.DatabaseInitializer();
            }
            else if (dataDriven.Equals(SF.Framework.DatabaseConfig.DataDrivenType.ADO_NET.ToString(), StringComparison.CurrentCultureIgnoreCase))
            {
                return null;
            }

            throw ExceptionHelper.ThrowConfigException("系统参数配置文件中的 数据驱动方式 参数 Database:DataDriven，所设置的值 " + dataDriven + " 不符合系统规定（可设置的值为 " + SF.Framework.DatabaseConfig.DataDrivenType.EntityFramework.ToString() + " 或  " + SF.Framework.DatabaseConfig.DataDrivenType.ADO_NET.ToString() + "），请重新设置。");
        }
    }
}

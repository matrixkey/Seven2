﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.DataInterface;

namespace SF.Framework.BLL
{
    /// <summary>
    /// 一组对 IUnitOfWork 的快速提交保存的扩展方法
    /// </summary>
    internal static class IUnitOfWorkExtensions
    {
        /// <summary>
        /// 提交 IUnitOfWork ，最终返回业务操作结果。若出现异常，则会尝试回滚（即使没开启事务，也能无错且不消耗额外资源地执行假回滚）。
        /// </summary>
        /// <param name="unit">工作单元对象</param>
        /// <returns>业务操作结果</returns>
        public static OperationResult TryCommit(this IUnitOfWork unit)
        {
            var result = new OperationResult();

            try
            {
                int count = unit.Commit();
                result.Data = count;
                result.Success = true; 
                result.Message = "操作成功！";
            }
            catch (Exception e)
            {
                unit.Rollback();
                result.Data = -1;
                result.Success = false; 
                result.Message = "操作失败！错误信息：" + e.Message;
            }

            return result;
        }

        /// <summary>
        /// 以 try...catch... 语句体方式执行一个具有int返回值的操作方法。
        /// </summary>
        /// <param name="unit">工作单元对象</param>
        /// <param name="tryAction">要执行的具有返回值的操作方法</param>
        /// <returns>业务操作结果</returns>
        public static OperationResult TryExecute(this IUnitOfWork unit, Func<int> tryAction)
        {
            var result = new OperationResult();

            int count = 0;
            try
            {
                count = tryAction();
            }
            catch (Exception e)
            {
                unit.Rollback();
                result.Data = -1;
                result.Success = false;
                result.Message = "操作失败！错误信息：" + e.Message;

                return result;
            }

            if (count > -1)
            {
                result.Data = count;
                result.Success = true;
                result.Message = "操作成功！";
            }
            else
            {
                result.Data = -1;
                result.Success = false;
                result.Message = "操作失败！";
            }

            return result;
        }
    }
}

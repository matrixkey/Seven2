﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.PermissionCache;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.BLL.Powerful.Master
{
    partial class SysUserPermissionBll
    {
        #region 获取数据结果

        /// <summary>
        /// 获取用户菜单权限数据
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="rangeFilter">是否根据管理范围过滤</param>
        /// <returns></returns>
        public CommonDataResult GetUserPermissionDataList(string userId, bool rangeFilter)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                if (rangeFilter)
                {
                    #region 从缓存的菜单模型中计算

                    IEnumerable<string> menuIds;
                    var menuRange = CurrentUserMenuRange;
                    if (menuRange.Count() > 0)
                    {
                        menuRange = menuRange.OrderBy(o => o.SortNumber);
                        menuIds = menuRange.Where(w => !string.IsNullOrWhiteSpace(w.ActionID)).Select(s => s.ID);
                    }
                    else { return new CommonDataResult(new List<object>()); }

                    //用户已有的、并且在“指定菜单范围内”的用户权限集合
                    var existUserPermissionModels = unit.Master.SysUserPermission.GetUserPermissionModels(userId, menuIds, false).ToList();
                    //在当前权限范围中找到“指定菜单范围”的权限
                    IEnumerable<MenuPermissionRangeCacheModel> otherUserPermissionData = menuIds.IsNullOrEmpty() ? Enumerable.Empty<MenuPermissionRangeCacheModel>() : CurrentUserMenuPermissionRange.Where(w => menuIds.Contains(w.MenuID));

                    var tempIds = existUserPermissionModels.Select(s => s.MenuPermissionID);
                    //组建无用户权限记录的默认用户权限数据（当前权限范围中，除去 用户已有权限范围 的部分，就是无用户权限记录的数据）
                    foreach (var item in otherUserPermissionData.Where(w => !tempIds.Contains(w.ID)))
                    {
                        existUserPermissionModels.Add(new UserPermissionListModel
                        {
                            UserID = userId,
                            MenuPermissionID = item.ID,
                            Check = true,
                            Indeterminate = true,
                            MenuPermissionName = item.Name,
                            MenuPermissionSortNumber = item.SortNumber,
                            MenuID = item.MenuID
                        });
                    }
                    existUserPermissionModels = existUserPermissionModels.TryOrderBy(o => o.MenuPermissionSortNumber).ToList();

                    return new CommonDataResult(menuRange.Select(s => new
                    {
                        s.ID,
                        s.Name,
                        iconCls = string.IsNullOrWhiteSpace(s.IconClass) ? SystemIconClassDefine.DefaultIconClassName : s.IconClass,
                        s.ParentID,
                        Permissions = existUserPermissionModels.Where(w => w.MenuID == s.ID).Select(ss => new { ss.MenuPermissionID, ss.MenuPermissionName, State = Other.PermissionHelper.GetCheckboxState(ss.Check, ss.Indeterminate) })
                    }));

                    #endregion
                }
                else
                {
                    #region 从数据库中计算

                    var menuRange = unit.Master.SysMenu.GetAllMenus();
                    if (menuRange.Count() == 0)
                    {
                        return new CommonDataResult(new List<object>());
                    }

                    IEnumerable<string> menuIds = menuRange.Where(w => !string.IsNullOrWhiteSpace(w.FK_ActionID)).Select(s => s.ID).Distinct();
                    //用户已有的所有用户权限集合
                    var existUserPermissionModels = unit.Master.SysUserPermission.GetUserPermissionModels(userId, menuIds, false).ToList();
                    //所有菜单的权限
                    var allMenuPermissions = unit.Master.SysMenuPermission.GetAllMenuPermissions();

                    var tempIds = existUserPermissionModels.Select(s => s.MenuPermissionID);
                    //组建无用户权限记录的默认用户权限数据（当前权限范围中，除去 用户已有权限范围 的部分，就是无用户权限记录的数据）
                    foreach (var item in allMenuPermissions.Where(w => !tempIds.Contains(w.ID)))
                    {
                        existUserPermissionModels.Add(new UserPermissionListModel
                        {
                            UserID = userId,
                            MenuPermissionID = item.ID,
                            Check = true,
                            Indeterminate = true,
                            MenuPermissionName = item.Name,
                            MenuPermissionSortNumber = item.SortNumber,
                            MenuID = item.FK_MenuID
                        });
                    }
                    existUserPermissionModels = existUserPermissionModels.TryOrderBy(o => o.MenuPermissionSortNumber).ToList();

                    return new CommonDataResult(menuRange.Select(s => new
                    {
                        s.ID,
                        s.Name,
                        iconCls = string.IsNullOrWhiteSpace(s.IconClass) ? SystemIconClassDefine.DefaultIconClassName : s.IconClass,
                        s.ParentID,
                        Permissions = existUserPermissionModels.Where(w => w.MenuID == s.ID).Select(ss => new { ss.MenuPermissionID, ss.MenuPermissionName, State = Other.PermissionHelper.GetCheckboxState(ss.Check, ss.Indeterminate) })
                    }));

                    #endregion
                }
            }
        }

        #endregion

        #region CUD操作

        /// <summary>
        /// 保存用户权限
        /// </summary>
        /// <param name="models">权限数据模型</param>
        /// <param name="userId">用户ID</param>
        /// <param name="otherUserIds">应用到其他用户ids</param>
        /// <returns></returns>
        public OperationResult SaveUserPermission(IEnumerable<UserPermissionFormModel> models, string userId, string otherUserIds)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                #region 用户权限组装

                var oldPermission = unit.Master.SysUserPermission.GetUserPermissions(userId);

                IList<SysUserPermission> inserts = new List<SysUserPermission>(), updates = new List<SysUserPermission>();
                foreach (var model in models)
                {
                    var temp = oldPermission.FirstOrDefault(f => f.FK_PermissionID == model.PermissionID);
                    if (temp == null)
                    {
                        inserts.Add(new SysUserPermission { FK_UserID = userId, FK_PermissionID = model.PermissionID, Check = model.Check, Indeterminate = model.Indeterminate });
                    }
                    else
                    {
                        temp.Check = model.Check;
                        temp.Indeterminate = model.Indeterminate;
                        updates.Add(temp);
                    }
                }

                #endregion

                unit.BeginTransaction();
                if (inserts.Count() > 0) { unit.Master.SysUserPermission.AddRange(inserts); }
                if (updates.Count() > 0) { unit.Master.SysUserPermission.UpdateRange(updates); }
                this.WriteLog(unit, SystemLogType.Update, "设置用户【ID：" + userId + "】的权限");

                return unit.TryCommit();
            }
        }

        #endregion
    }
}

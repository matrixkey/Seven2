﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.PermissionCache;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.BLL.Powerful.Master
{
    partial class SysMenuBll
    {
        #region 获取数据模型

        /// <summary>
        /// 根据父级菜单ID获取菜单新建数据模型
        /// </summary>
        /// <param name="parentId">父级菜单ID</param>
        /// <returns>菜单新建数据模型</returns>
        public MenuFormModel GetMenuCreateModel(string parentId)
        {
            var model = new MenuFormModel();
            model.ParentID = parentId;
            if (parentId.Equals(EntityContract.ParentIDDefaultValueWhenIDIsString, StringComparison.CurrentCultureIgnoreCase))
            {
                model.ParentName = EntityContract.ParentNameDefualtValueWhenIDIsString;
            }
            else
            {
                using (var unit = IUnitOfWorkFactory.UnitOfWork)
                {
                    model.ParentName = unit.Master.SysMenu.GetMenuName(parentId);
                }
            }
            model.IconClass = SystemIconClassDefine.DefaultIconClassName;
            model.IsShow = true;

            return model;
        }

        /// <summary>
        /// 根据菜单ID获取该菜单编辑数据模型
        /// </summary>
        /// <param name="id">菜单ID</param>
        /// <returns>菜单编辑数据模型</returns>
        public MenuFormModel GetMenuEditModel(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var model = unit.Master.SysMenu.GetMenuFormModel(id);
                model.ParentName = model.ParentID == EntityContract.ParentIDDefaultValueWhenIDIsString ? EntityContract.ParentNameDefualtValueWhenIDIsString : unit.Master.SysMenu.GetMenuName(model.ParentID);

                return model;
            }
        }

        #endregion

        #region 获取数据结果

        /// <summary>
        /// 获取所有菜单的树形数据
        /// </summary>
        /// <param name="showRoot">是否显示“根目录”节点</param>
        /// <returns>业务数据结果</returns>
        public CommonDataResult GetMenuTreeData(bool showRoot)
        {
            IEnumerable<SysMenu> menus;

            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                menus = unit.Master.SysMenu.GetAllMenus();
            }

            if (showRoot)
            {
                var result = new List<object>();
                result.Add(new { id = EntityContract.ParentIDDefaultValueWhenIDIsString, text = EntityContract.ParentNameDefualtValueWhenIDIsString, iconCls = "icon-root", attributes = new { Code = "00", SortNumber = -1 } });
                foreach (var item in menus)
                {
                    result.Add(new
                    {
                        id = item.ID,
                        text = item.Name,
                        item.ParentID,
                        iconCls = string.IsNullOrWhiteSpace(item.IconClass) ? SystemIconClassDefine.DefaultIconClassName : item.IconClass,
                        attributes = new { item.Code, item.SortNumber, IsParent = string.IsNullOrWhiteSpace(item.FK_ActionID) }
                    });
                }

                return new CommonDataResult(result);
            }
            else
            {
                return new CommonDataResult(menus.Select(s => new
                {
                    id = s.ID,
                    text = s.Name,
                    s.ParentID,
                    iconCls = string.IsNullOrWhiteSpace(s.IconClass) ? SystemIconClassDefine.DefaultIconClassName : s.IconClass,
                    attributes = new { s.Code, s.SortNumber, IsParent = string.IsNullOrWhiteSpace(s.FK_ActionID) }
                }));
            }
        }

        /// <summary>
        /// 获取指定父级菜单ID的下一级菜单信息数据
        /// </summary>
        /// <param name="id">父级菜单ID</param>
        /// <returns>下一级菜单信息数据</returns>
        public CommonDataResult GetSonMenuData(string id)
        {
            IEnumerable<MenuListModel> sonModels; IEnumerable<KeyValuePair<string, int>> count;
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                sonModels = unit.Master.SysMenu.GetSonMenuModels(id);
                var tempSonIDs = sonModels.Select(s => s.ID);
                count = unit.Master.SysMenu.GetSonsCount(tempSonIDs).Where(w => w.Value > 0);
            }

            return new CommonDataResult(sonModels.Select(s => new
            {
                s.ID,
                s.Name,
                s.Code,
                s.SortNumber,
                iconCls = string.IsNullOrWhiteSpace(s.IconClass) ? SystemIconClassDefine.DefaultIconClassName : s.IconClass,
                s.ParentID,
                ControllerName = (string.IsNullOrWhiteSpace(s.ControllerAreaName) ? "" : "[" + s.ControllerAreaName + "]") + s.ControllerName,
                s.ActionName,
                s.IsShow,
                state = count.Any(a => a.Key == s.ID) ? "closed" : "open"
            }));
        }

        /// <summary>
        /// 获取根目录菜单信息数据
        /// </summary>
        /// <param name="filter">是否根据用户范围过滤，默认是</param>
        /// <returns></returns>
        public CommonDataResult GetRootMenuData(bool filter = true)
        {
            if (filter)
            {
                var data = CurrentUserMenuRange
                        .Where(w => w.ParentID == EntityContract.ParentIDDefaultValueWhenIDIsString)
                        .TryOrderBy(m => m.SortNumber)
                        .Select(s => new { s.ID, s.Name, IconClass = string.IsNullOrWhiteSpace(s.IconClass) ? SystemIconClassDefine.DefaultIconClassName : s.IconClass, s.Code });

                return new CommonDataResult(data);
            }
            else
            {
                using (var unit = IUnitOfWorkFactory.UnitOfWork)
                {
                    var data = unit.Master.SysMenu.GetRootMenus().Select(s => new { s.ID, s.Name, IconClass = string.IsNullOrWhiteSpace(s.IconClass) ? SystemIconClassDefine.DefaultIconClassName : s.IconClass, s.Code });

                    return new CommonDataResult(data);
                }
            }
        }

        /// <summary>
        /// 根据根目录菜单信息获取子菜单树形数据
        /// </summary>
        /// <param name="parentId">根目录菜单ID</param>
        /// <param name="parentCode">根目录菜单Code</param>
        /// <param name="filter">是否根据用户范围过滤，默认是</param>
        /// <returns></returns>
        public CommonDataResult GetChildrenMenuTreeData(string parentId, string parentCode, bool filter = true)
        {
            IEnumerable<MenuListModel> data;
            if (filter)
            {
                var menuCache = this.CurrentUserMenuRange;
                var query = menuCache.Where(w => w.Code.StartsWith(parentCode) && w.ID != parentId).TryOrderBy(o => o.SortNumber).Select(s => new
                {
                    s.ID,
                    s.Name,
                    s.Code,
                    s.SortNumber,
                    s.IconClass,
                    s.ParentID,
                    s.ActionName,
                    s.ControllerName,
                    s.ControllerAreaName,
                    IsShow = true
                });

                data = query.Select(s => s.CastTo<MenuListModel>());
            }
            else
            {
                using (var unit = IUnitOfWorkFactory.UnitOfWork)
                {
                    data = unit.Master.SysMenu.GetChildrenMenuModels(parentCode);
                }
            }

            return new CommonDataResult(data.Select(s => new
            {
                id = s.ID,
                text = s.Name,
                iconCls = string.IsNullOrWhiteSpace(s.IconClass) ? SystemIconClassDefine.DefaultIconClassName : s.IconClass,
                s.ParentID,
                attributes = new
                {
                    title = s.Name,
                    href = MvcUrlHelper.GetRealUrl(s.ControllerAreaName, s.ControllerName, s.ActionName),
                    iconCls = string.IsNullOrWhiteSpace(s.IconClass) ? SystemIconClassDefine.DefaultIconClassName : s.IconClass
                }
            }));
        }

        #endregion

        #region 登录时判定权限 相关

        /// <summary>
        /// 根据控制器、行为范围获取对应的菜单范围模型集合
        /// </summary>
        /// <param name="controllerActionsRange">控制器、行为范围</param>
        /// <returns>菜单范围模型集合</returns>
        public IEnumerable<MenuRangeCacheModel> GetMenuRangeCacheModels(IEnumerable<ControllerActionRangeCacheModel> controllerActionsRange)
        {
            //存储所有菜单，以便循环递归时不需要从数据库中查询
            IEnumerable<SysMenu> allMenus;
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                allMenus = unit.Master.SysMenu.GetAllMenus();
            }

            //获得有访问权限的菜单
            List<string> tempMenuIds = new List<string>();
            List<SysMenu> tempResultMenus = new List<SysMenu>();
            foreach (var item in controllerActionsRange)
            {
                if (tempMenuIds.Contains(item.MenuID)) { continue; }
                var menu = allMenus.FirstOrDefault(f => f.ID == item.MenuID);
                if (menu != null && menu.FK_ActionID == item.ActionID && menu.IsShow)
                {
                    tempResultMenus.Add(menu);
                    tempMenuIds.Add(menu.ID);
                }
            }

            //最终，根据拥有浏览权限的菜单，往父级菜单推，直至顶级菜单
            var parentMenusDic = new Dictionary<string, SysMenu>();

            foreach (var item in tempResultMenus)
            {
                var parentMenu = allMenus.FirstOrDefault(m => m.ID == item.ParentID);
                if (parentMenu != null && !parentMenusDic.ContainsKey(parentMenu.ID))
                {
                    parentMenusDic.Add(parentMenu.ID, parentMenu);
                    //递归获取父级菜单
                    while (parentMenu.ParentID != EntityContract.ParentIDDefaultValueWhenIDIsString)
                    {
                        parentMenu = allMenus.FirstOrDefault(m => m.ID == parentMenu.ParentID);
                        if (parentMenu != null && !parentMenusDic.ContainsKey(parentMenu.ID))
                        {
                            parentMenusDic.Add(parentMenu.ID, parentMenu);
                        }
                    }
                }
            }

            tempResultMenus.AddRange(parentMenusDic.Select(s => s.Value));

            //最后根据计算所得的菜单实体集合，组装 菜单范围缓存模型 集合
            List<MenuRangeCacheModel> result = new List<MenuRangeCacheModel>();
            foreach (var item in tempResultMenus)
            {
                var actionInfo = controllerActionsRange.FirstOrDefault(f => f.MenuID == item.ID && f.ActionID == item.FK_ActionID);
                bool actionExist = actionInfo != null;
                result.Add(new MenuRangeCacheModel
                {
                    ID = item.ID,
                    Name = item.Name,
                    Code = item.Code,
                    SortNumber = item.SortNumber,
                    IconClass = item.IconClass,
                    ParentID = item.ParentID,
                    ActionID = item.FK_ActionID,
                    ActionName = actionExist ? actionInfo.ActionName : string.Empty,
                    ControllerName = actionExist ? actionInfo.ControllerName : string.Empty,
                    ControllerAreaName = actionExist ? actionInfo.ControllerAreaName : string.Empty
                });
            }

            return result;
        }

        /// <summary>
        /// 根据用户ID获取可用的菜单范围模型集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="all">是否获取全部，默认否。若是，将忽略权限。</param>
        /// <returns>菜单范围模型集合</returns>
        public IEnumerable<MenuRangeCacheModel> GetMenuRangeCacheModels(string userId, bool all = false)
        {
            IEnumerable<ControllerActionRangeCacheModel> menuControllerActionRange = BllFactory.Master.SysAction.GetControllerActionRangeCacheModels(userId, all);
            if (menuControllerActionRange.IsNullOrEmpty()) { return new List<MenuRangeCacheModel>(); }
            return this.GetMenuRangeCacheModels(menuControllerActionRange);
        }

        #endregion

        #region CUD操作

        /// <summary>
        /// 菜单保存，返回的结果中包含菜单对象，用于前台操作treegrid
        /// </summary>
        /// <param name="model">菜单数据模型</param>
        /// <returns>业务操作结果</returns>
        public OperationResult SaveMenu(MenuFormModel model)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                OperationResult result;

                if (string.IsNullOrWhiteSpace(model.ID))
                {
                    #region 新增

                    SysMenu entity = model.CastTo<SysMenu>();
                    entity.Code = unit.Master.SysMenu.GetNewMenuCode(entity.ParentID, entity.SortNumber);

                    unit.Master.SysMenu.Add(entity);
                    this.WriteLog(unit, SystemLogType.Add, "新增菜单", entity.Name, entity.ID);

                    result = unit.TryCommit();
                    if (result.Success)
                    {
                        result.Data = new
                        {
                            state = "insert",
                            newObj = new
                            {
                                entity.ID,
                                entity.Name,
                                iconCls = string.IsNullOrWhiteSpace(entity.IconClass) ? SystemIconClassDefine.DefaultIconClassName : entity.IconClass,
                                entity.Code,
                                entity.SortNumber,
                                entity.ParentID,
                                model.ControllerName,
                                model.ActionName,
                                entity.IsShow
                            }
                        };
                    }

                    #endregion
                }
                else
                {
                    #region 更新

                    bool isMove = false;
                    var menus = new List<SysMenu>();

                    #region 当前菜单信息

                    SysMenu entity = unit.Master.SysMenu.Find(model.ID);
                    isMove = entity.ParentID != model.ParentID;
                    string tempCode = entity.Code;
                    entity.Name = model.Name;
                    entity.SortNumber = model.SortNumber;
                    if (isMove)
                    {
                        //父级菜单已改变，重建Code
                        entity.ParentID = model.ParentID;
                        entity.Code = unit.Master.SysMenu.GetNewMenuCode(entity.ParentID, entity.SortNumber);
                    }
                    entity.IconClass = model.IconClass;
                    entity.FK_ActionID = model.FK_ActionID;
                    entity.IsShow = model.IsShow;
                    menus.Add(entity);

                    #endregion

                    #region 因当前菜单的父级菜单有所调整而导致的子菜单Code更新

                    if (isMove)
                    {
                        //父级菜单已改变，重建子集的Code
                        var childrenMenu = unit.Master.SysMenu.GetChildrenMenusByMenuCode(tempCode);
                        if (childrenMenu.Count() > 0)
                        {
                            foreach (var item in childrenMenu)
                            {
                                item.Code = entity.Code + item.Code.Substring(tempCode.Length);
                            }
                            menus.AddRange(childrenMenu);
                        }
                    }

                    #endregion

                    unit.Master.SysMenu.UpdateRange(menus);
                    this.WriteLog(unit, SystemLogType.Update, "更新菜单", entity.Name, entity.ID);

                    result = unit.TryCommit();
                    if (result.Success)
                    {
                        result.Data = new
                        {
                            state = isMove ? "move" : "update",
                            newObj = new
                            {
                                entity.ID,
                                entity.Name,
                                iconCls = string.IsNullOrWhiteSpace(entity.IconClass) ? SystemIconClassDefine.DefaultIconClassName : entity.IconClass,
                                entity.Code,
                                entity.SortNumber,
                                entity.ParentID,
                                model.ControllerName,
                                model.ActionName,
                                entity.IsShow
                            }
                        };
                    }

                    #endregion
                }

                return result;
            }
        }

        /// <summary>
        /// 删除菜单
        /// </summary>
        /// <param name="id">菜单ID</param>
        /// <returns>业务操作结果</returns>
        public OperationResult RemoveMenu(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                if (unit.Master.SysMenu.CheckSonMenuExist(id)) { return new OperationResult(false, "该菜单存在下一级菜单，无法删除。"); }

                var entity = unit.Master.SysMenu.Find(id);
                var result = unit.TryExecute(() => { return unit.Master.SysMenu.DeleteMenuWithRelation(id); });
                if (result.Success)
                {
                    this.WriteLog(unit, SystemLogType.Delete, "删除菜单", entity.Name, entity.ID, true);
                }

                return result;
            }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.PermissionCache;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.BLL.Powerful.Master
{
    partial class SysUserBll
    {
        #region 获取模型

        public UserFormModel GetUserCreateModel()
        {
            var model = new UserFormModel();

            return model;
        }

        /// <summary>
        /// 获取指定用户ID的用户编辑数据模型
        /// </summary>
        /// <param name="id">用户ID</param>
        /// <returns>用户编辑数据模型</returns>
        public UserFormModel GetUserEditModel(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                return unit.Master.SysUser.GetUserFormModel(id);
            }
        }

        #endregion

        #region 获取数据结果

        public PagerDataResult GetUserListPagerData(string queryModel, int pageIndex, int pageSize)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var data = unit.Master.SysUser.GetUserListPagerData(queryModel, pageIndex, pageSize);

                return new PagerDataResult(data);
            }
        }

        #endregion

        #region 登录校验

        /// <summary>
        /// 用户登录校验
        /// </summary>
        /// <param name="loginName">登录名</param>
        /// <param name="password">登录密码</param>
        /// <param name="ipAddress">IP地址</param>
        /// <returns>业务操作结果</returns>
        public OperationResult CheckLogin(string loginName, string password, string ipAddress)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                //根据登录帐号获取用户账号信息模型，附带User、Role实体信息
                UserAccountModel accountModel = unit.Master.SysUser.GetAccountModelByAccount(loginName);

                if (accountModel != null && accountModel.UserEntity != null)
                {
                    if (accountModel.UserEntity.Password.Equals(SF.Security.Cryptography.DESCrypto.Encrypt(password, EntityContract.AccountPasswordEncryptionDefaultKey)))
                    {
                        unit.Master.SysLog.WriteLog(SystemLogType.Login, "登录成功。", accountModel.UserEntity.UserName, accountModel.UserEntity.UserName, ipAddress, true);
                        return new OperationResult(true, "登录成功。", accountModel);
                    }
                    else
                    {
                        //unit.Sys_Log.WriteLog(SystemLogType.登录, "登录失败。登录密码不正确。", accountModel.User.UserName, accountModel.User.RealName, ipAddress, true);
                        return new OperationResult(false, "登录密码不正确。");
                    }
                    //if ((string.IsNullOrWhiteSpace(password) && string.IsNullOrWhiteSpace(accountModel.User.Password)) || accountModel.User.Password == EncryptHelper.Encrypt(password))
                    //{
                    //    if (accountModel.User.IsDeleted)
                    //    {
                    //        unit.Sys_Log.WriteLog(SystemLogType.登录, "登录失败。该帐号已被删除。", accountModel.User.UserName, accountModel.User.RealName, ipAddress, true);
                    //        return new OperationResult(false, "登录失败。该帐号已被删除。");
                    //    }

                    //    if (accountModel.User.UserState == UserState.禁用)
                    //    {
                    //        unit.Sys_Log.WriteLog(SystemLogType.登录, "登录失败。该帐号已被禁用。", accountModel.User.UserName, accountModel.User.RealName, ipAddress, true);
                    //        return new OperationResult(false, "登录失败。该帐号已被禁用。");
                    //    }
                    //    else
                    //    {
                    //        unit.Sys_Log.WriteLog(SystemLogType.登录, "登录成功。", accountModel.User.UserName, accountModel.User.RealName, ipAddress, true);
                    //        return new OperationResult(true, "登录成功。", accountModel);
                    //    }
                    //}
                }
                else
                {
                    //unit.Sys_Log.WriteLog(SystemLogType.登录, "登录失败。指定账号的用户不存在。", loginName, "未知账号", ipAddress, true);
                    return new OperationResult(false, "指定账号的用户不存在。");
                }
            }
        }

        #endregion

        #region CUD操作

        /// <summary>
        /// 保存用户
        /// </summary>
        /// <param name="model">用户数据模型</param>
        /// <returns>业务操作结果</returns>
        public OperationResult SaveUser(UserFormModel model)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                if (string.IsNullOrWhiteSpace(model.ID))
                {
                    var entity = model.CopyByDelegate<UserFormModel, SysUser>();
                    entity.ID = GuidHelper.GetNewFormatGuid();
                    entity.Password = SF.Security.Cryptography.DESCrypto.Encrypt(model.Password, EntityContract.AccountPasswordEncryptionDefaultKey);

                    unit.Master.SysUser.Add(entity);

                    //var relation = new RelationUserStaff();
                    //relation.FK_UserID = entity.ID;
                    //relation.FK_StaffID = model.FK_StaffID;

                    //unit.Master.RelationUserStaff.Add(relation);

                    this.WriteLog(unit, SystemLogType.Add, "新增用户", entity.UserName, entity.ID);
                }
                else
                {
                    var entity = unit.Master.SysUser.Find(model.ID);
                    entity.UserName = model.UserName;

                    unit.Master.SysUser.Update(entity);

                    //var relation = unit.Master.RelationUserStaff.FindByUserID(entity.ID);
                    //if (relation != null && relation.FK_StaffID != model.FK_StaffID)
                    //{
                    //    relation.FK_StaffID = model.FK_StaffID;
                    //    unit.Master.RelationUserStaff.Update(relation);
                    //}
                    this.WriteLog(unit, SystemLogType.Update, "更新用户", entity.UserName, entity.ID);
                }

                return unit.TryCommit();
            }
        }

        #endregion
    }
}

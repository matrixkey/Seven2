﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.BLL.Powerful.Master
{
    partial class BasicDataDictionaryBll
    {
        #region 获取模型数据

        /// <summary>
        /// 获取数据字典新建数据模型
        /// </summary>
        /// <param name="parentId">父级ID</param>
        /// <param name="parentName">父级名称</param>
        /// <param name="typeSymbol">父级类别标识</param>
        /// <returns>数据字典新建数据模型</returns>
        public DataDictionaryFormModel GetDataDictionaryCreateModel(string parentId, string parentName, string typeSymbol = "")
        {
            var model = new DataDictionaryFormModel();
            model.ParentID = parentId;
            model.ParentName = parentName;
            model.IsShow = true;
            model.TypeSymbol = typeSymbol;

            return model;
        }

        /// <summary>
        /// 获取指定主键ID的数据字典编辑数据模型
        /// </summary>
        /// <param name="id">数据字典ID</param>
        /// <returns>数据字典编辑数据模型</returns>
        public DataDictionaryFormModel GetDataDictionaryEditModel(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                return unit.Master.BasicDataDictionary.GetDataDictionaryFormModel(id);
            }
        }

        #endregion

        #region 获取业务数据

        /// <summary>
        /// 获取父级数据字典的树形数据
        /// </summary>
        /// <returns>业务数据结果</returns>
        public CommonDataResult GetParentDataDictionaryTreeData()
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var data = unit.Master.BasicDataDictionary.GetChildrenDataDictionary(EntityContract.ParentIDDefaultValueWhenIDIsString);

                return new CommonDataResult(data.Select(s => new
                {
                    id = s.ID,
                    text = s.Name + (s.IsSystem ? "[系统] " : " ") + (s.IsShow ? "" : "[不显示]"),
                    attributes = new { s.TypeSymbol }
                }));
            }
        }

        /// <summary>
        /// 获取指定父级的子集数据字典模型集合
        /// </summary>
        /// <param name="parentId">父级ID</param>
        /// <returns>业务数据结果</returns>
        public CommonDataResult GetChildrenDataDictionaryData(string parentId)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var data = unit.Master.BasicDataDictionary.GetChildrenDataDictionary(parentId);

                return new CommonDataResult(data);
            }
        }

        #endregion

        #region CUD操作

        /// <summary>
        /// 保存数据字典
        /// </summary>
        /// <param name="model">数据字典数据模型</param>
        /// <returns>业务操作结果</returns>
        public OperationResult SaveDataDictionary(DataDictionaryFormModel model)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                if (model.ParentID == EntityContract.ParentIDDefaultValueWhenIDIsString)
                {
                    if (unit.Master.BasicDataDictionary.CheckParentNameExist(model.Name, model.ID)) { return new OperationResult(false, "存在同名的父级数据字典，操作失败。"); }
                }
                else
                {
                    if (unit.Master.BasicDataDictionary.CheckChildNameExist(model.Name, model.ID)) { return new OperationResult(false, "同级目录中存在同名的子级数据字典，操作失败。"); }
                }

                if (string.IsNullOrWhiteSpace(model.ID))
                {
                    var entity = model.CopyByDelegate<DataDictionaryFormModel, BasicDataDictionary>();

                    unit.Master.BasicDataDictionary.Add(entity);
                    this.WriteLog(unit, SystemLogType.Add, "新增数据字典", entity.Name, entity.ID);
                }
                else
                {
                    var entity = unit.Master.BasicDataDictionary.Find(model.ID);
                    entity.Name = model.Name;
                    entity.BusinessCode = model.BusinessCode;
                    entity.IsShow = model.IsShow;

                    unit.Master.BasicDataDictionary.Update(entity);
                    this.WriteLog(unit, SystemLogType.Update, "更新数据字典", entity.Name, entity.ID);
                }

                return unit.TryCommit();
            }
        }

        /// <summary>
        /// 删除数据字典
        /// </summary>
        /// <param name="id">数据字典ID</param>
        /// <returns>业务操作结果</returns>
        public OperationResult RemoveDataDictionary(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                if (unit.Master.BasicDataDictionary.CheckChildrenExist(id)) { return new OperationResult(false, "存在子级数据字典，无法删除。"); }

                var entity = unit.Master.BasicDataDictionary.Find(id);

                this.WriteLog(unit, SystemLogType.Delete,"删除数据字典",entity.Name,entity.ID);
                unit.Master.BasicDataDictionary.Delete(entity);

                return unit.TryCommit();
            }
        }

        #endregion
    }
}

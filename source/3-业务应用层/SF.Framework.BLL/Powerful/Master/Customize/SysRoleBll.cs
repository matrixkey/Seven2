﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.BLL.Powerful.Master
{
    partial class SysRoleBll
    {
        #region 获取模型

        /// <summary>
        /// 获取角色新建数据模型
        /// </summary>
        /// <returns>角色新建数据模型</returns>
        public RoleFormModel GetRoleCreateModel()
        {
            var model = new RoleFormModel();

            return model;
        }

        /// <summary>
        /// 获取指定角色ID的角色编辑数据模型
        /// </summary>
        /// <param name="id">角色ID</param>
        /// <returns>角色编辑数据模型</returns>
        public RoleFormModel GetRoleEditModel(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                return unit.Master.SysRole.GetRoleFormModel(id);
            }
        }

        #endregion

        #region 获取数据结果

        public PagerDataResult GetRoleListPagerData(string queryModel, int pageIndex, int pageSize)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var data = unit.Master.SysRole.GetRoleListPagerData(queryModel, pageIndex, pageSize);

                return new PagerDataResult(data);
            }
        }

        /// <summary>
        /// 获取角色列表数据，供角色选择器使用
        /// </summary>
        /// <param name="roleIds">角色ID集合</param>
        /// <param name="roleName">角色名称，模糊查询</param>
        /// <returns>业务数据结果</returns>
        public CommonDataResult GetRoleSelectorModelsData(IEnumerable<string> roleIds, string roleName)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var data = unit.Master.SysRole.GetRoleSelectorModels(roleIds, roleName);

                return new CommonDataResult(data);
            }
        }

        #endregion

        #region CUD操作

        /// <summary>
        /// 保存角色
        /// </summary>
        /// <param name="model">角色数据模型</param>
        /// <returns>业务操作结果</returns>
        public OperationResult SaveRole(RoleFormModel model)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                if (string.IsNullOrWhiteSpace(model.ID))
                {
                    var entity = model.CopyByDelegate<RoleFormModel, SysRole>();
                    entity.ID = GuidHelper.GetNewFormatGuid();

                    unit.Master.SysRole.Add(entity);

                    this.WriteLog(unit, SystemLogType.Add, "新增角色", entity.Name, entity.ID);
                }
                else
                {
                    var entity = unit.Master.SysRole.Find(model.ID);
                    entity.Name = model.Name;
                    entity.SortNumber = model.SortNumber;

                    unit.Master.SysRole.Update(entity);
                    this.WriteLog(unit, SystemLogType.Update, "更新角色", entity.Name, entity.ID);
                }

                return unit.TryCommit();
            }
        }

        /// <summary>
        /// 删除角色
        /// </summary>
        /// <param name="id">角色ID</param>
        /// <returns>业务操作结果</returns>
        public OperationResult RemoveRole(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                if (unit.Master.RelationUserRole.CheckUserExistInRole(id)) { return new OperationResult(false, "该角色下存在用户，无法删除。"); }

                var entity = unit.Master.SysRole.Find(id);

                this.WriteLog(unit, SystemLogType.Delete, "删除角色", entity.Name, entity.ID);
                unit.Master.SysRole.Delete(entity);

                return unit.TryCommit();
            }
        }

        #endregion
    }
}

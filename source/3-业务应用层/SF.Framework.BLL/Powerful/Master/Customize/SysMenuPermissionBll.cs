﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.PermissionCache;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.BLL.Powerful.Master
{
    partial class SysMenuPermissionBll
    {
        #region 获取数据模型

        /// <summary>
        /// 根据菜单信息获取菜单权限新建数据模型
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <param name="menuName">菜单名称</param>
        /// <returns>菜单权限新建数据模型</returns>
        public MenuPermissionFormModel GetMenuPermissionCreateModel(string menuId, string menuName)
        {
            var model = new MenuPermissionFormModel();
            model.FK_MenuID = menuId;
            model.MenuName = menuName;
            model.ActionInfos = new List<string>();

            return model;
        }

        /// <summary>
        /// 获取指定ID的菜单权限数据编辑模型
        /// </summary>
        /// <param name="id">菜单权限ID</param>
        /// <returns>菜单权限数据编辑模型</returns>
        public MenuPermissionFormModel GetMenuPermissionEditModel(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                return unit.Master.SysMenuPermission.GetMenuPermissionEditModel(id);
            }
        }

        #endregion

        #region 获取数据结果

        /// <summary>
        /// 获取指定菜单的菜单权限列表数据
        /// </summary>
        /// <param name="menuId">菜单ID</param>
        /// <returns>菜单权限列表数据</returns>
        public CommonDataResult GetMenuPermissionData(string menuId)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var data = unit.Master.SysMenuPermission.GetMenuPermissionModels(menuId);

                return new CommonDataResult(data);
            }
        }

        #endregion

        #region 登录时判定权限 相关

        /// <summary>
        /// 根据用户ID获取该用户可用的的菜单权限数据集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="all">是否获取全部，默认否。若是，将忽略权限。</param>
        /// <returns></returns>
        public IEnumerable<SysMenuPermission> GetUseableMenuPermissions(string userId, bool all = false)
        {
            IEnumerable<SysMenuPermission> result = new List<SysMenuPermission>();
            IList<string> menuPermissionIDs = new List<string>();

            IEnumerable<SysRolePermission> rolePermissions;

            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                if (all) { return unit.Master.SysMenuPermission.GetAllMenuPermissions(); }
                else
                {
                    IList<SysUserPermission> userPermissions = unit.Master.SysUserPermission.GetUserPermissions(userId).ToList();

                    if (userPermissions.IsNullOrEmpty())
                    {
                        result = unit.Master.SysRolePermission.GetUseableMenuPermissions(userId);
                        rolePermissions = new List<SysRolePermission>();
                    }
                    else
                    {
                        //获取不在用户权限表中记录过的、并且“不是总是可用”的菜单权限，当作“选中不打勾”状态处理
                        IEnumerable<string> temp = userPermissions.Select(s => s.FK_PermissionID);
                        var other = unit.Master.SysMenuPermission.GetMenuPermissionsExcept(temp, true);
                        if (other.Count() > 0)
                        {
                            foreach (var item in other)
                            {
                                userPermissions.Add(new SysUserPermission { FK_UserID = userId, FK_PermissionID = item.ID, Check = true, Indeterminate = false });
                            }
                        }

                        rolePermissions = unit.Master.SysRolePermission.GetRolePermissionsByUserID(userId);
                    }

                    if (userPermissions.Count() > 0)
                    {
                        #region 若用户权限不为空，则遍历用户权限集合，结合角色权限进行合并

                        foreach (var item in userPermissions)
                        {
                            if (item.Check && !item.Indeterminate)
                            {
                                //用户权限 选中并且打勾
                                menuPermissionIDs.Add(item.FK_PermissionID);
                            }
                            else if (item.Check && item.Indeterminate)
                            {
                                //用户权限 选中不打勾时，从角色权限中继承
                                if (rolePermissions.Any(a => a.FK_PermissionID == item.FK_PermissionID && a.Check))
                                {
                                    menuPermissionIDs.Add(item.FK_PermissionID);
                                }
                            }
                        }
                        if (menuPermissionIDs.Count() > 0) { result = unit.Master.SysMenuPermission.GetMenuPermissions(menuPermissionIDs); }

                        #endregion
                    }

                    var alwaysUserablePermissions = unit.Master.SysMenuPermission.GetAlwaysUseablePermissions();
                    if (alwaysUserablePermissions.Count() > 0)
                    {
                        List<SysMenuPermission> temp = new List<SysMenuPermission>();
                        temp.AddRange(result);
                        foreach (var item in alwaysUserablePermissions)
                        {
                            if (!temp.Any(a => a.ID == item.ID))
                            {
                                temp.Add(item);
                            }
                        }

                        result = temp.AsEnumerable();
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 将 菜单权限实体集合 组装成 菜单权限范围缓存模型集合
        /// </summary>
        /// <param name="menuPermissions">菜单权限实体集合</param>
        /// <returns>菜单权限范围缓存模型集合</returns>
        public IEnumerable<MenuPermissionRangeCacheModel> GetMenuPermissionRangeCacheModels(IEnumerable<SysMenuPermission> menuPermissions)
        {
            List<MenuPermissionRangeCacheModel> result = new List<MenuPermissionRangeCacheModel>();

            foreach (var item in menuPermissions)
            {
                result.Add(new MenuPermissionRangeCacheModel
                {
                    ID = item.ID,
                    Name = item.Name,
                    SortNumber = item.SortNumber,
                    MenuID = item.FK_MenuID
                });
            }

            return result;
        }

        /// <summary>
        /// 获取按钮权限范围模型集合，含不可用的按钮权限，以属性Disabled区分两者。
        /// </summary>
        /// <param name="userableButtonMenuPermissions">可用的按钮权限集合</param>
        /// <returns>按钮权限范围模型集合</returns>
        public IEnumerable<ButtonPermissionRangeCacheModel> GetButtonPermissionRangeCacheModel(IEnumerable<SysMenuPermission> userableButtonMenuPermissions)
        {
            IEnumerable<SysMenuPermission> other;
            IList<ButtonPermissionRangeCacheModel> result = new List<ButtonPermissionRangeCacheModel>();
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                other = unit.Master.SysMenuPermission.GetButtonMenuPermissionsExcept(userableButtonMenuPermissions.Select(s => s.ID));
            }
            foreach (var item in userableButtonMenuPermissions)
            {
                result.Add(new ButtonPermissionRangeCacheModel
                {
                    MenuID = item.FK_MenuID,
                    ButtonName = item.ButtonName,
                    ButtonIconClass = string.IsNullOrWhiteSpace(item.ButtonIconClass) ? SystemIconClassDefine.DefaultIconClassName : item.ButtonIconClass,
                    ButtonSymbol = item.ButtonSymbol,
                    ButtonHandler = item.ButtonHandler,
                    IsDisabled = false,
                    SortNumber = item.SortNumber,
                    GroupMark = item.GroupMark
                });
            }
            foreach (var item in other)
            {
                result.Add(new ButtonPermissionRangeCacheModel
                {
                    MenuID = item.FK_MenuID,
                    ButtonName = item.ButtonName,
                    ButtonIconClass = string.IsNullOrWhiteSpace(item.ButtonIconClass) ? SystemIconClassDefine.DefaultIconClassName : item.ButtonIconClass,
                    ButtonSymbol = item.ButtonSymbol,
                    ButtonHandler = item.ButtonHandler,
                    IsDisabled = true,
                    SortNumber = item.SortNumber,
                    GroupMark = item.GroupMark
                });
            }

            return result;
        }

        /// <summary>
        /// 获取按钮权限范围模型集合，含不可用的按钮权限，以属性Disabled区分两者。
        /// </summary>
        /// <param name="userid">用户ID</param>
        /// <param name="all">是否获取全部，默认否。若是，将忽略权限。</param>
        /// <returns>按钮权限范围模型集合</returns>
        public IEnumerable<ButtonPermissionRangeCacheModel> GetButtonPermissionRangeCacheModel(string userid, bool all = false)
        {
            IEnumerable<SysMenuPermission> userable = this.GetUseableMenuPermissions(userid, all);
            return this.GetButtonPermissionRangeCacheModel(userable.Where(w => w.Type == MenuPermissionType.Button));
        }

        #endregion

        #region CUD操作

        /// <summary>
        /// 保存菜单权限
        /// </summary>
        /// <param name="model">菜单权限数据模型</param>
        /// <returns>业务操作结果</returns>
        public OperationResult SaveMenuPermission(MenuPermissionFormModel model)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                if (string.IsNullOrWhiteSpace(model.ID))
                {
                    var entity = model.CopyByDelegate<MenuPermissionFormModel, SysMenuPermission>();
                    unit.Master.SysMenuPermission.Add(entity);
                    this.WriteLog(unit, SystemLogType.Add, "新增菜单权限", entity.Name, entity.ID);
                }
                else
                {
                    var entity = unit.Master.SysMenuPermission.Find(model.ID);
                    entity.Name = model.Name;
                    entity.Type = model.Type;
                    entity.SortNumber = model.SortNumber;
                    entity.ActionIDs = model.ActionIDs;
                    entity.GroupMark = model.GroupMark;
                    entity.ButtonName = model.ButtonName;
                    entity.ButtonIconClass = model.ButtonIconClass;
                    entity.ButtonSymbol = model.ButtonSymbol;
                    entity.ButtonHandler = model.ButtonHandler;
                    unit.Master.SysMenuPermission.Update(entity);
                    this.WriteLog(unit, SystemLogType.Update, "更新菜单权限", entity.Name, entity.ID);
                }

                return unit.TryCommit();
            }
        }

        /// <summary>
        /// 删除指定ID的菜单权限
        /// </summary>
        /// <param name="id">菜单权限ID</param>
        /// <returns>业务操作结果</returns>
        public OperationResult RemoveMenuPermission(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var entity = unit.Master.SysMenuPermission.Find(id);
                var result = unit.TryExecute(() => { return unit.Master.SysMenuPermission.DeleteMenuPermissionWithRelation(id); });
                if (result.Success)
                {
                    this.WriteLog(unit, SystemLogType.Delete, "删除菜单权限", entity.Name, entity.ID, true);
                }

                return result;
            }
        }

        #endregion
    }
}

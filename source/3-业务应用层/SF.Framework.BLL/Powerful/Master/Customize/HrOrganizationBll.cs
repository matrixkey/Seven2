﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.BLL.Powerful.Master
{
    partial class HrOrganizationBll
    {
        #region 获取数据模型

        /// <summary>
        /// 获取组织机构新建数据模型
        /// </summary>
        /// <param name="parentId">父级机构ID</param>
        /// <param name="createType">创建类型</param>
        /// <returns>组织机构新建数据模型</returns>
        public OrganizationFormModel GetOrganCreateModel(string parentId, OrganizationType createType)
        {
            var model = new OrganizationFormModel();
            model.ParentID = parentId;
            if (parentId.Equals(EntityContract.ParentIDDefaultValueWhenIDIsString, StringComparison.CurrentCultureIgnoreCase))
            {
                model.ParentName = EntityContract.ParentNameDefualtValueWhenIDIsString;
            }
            else
            {
                using (var unit = IUnitOfWorkFactory.UnitOfWork)
                {
                    model.ParentName = unit.Master.HrOrganization.GetOrganName(parentId);
                }
            }
            model.Type = createType;
            model.SortNumber = 1;

            return model;
        }

        /// <summary>
        /// 获取指定ID的机构编辑数据模型
        /// </summary>
        /// <param name="id">机构ID</param>
        /// <returns>机构编辑数据模型</returns>
        public OrganizationFormModel GetOrganEditModel(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                return unit.Master.HrOrganization.GetOrganFormModel(id);
            }
        }

        #endregion

        #region 获取业务数据结果

        /// <summary>
        /// 获取指定父级菜单ID的下一级菜单信息树形数据
        /// </summary>
        /// <param name="id">父级菜单ID</param>
        /// <returns>下一级菜单信息树形数据</returns>
        public CommonDataResult GetSonOrganTreeData(string id)
        {
            IEnumerable<OrganListModel> sonModels; IEnumerable<KeyValuePair<string, int>> count;
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                sonModels = unit.Master.HrOrganization.GetSonOrganModels(id);
                var tempSonIDs = sonModels.Select(s => s.ID);
                count = unit.Master.HrOrganization.GetSonsCount(tempSonIDs).Where(w => w.Value > 0);
            }

            return new CommonDataResult(sonModels.Select(s => new
            {
                id = s.ID,
                text = s.Name,
                s.ParentID,
                iconCls = Other.DefaultIconClassGetter.GetOrganIcon(s.Type),
                state = count.Any(a => a.Key == s.ID) ? "closed" : "open",
                attributes = new { s.Code, s.SortNumber, s.ParentID, IsGroup = s.Type == OrganizationType.TopGroup || s.Type == OrganizationType.ChildrenGroup, IsCompany = s.Type == OrganizationType.ParentCompany || s.Type == OrganizationType.ConstituentCompany, IsDept = s.Type == OrganizationType.Department }
            }));
        }

        /// <summary>
        /// 获取集团、公司级别的组织机构列表树形数据
        /// </summary>
        /// <returns>集团、公司级别的组织机构列表树形数据</returns>
        public CommonDataResult GetGroupCompanyTreeData()
        {
            IEnumerable<OrganListModel> groupCompanyModels;
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                groupCompanyModels = unit.Master.HrOrganization.GetGroupCompanyModels();
            }

            return new CommonDataResult(groupCompanyModels.Select(s => new
            {
                id = s.ID,
                text = s.Name,
                s.ParentID,
                iconCls = Other.DefaultIconClassGetter.GetOrganIcon(s.Type),
                attributes = new { s.Code, s.SortNumber, s.ParentID, IsGroup = s.Type == OrganizationType.TopGroup || s.Type == OrganizationType.ChildrenGroup, IsCompany = s.Type == OrganizationType.ParentCompany || s.Type == OrganizationType.ConstituentCompany, IsDept = s.Type == OrganizationType.Department }
            }));
        }

        /// <summary>
        /// 获取子集组织机构列表树形数据
        /// </summary>
        /// <param name="parentId">父级机构ID，只查询该机构下的子集机构</param>
        /// <returns>可用组织机构列表树形数据</returns>
        public CommonDataResult GetChildrenOrganTreeData(string parentId)
        {
            IEnumerable<OrganListModel> organModels;
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                organModels = unit.Master.HrOrganization.GetChildrenOrganModels(parentId);
            }

            return new CommonDataResult(organModels.Select(s => new
            {
                id = s.ID,
                text = s.Name,
                s.ParentID,
                iconCls = Other.DefaultIconClassGetter.GetOrganIcon(s.Type),
                attributes = new { s.Code, s.SortNumber, s.ParentID, IsGroup = s.Type == OrganizationType.TopGroup || s.Type == OrganizationType.ChildrenGroup, IsCompany = s.Type == OrganizationType.ParentCompany || s.Type == OrganizationType.ConstituentCompany, IsDept = s.Type == OrganizationType.Department }
            }));
        }

        #endregion

        #region CUD操作

        /// <summary>
        /// 保存组织机构
        /// </summary>
        /// <param name="model">组织机构数据模型</param>
        /// <returns>业务操作结果</returns>
        public OperationResult SaveOrganization(OrganizationFormModel model)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                OperationResult result;

                if (string.IsNullOrWhiteSpace(model.ID))
                {
                    #region 新增

                    HrOrganization entity = model.CastTo<HrOrganization>();
                    entity.Code = unit.Master.HrOrganization.GetNewOrganCode(entity.ParentID, entity.SortNumber);

                    unit.Master.HrOrganization.Add(entity);
                    this.WriteLog(unit, SystemLogType.Add, "新增组织机构", entity.Name, entity.ID);

                    result = unit.TryCommit();
                    if (result.Success)
                    {
                        result.Data = new
                        {
                            state = "insert",
                            newObj = new
                            {
                                id = entity.ID,
                                text = entity.Name,
                                iconCls = Other.DefaultIconClassGetter.GetOrganIcon(entity.Type),
                                entity.ParentID,
                                attributes = new
                                {
                                    entity.Code,
                                    entity.SortNumber,
                                    entity.ParentID,
                                    IsGroup = entity.Type == OrganizationType.TopGroup || entity.Type == OrganizationType.ChildrenGroup,
                                    IsCompany = entity.Type == OrganizationType.ParentCompany || entity.Type == OrganizationType.ConstituentCompany,
                                    IsDept = entity.Type == OrganizationType.Department
                                }
                            }
                        };
                    }

                    #endregion
                }
                else
                {
                    #region 更新

                    bool isMove = false;
                    var organs = new List<HrOrganization>();

                    #region 当前组织机构信息

                    HrOrganization entity = unit.Master.HrOrganization.Find(model.ID);
                    isMove = entity.ParentID != model.ParentID;
                    string tempCode = entity.Code;
                    entity.Name = model.Name;
                    entity.ShortName = model.ShortName;
                    entity.PrefixName = model.PrefixName;
                    entity.SortNumber = model.SortNumber;
                    entity.FK_ResponsiblePersonsStaffID = model.FK_ResponsiblePersonsStaffID;
                    entity.PhoneNumber = model.PhoneNumber;
                    entity.Fax = model.Fax;
                    if (isMove)
                    {
                        //父级机构已改变，重建Code
                        entity.ParentID = model.ParentID;
                        entity.Code = unit.Master.HrOrganization.GetNewOrganCode(entity.ParentID, entity.SortNumber);
                    }
                    organs.Add(entity);

                    #endregion

                    #region 因当前菜单的父级菜单有所调整而导致的子菜单Code更新

                    if (isMove)
                    {
                        //父级机构已改变，重建子集的Code
                        var childrenOrgans = unit.Master.HrOrganization.GetChildrenOrgansByOrganCode(tempCode);
                        if (childrenOrgans.Count() > 0)
                        {
                            foreach (var item in childrenOrgans)
                            {
                                item.Code = entity.Code + item.Code.Substring(tempCode.Length);
                            }
                            organs.AddRange(childrenOrgans);
                        }
                    }

                    #endregion

                    unit.Master.HrOrganization.UpdateRange(organs);
                    this.WriteLog(unit, SystemLogType.Update, "更新组织机构", entity.Name, entity.ID);
                    result = unit.TryCommit();
                    if (result.Success)
                    {
                        result.Data = new
                        {
                            state = isMove ? "move" : "update",
                            newObj = new
                            {
                                id = entity.ID,
                                text = entity.Name,
                                iconCls = Other.DefaultIconClassGetter.GetOrganIcon(entity.Type),
                                entity.ParentID,
                                attributes = new
                                {
                                    entity.Code,
                                    entity.SortNumber,
                                    entity.ParentID,
                                    IsGroup = entity.Type == OrganizationType.TopGroup || entity.Type == OrganizationType.ChildrenGroup,
                                    IsCompany = entity.Type == OrganizationType.ParentCompany || entity.Type == OrganizationType.ConstituentCompany,
                                    IsDept = entity.Type == OrganizationType.Department
                                }
                            }
                        };
                    }

                    #endregion
                }

                return result;
            }
        }

        /// <summary>
        /// 删除组织机构（假删除）
        /// </summary>
        /// <param name="id">组织机构ID</param>
        /// <returns>业务操作结果</returns>
        public OperationResult RemoveOrganization(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                if (unit.Master.HrOrganization.CheckChildrenExist(id)) { return new OperationResult(false, "目标机构存在子机构，无法删除！"); }
                if (unit.Master.HrPosition.CheckPositionExistInOrgan(id)) { return new OperationResult(false, "目标机构存在岗位，无法删除"); }

                var entity = unit.Master.HrOrganization.Find(id);

                entity.IsDeleted = true;

                unit.Master.HrOrganization.Update(entity);
                this.WriteLog(unit, SystemLogType.Delete, "删除组织机构", entity.Name, entity.ID);

                return unit.TryCommit();
            }
        }

        #endregion
    }
}

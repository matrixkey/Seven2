﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.PermissionCache;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.BLL.Powerful.Master
{
    partial class SysRolePermissionBll
    {
        #region 获取数据结果

        /// <summary>
        /// 获取角色菜单权限数据
        /// </summary>
        /// <param name="roleId">角色ID</param>
        /// <param name="rangeFilter">是否根据管理范围过滤</param>
        /// <returns></returns>
        public CommonDataResult GetRolePermissionDataList(string roleId, bool rangeFilter)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                if (rangeFilter)
                {
                    #region 从缓存中的菜单模型中计算

                    IEnumerable<string> menuIds;
                    var menuRange = CurrentUserMenuRange;
                    if (menuRange.Count() > 0)
                    {
                        menuRange = menuRange.OrderBy(o => o.SortNumber);
                        menuIds = menuRange.Where(w => !string.IsNullOrWhiteSpace(w.ActionID)).Select(s => s.ID);
                    }
                    else { return new CommonDataResult(new List<object>()); }

                    //角色已有的、并且在“指定菜单范围内”的角色权限集合
                    var existRolePermissionModels = unit.Master.SysRolePermission.GetRolePermissionModels(roleId, menuIds, true).ToList();
                    //在当前权限范围中找到“指定菜单范围”的权限
                    IEnumerable<MenuPermissionRangeCacheModel> otherRolePermissionData = menuIds.IsNullOrEmpty() ? Enumerable.Empty<MenuPermissionRangeCacheModel>() : CurrentUserMenuPermissionRange.Where(w => menuIds.Contains(w.MenuID));

                    var tempIds = existRolePermissionModels.Select(s => s.MenuPermissionID);
                    //组建无角色权限记录的默认角色权限数据
                    foreach (var item in otherRolePermissionData.Where(w => !tempIds.Contains(w.ID)))
                    {
                        existRolePermissionModels.Add(new RolePermissionListModel
                        {
                            RoleID = roleId,
                            MenuPermissionID = item.ID,
                            MenuPermissionName = item.Name,
                            MenuPermissionSortNumber = item.SortNumber,
                            MenuID = item.MenuID,
                            Check = false
                        });
                    }

                    existRolePermissionModels = existRolePermissionModels.TryOrderBy(o => o.MenuPermissionSortNumber).ToList();

                    var data = menuRange.Select(s => new
                    {
                        s.ID,
                        s.Name,
                        iconCls = string.IsNullOrWhiteSpace(s.IconClass) ? SystemIconClassDefine.DefaultIconClassName : s.IconClass,
                        s.ParentID,
                        Permissions = existRolePermissionModels.Where(w => w.MenuID == s.ID).Select(ss => new { ss.MenuPermissionID, ss.MenuPermissionName, State = Other.PermissionHelper.GetCheckboxState(ss.Check) })
                    });

                    return new CommonDataResult(data);

                    #endregion
                }
                else
                {
                    #region 从数据库中计算

                    var menuRange = unit.Master.SysMenu.GetAllMenus();
                    if (menuRange.Count() == 0)
                    {
                        return new CommonDataResult(new List<object>());
                    }

                    IEnumerable<string> menuIds = menuRange.Where(w => !string.IsNullOrWhiteSpace(w.FK_ActionID)).Select(s => s.ID).Distinct();
                    //角色已有的所有角色权限集合
                    var existRolePermissionModels = unit.Master.SysRolePermission.GetRolePermissionModels(roleId, menuIds, true).ToList();
                    //所有菜单的权限
                    var allMenuPermissions = unit.Master.SysMenuPermission.GetAllMenuPermissions();

                    var tempIds = existRolePermissionModels.Select(s => s.MenuPermissionID);
                    //组建无角色权限记录的默认角色权限数据
                    foreach (var item in allMenuPermissions.Where(w => !tempIds.Contains(w.ID)))
                    {
                        existRolePermissionModels.Add(new RolePermissionListModel
                        {
                            RoleID = roleId,
                            MenuPermissionID = item.ID,
                            MenuPermissionName = item.Name,
                            MenuPermissionSortNumber = item.SortNumber,
                            MenuID = item.FK_MenuID,
                            Check = false
                        });
                    }

                    existRolePermissionModels = existRolePermissionModels.TryOrderBy(o => o.MenuPermissionSortNumber).ToList();

                    var data = menuRange.Select(s => new
                    {
                        s.ID,
                        s.Name,
                        iconCls = string.IsNullOrWhiteSpace(s.IconClass) ? SystemIconClassDefine.DefaultIconClassName : s.IconClass,
                        s.ParentID,
                        Permissions = existRolePermissionModels.Where(w => w.MenuID == s.ID).Select(ss => new { ss.MenuPermissionID, ss.MenuPermissionName, State = Other.PermissionHelper.GetCheckboxState(ss.Check) })
                    });

                    return new CommonDataResult(data);

                    #endregion
                }
            }
        }

        #endregion

        #region CUD操作

        /// <summary>
        /// 保存角色权限
        /// </summary>
        /// <param name="models">权限数据模型</param>
        /// <param name="roleId">角色ID</param>
        /// <param name="otherRoleIds">应用到其他角色ids</param>
        /// <returns></returns>
        public OperationResult SaveRolePermission(IEnumerable<RolePermissionFormModel> models, string roleId, string otherRoleIds)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                #region 角色权限组装

                var oldPermission = unit.Master.SysRolePermission.GetRolePermissions(roleId);

                IList<SysRolePermission> inserts = new List<SysRolePermission>(), updates = new List<SysRolePermission>();
                foreach (var model in models)
                {
                    var temp = oldPermission.FirstOrDefault(f => f.FK_PermissionID == model.PermissionID);
                    if (temp == null)
                    {
                        inserts.Add(new SysRolePermission { FK_RoleID = roleId, FK_PermissionID = model.PermissionID, Check = model.Check });
                    }
                    else
                    {
                        temp.Check = model.Check;
                        updates.Add(temp);
                    }
                }

                #endregion

                unit.BeginTransaction();
                if (inserts.Count() > 0) { unit.Master.SysRolePermission.AddRange(inserts); }
                if (updates.Count() > 0) { unit.Master.SysRolePermission.UpdateRange(updates); }
                this.WriteLog(unit, SystemLogType.Update, "设置角色【ID："+ roleId +"】的权限");

                return unit.TryCommit();
            }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.BLL.Powerful.Master
{
    partial class SysLogBll
    {
        /// <summary>
        /// 写入系统日志（立即提交）
        /// </summary>
        /// <param name="type">日志类型</param>
        /// <param name="content">日志信息</param>
        public void WriteLog(SystemLogType type, string content)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                base.WriteLog(unit, type, content, true);
            }
        }
    }
}

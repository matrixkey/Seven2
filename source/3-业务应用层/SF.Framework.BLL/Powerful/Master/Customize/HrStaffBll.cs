﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.BLL.Powerful.Master
{
    partial class HrStaffBll
    {
        /// <summary>
        /// 员工关系之间的间隔符号
        /// </summary>
        private static readonly string relationJoinStringOut = ";";
        /// <summary>
        /// 员工关系内部的间隔符号，用于间隔 关系对象ID和关系类型
        /// </summary>
        private static readonly string relationJoinStringIn = ",";

        #region 获取模型数据

        /// <summary>
        /// 获取员工的新建数据模型
        /// </summary>
        /// <returns>员工的新建数据模型</returns>
        public StaffFormModel GetStaffCreateModel()
        {
            var model = new StaffFormModel();
            model.PositionRelations = new List<StaffPositionOrganModel>();

            return model;
        }

        /// <summary>
        /// 获取指定id的员工的编辑数据模型
        /// </summary>
        /// <param name="id">员工ID</param>
        /// <returns>员工的编辑数据模型</returns>
        public StaffFormModel GetStaffEditModel(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var model = unit.Master.HrStaff.GetStaffFormModel(id);
                model.StringPositionRelations = string.Join(relationJoinStringOut, model.PositionRelations.Select(s => s.FK_PositionID + relationJoinStringIn + s.IsMaster));

                return model;
            }
        }

        #endregion

        #region 获取业务数据结果

        /// <summary>
        /// 获取员工列表分页数据
        /// </summary>
        /// <param name="realName">员工名称，模糊查询</param>
        /// <param name="organId">机构ID，表示查询范围</param>
        /// <param name="queryModel">序列化的查询条件</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">每页条数</param>
        /// <returns>员工列表分页数据</returns>
        public PagerDataResult GetStaffListPagerData(string realName, string organId, string queryModel, int pageIndex, int pageSize)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var data = unit.Master.HrStaff.GetStaffListPagerData(null, realName, organId, queryModel, pageIndex, pageSize);

                return new PagerDataResult(data);
            }
        }

        /// <summary>
        /// 获取员工列表分页数据
        /// </summary>
        /// <param name="staffIds">员工ID集合</param>
        /// <returns>员工列表分页数据</returns>
        public PagerDataResult GetStaffListPagerData(IEnumerable<string> staffIds)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var data = unit.Master.HrStaff.GetStaffListPagerData(staffIds, null, null, null, 1, 10);

                return new PagerDataResult(data);
            }
        }

        #endregion

        #region CUD操作

        /// <summary>
        /// 保存员工
        /// </summary>
        /// <param name="model">员工数据模型</param>
        /// <returns>业务操作结果</returns>
        public OperationResult SaveStaff(StaffFormModel model)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                if (string.IsNullOrWhiteSpace(model.ID))
                {
                    HrStaff entity = model.CopyByDelegate<StaffFormModel, HrStaff>();

                    unit.Master.HrStaff.Add(entity);

                    //处理岗位关系
                    var relationPosition = this.ParseStaffPositionRelation(model.StringPositionRelations, entity.ID);
                    unit.Master.RelationStaffPosition.AddRange(relationPosition);

                    this.WriteLog(unit, SystemLogType.Add, "新增员工", entity.RealName, entity.ID);
                }
                else
                {
                    HrStaff entity = unit.Master.HrStaff.Find(model.ID);
                    entity.RealName = model.RealName;

                    unit.Master.HrStaff.Update(entity);

                    #region 处理岗位关系

                    var relationPosition = this.ParseStaffPositionRelation(model.StringPositionRelations, entity.ID);
                    var relationPositionEntities = unit.Master.RelationStaffPosition.GetRelationsByStaffID(entity.ID);
                    List<RelationStaffPosition> insertsRelationPosition = new List<RelationStaffPosition>(), updatesRelationPosition = new List<RelationStaffPosition>(), deletesRelationPosition = new List<RelationStaffPosition>();
                    foreach (var item in relationPosition)
                    {
                        var relation = relationPositionEntities.FirstOrDefault(f => f.FK_PositionID == item.FK_PositionID);
                        if (relation == null)
                        { insertsRelationPosition.Add(item); }
                        else
                        {
                            if (relation.IsMaster != item.IsMaster) { relation.IsMaster = item.IsMaster; updatesRelationPosition.Add(relation); }
                        }
                    }
                    foreach (var item in relationPositionEntities)
                    {
                        if (!relationPosition.Any(a => a.FK_PositionID == item.FK_PositionID))
                        { deletesRelationPosition.Add(item); }
                    }
                    unit.Master.RelationStaffPosition.AddRange(insertsRelationPosition);
                    unit.Master.RelationStaffPosition.UpdateRange(updatesRelationPosition);
                    unit.Master.RelationStaffPosition.DeleteRange(deletesRelationPosition);

                    #endregion

                    this.WriteLog(unit, SystemLogType.Update, "更新员工", entity.RealName, entity.ID);
                }

                return unit.TryCommit();
            }
        }

        /// <summary>
        /// 删除员工
        /// </summary>
        /// <param name="id">员工ID</param>
        /// <returns>操作结果</returns>
        public OperationResult RemoveStaff(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var entity = unit.Master.HrStaff.Find(id);
                var relationPosition = unit.Master.RelationStaffPosition.GetRelationsByStaffID(id);

                this.WriteLog(unit, SystemLogType.Delete, "删除员工", entity.RealName, entity.ID);
                unit.Master.HrStaff.Delete(entity);
                unit.Master.RelationStaffPosition.DeleteRange(relationPosition);

                return unit.TryCommit();
            }
        }

        /// <summary>
        /// 解析字符串格式的岗位关系，其格式为 "岗位ID,是否主从;岗位ID,是否主从;岗位ID,是否主从"
        /// </summary>
        /// <param name="relation">字符串格式的岗位关系</param>
        /// <param name="staffId">员工ID</param>
        /// <returns>返回员工岗位关系实体模型集合</returns>
        private IEnumerable<RelationStaffPosition> ParseStaffPositionRelation(string relation, string staffId)
        {
            List<RelationStaffPosition> result = new List<RelationStaffPosition>();

            var tempSingleRelation = relation.ToArray(relationJoinStringOut, true);
            foreach (var item in tempSingleRelation)
            {
                var tempArraySingleRelation = item.ToArray(relationJoinStringIn, true);
                result.Add(new RelationStaffPosition { FK_StaffID = staffId, FK_PositionID = tempArraySingleRelation[0], IsMaster = tempArraySingleRelation[1].ToBooleanWithFriendly() });
            }

            return result;
        }

        #endregion
    }
}

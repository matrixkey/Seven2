﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;

namespace SF.Framework.BLL.Powerful.Master
{
    partial class ToolProvinceCityAreaBll
    {
        #region CUD操作

        /// <summary>
        /// 导入省市区数据到数据
        /// </summary>
        /// <param name="sampleModels">省市区建议模型</param>
        /// <returns>操作结果</returns>
        public OperationResult ImportProvinceCityArea(IEnumerable<SF.Framework.DataModel.Import.ProvinceCityAreaExcelSampleModel> sampleModels)
        {
            var models = this.ParseProvinceCityAreaExcelSampleModel(sampleModels);

            List<ToolProvinceCityArea> inserts = new List<ToolProvinceCityArea>(), updates = new List<ToolProvinceCityArea>(), deletes = new List<ToolProvinceCityArea>();
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var allData = unit.Master.ToolProvinceCityArea.GetAll();
                foreach (var item in models)
                {
                    var existData = allData.FirstOrDefault(f => f.Code == item.Code);
                    if (existData != null)
                    {
                        if (existData.Name != item.Name || existData.ParentCode != item.ParentCode)
                        {
                            updates.Add(existData);
                        }
                    }
                    else
                    {
                        inserts.Add(new ToolProvinceCityArea { Code = item.Code, Name = item.Name, ParentCode = item.ParentCode });
                    }
                }

                foreach (var item in allData)
                {
                    if (!models.Any(a => a.Code == item.Code))
                    {
                        deletes.Add(item);
                    }
                }

                unit.Master.ToolProvinceCityArea.AddRange(inserts);
                unit.Master.ToolProvinceCityArea.UpdateRange(updates);
                unit.Master.ToolProvinceCityArea.DeleteRange(deletes);

                var result = unit.TryCommit();
                if (result.Success)
                {
                    result.Message += "新增" + inserts.Count + "条，更新" + updates.Count + "条，删除" + deletes.Count + "条。";
                }

                return result;
            }
        }

        #endregion

        /// <summary>
        /// 将省市区简易模型集合解析为省市区有效模型集合
        /// </summary>
        /// <param name="sampleModels">省市区简易模型集合</param>
        /// <returns>省市区有效模型集合</returns>
        private IEnumerable<SF.Framework.DataModel.Import.ProvinceCityAreaModel> ParseProvinceCityAreaExcelSampleModel(IEnumerable<SF.Framework.DataModel.Import.ProvinceCityAreaExcelSampleModel> sampleModels)
        {
            List<SF.Framework.DataModel.Import.ProvinceCityAreaModel> dataModels = new List<SF.Framework.DataModel.Import.ProvinceCityAreaModel>();
            foreach (var item in sampleModels)
            {
                if (string.IsNullOrWhiteSpace(item.CodeAndName)) { continue; }
                string codeAndName = item.CodeAndName.Trim();
                string code = codeAndName.Substring(0, 6);
                string name = codeAndName.Replace(code, "").Trim();
                SF.Framework.DataModel.Import.ProvinceCityAreaModel itemModel = new SF.Framework.DataModel.Import.ProvinceCityAreaModel { Code = code, Name = name };
                if (!code.EndsWith("0000"))
                {
                    if (code.EndsWith("00"))
                    {
                        itemModel.ParentCode = code.Substring(0, 2) + "0000";
                    }
                    else
                    {
                        itemModel.ParentCode = code.Substring(0, 4) + "00";
                    }
                }
                dataModels.Add(itemModel);
            }

            return dataModels;
        }
    }
}

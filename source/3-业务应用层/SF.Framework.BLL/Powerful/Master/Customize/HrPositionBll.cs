﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.BLL.Powerful.Master
{
    partial class HrPositionBll
    {
        #region 获取数据模型

        /// <summary>
        /// 获取岗位新建数据模型
        /// </summary>
        /// <param name="organId">机构ID</param>
        /// <param name="organName">机构名称</param>
        /// <returns>岗位新建数据模型</returns>
        public PositionFormModel GetPositionCreateModel(string organId, string organName)
        {
            var model = new PositionFormModel();
            model.FK_OrganID = organId;
            model.OrganName = organName;

            return model;
        }

        /// <summary>
        /// 获取指定ID的岗位编辑数据模型
        /// </summary>
        /// <param name="id">岗位ID</param>
        /// <returns>岗位编辑数据模型</returns>
        public PositionFormModel GetPositionEditModel(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                return unit.Master.HrPosition.GetPositionFormModel(id);
            }
        }

        #endregion

        #region 获取业务数据结果

        /// <summary>
        /// 根据部门ID获取该部门下的岗位数据模型集合
        /// </summary>
        /// <param name="organId">机构ID</param>
        /// <returns>业务数据结果</returns>
        public CommonDataResult GetPositionModelsData(string organId)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var data = unit.Master.HrPosition.GetPositionModels(organId);

                return new CommonDataResult(data);
            }
        }

        /// <summary>
        /// 获取岗位列表数据，供岗位选择器使用，带岗位的完整所属信息
        /// </summary>
        /// <param name="positionIds">岗位ID集合</param>
        /// <param name="positionName">岗位名称，模糊查询</param>
        /// <param name="organId">机构ID</param>
        /// <returns>业务数据结果</returns>
        public CommonDataResult GetPositionSelectorModelsData(IEnumerable<string> positionIds, string positionName, string organId)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var data = unit.Master.HrPosition.GetPositionSelectorModels(positionIds, positionName, organId);

                return new CommonDataResult(data);
            }
        }

        #endregion

        #region CUD操作

        /// <summary>
        /// 保存岗位
        /// </summary>
        /// <param name="model">岗位数据模型</param>
        /// <returns>业务操作结果</returns>
        public OperationResult SavePosition(PositionFormModel model)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                if (string.IsNullOrWhiteSpace(model.ID))
                {
                    HrPosition entity = model.CopyByDelegate<PositionFormModel, HrPosition>();

                    unit.Master.HrPosition.Add(entity);
                    this.WriteLog(unit, SystemLogType.Add, "新增岗位", entity.Name, entity.ID);
                }
                else
                {
                    HrPosition entity = unit.Master.HrPosition.Find(model.ID);
                    entity.Name = model.Name;
                    entity.SortNumber = model.SortNumber;

                    unit.Master.HrPosition.Update(entity);
                    this.WriteLog(unit, SystemLogType.Update, "更新岗位", entity.Name, entity.ID);
                }

                return unit.TryCommit();
            }
        }

        /// <summary>
        /// 删除岗位
        /// </summary>
        /// <param name="id">岗位ID</param>
        /// <returns>操作结果</returns>
        public OperationResult RemovePosition(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                //if (unit.Master.HrStaff.CheckStaffExistInPosition(id)) { return new OperationResult(false, "该岗位下存在员工，无法删除。"); }

                var entity = unit.Master.HrPosition.Find(id);

                this.WriteLog(unit, SystemLogType.Delete, "删除岗位", entity.Name, entity.ID);
                unit.Master.HrPosition.Delete(entity);

                return unit.TryCommit();
            }
        }

        #endregion
    }
}

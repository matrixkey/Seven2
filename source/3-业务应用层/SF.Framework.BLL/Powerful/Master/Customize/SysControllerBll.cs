﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Views;
using SF.Framework.DataModel.Repositories;
using SF.Utilities;

namespace SF.Framework.BLL.Powerful.Master
{
    partial class SysControllerBll
    {
        #region 获取模型

        /// <summary>
        /// 获取控制器的创建数据模型
        /// </summary>
        /// <returns>控制器的创建数据模型</returns>
        public ControllerFormModel GetControllerCreateModel()
        {
            var model = new ControllerFormModel();
            model.LimitedAttribute = ActionLimitedAttribute.Follow;

            return model;
        }

        /// <summary>
        /// 获取指定主键的控制器的编辑数据模型
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns>控制器的编辑数据模型</returns>
        public ControllerFormModel GetControllerEditModel(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                return unit.Master.SysController.GetControllerFormModel(id);
            }
        }

        /// <summary>
        /// 获取所有控制器数据模型集合
        /// </summary>
        /// <returns>控制器的数据模型集合</returns>
        public IEnumerable<ControllerListModel> GetAllControllerModels()
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                return unit.Master.SysController.GetAllControllerModels();
            }
        }

        #endregion

        #region 获取数据结果

        /// <summary>
        /// 获取控制器列表数据
        /// </summary>
        /// <returns></returns>
        public CommonDataResult GetControllerListData()
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var data = unit.Master.SysController.GetAllControllerModels();

                return new CommonDataResult(data);
            }
        }

        /// <summary>
        /// 获取控制器树形数据
        /// </summary>
        /// <returns></returns>
        public CommonDataResult GetControllerTreeData()
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var data = unit.Master.SysController.GetAllControllerModels();

                return new CommonDataResult(data.Select(s => new { id = s.ID, text = (string.IsNullOrWhiteSpace(s.AreaName) ? "" : "[" + s.AreaName + "]") + s.Name }));
            }
        }

        #endregion

        #region 检查


        #endregion

        #region CUD操作

        /// <summary>
        /// 控制器保存操作
        /// </summary>
        /// <param name="model">控制器数据模型</param>
        /// <returns>操作结果</returns>
        public OperationResult SaveController(ControllerFormModel model)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                if (unit.Master.SysController.CheckControllerNameExist(model.Name, model.AreaName, model.ID)) { return new OperationResult(false, (string.IsNullOrWhiteSpace(model.AreaName) ? "" : "区域 " + model.AreaName + " 下的") + "控制器 " + model.Name + " 已经存在于数据库中，无法再次添加。"); }
                if (string.IsNullOrWhiteSpace(model.ID))
                {
                    var entity = model.CopyByDelegate<ControllerFormModel, SysController>();

                    unit.Master.SysController.Add(entity);
                    this.WriteLog(unit, SystemLogType.Add, "新增控制器", entity.Name, entity.ID);
                }
                else
                {
                    var entity = unit.Master.SysController.Find(model.ID);
                    entity.Name = model.Name;
                    entity.AreaName = model.AreaName;
                    entity.SortNumber = model.SortNumber;
                    entity.Description = model.Description;
                    entity.LimitedAttribute = model.LimitedAttribute;

                    unit.Master.SysController.Update(entity);
                    this.WriteLog(unit, SystemLogType.Update, "更新控制器", entity.Name, entity.ID);
                }
                return unit.TryCommit();
            }
        }

        /// <summary>
        /// 控制器移除操作
        /// </summary>
        /// <param name="id">控制器ID</param>
        /// <returns>操作结果</returns>
        public OperationResult RemoveController(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                if (unit.Master.SysAction.CheckActionExist(id)) { return new OperationResult(false, "该控制器下存在行为数据，无法删除。"); }

                var entity = unit.Master.SysController.Find(id);

                this.WriteLog(unit, SystemLogType.Delete, "删除控制器", entity.Name, entity.ID);
                unit.Master.SysController.Delete(entity);

                return unit.TryCommit();
            }
        }

        #endregion
    }
}

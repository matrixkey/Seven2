﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.Repositories;
using SF.Framework.DataModel.PermissionCache;
using SF.Framework.DataModel.Views;
using SF.Utilities;

namespace SF.Framework.BLL.Powerful.Master
{
    partial class SysActionBll
    {
        #region 获取模型

        /// <summary>
        /// 获取行为的创建数据模型
        /// </summary>
        /// <param name="controllerId">控制器ID</param>
        /// <param name="controllerName">控制器名称</param>
        /// <param name="controllerAreaName">控制器区域名称</param>
        /// <returns>获取行为的创建数据模型</returns>
        public ActionFormModel GetActionCreateModel(string controllerId, string controllerName, string controllerAreaName)
        {
            var model = new ActionFormModel();
            model.FK_ControllerID = controllerId;
            model.ControllerName = controllerName;
            model.ControllerAreaName = controllerAreaName;
            model.Type = ActionType.View;
            model.LimitedAttribute = ActionLimitedAttribute.AllowLogined;

            return model;
        }

        /// <summary>
        /// 获取指定主键的行为的编辑数据模型
        /// </summary>
        /// <param name="id">主键</param>
        /// <returns>行为的编辑数据模型</returns>
        public ActionFormModel GetActionEditModel(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                return unit.Master.SysAction.GetActionFormModel(id);
            }
        }

        #endregion

        #region 获取数据结果

        /// <summary>
        /// 获取指定控制器的行为列表数据
        /// </summary>
        /// <param name="controllerId">控制器ID</param>
        /// <param name="actionName">行为名称，用于模糊查询，默认为空，空则不进行匹配查询</param>
        /// <returns>业务数据结果</returns>
        public CommonDataResult GetActionListData(string controllerId, string actionName = "")
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var data = unit.Master.SysAction.GetActionModels(controllerId, actionName);

                return new CommonDataResult(data);
            }
        }

        /// <summary>
        /// 获取指定ID集合的行为列表数据
        /// </summary>
        /// <param name="ids">行为id集合</param>
        /// <returns>业务数据结果</returns>
        public CommonDataResult GetActionListData(IEnumerable<string> ids)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                var data = unit.Master.SysAction.GetActionModels(ids);

                return new CommonDataResult(data);
            }
        }

        #endregion

        #region 登录时判定权限 相关

        /// <summary>
        /// 根据菜单权限集合获取对应的菜单、控制器、行为数据模型集合
        /// </summary>
        /// <param name="menuPermissionsRange">菜单权限集合</param>
        /// <returns>菜单、控制器、行为数据模型集合</returns>
        public IEnumerable<ControllerActionRangeCacheModel> GetControllerActionRangeCacheModels(IEnumerable<SysMenuPermission> menuPermissionsRange)
        {
            if (menuPermissionsRange.IsNullOrEmpty()) { return new List<ControllerActionRangeCacheModel>(); }

            string actionIds = string.Join(",", menuPermissionsRange.Select(s => s.ActionIDs));
            var tempActionIdArray = actionIds.ToArray(",", true);
            if (tempActionIdArray.IsNullOrEmpty()) { return new List<ControllerActionRangeCacheModel>(); }

            Dictionary<string, List<string>> tempMenuActionDic = new Dictionary<string, List<string>>();
            foreach (var item in menuPermissionsRange)
            {
                List<string> temp;
                if (tempMenuActionDic.TryGetValue(item.FK_MenuID, out temp))
                {
                    temp.AddRange(item.ActionIDs.ToArray(",", true));
                }
                else
                {
                    temp = item.ActionIDs.ToArray(",", true).ToList();
                    tempMenuActionDic.Add(item.FK_MenuID, temp);
                }
            }

            IEnumerable<ActionListModel> actionInfos;
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                //先根据ActionID集合获取 行为信息（含控制器信息）
                actionInfos = unit.Master.SysAction.GetActionModels(tempActionIdArray);
            }

            //再结合 菜单权限集合 和 行为信息，组装出最终结果
            List<ControllerActionRangeCacheModel> result = new List<ControllerActionRangeCacheModel>();
            foreach (var item in tempMenuActionDic)
            {
                var tempActionIds = item.Value;
                foreach (var itemActionId in tempActionIds)
                {
                    var tempAction = actionInfos.FirstOrDefault(f => f.ID == itemActionId);
                    if (tempAction != null)
                    {
                        result.Add(new ControllerActionRangeCacheModel { MenuID = item.Key, ActionID = itemActionId, ActionName = tempAction.Name, ControllerName = tempAction.ControllerName, ControllerAreaName = tempAction.ControllerAreaName });
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// 根据用户ID获取该用户可用的菜单、控制器、行为数据模型集合
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="all">是否获取全部，默认否。若是，将忽略权限。</param>
        /// <returns>菜单、控制器、行为数据模型集合</returns>
        public IEnumerable<ControllerActionRangeCacheModel> GetControllerActionRangeCacheModels(string userId, bool all)
        {
            var menuPermissions = BllFactory.Master.SysMenuPermission.GetUseableMenuPermissions(userId, all);

            return this.GetControllerActionRangeCacheModels(menuPermissions);
        }

        #endregion

        #region CUD操作

        /// <summary>
        /// 行为保存操作
        /// </summary>
        /// <param name="model">行为数据模型</param>
        /// <returns>业务操作结果</returns>
        public OperationResult SaveAction(ActionFormModel model)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                if (unit.Master.SysAction.CheckActionExist(model.Name, model.FK_ControllerID, model.ID)) { return new OperationResult(false, "控制器 " + model.ControllerName + " 的行为 " + model.Name + " 已经存在于数据库中，无法再次添加。"); }
                if (string.IsNullOrWhiteSpace(model.ID))
                {
                    var entity = model.CopyByDelegate<ActionFormModel, SysAction>();

                    unit.Master.SysAction.Add(entity);
                    this.WriteLog(unit, SystemLogType.Add, "新增行为", entity.Name, entity.ID);
                }
                else
                {
                    var entity = unit.Master.SysAction.Find(model.ID);
                    entity.Name = model.Name;
                    entity.Type = model.Type;
                    entity.SortNumber = model.SortNumber;
                    entity.Description = model.Description;
                    entity.LimitedAttribute = model.LimitedAttribute;

                    unit.Master.SysAction.Update(entity);
                    this.WriteLog(unit, SystemLogType.Update, "更新行为", entity.Name, entity.ID);
                }
                return unit.TryCommit();
            }
        }

        /// <summary>
        /// 移除行为
        /// </summary>
        /// <param name="id">行为ID</param>
        /// <returns>业务操作结果</returns>
        public OperationResult RemoveAction(string id)
        {
            using (var unit = IUnitOfWorkFactory.UnitOfWork)
            {
                if (unit.Master.SysAction.CheckDirectRelatedMenuExist(id)) { return new OperationResult(false, "该行为被菜单直接关联，无法删除。"); }

                var entity = unit.Master.SysAction.Find(id);

                var result = unit.TryExecute(() => { return unit.Master.SysAction.DeleteActionWithRelation(id); });
                if (result.Success)
                {
                    this.WriteLog(unit, SystemLogType.Delete, "删除行为", entity.Name, entity.ID, true);
                }

                return result;
            }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SF.Framework.DataInterface;
using SF.Framework.DataModel.PermissionCache;
using SF.Framework.BLL.Security;
using SF.Utilities;

namespace SF.Framework.BLL
{
    /// <summary>
    /// 业务基类
    /// </summary>
    /// <typeparam name="TEntity">实体类型</typeparam>
    public abstract class BaseBll<TEntity>
        where TEntity : SF.Framework.EntityModelBaseForReflection
    {
        private CurrentUserInfoModel _currentUserBaseInfo;

        private IEnumerable<MenuRangeCacheModel> _currentUserMenuRange;

        private IEnumerable<MenuPermissionRangeCacheModel> _currentUserMenuPermissionRange;

        private IEnumerable<ControllerActionRangeCacheModel> _currentUserMenuControllerActionRange;

        /// <summary>
        /// 当前登录用户信息集合（不包含范围信息）
        /// </summary>
        public CurrentUserInfoModel CurrentUserBaseInfo
        {
            get { if (_currentUserBaseInfo == null) { _currentUserBaseInfo = new Permission().GetCurrentUserBaseInfo(); } return _currentUserBaseInfo; }
        }

        /// <summary>
        /// 当前用户的有权使用的菜单范围
        /// </summary>
        protected IEnumerable<MenuRangeCacheModel> CurrentUserMenuRange
        {
            get
            {
                if (_currentUserMenuRange == null)
                {
                    _currentUserMenuRange = new Permission().GetMenuCacheRange(CurrentUserBaseInfo.UserID, CurrentUserBaseInfo.UserName);
                } return _currentUserMenuRange;
            }
        }

        /// <summary>
        /// 当前用户的有权使用的菜单权限范围
        /// </summary>
        protected IEnumerable<MenuPermissionRangeCacheModel> CurrentUserMenuPermissionRange
        {
            get
            {
                if (_currentUserMenuPermissionRange == null)
                {
                    _currentUserMenuPermissionRange = new Permission().GetMenuPermissionCacheRange(CurrentUserBaseInfo.UserID, CurrentUserBaseInfo.UserName);
                } return _currentUserMenuPermissionRange;
            }
        }

        /// <summary>
        /// 当前用户的有权使用的菜单、控制器、行为范围
        /// </summary>
        protected IEnumerable<ControllerActionRangeCacheModel> CurrentUserMenuControllerActionRange
        {
            get
            {
                if (_currentUserMenuControllerActionRange == null)
                {
                    _currentUserMenuControllerActionRange = new Permission().GetControllerActionCacheRange(CurrentUserBaseInfo.UserID, CurrentUserBaseInfo.UserName);
                } return _currentUserMenuControllerActionRange;
            }
        }

        /// <summary>
        /// 在指定工作单元中写入操作日志（不提交）
        /// </summary>
        /// <param name="unit">工作单元</param>
        /// <param name="type">日志类型</param>
        /// <param name="content">日志内容</param>
        /// <param name="immediatelySave">是否立即保存，默认否</param>
        protected void WriteLog(IUnitOfWork unit, SystemLogType type, string content, bool immediatelySave = false)
        {
            Check.NotEmpty(content);

            BLL.Security.CurrentUserInfoModel current = null;
            SF.Utilities.Trying.Try(() => { current = this.CurrentUserBaseInfo; });
            unit.Master.SysLog.WriteLog(type, content, current == null ? "guest" : current.UserName, current == null ? "未知用户" : current.RealName, current == null ? "127.0.0.0" : CurrentUserBaseInfo.IpAddress, immediatelySave);
        }

        /// <summary>
        /// 在指定工作单元中写入操作日志（不提交）
        /// </summary>
        /// <param name="unit">工作单元</param>
        /// <param name="type">日志类型</param>
        /// <param name="mainContent">日志内容主体信息</param>
        /// <param name="objectId">操作对象业务主键ID</param>
        /// <param name="immediatelySave">是否立即保存，默认否</param>
        protected void WriteLog(IUnitOfWork unit, SystemLogType type, string mainContent, string objectId, bool immediatelySave = false)
        {
            if (!string.IsNullOrWhiteSpace(objectId))
            {
                mainContent += "【ID：" + objectId + "】";
            }
            this.WriteLog(unit, type, mainContent, immediatelySave);
        }

        /// <summary>
        /// 在指定工作单元中写入操作日志（不提交）
        /// </summary>
        /// <param name="unit">工作单元</param>
        /// <param name="type">日志类型</param>
        /// <param name="mainContent">日志内容主体信息</param>
        /// <param name="objectName">操作对象名称</param>
        /// <param name="objectId">操作对象业务主键ID</param>
        /// <param name="immediatelySave">是否立即保存，默认否</param>
        protected void WriteLog(IUnitOfWork unit, SystemLogType type, string mainContent, string objectName, string objectId, bool immediatelySave = false)
        {
            if (!string.IsNullOrWhiteSpace(objectName))
            {
                mainContent += "——" + objectName;
            }
            if (!string.IsNullOrWhiteSpace(objectId))
            {
                mainContent += "【ID：" + objectId + "】";
            }
            this.WriteLog(unit, type, mainContent, immediatelySave);
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.BLL
{
    /// <summary>
    /// 业务结果基类
    /// </summary>
    public abstract class BllResult
    { }

    /// <summary>
    /// 操作结果类
    /// </summary>
    public class OperationResult : BllResult
    {
        /// <summary>
        /// 是否成功
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// 是否在UI中移除
        /// </summary>
        public bool RemoveOnUI { get; set; }

        /// <summary>
        /// 文本消息
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// 数据对象
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        public OperationResult()
        {

        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="success">是否成功</param>
        public OperationResult(bool success)
            : this()
        {
            this.Success = success;
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="success">是否成功</param>
        /// <param name="message">文本消息</param>
        public OperationResult(bool success, string message)
            : this(success)
        {
            this.Message = message;
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="success">是否成功</param>
        /// <param name="message">文本消息</param>
        /// <param name="data">数据对象</param>
        public OperationResult(bool success, string message, object data)
            : this(success, message)
        {
            this.Data = data;
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="success">是否成功</param>
        /// <param name="message">文本消息</param>
        /// <param name="data">数据对象</param>
        public OperationResult(bool success, string[] message, object data = null)
            : this(success)
        {
            if (message.Length == 1)
            {
                this.Message = success ? message[0] : string.Empty;
            }
            else if (message.Length >= 2)
            {
                this.Message = success ? message[0] : message[1];
            }
            this.Data = data;
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="success">是否成功</param>
        /// <param name="remove">是否在UI中移除</param>
        /// <param name="message">文本消息</param>
        /// <param name="data">数据对象</param>
        public OperationResult(bool success, bool remove, string message = null, object data = null)
            : this(success, message, data)
        {
            this.RemoveOnUI = remove;
        }
    }

    /// <summary>
    /// 常规数据结果类
    /// </summary>
    public class CommonDataResult : BllResult
    {
        /// <summary>
        /// 数据对象
        /// </summary>
        public object Data { get; set; }

        /// <summary>
        /// 构造函数
        /// </summary>
        public CommonDataResult()
        {

        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="data">数据对象</param>
        public CommonDataResult(object data)
            : this()
        {
            this.Data = data;
        }
    }

    /// <summary>
    /// 分页数据结果类
    /// </summary>
    public class PagerDataResult : BllResult
    {
        public SF.ComponentModel.Paging.PagingData PagerData { get; set; }

        /// <summary>
        /// 无参构造
        /// </summary>
        public PagerDataResult()
        { }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="pagingData">分页数据对象</param>
        public PagerDataResult(SF.ComponentModel.Paging.PagingData pagingData)
            : this()
        {
            this.PagerData = pagingData;
        }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="pageIndex">表示页面索引号，从 0 开始计数。</param>
        /// <param name="pageSize">表示页面的尺寸，即一页有多少行数据。</param>
        /// <param name="rowCount">表示页面数据总行数。</param>
        /// <param name="data">表示页面数据内容。</param>
        public PagerDataResult(int pageIndex, int pageSize, int rowCount, IEnumerable data)
            : this(new SF.ComponentModel.Paging.PagingData(pageIndex, pageSize, rowCount, data))
        {
        }
    }
}

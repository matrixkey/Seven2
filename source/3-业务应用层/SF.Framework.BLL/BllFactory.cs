﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.BLL
{
    partial class BllFactory
    {
        /// <summary>
        /// 内工厂缓存
        /// </summary>
        private Dictionary<Type, object> powerfulFactoryCache = new Dictionary<Type, object>();

        /// <summary>
        /// 获取内工厂对象
        /// </summary>
        /// <typeparam name="R">内工厂类型</typeparam>
        /// <returns>内工厂对象</returns>
        private R GetPowerfulFactoryByInstance<R>()
            where R : class, new()
        {
            Type t = typeof(R);
            if (!powerfulFactoryCache.ContainsKey(t))
            {
                var powerfulFactory = new R();
                powerfulFactoryCache.Add(t, powerfulFactory);

                return powerfulFactory;
            }
            else { return (R)powerfulFactoryCache[t]; }
        }
    }
}

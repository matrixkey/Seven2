﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.PermissionCache;
using SF.Framework.BLL.Powerful;
using SF.Utilities;

namespace SF.Framework.BLL.Security
{
    public class Permission
    {
        /// <summary>
        /// 获取登录用户的基本信息
        /// </summary>
        /// <returns>登录用户的基本信息</returns>
        public CurrentUserInfoModel GetCurrentUserBaseInfo()
        {
            HttpContext context = HttpContext.Current; CurrentUserInfoModel UserInfo = null;

            #region 从Forms验证存储的票据中获取用户的基本信息

            // 1. 读登录Cookie
            HttpCookie cookie = context.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (cookie != null && !string.IsNullOrEmpty(cookie.Value))
            {
                // 2. 解密Cookie值，获取FormsAuthenticationTicket对象
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(cookie.Value);
                if (ticket != null && !string.IsNullOrEmpty(ticket.UserData))
                {
                    // 3. 还原用户数据
                    UserInfo = SF.Utilities.Serialization.ToObject<CurrentUserInfoModel>(ticket.UserData);
                }
            }
            else
            {
                //如不存在当前登录信息，抛出异常
                throw new Exception("当前登录信息不存在。");
            }

            #endregion

            return UserInfo;
        }



        #region 获取缓存信息

        #region 获取缓存中的菜单权限范围

        /// <summary>
        /// 根据用户信息获取该用户缓存中的菜单权限范围，若未在缓存中找到数据，则从数据库中重新获取。
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="userName">用户名</param>
        /// <returns>菜单权限范围</returns>
        public IEnumerable<MenuPermissionRangeCacheModel> GetMenuPermissionCacheRange(string userId, string userName)
        {
            SecurityCache SC = new SecurityCache();
            object range = SC.GetMenuPermissionRangeSession(userName);
            if (range == null)
            {
                bool all = userName.Equals(SystemEnvironment.SuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase);
                var MPB = BllFactory.Master.SysMenuPermission;
                IEnumerable<SysMenuPermission> menuPermissionEntities = MPB.GetUseableMenuPermissions(userId, all);
                IEnumerable<MenuPermissionRangeCacheModel> menuPermissionRange = MPB.GetMenuPermissionRangeCacheModels(menuPermissionEntities);
                SC.AddMenuPermissionRangeSession(userName, menuPermissionRange);
                return menuPermissionRange;
            }
            else
            {
                return (IEnumerable<MenuPermissionRangeCacheModel>)range;
            }
        }

        /// <summary>
        /// 根据用户信息获取该用户缓存中的菜单权限范围，若未在缓存中找到数据，则从数据库中重新获取。
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns>菜单权限范围</returns>
        public IEnumerable<MenuPermissionRangeCacheModel> GetMenuPermissionCacheRange(string userName)
        {
            SecurityCache SC = new SecurityCache();
            object range = SC.GetMenuPermissionRangeSession(userName);
            if (range == null)
            {
                string userId = this.GetCurrentUserBaseInfo().UserID;
                bool all = userName.Equals(SystemEnvironment.SuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase);
                var MPB = BllFactory.Master.SysMenuPermission;
                IEnumerable<SysMenuPermission> menuPermissionEntities = MPB.GetUseableMenuPermissions(userId, all);
                IEnumerable<MenuPermissionRangeCacheModel> menuPermissionRange = MPB.GetMenuPermissionRangeCacheModels(menuPermissionEntities);
                SC.AddMenuPermissionRangeSession(userName, menuPermissionRange);
                return menuPermissionRange;
            }
            else
            {
                return (IEnumerable<MenuPermissionRangeCacheModel>)range;
            }
        }

        /// <summary>
        /// 获取当前用户缓存中的菜单权限范围，若未在缓存中找到数据，则从数据库中重新获取。
        /// </summary>
        /// <returns>菜单权限范围</returns>
        public IEnumerable<MenuPermissionRangeCacheModel> GetCurrentUserMenuPermissionCacheRange()
        {
            var currentInfo = this.GetCurrentUserBaseInfo();
            string userId = currentInfo.UserID;
            string userName = currentInfo.UserName;
            return this.GetMenuPermissionCacheRange(userId, userName);
        }

        #endregion

        #region 获取缓存中的菜单按钮权限范围

        /// <summary>
        /// 根据用户信息获取该用户缓存中的菜单按钮权限范围
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="userName">用户名</param>
        /// <returns>菜单按钮权限范围</returns>
        public IEnumerable<ButtonPermissionRangeCacheModel> GetMenuButtonPermissionCacheRange(string userId, string userName)
        {
            SecurityCache SC = new SecurityCache();
            object range = SC.GetMenuButtonPermissionRangeSession(userName);
            if (range == null)
            {
                bool all = userName.Equals(SystemEnvironment.SuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase);
                IEnumerable<ButtonPermissionRangeCacheModel> menuButtonPermissionsRange = BllFactory.Master.SysMenuPermission.GetButtonPermissionRangeCacheModel(userId, all);
                SC.AddMenuButtonPermissionRangeSession(userName, menuButtonPermissionsRange);
                return menuButtonPermissionsRange;
            }
            else
            {
                return (IEnumerable<ButtonPermissionRangeCacheModel>)range;
            }
        }

        /// <summary>
        /// 根据用户信息获取该用户缓存中的菜单按钮权限范围
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns>菜单按钮权限范围</returns>
        public IEnumerable<ButtonPermissionRangeCacheModel> GetMenuButtonPermissionCacheRange(string userName)
        {
            SecurityCache SC = new SecurityCache();
            object range = SC.GetMenuButtonPermissionRangeSession(userName);
            if (range == null)
            {
                string userId = this.GetCurrentUserBaseInfo().UserID;
                bool all = userName.Equals(SystemEnvironment.SuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase);
                IEnumerable<ButtonPermissionRangeCacheModel> menuButtonPermissionsRange = BllFactory.Master.SysMenuPermission.GetButtonPermissionRangeCacheModel(userId, all);
                SC.AddMenuButtonPermissionRangeSession(userName, menuButtonPermissionsRange);
                return menuButtonPermissionsRange;
            }
            else
            {
                return (IEnumerable<ButtonPermissionRangeCacheModel>)range;
            }
        }

        /// <summary>
        /// 获取当前用户缓存中的菜单按钮权限范围
        /// </summary>
        /// <returns>菜单按钮权限范围</returns>
        public IEnumerable<ButtonPermissionRangeCacheModel> GetCurrentUserMenuButtonPermissionCacheRange()
        {
            var currentInfo = this.GetCurrentUserBaseInfo();
            string userId = currentInfo.UserID;
            string userName = currentInfo.UserName;
            return this.GetMenuButtonPermissionCacheRange(userId, userName);
        }

        #endregion

        #region 获取缓存中的 控制器、行为 范围

        /// <summary>
        /// 根据用户信息获取当前用户缓存中的控制器、行为 范围
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public IEnumerable<ControllerActionRangeCacheModel> GetControllerActionCacheRange(string userId, string userName)
        {
            SecurityCache SC = new SecurityCache();
            object range = SC.GetControllerActionRangeSession(userName);
            if (range == null)
            {
                bool all = userName.Equals(SystemEnvironment.SuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase);
                IEnumerable<ControllerActionRangeCacheModel> controllerActionRange = BllFactory.Master.SysAction.GetControllerActionRangeCacheModels(userId, all);
                SC.AddControllerActionRangeSession(userName, controllerActionRange);

                return controllerActionRange;
            }
            else
            {
                return (IEnumerable<ControllerActionRangeCacheModel>)range;
            }
        }

        /// <summary>
        /// 根据用户信息获取当前用户缓存中的控制器、行为 范围
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public IEnumerable<ControllerActionRangeCacheModel> GetControllerActionCacheRange(string userName)
        {
            SecurityCache SC = new SecurityCache();
            object range = SC.GetControllerActionRangeSession(userName);
            if (range == null)
            {
                string userId = this.GetCurrentUserBaseInfo().UserID;
                bool all = userName.Equals(SystemEnvironment.SuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase);
                IEnumerable<ControllerActionRangeCacheModel> controllerActionRange = BllFactory.Master.SysAction.GetControllerActionRangeCacheModels(userId, all);
                SC.AddControllerActionRangeSession(userName, controllerActionRange);

                return controllerActionRange;
            }
            else
            {
                return (IEnumerable<ControllerActionRangeCacheModel>)range;
            }
        }

        /// <summary>
        /// 获取当前用户缓存中的控制器行为范围
        /// </summary>
        /// <returns>控制器行为范围</returns>
        public IEnumerable<ControllerActionRangeCacheModel> GetCurrentUserControllerActionCacheRange()
        {
            var currentInfo = GetCurrentUserBaseInfo();
            string userId = currentInfo.UserID;
            string userName = currentInfo.UserName;
            return this.GetControllerActionCacheRange(userId, userName);
        }

        #endregion

        #region 获取缓存中的菜单范围

        /// <summary>
        /// 根据用户信息获取该用户缓存中的菜单范围，若未在缓存中找到数据，则从数据库中重新获取。
        /// </summary>
        /// <param name="userId">用户ID</param>
        /// <param name="userName">用户名</param>
        /// <returns>菜单范围</returns>
        public IEnumerable<MenuRangeCacheModel> GetMenuCacheRange(string userId, string userName)
        {
            SecurityCache SC = new SecurityCache();
            object range = SC.GetMenuRangeSession(userName);
            if (range == null)
            {
                IEnumerable<MenuRangeCacheModel> menuRange = BllFactory.Master.SysMenu.GetMenuRangeCacheModels(userId, userName.Equals(SystemEnvironment.SuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase));
                SC.AddMenuRangeSession(userName, menuRange);
                return menuRange;
            }
            else
            {
                return (IEnumerable<MenuRangeCacheModel>)range;
            }
        }

        /// <summary>
        /// 根据用户信息获取该用户缓存中的菜单范围，若未在缓存中找到数据，则从数据库中重新获取。
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns>菜单范围</returns>
        public IEnumerable<MenuRangeCacheModel> GetMenuCacheRange(string userName)
        {
            SecurityCache SC = new SecurityCache();
            object range = SC.GetMenuRangeSession(userName);
            if (range == null)
            {
                string userId = this.GetCurrentUserBaseInfo().UserID;
                bool all = userName.Equals(SystemEnvironment.SuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase);
                IEnumerable<MenuRangeCacheModel> menuRange = BllFactory.Master.SysMenu.GetMenuRangeCacheModels(userId, all);
                SC.AddMenuRangeSession(userName, menuRange);
                return menuRange;
            }
            else
            {
                return (IEnumerable<MenuRangeCacheModel>)range;
            }
        }

        /// <summary>
        /// 获取当前用户缓存中的菜单范围，若未在缓存中找到数据，则从数据库中重新获取。
        /// </summary>
        /// <returns>菜单范围</returns>
        public IEnumerable<MenuRangeCacheModel> GetCurrentUserMenuCacheRange()
        {
            var currentInfo = GetCurrentUserBaseInfo();
            string userId = currentInfo.UserID;
            string userName = currentInfo.UserName;
            return this.GetMenuCacheRange(userId, userName);
        }

        #endregion

        #endregion



        /// <summary>
        /// 获取当前用户的 指定控制器、指定行为 的菜单下的按钮权限数据，返回json格式的字符串
        /// </summary>
        /// <param name="area">区域名称</param>
        /// <param name="controller">控制器名称</param>
        /// <param name="action">行为名称</param>
        /// <param name="groupMarks">分组标记，表示只获取指定分组标记的按钮权限数据。不区分大小写。</param>
        /// <returns>Json格式的字符串</returns>
        public string GetCurrentUserButtons(string area, string controller, string action, IEnumerable<string> groupMarks)
        {
            CurrentUserInfoModel info = this.GetCurrentUserBaseInfo();
            IEnumerable<ButtonPermissionRangeCacheModel> permissions = this.GetMenuButtonPermissionCacheRange(info.UserID, info.UserName);
            IEnumerable<MenuRangeCacheModel> menus = this.GetMenuCacheRange(info.UserID, info.UserName);

            var tempType = StringComparison.InvariantCultureIgnoreCase;
            MenuRangeCacheModel menu = menus.FirstOrDefault(f => !string.IsNullOrWhiteSpace(f.ControllerName) && !string.IsNullOrWhiteSpace(f.ActionName) && f.ControllerName.Equals(controller, tempType) && f.ActionName.Equals(action, tempType) && (string.IsNullOrWhiteSpace(area) ? (string.IsNullOrWhiteSpace(f.ControllerAreaName)) : (f.ControllerAreaName.Equals(area, tempType))));
            string menuId = menu == null ? "" : menu.ID;

            var data = permissions.Where(w => w.MenuID == menuId).TryOrderBy(o => o.SortNumber).Select(s => new { s.GroupMark, s.ButtonName, s.ButtonIconClass, s.ButtonSymbol, s.ButtonHandler, s.IsDisabled });
            if (groupMarks.Count() > 0) { data = data.Where(w => !string.IsNullOrWhiteSpace(w.GroupMark) && groupMarks.Contains(w.GroupMark.ToLower())); }
            return data.ToJson();
        }

        /// <summary>
        /// 判定指定用户是否存在指定控制器、指定动作的使用权限
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="areaName">区域名称</param>
        /// <param name="controllerName">控制器名</param>
        /// <param name="actionName">动作名</param>
        /// <returns>如果有权使用，返回 true ，否则返回 false 。</returns>
        public bool CheckAuth(string userName, string areaName, string controllerName, string actionName)
        {
            bool result = false;
            var range = this.GetControllerActionCacheRange(userName);
            var tempType = StringComparison.InvariantCultureIgnoreCase;
            result = range.Any(a => a.ControllerName.Equals(controllerName, tempType) && a.ActionName.Equals(actionName, tempType) && (string.IsNullOrWhiteSpace(areaName) ? string.IsNullOrWhiteSpace(a.ControllerAreaName) : areaName.Equals(a.ControllerAreaName, tempType)));

            return result;
        }
    }
}

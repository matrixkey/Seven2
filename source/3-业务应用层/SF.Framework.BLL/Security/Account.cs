﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.PermissionCache;
using SF.Framework.BLL.Powerful.Master;
using SF.Utilities;

namespace SF.Framework.BLL.Security
{
    public class Account
    {
        /// <summary>
        /// 登录校验
        /// </summary>
        /// <param name="model">登录模型信息</param>
        /// <param name="theVerCode">初始验证码</param>
        /// <returns>业务操作结果</returns>
        public OperationResult Login(LoginModel model, string theVerCode)
        {
            if (string.IsNullOrWhiteSpace(model.UserName)) { return new OperationResult(false, "登录名不能为空！"); }

            #region 检验验证码

            if (!theVerCode.Equals(model.VerCode.ToLower()))
            {
                //return new OperationResult(false, "验证码不正确！");
            }

            #endregion

            SysUserBll UB = BllFactory.Master.SysUser;
            string ipAddress = HttpContext.Current.Request.UserHostAddress;
            OperationResult result = UB.CheckLogin(model.UserName, model.Password, ipAddress);
            if (result.Success)
            {
                SF.Framework.DataModel.PermissionCache.UserAccountModel userModel = result.Data as SF.Framework.DataModel.PermissionCache.UserAccountModel;

                CurrentUserInfoModel loginInfo = new CurrentUserInfoModel(); SecurityCache SC = new SecurityCache();
                #region 将额外信息赋值给用户数据模型

                loginInfo.UserID = userModel.UserEntity.ID;
                loginInfo.UserName = userModel.UserEntity.UserName;
                loginInfo.IpAddress = ipAddress;

                #endregion

                #region Forms身份验证记录

                string data = loginInfo.ToJson();

                DateTime expiration = model.RememberLoginName
                    ? DateTime.Now.AddDays(7)
                    : DateTime.Now.Add(FormsAuthentication.Timeout);
                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1, loginInfo.UserName, DateTime.Now, expiration,
                    true, data, FormsAuthentication.FormsCookiePath);
                HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(ticket));
                cookie.Expires = ticket.Expiration;
                HttpContext.Current.Response.Cookies.Set(cookie);

                #endregion

                #region 记住账号

                if (model.RememberLoginName)
                {
                    SC.AddLoginNameCookie(loginInfo.UserName, DateTime.Now.AddDays(7));
                }
                else
                {
                    SC.RemoveLoginNameCookie(loginInfo.UserName);
                }

                #endregion

                #region 缓存范围

                SysMenuPermissionBll MPB = BllFactory.Master.SysMenuPermission;
                SysActionBll AB = BllFactory.Master.SysAction;
                SysMenuBll MB = BllFactory.Master.SysMenu;

                //用户ID => 可用的的菜单权限实体集合
                IEnumerable<SysMenuPermission> menuPermissionEntities = MPB.GetUseableMenuPermissions(loginInfo.UserID, loginInfo.UserName.Equals(SystemEnvironment.SuperAdminAccountName, StringComparison.InvariantCultureIgnoreCase));

                //可用的的菜单权限实体集合 => 菜单权限范围
                IEnumerable<MenuPermissionRangeCacheModel> menuPermissionRange = MPB.GetMenuPermissionRangeCacheModels(menuPermissionEntities);
                SC.AddMenuPermissionRangeSession(loginInfo.UserName, menuPermissionRange);

                //可用的的菜单权限实体集合 => 按钮权限范围
                IEnumerable<ButtonPermissionRangeCacheModel> buttonMenuPermissionRange = MPB.GetButtonPermissionRangeCacheModel(menuPermissionEntities.Where(w => w.Type == MenuPermissionType.Button));
                SC.AddMenuButtonPermissionRangeSession(loginInfo.UserName, buttonMenuPermissionRange);

                //可用的的菜单权限实体集合 => ActionIDs => 可用的控制器、行为范围
                IEnumerable<ControllerActionRangeCacheModel> controllerActionRange = AB.GetControllerActionRangeCacheModels(menuPermissionEntities);
                SC.AddControllerActionRangeSession(loginInfo.UserName, controllerActionRange);

                //可用的控制器、行为范围 => 可用的菜单范围
                IEnumerable<MenuRangeCacheModel> menuRange = MB.GetMenuRangeCacheModels(controllerActionRange);
                SC.AddMenuRangeSession(loginInfo.UserName, menuRange);

                #endregion
            }

            return result;
        }

        /// <summary>
        /// 用指定用户名进行注销退出
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns>操作结果</returns>
        public OperationResult Logout(string userName)
        {
            OperationResult result = new OperationResult(true);
            if (result.Success)
            {
                SecurityCache C = new SecurityCache();
                C.RemoveMenuRangeSession(userName);
                C.RemoveControllerActionRangeSession(userName);
                C.RemoveMenuPermissionRangeSession(userName);
                C.RemoveMenuButtonPermissionRangeSession(userName);
                BllFactory.Master.SysLog.WriteLog(SystemLogType.Logout, "注销退出成功。");
                FormsAuthentication.SignOut();
            }
            return result;
        }
    }
}

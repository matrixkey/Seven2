﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

using SF.Framework.Entity.Master;
using SF.Framework.DataModel.PermissionCache;
using SF.Security.Cryptography;
using SF.Utilities;

namespace SF.Framework.BLL.Security
{
    public class SecurityCache
    {
        #region 登录名

        /// <summary>
        /// 添加“登录名”cookie
        /// </summary>
        /// <param name="value">登录名</param>
        /// <param name="expires">过期时间</param>
        public void AddLoginNameCookie(string value, DateTime expires)
        {
            HttpCookie LoginNameCookie = new HttpCookie(SystemCacheDefine.LoginNameCookieName, DESCrypto.Encrypt(value));
            LoginNameCookie.Expires = expires;
            HttpContext.Current.Response.Cookies.Set(LoginNameCookie);
        }

        /// <summary>
        /// 移除“登录名”cookie
        /// </summary>
        /// <param name="userName">用户名</param>
        public void RemoveLoginNameCookie(string userName)
        {
            HttpCookie LoginNameCookie = HttpContext.Current.Request.Cookies[SystemCacheDefine.LoginNameCookieName];
            if (LoginNameCookie != null)
            {
                LoginNameCookie.Expires = DateTime.Now.AddDays(-1);
                HttpContext.Current.Response.AppendCookie(LoginNameCookie); //这句一定要加上，否则无法删除
            }
        }

        #endregion


        #region 菜单权限范围

        /// <summary>
        /// 添加“菜单权限范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="value">菜单权限范围</param>
        public void AddMenuPermissionRangeSession(string userName, IEnumerable<MenuPermissionRangeCacheModel> value)
        {
            string name = SystemCacheDefine.MenuPermissionRangeCookieNamePrefix + userName;
            var Range = HttpContext.Current.Session[name];
            if (Range == null)
            {
                HttpContext.Current.Session.Add(name, value);
            }
            else
            {
                HttpContext.Current.Session[name] = value;
            }
        }

        /// <summary>
        /// 移除“菜单权限范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        public void RemoveMenuPermissionRangeSession(string userName)
        {
            HttpContext.Current.Session.Remove(SystemCacheDefine.MenuPermissionRangeCookieNamePrefix + userName);
        }

        /// <summary>
        /// 获取“菜单权限范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public object GetMenuPermissionRangeSession(string userName)
        {
            return HttpContext.Current.Session[SystemCacheDefine.MenuPermissionRangeCookieNamePrefix + userName];
        }

        #endregion


        #region 菜单按钮权限范围

        /// <summary>
        /// 添加“菜单按钮权限范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="value">菜单按钮权限范围</param>
        public void AddMenuButtonPermissionRangeSession(string userName, IEnumerable<ButtonPermissionRangeCacheModel> value)
        {
            string name = SystemCacheDefine.MenuButtonPermissionRangeCookieNamePrefix + userName;
            var Range = HttpContext.Current.Session[name];
            if (Range == null)
            {
                HttpContext.Current.Session.Add(name, value);
            }
            else
            {
                HttpContext.Current.Session[name] = value;
            }
        }

        /// <summary>
        /// 移除“菜单按钮权限范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        public void RemoveMenuButtonPermissionRangeSession(string userName)
        {
            HttpContext.Current.Session.Remove(SystemCacheDefine.MenuButtonPermissionRangeCookieNamePrefix + userName);
        }

        /// <summary>
        /// 获取“菜单按钮权限范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public object GetMenuButtonPermissionRangeSession(string userName)
        {
            return HttpContext.Current.Session[SystemCacheDefine.MenuButtonPermissionRangeCookieNamePrefix + userName];
        }

        #endregion


        #region 控制器行为范围

        /// <summary>
        /// 添加“控制器行为范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="value">控制器行为范围</param>
        public void AddControllerActionRangeSession(string userName, IEnumerable<ControllerActionRangeCacheModel> value)
        {
            string name = SystemCacheDefine.ControllerActionRangeCookieNamePrefix + userName;
            var Range = HttpContext.Current.Session[name];
            if (Range == null)
            {
                HttpContext.Current.Session.Add(name, value);
            }
            else
            {
                HttpContext.Current.Session[name] = value;
            }
        }

        /// <summary>
        /// 移除“控制器行为范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        public void RemoveControllerActionRangeSession(string userName)
        {
            HttpContext.Current.Session.Remove(SystemCacheDefine.ControllerActionRangeCookieNamePrefix + userName);
        }

        /// <summary>
        /// 获取“控制器行为范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public object GetControllerActionRangeSession(string userName)
        {
            return HttpContext.Current.Session[SystemCacheDefine.ControllerActionRangeCookieNamePrefix + userName];
        }

        #endregion


        #region 菜单范围

        /// <summary>
        /// 添加“菜单范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <param name="value">菜单范围</param>
        public void AddMenuRangeSession(string userName, IEnumerable<MenuRangeCacheModel> value)
        {
            string name = SystemCacheDefine.MenuRangeCookieNamePrefix + userName;
            var Range = HttpContext.Current.Session[name];
            if (Range == null)
            {
                HttpContext.Current.Session.Add(name, value);
            }
            else
            {
                HttpContext.Current.Session[name] = value;
            }
        }

        /// <summary>
        /// 移除“菜单范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        public void RemoveMenuRangeSession(string userName)
        {
            HttpContext.Current.Session.Remove(SystemCacheDefine.MenuRangeCookieNamePrefix + userName);
        }

        /// <summary>
        /// 获取“菜单范围”session
        /// </summary>
        /// <param name="userName">用户名</param>
        /// <returns></returns>
        public object GetMenuRangeSession(string userName)
        {
            return HttpContext.Current.Session[SystemCacheDefine.MenuRangeCookieNamePrefix + userName];
        }

        #endregion
    }
}

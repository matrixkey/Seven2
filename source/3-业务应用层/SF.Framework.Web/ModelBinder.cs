﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SF.Framework.Web
{
    /// <summary>
    /// 去除前后空格的模型绑定器
    /// </summary>
    public class TrimModelBinder : DefaultModelBinder
    {
        /// <summary>
        /// 重写SetProperty 自动去除前后空格 （若去掉前后空格之后该值为空，则不去掉前后空格）
        /// 可以直接在global中注册 ModelBinders.Binders.DefaultBinder = new Models.TrimModelBinder();
        /// </summary>
        /// <param name="controllerContext">运行控制器的上下文</param>
        /// <param name="bindingContext">绑定模型的上下文</param>
        /// <param name="propertyDescriptor">描述要设置的属性</param>
        /// <param name="value"></param>
        protected override void SetProperty(ControllerContext controllerContext,
            ModelBindingContext bindingContext,
            System.ComponentModel.PropertyDescriptor propertyDescriptor, object value)
        {
            object temp = value;
            if (propertyDescriptor.PropertyType == typeof(string))
            {
                var stringValue = (string)value;
                if (!string.IsNullOrEmpty(stringValue) && !string.IsNullOrWhiteSpace(stringValue.Trim()))
                { stringValue = stringValue.Trim(); }
                temp = stringValue;
            }

            base.SetProperty(controllerContext, bindingContext, propertyDescriptor, temp);
        }
    }

    /// <summary>
    /// 对Json数据自动反序列化的模型绑定器类，使其支持对象集合参数
    /// </summary>
    /// <typeparam name="T">Json数据转换的目标类型</typeparam>
    public class JsonBinder<T> : IModelBinder
    {
        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            //从请求中获取提交的参数数据 
            var json = controllerContext.HttpContext.Request.Form[bindingContext.ModelName] as string;
            //提交参数是对象 
            if (json.StartsWith("{") && json.EndsWith("}"))
            {
                return SF.Utilities.Serialization.DeserializeJsToObject(json, typeof(T));
            }
            //提交参数是数组 
            if (json.StartsWith("[") && json.EndsWith("]"))
            {
                return SF.Utilities.Serialization.DeserializeJsToArray<T>(json);
            }
            return null;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Optimization;

using SF.Web.Mvc.Optimization;

namespace SF.Framework.Web.Config
{
    public class BundleConfig
    {
        // 有关绑定的详细信息，请访问 http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //jquery 及其 扩展
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery/jquery-1.11.0.min.js", "~/Scripts/jquery-extensions/jquery.jdirk.js"));

            //总样式
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/site.css", "~/Content/form.css", "~/Content/icons/icons.css"));


            #region jquery-easyui

            bundles.Add("~/bundles/jquery-easyui",

                //"~/Scripts/jquery/jquery-1.11.0.min.js",
                //"~/Scripts/jquery-extensions/jquery.jdirk.js",

                new BundlePath { KeepPath = false, Type = BundleType.Style, VirtualPath = "~/Scripts/jquery-easyui/jquery-easyui-theme/default/easyui.css" },
                "~/Scripts/jquery-easyui/jquery-easyui-theme/icon.css",

                "~/Scripts/jquery-easyui/jquery-easyui-1.3.6/jquery.easyui.min.js",
                "~/Scripts/jquery-easyui/jquery-easyui-1.3.6/locale/easyui-lang-zh_CN.js"
                );

            #endregion

            #region jquery-easyui-extensions-common

            bundles.Add("~/bundles/jquery-easyui-extensions-common",

                new BundlePath { KeepPath = false, Type = BundleType.Style, VirtualPath = "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.css" },
                "~/Scripts/jquery-easyui-extensions/icons/icon-all.min.css",

                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.js",

                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.progressbar.js",
                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.panel.js",
                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.layout.js",
                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.linkbutton.js",
                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.validatebox.js",
                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.combo.js",
                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.menu.js",
                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.slider.js",

                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.tree.js",
                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.form.js",
                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.combobox.js",
                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.searchbox.js",
                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.window.js",

                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.dialog.js",
                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.datagrid.js",
                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.combotree.js",

                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.tabs.js",
                "~/Scripts/jquery-easyui-extensions/jeasyui.extensions.treegrid.js",

                "~/Scripts/jquery-easyui-extensions/jquery-easyui-toolbar/jquery.toolbar.js"
                );

            #endregion

            #region jquery-easyui-extensions-common-self

            bundles.Add("~/bundles/jquery-easyui-extensions-common-self",

                "~/Scripts/jquery-easyui-extensions-self/css/icon-default.css",

                "~/Scripts/jquery-easyui-extensions-self/jeasyui.extensions.layout.self.js",
                "~/Scripts/jquery-easyui-extensions-self/jeasyui.extensions.validatebox.self.js",
                "~/Scripts/jquery-easyui-extensions-self/jeasyui.extensions.tree.self.js",
                "~/Scripts/jquery-easyui-extensions-self/jeasyui.extensions.combobox.self.js",
                "~/Scripts/jquery-easyui-extensions-self/jeasyui.extensions.gridselector.self.js"
                );

            #endregion

            #region common

            bundles.Add(new ScriptBundle("~/bundles/common").Include(
                        "~/Scripts/jquery-easyui-extensions-self/jeasyui.helper.js",
                        "~/Scripts/common/common.js",
                        "~/Scripts/jquery-easyui-extensions-self/jeasyui.ajaxSetup.logoutExit.js"));

            #endregion

            #region jquery-plugins

            bundles.Add(new ScriptBundle("~/bundles/my97").Include(
                        "~/Scripts/jquery-plugins/My97DatePicker/WdatePicker.js"));

            #endregion

            #region securityJs

            bundles.Add(new ScriptBundle("~/bundles/securityJs").Include(
                        "~/Scripts/security/A2D.js"));

            #endregion

            BundleTable.EnableOptimizations = SystemEnvironment.EnableOptimizations;
        }
    }
}

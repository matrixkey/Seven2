﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

using SF.Framework.BLL.Security;

namespace SF.Framework.Web
{
    //[Filter.Auth]
    public abstract class BaseController : Controller
    {
        private CurrentUserInfoModel _currentUserBaseInfo;

        /// <summary>
        /// 当前登录用户信息集合（不包含范围信息）
        /// </summary>
        public CurrentUserInfoModel CurrentUserBaseInfo
        {
            get { if (_currentUserBaseInfo == null) { _currentUserBaseInfo = new Permission().GetCurrentUserBaseInfo(); } return _currentUserBaseInfo; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.IO;
using Noesis.Javascript;

namespace SF.Framework.Web.Security
{
    /// <summary>
    /// 调用前端的A2D.js进行加密解密的帮助器
    /// </summary>
    public static class JsSecurity
    {
        static JsSecurity()
        {
            MainJSPath = System.IO.Path.Combine(AppDomain.CurrentDomain.SetupInformation.ApplicationBase, "Scripts\\security\\A2D.js");
        }

        public class MockWindow
        {
        }
        public class MockDocument
        {
        }

        /// <summary>
        /// 前端A2D.js的路径，请在调用加密解密之前设置该值
        /// </summary>
        public static string MainJSPath { get; set; }

        static MockWindow wnd = new MockWindow();
        static MockDocument doc = new MockDocument();

        /// <summary>
        /// 检查前端A2D.js的路径是否有效
        /// </summary>
        private static void CheckMainJSPath()
        {
            if (string.IsNullOrWhiteSpace(MainJSPath))
            {
                throw new Exception("前端A2D.js的路径未设置，无法进行加密解密操作。");
            }
            if (!File.Exists(MainJSPath))
            {
                throw new Exception("前端A2D.js的路径无效，无法进行加密解密操作。");
            }
        }

        /// <summary>
        /// 利用前端A2D.js对指定内容进行加密
        /// </summary>
        /// <param name="plainText">要加密的内容</param>
        /// <returns>密文</returns>
        public static string Encode(string plainText)
        {
            CheckMainJSPath();
            using (JavascriptContext context = new JavascriptContext())
            {
                context.SetParameter("window", wnd);
                context.SetParameter("document", doc);
                context.SetParameter("message", plainText);
                string js = File.ReadAllText(MainJSPath);
                js += @"result = window.$A2D.DES.encrypt(message);";
                context.Run(js);
                string result = (string)context.GetParameter("result");
                return result;
            }
        }

        /// <summary>
        /// 利用前端A2D.js对指定内容进行解密
        /// </summary>
        /// <param name="encryptedText">要解密的内容</param>
        /// <returns>明文</returns>
        public static string Decode(string encryptedText)
        {
            CheckMainJSPath();
            using (JavascriptContext context = new JavascriptContext())
            {
                context.SetParameter("window", wnd);
                context.SetParameter("document", doc);
                context.SetParameter("encryptedMessage", encryptedText);
                string js = File.ReadAllText(MainJSPath);
                js += @"result = window.$A2D.DES.decrypt(encryptedMessage);";
                context.Run(js);
                string result = (string)context.GetParameter("result");
                return result;
            }
        }
    }
}

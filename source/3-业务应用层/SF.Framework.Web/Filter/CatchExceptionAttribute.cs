﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SF.Framework.Web
{
    /// <summary>
    /// 异常捕获反馈处理
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class CatchExceptionAttribute : HandleErrorAttribute
    {
        /// <summary>
        /// Action 类型
        /// </summary>
        public ActionType? Type { get; set; }

        /// <summary>
        /// 是否记录异常信息
        /// </summary>
        private bool IsRecord { get; set; }

        public CatchExceptionAttribute()
            : this(null, true)
        {

        }

        public CatchExceptionAttribute(bool isRecord)
            : this(null, isRecord)
        {
        }

        public CatchExceptionAttribute(ActionType actionType)
            : this(actionType, true)
        {
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="actionType">Action 类型</param>
        /// <param name="isRecord">是否记录异常信息</param>
        public CatchExceptionAttribute(ActionType? actionType, bool isRecord)
        {
            this.Type = actionType;
            this.IsRecord = IsRecord;
        }

        /// <summary>
        /// 在发生异常时调用
        /// </summary>
        /// <param name="filterContext">操作-筛选器上下文</param>
        public override void OnException(ExceptionContext filterContext)
        {
            string ExceptionID = Guid.NewGuid().ToString(), msg = filterContext.Exception.Message + " [信息编号：" + ExceptionID + "]";
            var r = filterContext.RouteData; var context = filterContext.HttpContext;
            string c = r.Values["controller"].ToString();
            string a = r.Values["action"].ToString();
            string userName = string.Empty;
            try { userName = context.User.Identity.Name; }
            catch { userName = "guest"; }
            if (string.IsNullOrWhiteSpace(userName)) { userName = "guest"; }

            string message = "[" + userName + "]发生异常——异常信息：" + msg + "；所在Controller：" + c + "；所在Action：" + a + "；";

            if (this.IsRecord)
            {
                //可能需要其他记录处理
            }
            SF.Utilities.Log4Helper.Error(this, filterContext.Exception, message);

            filterContext.ExceptionHandled = true;
            if (this.Type.HasValue)
            {
                if (this.Type.Value == Framework.ActionType.View)
                {
                    filterContext.Result = new RedirectResult("/Blank/ShowMessagePage?message=" + msg);
                }
                else if (this.Type.Value == Framework.ActionType.Data)
                {
                    filterContext.Result = new SF.Framework.BLL.CommonDataResult(new List<object>()).SerializeToJsonResult();
                }
                else if (this.Type.Value == Framework.ActionType.Operation)
                {
                    filterContext.Result = new SF.Framework.BLL.OperationResult(false, msg).SerializeToJsonResult();
                }
            }
            else
            { base.OnException(filterContext); }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

using SF.Utilities;

namespace SF.Framework.Web
{
    /// <summary>
    /// 获取菜单按钮权限的过滤器
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class AutoSerializeButtonPermissionsAttribute : ActionFilterAttribute
    {
        #region 属性

        /// <summary>
        /// 当前区域名称
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// 当前控制器名称
        /// </summary>
        private string ControllerName { get; set; }

        /// <summary>
        /// 当前动作名称
        /// </summary>
        private string ActionName { get; set; }

        /// <summary>
        /// 指定动作名称
        /// </summary>
        private string SpecialActionName { get; set; }

        /// <summary>
        /// 分组标记
        /// </summary>
        private IEnumerable<string> GroupMarks { get; set; }

        #endregion

        /// <summary>
        /// 构造方法
        /// </summary>
        /// <param name="groupMarks">分组标记，表示只获取当前ControllerName和当前ActionName下指定分组标记的按钮权限数据。若多个，可以用英文逗号相连。不区分大小写。并且将以ViewData["ButtonPermissionsGroup" + 索引号（1开始）]的形式传入视图中。</param>
        /// <param name="specialActionName">特定的action名称，表示只获取当前ControllerName和特定ActionName下指定分组标记的按钮权限数据。</param>
        public AutoSerializeButtonPermissionsAttribute(string groupMarks = "", string specialActionName = "")
        {
            this.SpecialActionName = specialActionName;
            this.GroupMarks = groupMarks.ToLower().ToArray(",", true);
        }

        /// <summary>
        /// 在 Action 结果返回之前对范围内的按钮权限数据进行序列化，并以ViewData["ButtonPermissionsGroup" + 索引号（1开始）]的形式存入，以便在试图中获取。
        /// </summary>
        /// <param name="filterContext">筛选器上下文</param>
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            this.AreaName = filterContext.RouteData.DataTokens["area"].ToString();
            this.ControllerName = filterContext.RouteData.Values["controller"].ToString();
            this.ActionName = !string.IsNullOrWhiteSpace(this.SpecialActionName) ? this.SpecialActionName : filterContext.RouteData.Values["action"].ToString();

            filterContext.Controller.ViewData["ButtonPermissions"] = new SF.Framework.BLL.Security.Permission().GetCurrentUserButtons(this.AreaName, this.ControllerName, this.ActionName, this.GroupMarks);
            int k = 1;
            foreach (var item in this.GroupMarks)
            {
                filterContext.Controller.ViewData["ButtonPermissionsGroup" + k] = item;
                k++;
            }

            base.OnResultExecuting(filterContext);
        }
    }
}

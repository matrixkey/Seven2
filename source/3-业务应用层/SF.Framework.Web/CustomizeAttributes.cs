﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.Web
{
    /// <summary>
    /// 表示在反射获取控制器、行为时要忽略的标记
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class NotMappedAttribute : Attribute { }



    /// <summary>
    /// 表示 只要登录就允许使用 的标记。
    /// 需注意的是，
    ///     当添加此特性的对象为行为，并且其所属控制器的“限制特性”非空且非跟随，则以其所属控制器的“限制特性”为准。
    ///     当添加此特性的控制器或行为在数据库中存在记录，则以数据库中的“限制特性”为准。
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class AllowLoginedAttribute : Attribute { }



    /// <summary>
    /// 表示 只允许超级管理员使用 的标记。
    /// 需注意的是，
    ///     当添加此特性的对象为行为，并且其所属控制器的“限制特性”非空且非跟随，则以其所属控制器的“限制特性”为准。
    ///     当添加此特性的控制器或行为在数据库中存在记录，则以数据库中的“限制特性”为准。
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false)]
    public class AllowOnlySuperAdminAttribute : Attribute { }



    /// <summary>
    /// 自定义相应编码，用来多层Auth过滤时判定使用
    /// </summary>
    public enum CustomStatusCode
    {
        无权访问 = 318,
        登录超时 = 319,
        未登录 = 320
    }
}

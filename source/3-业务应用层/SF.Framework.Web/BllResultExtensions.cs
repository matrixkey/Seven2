﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

using SF.Framework.BLL;
using SF.Utilities;

namespace SF.Framework.Web
{
    /// <summary>
    /// 一组对业务结果对象的扩展方法
    /// </summary>
    public static class BllResultExtensions
    {
        #region OperationResult

        /// <summary>
        /// 将业务操作结果对象序列化成JsonResult对象
        /// </summary>
        /// <param name="result">要序列化的业务操作结果对象</param>
        /// <returns>JsonResult对象</returns>
        public static JsonResult SerializeToJsonResult(this OperationResult result)
        {
            return SerializeToJsonResult(result, false, DateFormatType.None);
        }

        /// <summary>
        /// 将业务操作结果对象序列化成JsonResult对象
        /// </summary>
        /// <param name="result">要序列化的业务操作结果对象</param>
        /// <param name="enumTransfer">序列化时是否转换枚举Value为枚举Text，默认否</param>
        /// <returns>JsonResult对象</returns>
        public static JsonResult SerializeToJsonResult(this OperationResult result, bool enumTransfer)
        {
            return SerializeToJsonResult(result, enumTransfer, DateFormatType.None);
        }

        /// <summary>
        /// 将业务操作结果对象序列化成JsonResult对象
        /// </summary>
        /// <param name="result">要序列化的业务操作结果对象</param>
        /// <param name="dateFormatType">序列化时对日期数据的格式化类型</param>
        /// <returns>JsonResult对象</returns>
        public static JsonResult SerializeToJsonResult(this OperationResult result, DateFormatType dateFormatType)
        {
            return SerializeToJsonResult(result, false, dateFormatType);
        }

        /// <summary>
        /// 将业务操作结果对象序列化成JsonResult对象
        /// </summary>
        /// <param name="result">要序列化的业务操作结果对象</param>
        /// <param name="enumTransfer">序列化时是否转换枚举Value为枚举Text</param>
        /// <param name="dateFormatType">序列化时对日期数据的格式化类型</param>
        /// <returns>JsonResult对象</returns>
        public static JsonResult SerializeToJsonResult(this OperationResult result, bool enumTransfer, DateFormatType dateFormatType)
        {
            return new SF.Web.Mvc.JsonSerializeResult
            {
                Data = new
                {
                    success = result.Success,
                    remove = result.RemoveOnUI,
                    message = result.Message,
                    data = result.Data
                },
                EnumTransfer = enumTransfer,
                DateFormatType = dateFormatType
            };
        }

        #endregion

        #region CommonDataResult

        /// <summary>
        /// 将业务常规数据结果对象序列化成JsonResult对象
        /// </summary>
        /// <param name="result">要序列化的业务数据结果对象</param>
        /// <param name="enumTransfer">序列化时是否转换枚举Value为枚举Text，默认否</param>
        /// <returns>JsonResult对象</returns>
        public static JsonResult SerializeToJsonResult(this CommonDataResult result)
        {
            return SerializeToJsonResult(result, false, DateFormatType.None);
        }

        /// <summary>
        /// 将业务常规数据结果对象序列化成JsonResult对象
        /// </summary>
        /// <param name="result">要序列化的业务数据结果对象</param>
        /// <param name="enumTransfer">序列化时是否转换枚举Value为枚举Text，默认否</param>
        /// <returns>JsonResult对象</returns>
        public static JsonResult SerializeToJsonResult(this CommonDataResult result, bool enumTransfer)
        {
            return SerializeToJsonResult(result, enumTransfer, DateFormatType.None);
        }

        /// <summary>
        /// 将业务常规数据结果对象序列化成JsonResult对象
        /// </summary>
        /// <param name="result">要序列化的业务数据结果对象</param>
        /// <param name="enumTransfer">序列化时是否转换枚举Value为枚举Text，默认否</param>
        /// <param name="dateFormatType">序列化时对日期数据的格式化类型，默认不进行转换</param>
        /// <returns>JsonResult对象</returns>
        public static JsonResult SerializeToJsonResult(this CommonDataResult result, DateFormatType dateFormatType)
        {
            return SerializeToJsonResult(result, false, dateFormatType);
        }

        /// <summary>
        /// 将业务常规数据结果对象序列化成JsonResult对象
        /// </summary>
        /// <param name="result">要序列化的业务数据结果对象</param>
        /// <param name="enumTransfer">序列化时是否转换枚举Value为枚举Text</param>
        /// <param name="dateFormatType">序列化时对日期数据的格式化类型</param>
        /// <returns>JsonResult对象</returns>
        public static JsonResult SerializeToJsonResult(this CommonDataResult result, bool enumTransfer, DateFormatType dateFormatType)
        {
            return new SF.Web.Mvc.JsonSerializeResult
            {
                Data = result.Data,
                EnumTransfer = enumTransfer,
                DateFormatType = dateFormatType
            };
        }

        #endregion

        #region PagerDataResult

        /// <summary>
        /// 将业务分页数据结果对象序列化成JsonResult对象（为 easyui-datagrid 的数据格式特地扩展本方法）
        /// </summary>
        /// <param name="result">要序列化的业务分页数据结果对象</param>
        /// <returns>JsonResult对象</returns>
        public static JsonResult SerializeToDatagridJsonResult(this PagerDataResult result)
        {
            return SerializeToDatagridJsonResult(result, false, DateFormatType.None);
        }

        /// <summary>
        /// 将业务分页数据结果对象序列化成JsonResult对象（为 easyui-datagrid 的数据格式特地扩展本方法）
        /// </summary>
        /// <param name="result">要序列化的业务分页数据结果对象</param>
        /// <param name="enumTransfer">序列化时是否转换枚举Value为枚举Text</param>
        /// <returns>JsonResult对象</returns>
        public static JsonResult SerializeToDatagridJsonResult(this PagerDataResult result, bool enumTransfer)
        {
            return SerializeToDatagridJsonResult(result, enumTransfer, DateFormatType.None);
        }

        /// <summary>
        /// 将业务分页数据结果对象序列化成JsonResult对象（为 easyui-datagrid 的数据格式特地扩展本方法）
        /// </summary>
        /// <param name="result">要序列化的业务分页数据结果对象</param>
        /// <param name="dateFormatType">序列化时对日期数据的格式化类型</param>
        /// <returns>JsonResult对象</returns>
        public static JsonResult SerializeToDatagridJsonResult(this PagerDataResult result, DateFormatType dateFormatType)
        {
            return SerializeToDatagridJsonResult(result, false, dateFormatType);
        }

        /// <summary>
        /// 将业务分页数据结果对象序列化成JsonResult对象（为 easyui-datagrid 的数据格式特地扩展本方法）
        /// </summary>
        /// <param name="result">要序列化的业务分页数据结果对象</param>
        /// <param name="enumTransfer">序列化时是否转换枚举Value为枚举Text</param>
        /// <param name="dateFormatType">序列化时对日期数据的格式化类型</param>
        /// <returns>JsonResult对象</returns>
        public static JsonResult SerializeToDatagridJsonResult(this PagerDataResult result, bool enumTransfer, DateFormatType dateFormatType)
        {
            return new SF.Web.Mvc.JsonSerializeResult
            {
                Data = new { total = result.PagerData.RowCount, rows = result.PagerData.Data },
                EnumTransfer = enumTransfer,
                DateFormatType = dateFormatType
            };
        }











        /// <summary>
        /// 将业务分页数据结果对象序列化成JsonResult对象（为 easyui-datagrid 的数据格式特地扩展本方法）
        /// </summary>
        /// <param name="result">要序列化的业务分页数据结果对象</param>
        /// <returns>JsonResult对象</returns>
        public static JsonResult SerializeToJsonResult(this PagerDataResult result)
        {
            return SerializeToJsonResult(result, false, DateFormatType.None);
        }

        /// <summary>
        /// 将业务分页数据结果对象序列化成JsonResult对象（为 easyui-datagrid 的数据格式特地扩展本方法）
        /// </summary>
        /// <param name="result">要序列化的业务分页数据结果对象</param>
        /// <param name="enumTransfer">序列化时是否转换枚举Value为枚举Text</param>
        /// <returns>JsonResult对象</returns>
        public static JsonResult SerializeToJsonResult(this PagerDataResult result, bool enumTransfer)
        {
            return SerializeToJsonResult(result, enumTransfer, DateFormatType.None);
        }

        /// <summary>
        /// 将业务分页数据结果对象序列化成JsonResult对象（为 easyui-datagrid 的数据格式特地扩展本方法）
        /// </summary>
        /// <param name="result">要序列化的业务分页数据结果对象</param>
        /// <param name="dateFormatType">序列化时对日期数据的格式化类型</param>
        /// <returns>JsonResult对象</returns>
        public static JsonResult SerializeToJsonResult(this PagerDataResult result, DateFormatType dateFormatType)
        {
            return SerializeToJsonResult(result, false, dateFormatType);
        }

        /// <summary>
        /// 将业务分页数据结果对象序列化成JsonResult对象（为 easyui-datagrid 的数据格式特地扩展本方法）
        /// </summary>
        /// <param name="result">要序列化的业务分页数据结果对象</param>
        /// <param name="enumTransfer">序列化时是否转换枚举Value为枚举Text</param>
        /// <param name="dateFormatType">序列化时对日期数据的格式化类型</param>
        /// <returns>JsonResult对象</returns>
        public static JsonResult SerializeToJsonResult(this PagerDataResult result, bool enumTransfer, DateFormatType dateFormatType)
        {
            return new SF.Web.Mvc.JsonSerializeResult
            {
                Data = new { rows = result.PagerData.Data },
                EnumTransfer = enumTransfer,
                DateFormatType = dateFormatType
            };
        }

        #endregion
    }
}

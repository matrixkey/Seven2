﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Web.Mvc;

using SF.Utilities;

namespace SF.Framework.Web.Reflection
{
    public class ControllerActionGetter
    {
        /// <summary>
        /// 获取所有继承了 BaseController 的控制器模型集合
        /// </summary>
        /// <param name="assemblyName">程序集名称</param>
        /// <param name="ignoreNotMapped">是否 忽略 <see cref="NotMappedAttribute"/> 特性标记，默认不忽略</param>
        /// <returns>控制器模型集合</returns>
        public static List<ControllerModel> GetControllersByAssembly(string assemblyName, bool ignoreNotMapped = false)
        {
            var result = new List<ControllerModel>();

            var types = Assembly.Load(assemblyName).GetTypes();
            Type baseType = typeof(BaseController), notMappedType = typeof(NotMappedAttribute);

            foreach (var type in types)
            {
                if (type.BaseType.IsInhertOf(baseType) && (ignoreNotMapped ? true : !type.HasAttribute(notMappedType)))
                {
                    var model = new ControllerModel();
                    model.ControllerName = GetControllerShortName(type.Name);
                    model.AreaName = GetControllerAreaName(type.Namespace, assemblyName);
                    model.Description = type.GetDescription();
                    model.LimitedAttribute = GetLimitedAttribute(type);
                    result.Add(model);
                }
            }
            return result;
        }

        /// <summary>
        /// 根据控制器获取其行为模型集合
        /// </summary>
        /// <param name="assemblyName">程序集名称</param>
        /// <param name="controllerName">控制器名称</param>
        /// <param name="controllerAreaName">控制器区域名称</param>
        /// <param name="ignoreNotMapped">是否 忽略 <see cref="NotMappedAttribute"/> 特性标记，默认不忽略</param>
        /// <returns>行为模型集合</returns>
        public static List<ActionModel> GetActionsByAssembly(string assemblyName, string controllerName, string controllerAreaName, bool ignoreNotMapped = false)
        {
            var result = new List<ActionModel>();

            var types = Assembly.Load(assemblyName).GetTypes();
            Type baseType = typeof(BaseController), notMappedType = typeof(NotMappedAttribute);

            foreach (var type in types)
            {
                if (type.BaseType.IsInhertOf(baseType))
                {
                    if ((string.IsNullOrWhiteSpace(controllerAreaName) ? true : GetControllerAreaName(type.Namespace, assemblyName).Equals(controllerAreaName, StringComparison.CurrentCultureIgnoreCase)) && GetControllerShortName(type.Name).Equals(controllerName, StringComparison.CurrentCultureIgnoreCase))
                    {
                        result.AddRange(GetActionsByControllerType(type));
                        break;
                    }
                }
            }
            return result;
        }

        /// <summary>
        /// 根据 控制器Type 获取其所有的行为模型集合
        /// </summary>
        /// <param name="type">控制器类型</param>
        /// <returns>行为模型集合</returns>
        public static List<ActionModel> GetActionsByControllerType(Type type)
        {
            var result = new List<ActionModel>();

            string controllerName = GetControllerShortName(type.Name);
            Type resultType = typeof(ActionResult), notMappedType = typeof(NotMappedAttribute), actionTypeType = typeof(CatchExceptionAttribute);
            ActionLimitedAttribute controllerLimited = GetLimitedAttribute(type);

            var members = type.GetMethods();
            foreach (var member in members)
            {
                //只获取返回类型为继承于“ActionResult”的Action
                if (member.ReturnType.IsInhertOf(resultType) && !member.HasAttribute(notMappedType))
                {
                    var model = new ActionModel();
                    model.ActionName = member.Name;
                    model.ControllerName = controllerName;
                    model.Description = member.GetDescription();
                    model.Type = GetActionType(member);
                    model.LimitedAttribute = controllerLimited == ActionLimitedAttribute.Follow ? GetLimitedAttribute(member) : controllerLimited;

                    result.Add(model);
                }
            }
            return result;
        }

        /// <summary>
        /// 获取控制器全名中的短名（不含Controller后缀）
        /// </summary>
        /// <param name="controllerLongName">控制器全名</param>
        /// <returns>控制器短名</returns>
        private static string GetControllerShortName(string controllerLongName)
        {
            int len = ("Controller").Length;
            return controllerLongName.Substring(0, controllerLongName.Length - len);
        }

        /// <summary>
        /// 获取控制器全名中的区域名
        /// 方式1：取 A.B.C.D.E 中的 D，这种方式的前提是 控制器的所属文件夹肯定是区域名文件夹（若手动再创建一级文件夹，则会出现问题）。
        /// 方式2：A.B.C.D.E 中，替换掉“当前应用程序集名称+区域根目录文件夹 A.B.C ”，取剩下的部分中的第一个片段。
        /// 此处使用方法2获取
        /// </summary>
        /// <param name="controllerNameSpace">控制器的命名空间</param>
        /// <param name="assemblyName">程序集名称</param>
        /// <returns>控制器区域名</returns>
        private static string GetControllerAreaName(string controllerNameSpace, string assemblyName)
        {
            string areaTokenName = "Areas";
            if (!controllerNameSpace.Contains("." + areaTokenName)) { return string.Empty; }
            string assemblyNameAndArea = assemblyName + "." + areaTokenName;
            return controllerNameSpace.ToArray(assemblyNameAndArea, true)[0].Trim('.').ToArray(".", true)[0];
        }

        /// <summary>
        /// 获取 Action 的类型
        /// 若存在 ActionTypeAttribute 特性，则通过该特性中的 Type 属性来判定，若不存在该特性，则通过 Action 的返回类型来判定。
        /// </summary>
        /// <param name="action">Action 的成员类型</param>
        /// <returns>Action 的类型，是个枚举值</returns>
        private static ActionType GetActionType(MethodInfo action)
        {
            var actionType = action.GetAttribute<CatchExceptionAttribute>(false);
            if (actionType != null && actionType.Type.HasValue)
            {
                return actionType.Type.Value;
            }

            //未指定ActionType特性时，通过其返回类型来判定
            if (action.ReturnType.IsInhertOf(typeof(ViewResult)) || action.ReturnType.IsInhertOf(typeof(PartialViewResult)))
            {
                return ActionType.View;
            }

            //由于返回数据和返回操作结果，其返回类新都是jsonResult，因此无法做出准确判定。故统一判定为 数据 。
            return ActionType.Data;
        }

        /// <summary>
        /// 获取控制器的限定标记。
        /// 判定次序如下：
        /// 存在 AllowAnonymousAttribute ，则返回 ActionLimitedAttribute.AllowAnonymous
        /// 存在 AllowOnlySuperAdminAttribute ，则返回 ActionLimitedAttribute.AllowOnlyAdmin
        /// 存在 AllowLoginedAttribute ，则返回 ActionLimitedAttribute.AllowLogined
        /// 都不存在，则返回 ActionLimitedAttribute.Follow
        /// </summary>
        /// <param name="controllerType">控制器类型</param>
        /// <returns>限定标记</returns>
        private static ActionLimitedAttribute GetLimitedAttribute(Type controllerType)
        {
            bool allowAnonymous = controllerType.HasAttribute(typeof(AllowAnonymousAttribute), false);
            if (allowAnonymous) { return ActionLimitedAttribute.AllowAnonymous; }

            bool allowOnlySuperAdmin = controllerType.HasAttribute(typeof(AllowOnlySuperAdminAttribute), false);
            if (allowOnlySuperAdmin) { return ActionLimitedAttribute.AllowOnlyAdmin; }

            bool allowLogined = controllerType.HasAttribute(typeof(AllowLoginedAttribute), false);
            if (allowLogined) { return ActionLimitedAttribute.AllowLogined; }

            return ActionLimitedAttribute.Follow;
        }

        /// <summary>
        /// 获取控制器的限定标记。
        /// 判定次序如下：
        /// 存在 AllowAnonymousAttribute ，则返回 ActionLimitedAttribute.AllowAnonymous
        /// 存在 AllowOnlySuperAdminAttribute ，则返回 ActionLimitedAttribute.AllowOnlyAdmin
        /// 存在 AllowLoginedAttribute ，则返回 ActionLimitedAttribute.AllowLogined
        /// 都不存在，则返回 ActionLimitedAttribute.Follow
        /// </summary>
        /// <param name="action">行为的成员类型</param>
        /// <returns>限定标记</returns>
        private static ActionLimitedAttribute GetLimitedAttribute(MethodInfo action)
        {
            bool allowAnonymous = action.HasAttribute(typeof(AllowAnonymousAttribute), false);
            if (allowAnonymous) { return ActionLimitedAttribute.AllowAnonymous; }

            bool allowOnlySuperAdmin = action.HasAttribute(typeof(AllowOnlySuperAdminAttribute), false);
            if (allowOnlySuperAdmin) { return ActionLimitedAttribute.AllowOnlyAdmin; }

            bool allowLogined = action.HasAttribute(typeof(AllowLoginedAttribute), false);
            if (allowLogined) { return ActionLimitedAttribute.AllowLogined; }

            return ActionLimitedAttribute.Follow;
        }
    }
}

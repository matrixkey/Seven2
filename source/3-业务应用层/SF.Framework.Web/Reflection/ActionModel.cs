﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF.Framework.Web.Reflection
{
    public class ActionModel
    {
        /// <summary>
        /// 行为名称
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// 控制器
        /// </summary>
        public string ControllerName { get; set; }

        /// <summary>
        /// 行为类型
        /// </summary>
        public ActionType Type { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 限制标记
        /// </summary>
        public ActionLimitedAttribute LimitedAttribute { get; set; }
    }
}

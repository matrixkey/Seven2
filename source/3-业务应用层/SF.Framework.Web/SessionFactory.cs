﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SF.Framework.Web
{
    public class SessionFactory : SF.Framework.ISessionFactory
    {
        /// <summary>
        /// 当前登录用户的用户名
        /// </summary>
        public string UserName
        {
            get
            {
                HttpContext current = HttpContext.Current;
                string result = string.Empty;
                if (current != null && current.User != null)
                {
                    result = current.User.Identity.IsAuthenticated ? current.User.Identity.Name : string.Empty;
                }

                return result;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

using SF.Framework.Web;

namespace SF.Framework.Controllers.B
{
    public class TBController : BaseController
    {
        public string Index()
        {
            return "1234567-BB";
        }

        public ViewResult Test1()
        {
            return View();
        }

        public string Index000()
        {
            var result = SF.Framework.BLL.BllFactory.SystemDatabase.SysConfig.T1();
            return "1234567-BB";
        }

        /*
         public ViewResult Index()
        {
            return View();
        }

        public ViewResult Index2()
        {
            return View();
        }

        public ActionResult Test1()
        {
            return PartialView();
        }

        public HttpStatusCodeContentResult Test2(int code = (int)System.Net.HttpStatusCode.OK)
        {
            return this.StatusCodeContent(code, "code描述");
        }

        public HttpStatusCodeContentResult Test3(int code = (int)System.Net.HttpStatusCode.OK)
        {
            return this.StatusCodeContent("123内容", code);
        }

        public JsonStatusResult Test4(bool? result)
        {
            return this.JsonStatus(result, JsonRequestBehavior.AllowGet);
        }

        public JsonSerializeResult Test5()
        {
            Newtonsoft.Json.JsonSerializerSettings setting = new Newtonsoft.Json.JsonSerializerSettings();
            //枚举值转换成字符串名字
            setting.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            return this.JsonSerialize(data: new { d1 = System.Net.HttpStatusCode.OK, d2 = 2 }, contentType: "application/json", contentEncoding: null, settings: setting, behavior: JsonRequestBehavior.AllowGet);
        }

        public JsonResult Test6()
        {
            return Json(new { d1 = System.Net.HttpStatusCode.OK, d2 = 2 }, JsonRequestBehavior.AllowGet);
        }
         */
    }
}
